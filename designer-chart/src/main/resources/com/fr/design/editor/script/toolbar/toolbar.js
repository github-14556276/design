!(function () {
    var RichEditorTextToolbar = BI.inherit(BI.Widget, {

        props: {
            baseCls: "bi-rich-editor-text-toolbar",
            height: 25,
            editor: null,
            buttons: [
                {type: "bi.rich_editor_font_chooser"},
                {type: "bi.rich_editor_size_chooser"},
                {type: "bi.rich_editor_bold_button"},
                {type: "bi.rich_editor_italic_button"},
                {type: "bi.rich_editor_underline_button"},
                {type: "bi.rich_editor_color_chooser"},
                {type: "bi.rich_editor_align_left_button"},
                {type: "bi.rich_editor_align_center_button"},
                {type: "bi.rich_editor_align_right_button"},
                {type: "bi.design.chart.common.editor.insert_param"}
            ]
        },

        _store: function () {
            return BI.Models.getModel("bi.model.design.chart.common.editor.toolbar");
        },

        watch: {
            isAuto: function (isAuto) {
                this[isAuto ? "hideCustomFontTool" : "showCustomFontTool"]();
            }
        },

        render: function () {
            var self = this,
                o = this.options;

            this.hasInsertCombo = o.buttons[o.buttons.length - 1].type === "bi.design.chart.common.editor.insert_param";

            this.buttons = BI.createWidgets(BI.map(o.buttons, function (i, btn) {
                return BI.extend(btn, {
                    editor: o.editor
                });
            }));

            if (this.hasInsertCombo) {
                var leftItems = BI.filter(this.buttons, function (idx) {
                    return idx !== self.buttons.length - 1;
                });
                return {
                    type: "bi.left_right_vertical_adapt",
                    lrgap: 10,
                    items: {
                        left: this._getButtons(leftItems),
                        right: [this.buttons[this.buttons.length - 1]]
                    }
                };
            }

            return {
                type: "bi.vertical_adapt",
                rgap: 10,
                items: this._getButtons(this.buttons)
            };
        },

        mounted: function () {
            var self = this;
            if (BI.isIE9Below()) {// IE8下必须要设置unselectable才能不blur输入框
                this.element.mousedown(function () {
                    self._noSelect(self.element[0]);
                });
                this._noSelect(this.element[0]);
            }

            this.model.isAuto && this.hideCustomFontTool();
        },

        _noSelect: function (element) {
            if (element.setAttribute && element.nodeName.toLowerCase() !== "input" && element.nodeName.toLowerCase() !== "textarea") {
                element.setAttribute("unselectable", "on");
            }
            for (var i = 0; i < element.childNodes.length; i++) {
                this._noSelect(element.childNodes[i]);
            }
        },

        hideCustomFontTool: function () {
            var self = this;
            BI.each(this.buttons, function (idx, button) {
                if (self.hasInsertCombo) {
                    idx !== self.buttons.length - 1 && button.setVisible(false);
                } else {
                    button.setVisible(false);
                }
            });
        },

        showCustomFontTool: function () {
            var self = this;
            BI.each(this.buttons, function (idx, button) {
                if (self.hasInsertCombo) {
                    idx !== self.buttons.length - 1 && button.setVisible(true);
                } else {
                    button.setVisible(true);
                }
            });
        },

        _getButtons: function (buttons) {
            return BI.map(buttons, function (idx, button) {
                if (button.options.used === false) {
                    return {
                        type: "bi.design.chart.common.editor.toolbar.disabled_item_wrapper",
                        button: button,
                        title: button.options.title
                    };
                }

                return button;
            });
        }
    });

    BI.shortcut("bi.design.chart.common.editor.toolbar", RichEditorTextToolbar);
})();