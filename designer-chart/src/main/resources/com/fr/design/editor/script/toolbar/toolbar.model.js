/*
 * @Maintainers: xiaofu.qin
 */
!(function () {

    var Model = BI.inherit(Fix.Model, {
        context: ["isAuto", "wId", "dimensionIds"]
    });

    BI.model("bi.model.design.chart.common.editor.toolbar", Model);

} ());