!(function () {

    var Model = BI.inherit(Fix.Model, {
        childContext: ["isAuto", "dimensionIds"],

        state: function () {
            var o = this.options;

            return {
                isAuto: o.isAuto,
                dimensionIds: o.dimensionIds,
                isEditorBlur: true
            };
        },

        computed: {
            fontStyleItems: function () {
                return [{
                    type: "bi.single_select_radio_item",
                    text: BI.i18nText("BI-Basic_Auto"),
                    width: 70,
                    logic: {
                        dynamic: true
                    },
                    value: true,
                    selected: this.options.isAuto
                }, {
                    type: "bi.single_select_radio_item",
                    text: BI.i18nText("BI-Basic_Custom"),
                    width: 80,
                    logic: {
                        dynamic: true
                    },
                    value: false,
                    selected: !this.options.isAuto
                }];
            }
        },

        actions: {
            changeIsAuto: function (isAuto) {
                this.model.isAuto = isAuto;
            },

            changeDimensionIds: function (dimensionIds) {
                this.model.dimensionIds = dimensionIds;
            },

            setEditorBlurState: function (isBlur) {
                this.model.isEditorBlur = isBlur;
            }
        }
    });

    BI.model("bi.model.design.chart.common.editor", Model);

}());
