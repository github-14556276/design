package com.fr.design.mainframe.chart.gui.data.table;

import com.fr.base.chart.chartdata.TopDefinitionProvider;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartdata.MoreNameCDDefinition;
import com.fr.chart.chartdata.OneValueCDDefinition;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.ChartDataFilterPane;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 属性表: 柱形, 饼图 数据集界面, "系列名使用"界面.
 *
 * @author kunsnat E-mail:kunsnat@gmail.com
 * @version 创建时间：2012-12-26 下午03:17:08
 */
public class SeriesTypeUseComboxPane extends BasicBeanPane<ChartCollection> {

    private SeriesNameUseFieldValuePane nameFieldValuePane;
    private SeriesNameUseFieldNamePane nameFieldNamePane;

    private ChartDataFilterPane dataScreeningPane;

    private ChartDataPane parent;
    private Plot initplot;
    private boolean isNeedSummary;

    protected UIButtonGroup<Integer> content;
    protected JPanel cardPane;

    public SeriesTypeUseComboxPane(ChartDataPane parent, Plot initplot) {
        this.initplot = initplot;
        this.parent = parent;
        this.isNeedSummary = true;
        initComponents();
    }

    protected void initComponents() {
        nameFieldValuePane = createValuePane();
        nameFieldNamePane = createNamePane();

        cardPane = new JPanel(new CardLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (content.getSelectedIndex() == 0) {
                    return nameFieldValuePane.getPreferredSize();
                } else {
                    return nameFieldNamePane.getPreferredSize();
                }
            }
        };
        cardPane.add(nameFieldValuePane, nameFieldValuePane.title4PopupWindow());
        cardPane.add(nameFieldNamePane, nameFieldNamePane.title4PopupWindow());

        content = new UIButtonGroup<>(new String[]{nameFieldValuePane.title4PopupWindow(), nameFieldNamePane.title4PopupWindow()});

        addItemChangeEvent();

        initLayout();
        content.setSelectedIndex(0);
    }

    private void addItemChangeEvent() {
        content.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCardPane();

            }
        });
    }

    protected void checkCardPane() {
        comboBoxItemStateChanged();
        CardLayout cl = (CardLayout) cardPane.getLayout();
        if (content.getSelectedIndex() == 0) {
            cl.show(cardPane, nameFieldValuePane.title4PopupWindow());
        } else {
            cl.show(cardPane, nameFieldNamePane.title4PopupWindow());
        }
    }

    protected void initLayout() {
        this.setLayout(new BorderLayout(4, LayoutConstants.VGAP_MEDIUM));
        JPanel northPane = new JPanel(new BorderLayout(4, 0));

        //使用系列名/系列值选项面板
        UILabel seriesLabel = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Series_Name_From"));
        seriesLabel.setPreferredSize(new Dimension(ChartDataPane.LABEL_WIDTH, ChartDataPane.LABEL_HEIGHT));
        JPanel borderLayoutPane = GUICoreUtils.createBorderLayoutPane(new Component[]{content, null, null, seriesLabel, null});
        northPane.add(borderLayoutPane);
        northPane.setBorder(BorderFactory.createEmptyBorder(10, 24, 0, 16));
        this.add(northPane, BorderLayout.NORTH);

        //系列名/系列值配置面板
        cardPane.setBorder(BorderFactory.createEmptyBorder(0, 24, 0, 15));
        this.add(cardPane, BorderLayout.CENTER);

        //数据筛选
        dataScreeningPane = new ChartDataFilterPane(this.initplot, parent);
        JPanel panel = new UIExpandablePane(Toolkit.i18nText("Fine-Design_Chart_Data_Filter"), 250, 24, dataScreeningPane);
        panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 15));
        dataScreeningPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        this.add(panel, BorderLayout.SOUTH);
    }

    public Dimension getPreferredSize() {
        Dimension preferredSize = super.getPreferredSize();
        return new Dimension(246, (int) preferredSize.getHeight());
    }

    /**
     * 检查box 是否使用, hasUse, 表示上层已经使用, 否则, 则此界面都可使用
     *
     * @param hasUse 是否使用
     */
    public void checkUseBox(boolean hasUse) {
        content.setEnabled(hasUse);
        nameFieldValuePane.checkUse(hasUse);
        dataScreeningPane.checkBoxUse();
    }

    /**
     * 切换 变更数据集时, 刷新Box选中项目
     *
     * @param list 列表
     */
    public void refreshBoxListWithSelectTableData(List list) {
        nameFieldValuePane.refreshBoxListWithSelectTableData(list);
        nameFieldNamePane.refreshBoxListWithSelectTableData(list);
    }

    /**
     * 清空所有的box设置
     */
    public void clearAllBoxList() {
        nameFieldValuePane.clearAllBoxList();
        nameFieldNamePane.clearAllBoxList();
    }

    /**
     * 界面标题
     *
     * @return 界面标题
     */
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Series_Name_From");
    }


    protected SeriesNameUseFieldValuePane createValuePane() {
        return new SeriesNameUseFieldValuePane();
    }

    protected SeriesNameUseFieldNamePane createNamePane() {
        return new SeriesNameUseFieldNamePane();
    }

    public void populateBean(ChartCollection ob, boolean isNeedSummary) {
        this.isNeedSummary = isNeedSummary;
        TopDefinitionProvider definition = ob.getSelectedChart().getFilterDefinition();
        if (definition instanceof OneValueCDDefinition) {
            this.setSelectedIndex(0);
            nameFieldValuePane.populateBean(ob, isNeedSummary);
        } else if (definition instanceof MoreNameCDDefinition) {
            this.setSelectedIndex(1);
            nameFieldNamePane.populateBean(ob, isNeedSummary);
        }
        dataScreeningPane.populateBean(ob, isNeedSummary);
        checkCardPane();
    }

    /**
     * 重新布局整个面板
     *
     * @param isNeedSummary 是否需要汇总
     */
    public void relayoutPane(boolean isNeedSummary) {
        this.isNeedSummary = isNeedSummary;
        if (this.getSelectedIndex() == 0) {
            nameFieldValuePane.relayoutPane(this.isNeedSummary);
        } else {
            nameFieldNamePane.relayoutPane(this.isNeedSummary);
        }
        dataScreeningPane.relayoutPane(this.isNeedSummary);
    }


    protected void comboBoxItemStateChanged() {
        if (this.getSelectedIndex() == 0) {
            nameFieldValuePane.relayoutPane(this.isNeedSummary);
        } else {
            nameFieldNamePane.relayoutPane(this.isNeedSummary);
        }
    }

    public void populateBean(ChartCollection ob) {
        this.populateBean(ob, true);
    }

    /**
     * 保存界面属性到Ob-ChartCollection
     */
    public void updateBean(ChartCollection ob) {
        if (this.getSelectedIndex() == 0) {
            nameFieldValuePane.updateBean(ob);
        } else {
            nameFieldNamePane.updateBean(ob);
        }

        dataScreeningPane.updateBean(ob);
    }

    /**
     * 保存数据
     *
     * @return 待保存的对象
     */
    public ChartCollection updateBean() {
        return null;
    }


    public int getSelectedIndex() {
        return content.getSelectedIndex();
    }

    public void setSelectedIndex(int index) {
        content.setSelectedIndex(index);
    }
}