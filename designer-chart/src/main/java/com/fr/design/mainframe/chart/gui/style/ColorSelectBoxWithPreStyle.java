package com.fr.design.mainframe.chart.gui.style;

import com.fr.chart.base.ColorWithPreStyle;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-07
 */
public class ColorSelectBoxWithPreStyle extends BasicPane {

    private static final int PREDEFINED_STYLE = 0;
    private static final int CUSTOM = 1;

    private UIButtonGroup<Integer> preButton;
    private ColorSelectBox colorSelectBox;

    public ColorSelectBoxWithPreStyle(int preferredWidth) {
        preButton = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Predefined"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});
        colorSelectBox = new ColorSelectBox(preferredWidth);
        initContent();
        initListener();
    }

    private void initContent() {
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double p = TableLayout.PREFERRED;

        double[] rowSize = {p, p, p};
        UILabel text = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Color"), SwingConstants.LEFT);
        Component[][] components = {
                new Component[]{text, preButton},
                new Component[]{null, colorSelectBox},
        };
        JPanel gapTableLayoutPane = TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
        this.setLayout(new BorderLayout());
        this.add(gapTableLayoutPane, BorderLayout.CENTER);
    }

    private void initListener() {
        preButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPreButton();
            }
        });
    }

    private void checkPreButton() {
        colorSelectBox.setVisible(preButton.getSelectedIndex() == CUSTOM);
        this.setPreferredSize(preButton.getSelectedIndex() == CUSTOM ? new Dimension(0, 55) : new Dimension(0, 23));
    }

    public String title4PopupWindow() {
        return null;
    }

    public void populate(ColorWithPreStyle colorWithPreStyle) {
        preButton.setSelectedIndex(colorWithPreStyle.isPredefinedStyle() ? PREDEFINED_STYLE : CUSTOM);
        colorSelectBox.setSelectObject(colorWithPreStyle.getColor());
        checkPreButton();
    }

    public ColorWithPreStyle update() {
        ColorWithPreStyle colorWithPreStyle = new ColorWithPreStyle();
        colorWithPreStyle.setPredefinedStyle(preButton.getSelectedIndex() == PREDEFINED_STYLE);
        colorWithPreStyle.setColor(colorSelectBox.getSelectObject());
        return colorWithPreStyle;
    }
}
