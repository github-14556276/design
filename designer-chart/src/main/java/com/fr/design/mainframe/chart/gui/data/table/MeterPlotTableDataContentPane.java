package com.fr.design.mainframe.chart.gui.data.table;

import com.fr.base.Utils;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartattr.MeterPlot;
import com.fr.chart.chartdata.MeterTableDefinition;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.BoldFontTextLabel;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.ChartDataFilterPane;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.extended.chart.StringFormula;
import com.fr.extended.chart.UIComboBoxWithNone;
import com.fr.general.GeneralUtils;
import com.fr.stable.StringUtils;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * 仪表盘, 属性表, 数据集数据界面.
 *
 * @author kunsnat E-mail:kunsnat@gmail.com
 * @version 创建时间：2012-12-21 下午04:51:50
 */
public class MeterPlotTableDataContentPane extends AbstractTableDataContentPane {
    private static final String METER_NAME = Toolkit.i18nText("Fine-Design_Chart_Category_Name");
    private static final String METER_VALUE = Toolkit.i18nText("Fine-Design_Chart_Value_Pointer");
    private static final String TARGET_VALUE = Toolkit.i18nText("Fine-Design_Chart_Target_Value");

    private static final int COMBOBOX_WIDTH = 115;
    private static final int COMBOBOX_HEIGHT = 20;

    private static final int LABEL_WIDTH = 85;

    private UIComboBox nameBox;
    private UIComboBox valueBox;
    private UIComboBox targetBox;
    private TinyFormulaPane custom;
    private ChartDataFilterPane filterPane;

    private JPanel boxPane;

    public MeterPlotTableDataContentPane(ChartDataPane parent) {
        this.setLayout(new BorderLayout());

        nameBox = new UIComboBoxWithNone();
        nameBox.setSelectedIndex(nameBox.getItemCount() -1);
        valueBox = new UIComboBox();
        targetBox = new UIComboBox();
        custom = new TinyFormulaPane();
        filterPane = new ChartDataFilterPane(new MeterPlot(), parent);

        boxPane = new JPanel();
        boxPane.setLayout(new BoxLayout(boxPane, BoxLayout.Y_AXIS));

        JPanel namePane = createPaneWithLabel(Toolkit.i18nText(METER_NAME), getNameComponent());
        boxPane.add(namePane);

        JPanel valuePane = createPaneWithLabel(Toolkit.i18nText(METER_VALUE), valueBox);
        boxPane.add(valuePane);

        JPanel targetPane = createPaneWithLabel(Toolkit.i18nText(TARGET_VALUE), targetBox);
        boxPane.add(targetPane);

        final JPanel customPane = createPaneWithLabel(StringUtils.EMPTY, custom);
        boxPane.add(customPane);

        customPane.setVisible(false);

        initTargetBoxValue();
        targetBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (targetBox.getSelectedIndex() == targetBox.getItemCount() - 1) {
                    customPane.setVisible(true);
                } else {
                    customPane.setVisible(false);
                }
            }
        });

        JPanel jPanel = TableLayout4VanChartHelper.createExpandablePaneWithTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Data_Filter"), filterPane);

        filterPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 15));
        boxPane.setBorder(BorderFactory.createEmptyBorder(7, 24, 7, 15));
        jPanel.setBorder(BorderFactory.createEmptyBorder(0, 12, 0, 5));

        this.add(getJSeparator(), BorderLayout.NORTH);
        this.add(boxPane, BorderLayout.CENTER);
        this.add(jPanel, BorderLayout.SOUTH);

        nameBox.addItemListener(tooltipListener);
        valueBox.addItemListener(tooltipListener);
        targetBox.addItemListener(tooltipListener);
    }

    private JPanel createPaneWithLabel(String labelName, Component component) {
        component.setPreferredSize(new Dimension(COMBOBOX_WIDTH, COMBOBOX_HEIGHT));
        UILabel label = new BoldFontTextLabel(labelName);
        label.setPreferredSize(new Dimension(LABEL_WIDTH, COMBOBOX_HEIGHT));
        JPanel paneWithLabel = GUICoreUtils.createBorderLayoutPane(new Component[]{component, null, null, label, null});
        paneWithLabel.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
        return paneWithLabel;
    }

    protected void refreshBoxListWithSelectTableData(List list) {
        refreshBoxItems(nameBox, list);
        refreshBoxItems(valueBox, list);
        refreshBoxItems(targetBox, list);
        initTargetBoxValue();
    }

    /**
     * 清空所有的box设置
     */
    public void clearAllBoxList() {
        clearBoxItems(nameBox);
        clearBoxItems(valueBox);
    }

    private void initTargetBoxValue() {
        targetBox.addItem(Toolkit.i18nText("Fine-Design_Chart_Custom"));
        targetBox.setSelectedItem(null);
    }

    /**
     * 更新仪表盘数据界面
     */
    public void populateBean(ChartCollection ob) {
        if (ob != null && ob.getSelectedChart().getFilterDefinition() instanceof MeterTableDefinition) {
            MeterTableDefinition meter = (MeterTableDefinition) ob.getSelectedChart().getFilterDefinition();

            populateNameComponent(meter);

            valueBox.setSelectedItem(meter.getValue());
            custom.getUITextField().setText(meter.getCustomTarget().getContent());
            if (meter.isCustom()) {
                targetBox.setSelectedIndex(targetBox.getItemCount() - 1);
            } else {
                targetBox.setSelectedItem(meter.getTargetValue());
            }
            filterPane.populateBean(ob);
        }
    }

    protected void populateNameComponent(MeterTableDefinition meter) {
        nameBox.setSelectedItem(meter.getName());
    }

    /**
     * 保存界面属性.
     */
    public void updateBean(ChartCollection ob) {
        if (ob != null) {
            MeterTableDefinition meter = getMeterTableDefinition();
            ob.getSelectedChart().setFilterDefinition(meter);

            updateNameComponent(meter);

            meter.setValue(Utils.objectToString(valueBox.getSelectedItem()));
            if (targetBox.getSelectedIndex() == targetBox.getItemCount() - 1) {
                meter.setCustom(true);
                meter.setCustomTarget(new StringFormula(custom.getUITextField().getText()));
            } else {
                meter.setCustom(false);
                meter.setTargetValue(GeneralUtils.objectToString(targetBox.getSelectedItem()));
            }
            filterPane.updateBean(ob);
        }
    }

    protected MeterTableDefinition getMeterTableDefinition() {
        return new MeterTableDefinition();
    }

    protected void updateNameComponent(MeterTableDefinition meter) {
        meter.setName(Utils.objectToString(nameBox.getSelectedItem()));
    }

    /**
     * 重新布局整个面板
     */
    public void redoLayoutPane() {
        filterPane.relayoutPane(this.isNeedSummaryCaculateMethod());
    }


    protected Component getNameComponent() {
        return nameBox;
    }
}