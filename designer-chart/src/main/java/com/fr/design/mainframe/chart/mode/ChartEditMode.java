package com.fr.design.mainframe.chart.mode;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/6/4
 */
//todo:refactor 弹出框图表没有单元格数据源,就不用一层层传下去了
public enum ChartEditMode {
    NORMAL,
    DUCHAMP
}
