package com.fr.design.mainframe.chart.gui.style.series;

import com.fr.base.BaseFormula;
import com.fr.chart.chartglyph.MapHotAreaColor;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Color;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-05-18
 */
public class ColorPickerPaneWithMaxMin extends ColorPickerPaneWithFormula {

    private double MAX = Double.MAX_VALUE / 2;
    private double MIN = -(Double.MAX_VALUE / 2);
    private BaseFormula[] valueArray;
    private JPanel autoPane;

    public ColorPickerPaneWithMaxMin(AbstractAttrNoScrollPane container, String meterString) {
        this(container, meterString, null);
    }

    public ColorPickerPaneWithMaxMin(AbstractAttrNoScrollPane container, String meterString, JPanel autoPane) {
        super(container, meterString);
        if (autoPane == null) {
            return;
        }
        this.autoPane = autoPane;
        this.getDesignTypeButtonGroup().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                checkAutoPane();
            }
        });
    }

    private void checkAutoPane() {
        if (getDesignTypeButtonGroup().getSelectedIndex() == 0) {
            autoPane.setVisible(true);
        } else {
            autoPane.setVisible(false);
        }
    }

    public void refreshGroupPane(Color[] colorArray, BaseFormula[] valueArray) {
        this.valueArray = valueArray;
        super.refreshGroupPane(colorArray, valueArray);
    }

    protected void setTextValue4Index(int index, String value) {
        TinyFormulaPane tinyFormulaPane = (TinyFormulaPane) textFieldList.get(index);
        setTextState(tinyFormulaPane, index, textFieldList.size() - 1, value);
    }

    protected JComponent getNewTextFieldComponent(int i, String value) {
        TinyFormulaPane textField = new TinyFormulaPaneWithEnable();
        textField.setBounds(0, i * 2 * TEXTFIELD_HEIGHT, TEXTFIELD_WIDTH, TEXTFIELD_HEIGHT);
        setTextState(textField, i, valueArray.length - 1, value);
        return textField;
    }

    protected String getValue4Index(int i) {
        if (i == 0) {
            return String.valueOf(MAX);
        }
        if (i == textFieldList.size() - 1) {
            return String.valueOf(MIN);
        }
        return ((TinyFormulaPane) textFieldList.get(i)).getUITextField().getText();
    }

    protected BaseFormula[] getValueArray(int count) {
        BaseFormula[] valueArray = new BaseFormula[count + 1];
        valueArray[0] = BaseFormula.createFormulaBuilder().build(MAX);
        valueArray[count] = BaseFormula.createFormulaBuilder().build(MIN);
        for (int i = 1; i < count; i++) {
            if (i >= textFieldList.size() - 1) {
                valueArray[i] = BaseFormula.createFormulaBuilder().build((count - i) * VALUE);
            } else {
                valueArray[i] = BaseFormula.createFormulaBuilder().build(getValue4Index(i));
            }
        }
        return valueArray;
    }

    private void setTextState(TinyFormulaPane tinyFormulaPane, int index, int maxIndex, String value) {
        boolean enable = false;
        if (index == 0) {
            value = "=∞";
        } else if (index == maxIndex) {
            value = "=-∞";
        } else {
            enable = true;
        }
        tinyFormulaPane.getUITextField().setText(value);
        tinyFormulaPane.setEnabled(enable);
    }

    public void populateBean(MapHotAreaColor hotAreaColor) {
        super.populateBean(hotAreaColor);
        if (autoPane != null) {
            checkAutoPane();
        }
    }

    public class TinyFormulaPaneWithEnable extends TinyFormulaPane {
        @Override
        public void setEnabled(boolean enabled) {
            super.setEnabled(enabled);
            formulaTextField.setEnabled(enabled);
            formulaTextFieldButton.setEnabled(enabled);
        }
    }
}
