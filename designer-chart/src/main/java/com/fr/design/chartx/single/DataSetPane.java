package com.fr.design.chartx.single;

import com.fr.chartx.data.DataSetDefinition;
import com.fr.data.impl.NameTableData;
import com.fr.design.beans.FurtherBasicBeanPane;
import com.fr.design.chartx.data.DataLayoutHelper;
import com.fr.design.chartx.fields.AbstractDataSetFieldsPane;
import com.fr.design.data.tabledata.wrapper.TableDataWrapper;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.data.DatabaseTableDataPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.List;

/**
 * Created by shine on 2019/5/21.
 */
public class DataSetPane extends FurtherBasicBeanPane<DataSetDefinition> {

    private DatabaseTableDataPane tableDataPane;

    private AbstractDataSetFieldsPane dataSetFieldsPane;

    public DataSetPane(AbstractDataSetFieldsPane dataSetFieldsPane) {
        initComps(dataSetFieldsPane);
    }

    private void initComps(AbstractDataSetFieldsPane dataSetFieldsPane) {
        tableDataPane = new DatabaseTableDataPane(null) {
            @Override
            protected void userEvent() {
                refreshBoxListAndTableName();
                checkBoxUse();
            }

            @Override
            protected void setBorder() {
            }

        };


        this.dataSetFieldsPane = dataSetFieldsPane;

        JPanel northPane = DataLayoutHelper.createDataLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Table_Data"), tableDataPane);
        this.setLayout(new BorderLayout(0, 6));
        this.add(northPane, BorderLayout.NORTH);
        this.add(dataSetFieldsPane, BorderLayout.CENTER);
        DataLayoutHelper.addNormalBorder(this);
        checkBoxUse();
    }

    /**
     * 检查box是否可用.
     */
    public void checkBoxUse() {
        TableDataWrapper dataWrap = tableDataPane.getTableDataWrapper();

        if (dataSetFieldsPane != null) {
            dataSetFieldsPane.checkBoxUse(dataWrap != null);
        }
    }

    /**
     * 刷新字段下拉列表
     */
    private void refreshBoxListAndTableName() {
        TableDataWrapper dataWrap = tableDataPane.getTableDataWrapper();

        if (dataWrap == null) {
            return;
        }

        List<String> columnNameList = dataWrap.calculateColumnNameList();

        if (dataSetFieldsPane != null) {
            dataSetFieldsPane.refreshBoxListWithSelectTableData(columnNameList);
            dataSetFieldsPane.setTableName(dataWrap.getTableDataName());
        }
    }

    @Override
    public boolean accept(Object ob) {
        return ob instanceof DataSetDefinition;
    }

    @Override
    public String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Chart_TableData");
    }

    @Override
    public void reset() {
        this.removeAll();
    }

    @Override
    public void populateBean(DataSetDefinition ob) {
        if (ob == null || ob.getColumnFieldCollection() == null) {
            return;
        }

        refreshBoxListAndTableName();

        tableDataPane.populateBean(ob.getNameTableData());

        dataSetFieldsPane.populateBean(ob.getColumnFieldCollection());

        checkBoxUse();
    }

    @Override
    public DataSetDefinition updateBean() {
        DataSetDefinition dataSetDefinition = new DataSetDefinition();

        TableDataWrapper tableDataWrapper = tableDataPane.getTableDataWrapper();
        if (tableDataWrapper != null) {
            dataSetDefinition.setNameTableData(new NameTableData(tableDataWrapper.getTableDataName()));
        }

        dataSetDefinition.setColumnFieldCollection(dataSetFieldsPane.updateBean());

        return dataSetDefinition;
    }
}
