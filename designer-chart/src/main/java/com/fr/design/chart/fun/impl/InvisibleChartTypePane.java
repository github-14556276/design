package com.fr.design.chart.fun.impl;

import com.fr.design.ChartTypeInterfaceManager;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/6/25
 */
public class InvisibleChartTypePane extends DefaultChartTypePane {
    @Override
    public String title4PopupWindow() {
        return ChartTypeInterfaceManager.TYPE_PANE_DEFAULT_TITLE;
    }
}
