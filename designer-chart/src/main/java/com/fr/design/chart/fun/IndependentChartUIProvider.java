package com.fr.design.chart.fun;

import com.fr.common.annotations.Compatible;

/**
 * Created by shine on 2019/09/05.
 */
@Compatible
@Deprecated
public interface IndependentChartUIProvider extends ChartTypeUIProvider {

}
