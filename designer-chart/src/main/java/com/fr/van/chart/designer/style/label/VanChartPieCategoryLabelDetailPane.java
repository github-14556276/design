package com.fr.van.chart.designer.style.label;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.stable.Constants;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.pie.style.VanChartPieCategoryLabelContentPane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-09
 */
public class VanChartPieCategoryLabelDetailPane extends VanChartPlotLabelDetailPane {

    public VanChartPieCategoryLabelDetailPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        super(plot, parent, inCondition);
    }

    protected void initToolTipContentPane(Plot plot) {
        setDataLabelContentPane(new VanChartPieCategoryLabelContentPane(getParentPane(), this, isInCondition()));
    }

    protected JPanel createLabelPositionPane(String title, Plot plot) {
        String[] positionName = new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Axis_Bottom"),
                Toolkit.i18nText("Fine-Design_Form_Center"),
                Toolkit.i18nText("Fine-Design_Chart_Axis_Top")
        };

        Integer[] positionValue = new Integer[]{Constants.BOTTOM, Constants.CENTER, Constants.TOP};

        UIButtonGroup<Integer> position = new UIButtonGroup<>(positionName, positionValue);
        setPosition(position);
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] row = {p, p};
        double[] col = {f, e};

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Layout_Position")), position},
        };

        return TableLayoutHelper.createTableLayoutPane(components, row, col);
    }

    protected void checkPositionPane(String title) {

    }

    protected JPanel createBorderAndBackgroundPane() {
        return null;
    }
}
