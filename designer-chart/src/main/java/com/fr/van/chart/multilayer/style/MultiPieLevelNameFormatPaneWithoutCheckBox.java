package com.fr.van.chart.multilayer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithoutCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class MultiPieLevelNameFormatPaneWithoutCheckBox extends CategoryNameFormatPaneWithoutCheckBox {

    public MultiPieLevelNameFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Level_Name");
    }
}
