package com.fr.van.chart.designer.component;

import com.fr.chart.base.GradientStyle;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.style.background.gradient.FixedGradientBar;
import com.fr.plugin.chart.type.GradientType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VanChartBeautyPaneWithGradientBar extends VanChartBeautyPane {

    private FixedGradientBar colorGradient;
    private JPanel gradientBarPane;

    public VanChartBeautyPaneWithGradientBar() {
        super();

        this.add(initGradientBarPane(), BorderLayout.SOUTH);

        initListener();
    }

    private JPanel initGradientBarPane() {
        colorGradient = new FixedGradientBar(4, 140);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize = {p, p};
        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, colorGradient},
        };

        gradientBarPane = TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);

        return gradientBarPane;
    }

    private void initListener() {
        getGradientTypeBox().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkGradientBarVisible();
            }
        });
    }

    private void checkGradientBarVisible() {
        if (colorGradient != null && gradientBarPane != null) {
            gradientBarPane.setVisible(getGradientTypeBox().getSelectedIndex() == 1);
        }
    }

    protected String[] getNameArray() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Auto"),
                Toolkit.i18nText("Fine-Design_Chart_Custom"),
                Toolkit.i18nText("Fine-Design_Chart_Off")
        };
    }

    public void populateBean(GradientStyle gradientStyle) {
        super.populateBean(gradientStyle);

        if (colorGradient != null) {
            colorGradient.updateColor(gradientStyle.getStartColor(), gradientStyle.getEndColor());
        }

        checkGradientBarVisible();
    }

    @Override
    public GradientStyle updateBean() {
        GradientStyle gradientStyle = super.updateBean();

        if (this.colorGradient != null && this.gradientBarPane != null) {
            gradientStyle.setStartColor(colorGradient.getSelectColorPointBtnP1().getColorInner());
            gradientStyle.setEndColor(colorGradient.getSelectColorPointBtnP2().getColorInner());
        }

        return gradientStyle;
    }

    protected int convertGradientTypeToIndex(GradientType gradientType) {
        switch (gradientType) {
            case CUSTOM:
                return 1;
            case NONE:
                return 2;
            default:
                return 0;
        }
    }

    protected GradientType convertIndexToGradientType(int index) {
        switch (index) {
            case 1:
                return GradientType.CUSTOM;
            case 2:
                return GradientType.NONE;
            default:
                return GradientType.AUTO;
        }
    }

}