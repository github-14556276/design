package com.fr.van.chart.map.designer.type;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextarea.UITextArea;
import com.fr.design.layout.TableLayout;
import com.fr.plugin.chart.base.GisLayer;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-07
 */
public class TileLayerPane extends JPanel {

    private UITextArea customTileLayer;
    private UITextArea attribution;


    public TileLayerPane() {
        double p = TableLayout.PREFERRED;
        double[] rowSize = {p, p};
        double[] COLUMN_SIZE = {TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH, TableLayout4VanChartHelper.SECOND_EDIT_AREA_WIDTH - 3};

        customTileLayer = new UITextArea();
        attribution = new UITextArea();
        Component[][] comps = new Component[][]{
                new Component[]{new UILabel("url"), customTileLayer},
                new Component[]{new UILabel("Attribution"), attribution}
        };
        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(comps, rowSize, COLUMN_SIZE);
        panel.setBorder(TableLayout4VanChartHelper.SECOND_EDIT_AREA_BORDER);

        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

    public void populate(GisLayer layer) {
        customTileLayer.setText(layer.getCustomTileLayer());
        attribution.setText(layer.getAttribution());
    }

    public void update(GisLayer layer) {
        layer.setCustomTileLayer(customTileLayer.getText());
        layer.setAttribution(attribution.getText());
    }
}
