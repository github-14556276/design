package com.fr.van.chart.designer.style;

import com.fr.van.chart.designer.component.background.VanChartBackgroundWithOutShadowWithRadiusPane;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-10-20
 */
public class VanChartTitleWithAutoBackground extends VanChartTitlePane {

    public VanChartTitleWithAutoBackground(VanChartStylePane parent) {
        super(parent);
    }

    protected VanChartBackgroundWithOutShadowWithRadiusPane createBackgroundPane() {
        //TODO Bjorn 地图标题背景自动逻辑
        //return new VanChartBackgroundWithOutShadowWithRadiusPane(true);
        return new VanChartBackgroundWithOutShadowWithRadiusPane();
    }
}
