package com.fr.van.chart.gauge;

import com.fr.chart.base.ChartConstants;
import com.fr.chart.base.GradientStyle;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.style.background.gradient.FixedGradientBar;
import com.fr.plugin.chart.type.GradientType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartBeautyPane;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-05-20
 */
public class VanChartGaugeBeautyPane extends VanChartBeautyPane {

    private FixedGradientBar colorGradient;
    private JPanel gradientBarPane;

    public VanChartGaugeBeautyPane() {
        super();
        this.add(initGradientBarPane(), BorderLayout.SOUTH);
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
        initListener();
    }

    private JPanel initGradientBarPane() {
        colorGradient = new FixedGradientBar(4, 140);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double[] rowSize = {p, p};
        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, colorGradient},
        };

        gradientBarPane = TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
        colorGradient.updateColor(ChartConstants.GRADIENT_END, ChartConstants.GRADIENT_START);
        return gradientBarPane;
    }

    private void initListener() {
        getGradientTypeBox().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkGradientBarVisible();
            }
        });
    }

    protected String labelName() {
        return Toolkit.i18nText("Fine-Design_Chart_Color_Style");
    }

    private void checkGradientBarVisible() {
        gradientBarPane.setVisible(getGradientTypeBox().getSelectedIndex() == 1);
    }

    protected String[] getNameArray() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Solid_Color"),
                Toolkit.i18nText("Fine-Design_Chart_Legend_Gradual"),
        };
    }

    public void populateBean(GradientStyle gradientStyle) {
        super.populateBean(gradientStyle);

        if (gradientStyle.getGradientType() == GradientType.CUSTOM) {
            colorGradient.updateColor(gradientStyle.getStartColor(), gradientStyle.getEndColor());
        }

        checkGradientBarVisible();
    }

    @Override
    public GradientStyle updateBean() {
        GradientStyle gradientStyle = super.updateBean();

        if (gradientStyle.getGradientType() == GradientType.CUSTOM) {
            gradientStyle.setStartColor(colorGradient.getSelectColorPointBtnP1().getColorInner());
            gradientStyle.setEndColor(colorGradient.getSelectColorPointBtnP2().getColorInner());
        }
        return gradientStyle;
    }

    protected int convertGradientTypeToIndex(GradientType gradientType) {
        switch (gradientType) {
            case CUSTOM:
                return 1;
            default:
                return 0;
        }
    }

    protected GradientType convertIndexToGradientType(int index) {
        switch (index) {
            case 1:
                return GradientType.CUSTOM;
            default:
                return GradientType.AUTO;
        }
    }
}
