package com.fr.van.chart.range.component;

import com.fr.chart.chartglyph.MapHotAreaColor;
import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.gui.ilable.BoldFontTextLabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.style.series.MapColorPickerPaneWithFormula;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Component;
import java.awt.Dimension;

/**
 * Created by Mitisky on 16/10/20.
 * 没有主题颜色,自动的时候没有划分阶段
 */
public class SectionIntervalConfigPaneWithOutNum extends MapColorPickerPaneWithFormula {
    private BoldFontTextLabel numLabel;

    public String getNameOfSubRange() {
        return Toolkit.i18nText("Fine-Design_Chart_Range_Num");
    }

    public SectionIntervalConfigPaneWithOutNum(AbstractAttrNoScrollPane container) {
        super(container);
        getDesignTypeButtonGroup().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (getDesignTypeButtonGroup().getSelectedIndex() == 0) {
                    setRegionVisible(false);
                } else {
                    setRegionVisible(true);
                }
            }
        });
    }

    private void setRegionVisible(boolean visible) {
        getRegionNumPane().setVisible(visible);
        numLabel.setVisible(visible);
    }

    @Override
    protected Component[][] createComponents() {
        numLabel = new BoldFontTextLabel(Toolkit.i18nText("Fine-Design_Chart_Value_Divided_Stage"));

        setRegionVisible(false);

        return new Component[][]{
                new Component[]{new BoldFontTextLabel(getNameOfSubRange()), getDesignTypeButtonGroup()},
                new Component[]{numLabel, getRegionNumPane()},
        };
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dim = super.getPreferredSize();
        return new Dimension((int) dim.getWidth(), (int) dim.getHeight() - (numLabel.isVisible() ? 0 : 30));
    }

    @Override
    public void populateBean(MapHotAreaColor hotAreaColor) {
        super.populateBean(hotAreaColor);
        setRegionVisible(hotAreaColor.getUseType() == 1);
    }
}
