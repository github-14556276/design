package com.fr.van.chart.pie.style;

import com.fr.design.ui.ModernUIPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartPieValueRichTextFieldListPane extends VanChartFieldListPane {

    public VanChartPieValueRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getSeriesNameButton());
        fieldPane.add(getValueButton());
        fieldPane.add(getPercentButton());
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getSeriesNameButton());
        fieldButtonList.add(getValueButton());
        fieldButtonList.add(getPercentButton());

        return fieldButtonList;
    }
}
