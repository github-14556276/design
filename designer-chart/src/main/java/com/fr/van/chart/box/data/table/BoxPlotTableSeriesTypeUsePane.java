package com.fr.van.chart.box.data.table;

import com.fr.chart.base.ChartConstants;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartdata.MoreNameCDDefinition;
import com.fr.chart.chartdata.NormalTableDataDefinition;
import com.fr.chart.chartdata.OneValueCDDefinition;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.beans.FurtherBasicBeanPane;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.BoldFontTextLabel;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.table.DataPaneHelper;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.ComparatorUtils;
import com.fr.plugin.chart.box.data.VanBoxTableDefinition;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

public class BoxPlotTableSeriesTypeUsePane extends BasicBeanPane<ChartCollection> {

    private static final boolean NEED_SUMMERY = false;

    private UIComboBox categoryCombox;

    private BoxPlotTableSeriesNameUseFieldValuePane nameFieldValuePane;
    private BoxPlotTableSeriesNameUseFieldNamePane nameFieldNamePane;

    private UIButtonGroup<Integer> seriesFrom;
    private JPanel cardPane;

    public BoxPlotTableSeriesTypeUsePane() {
        initComponents();
        initLayout();
        initListener();
    }

    private void initComponents() {
        nameFieldValuePane = new BoxPlotTableSeriesNameUseFieldValuePane();
        nameFieldNamePane = new BoxPlotTableSeriesNameUseFieldNamePane();

        nameFieldValuePane.relayoutPane(NEED_SUMMERY);
        nameFieldNamePane.relayoutPane(NEED_SUMMERY);

        cardPane = new JPanel(new CardLayout()) {
            public Dimension getPreferredSize() {
                if (seriesFrom.getSelectedIndex() == 0) {
                    return nameFieldValuePane.getPreferredSize();
                } else {
                    return nameFieldNamePane.getPreferredSize();
                }
            }
        };

        cardPane.setBorder(BorderFactory.createEmptyBorder(0, 24, 0, 15));
        cardPane.add(nameFieldValuePane, nameFieldValuePane.title4PopupWindow());
        cardPane.add(nameFieldNamePane, nameFieldNamePane.title4PopupWindow());

        seriesFrom = new UIButtonGroup<>(new String[]{nameFieldValuePane.title4PopupWindow(), nameFieldNamePane.title4PopupWindow()});
        seriesFrom.setSelectedIndex(0);

        addItemChangeEvent();
    }

    private void addItemChangeEvent() {
        seriesFrom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkCardPane();
            }
        });
    }

    private void checkCardPane() {
        comboBoxItemStateChanged();

        CardLayout cl = (CardLayout) cardPane.getLayout();
        if (seriesFrom.getSelectedIndex() == 0) {
            cl.show(cardPane, nameFieldValuePane.title4PopupWindow());
        } else {
            cl.show(cardPane, nameFieldNamePane.title4PopupWindow());
        }
    }

    private void initLayout() {
        this.setLayout(new BorderLayout(4, LayoutConstants.VGAP_MEDIUM));

        this.add(createNorthPane(), BorderLayout.NORTH);
        this.add(createCenterPane(), BorderLayout.CENTER);
        this.add(cardPane, BorderLayout.SOUTH);
    }

    private JPanel createNorthPane() {
        JPanel north = new JPanel(new BorderLayout(4, 0));
        north.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, getBackground()));
        UILabel label = new BoldFontTextLabel(Toolkit.i18nText("Fine-Design_Chart_Style_Category"));
        label.setPreferredSize(new Dimension(ChartDataPane.LABEL_WIDTH, ChartDataPane.LABEL_HEIGHT));

        categoryCombox = new UIComboBox();
        categoryCombox.setPreferredSize(new Dimension(100, 20));
        categoryCombox.addItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));
        categoryCombox.setSelectedItem(null);

        north.add(GUICoreUtils.createBorderLayoutPane(new Component[]{categoryCombox, null, null, label, null}));
        north.setPreferredSize(new Dimension(246, 20));
        north.setBorder(BorderFactory.createEmptyBorder(0, 24, 0, 15));

        return north;
    }

    private JPanel createCenterPane() {
        JPanel center = new JPanel(new BorderLayout(4, 0));

        UILabel label = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Series_Name_From"));
        label.setPreferredSize(new Dimension(ChartDataPane.LABEL_WIDTH, ChartDataPane.LABEL_HEIGHT));
        center.add(GUICoreUtils.createBorderLayoutPane(new Component[]{seriesFrom, null, null, label, null}));
        center.setBorder(BorderFactory.createEmptyBorder(0, 24, 0, 15));

        return center;
    }

    private void initListener() {
        categoryCombox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                checkBoxUse(categoryCombox.getSelectedItem() != null);
                makeToolTipUse(categoryCombox);
            }
        });
    }

    public void checkBoxUse(boolean hasUse) {
        categoryCombox.setEnabled(hasUse);
        seriesFrom.setEnabled(hasUse);
        nameFieldValuePane.checkUse(hasUse);
    }

    private void makeToolTipUse(UIComboBox comBox) {
        if (comBox.getSelectedItem() != null) {
            comBox.setToolTipText(comBox.getSelectedItem().toString());
        } else {
            comBox.setToolTipText(null);
        }
    }

    public void refreshBoxListWithSelectTableData(List list) {
        DataPaneHelper.refreshBoxItems(categoryCombox, list);

        categoryCombox.addItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));

        nameFieldValuePane.refreshBoxListWithSelectTableData(list);
        nameFieldNamePane.refreshBoxListWithSelectTableData(list);
    }

    public void clearAllBoxList() {
        DataPaneHelper.clearBoxItems(categoryCombox);

        categoryCombox.addItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));

        nameFieldValuePane.clearAllBoxList();
        nameFieldNamePane.clearAllBoxList();
    }

    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Chart_Series_Name_From");
    }

    protected List<FurtherBasicBeanPane<? extends ChartCollection>> initPaneList() {
        nameFieldValuePane = new BoxPlotTableSeriesNameUseFieldValuePane();
        nameFieldNamePane = new BoxPlotTableSeriesNameUseFieldNamePane();

        nameFieldValuePane.relayoutPane(NEED_SUMMERY);
        nameFieldNamePane.relayoutPane(NEED_SUMMERY);

        List<FurtherBasicBeanPane<? extends ChartCollection>> paneList = new ArrayList<FurtherBasicBeanPane<? extends ChartCollection>>();

        paneList.add(nameFieldValuePane);
        paneList.add(nameFieldNamePane);

        return paneList;
    }

    public void relayoutPane() {
        if (seriesFrom.getSelectedIndex() == 0) {
            nameFieldValuePane.relayoutPane(NEED_SUMMERY);
        } else {
            nameFieldNamePane.relayoutPane(NEED_SUMMERY);
        }
    }

    private void comboBoxItemStateChanged() {
        if (seriesFrom.getSelectedIndex() == 0) {
            nameFieldValuePane.relayoutPane(NEED_SUMMERY);
        } else {
            nameFieldNamePane.relayoutPane(NEED_SUMMERY);
        }
    }

    public void populateBean(ChartCollection collection) {
        NormalTableDataDefinition definition = BoxTableDefinitionHelper.getBoxTableDetailedDefinition(collection);

        if (definition == null) {
            categoryCombox.setSelectedItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));
            return;
        }

        if (definition instanceof OneValueCDDefinition) {
            seriesFrom.setSelectedIndex(0);
            nameFieldValuePane.populateDefinition(definition, NEED_SUMMERY);
        } else if (definition instanceof MoreNameCDDefinition) {
            seriesFrom.setSelectedIndex(1);
            nameFieldNamePane.populateDefinition(definition, NEED_SUMMERY);
        }

        populateCategoryItem(categoryCombox, definition.getCategoryName());

        checkCardPane();
    }

    public void updateBean(ChartCollection collection) {
        VanBoxTableDefinition table = BoxTableDefinitionHelper.getBoxTableDefinition(collection);

        if (table == null) {
            return;
        }

        NormalTableDataDefinition definition;

        if (seriesFrom.getSelectedIndex() == 0) {
            definition = nameFieldValuePane.updateDefinition();
        } else {
            definition = nameFieldNamePane.updateDefinition(table.getDetailedDefinition());
        }

        Object categoryName = categoryCombox.getSelectedItem();

        if (ArrayUtils.contains(ChartConstants.getNoneKeys(), categoryName)) {
            definition.setCategoryName(StringUtils.EMPTY);
        } else {
            definition.setCategoryName(categoryName == null ? null : categoryName.toString());
        }

        table.setDetailedDefinition(definition);
    }

    private void populateCategoryItem(UIComboBox categoryCombox, String item) {
        if (ComparatorUtils.equals(item, StringUtils.EMPTY)) {
            categoryCombox.setSelectedItem(Toolkit.i18nText("Fine-Design_Chart_Use_None"));
        } else if (!DataPaneHelper.boxItemsContainsObject(categoryCombox, item)) {
            categoryCombox.setSelectedItem(null);
        } else {
            DataPaneHelper.combineCustomEditValue(categoryCombox, item);
        }
    }

    public ChartCollection updateBean() {
        return null;
    }

}
