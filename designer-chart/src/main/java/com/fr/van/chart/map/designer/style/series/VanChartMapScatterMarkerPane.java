package com.fr.van.chart.map.designer.style.series;

import com.fr.chart.chartglyph.Marker;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.marker.VanChartCommonMarkerPane;

import java.awt.Component;

/**
 * Created by Mitisky on 16/5/19.
 * 只有标记点类型和半径
 */
public class VanChartMapScatterMarkerPane extends VanChartCommonMarkerPane {

    protected Component[][] getMarkerConfigComponent() {

        return new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Radius")), getRadius()}
        };
    }

    @Override
    protected Marker[] getMarkers() {
        return getNormalMarkersWithCustom(new MarkerType[0]);
    }

    @Override
    protected double[] getColumnSize() {
        double s = TableLayout4VanChartHelper.SECOND_EDIT_AREA_WIDTH;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;
        return new double[] {d, s};
    }
}
