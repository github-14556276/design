package com.fr.van.chart.map.line;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithoutCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class StartAndEndNameFormatPaneWithoutCheckBox extends CategoryNameFormatPaneWithoutCheckBox {
    public StartAndEndNameFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Start_And_End");
    }
}
