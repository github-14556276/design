package com.fr.van.chart.designer.component.format;

import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-04-06
 */
public class TargetValueFormatPaneWithCheckBox extends VanChartFormatPaneWithCheckBox {

    public TargetValueFormatPaneWithCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    @Override
    protected String getCheckBoxText() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Target_Value");
    }
}
