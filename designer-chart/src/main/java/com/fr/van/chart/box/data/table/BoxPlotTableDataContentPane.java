package com.fr.van.chart.box.data.table;

import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartdata.NormalTableDataDefinition;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.ChartDataFilterPane;
import com.fr.design.mainframe.chart.gui.data.table.AbstractTableDataContentPane;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.plugin.chart.box.VanChartBoxPlot;
import com.fr.plugin.chart.box.data.VanBoxTableDefinition;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class BoxPlotTableDataContentPane extends AbstractTableDataContentPane {

    private UIButtonGroup dataType;

    private BoxPlotTableSeriesTypeUsePane seriesTypeComboxPane;
    private BoxPlotTableResultDataSeriesPane resultDataSeriesPane;

    private JPanel filterPane;
    private ChartDataFilterPane dataScreeningPane;

    private ChartDataPane parent;
    private Plot initplot;

    public BoxPlotTableDataContentPane(Plot plot, ChartDataPane parent) {
        this.initplot = plot;
        this.parent = parent;

        this.setLayout(new BorderLayout());

        this.add(createDataTypePane(), BorderLayout.NORTH);
        this.add(createSeriesPane(), BorderLayout.CENTER);
        this.add(createFilterPane(), BorderLayout.SOUTH);

        initDataTypeListener();
        checkDataPaneVisible();
    }

    private JPanel createDataTypePane() {
        JPanel pane = new JPanel(new BorderLayout(4, 0));
        pane.setBorder(BorderFactory.createMatteBorder(0, 0, 6, 1, getBackground()));

        UILabel label = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Data_Form"));
        label.setPreferredSize(new Dimension(ChartDataPane.LABEL_WIDTH, ChartDataPane.LABEL_HEIGHT));

        String[] names = new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Detailed_Data"),
                Toolkit.i18nText("Fine-Design_Chart_Result_Data")
        };

        dataType = new UIButtonGroup(names);
        dataType.setSelectedIndex(0);
        dataType.setPreferredSize(new Dimension(100, 20));

        pane.add(GUICoreUtils.createBorderLayoutPane(new Component[]{dataType, null, null, label, null}));
        pane.setPreferredSize(new Dimension(246, 30));
        pane.setBorder(BorderFactory.createEmptyBorder(0, 24, 10, 15));

        return pane;
    }

    private JPanel createSeriesPane() {
        seriesTypeComboxPane = new BoxPlotTableSeriesTypeUsePane();
        resultDataSeriesPane = new BoxPlotTableResultDataSeriesPane();

        JPanel pane = new JPanel(new BorderLayout(4, 0));

        pane.add(seriesTypeComboxPane, BorderLayout.CENTER);
        pane.add(resultDataSeriesPane, BorderLayout.SOUTH);

        return pane;
    }

    private JPanel createFilterPane() {
        dataScreeningPane = new ChartDataFilterPane(initplot, parent, false);
        dataScreeningPane.setBorder(BorderFactory.createEmptyBorder(10, 24, 10, 15));

        filterPane = new UIExpandablePane(Toolkit.i18nText("Fine-Design_Chart_Data_Filter"), 290, 24, dataScreeningPane);
        filterPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

        return filterPane;
    }

    private void initDataTypeListener() {
        dataType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkDataPaneVisible();
            }
        });
    }

    private void checkDataPaneVisible() {
        if (seriesTypeComboxPane != null) {
            seriesTypeComboxPane.setVisible(dataType.getSelectedIndex() == 0);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.setVisible(dataType.getSelectedIndex() == 1);
        }

        if (filterPane != null) {
            filterPane.setVisible(dataType.getSelectedIndex() == 0);
        }
    }

    public void checkBoxUse(boolean hasUse) {
        if (dataType.getSelectedIndex() == 0 && seriesTypeComboxPane != null) {
            seriesTypeComboxPane.checkBoxUse(hasUse);
        }

        if (dataType.getSelectedIndex() == 1 && resultDataSeriesPane != null) {
            resultDataSeriesPane.checkBoxUse(hasUse);
        }

        dataScreeningPane.checkBoxUse();
    }

    protected void refreshBoxListWithSelectTableData(List list) {
        if (seriesTypeComboxPane != null) {
            seriesTypeComboxPane.refreshBoxListWithSelectTableData(list);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.refreshBoxListWithSelectTableData(list);
        }
    }

    public void clearAllBoxList() {
        if (seriesTypeComboxPane != null) {
            seriesTypeComboxPane.clearAllBoxList();
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.clearAllBoxList();
        }
    }

    public void updateBean(ChartCollection collection) {
        collection.getSelectedChart().setFilterDefinition(new VanBoxTableDefinition());
        VanBoxTableDefinition table = BoxTableDefinitionHelper.getBoxTableDefinition(collection);

        if (table != null) {
            boolean isDetailed = dataType.getSelectedIndex() == 0;

            table.setDetailed(isDetailed);
            ((VanChartBoxPlot) initplot).updateDetailedAttr(isDetailed);
        }
        if (seriesTypeComboxPane != null) {
            seriesTypeComboxPane.updateBean(collection);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.updateBean(collection);
        }
        if (dataScreeningPane != null) {
            updateDataScreeningPane(dataScreeningPane, collection);
        }
    }

    public void populateBean(ChartCollection collection) {
        VanBoxTableDefinition table = BoxTableDefinitionHelper.getBoxTableDefinition(collection);

        if (table == null) {
            dataType.setSelectedIndex(0);
            checkDataPaneVisible();
            return;
        }

        if (dataType != null) {
            dataType.setSelectedIndex(BoxTableDefinitionHelper.isDetailedTableDataType(collection) ? 0 : 1);
        }
        if (seriesTypeComboxPane != null) {
            seriesTypeComboxPane.populateBean(collection);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.populateBean(collection);
        }
        if (dataScreeningPane != null) {
            populateDataScreeningPane(dataScreeningPane, collection);
        }

        checkDataPaneVisible();
    }

    private void populateDataScreeningPane(ChartDataFilterPane dataScreeningPane, ChartCollection collection) {
        NormalTableDataDefinition detailedDefinition = BoxTableDefinitionHelper.getBoxTableDetailedDefinition(collection);

        if (detailedDefinition != null) {
            dataScreeningPane.populateDefinition(detailedDefinition, false);
        }
    }

    private void updateDataScreeningPane(ChartDataFilterPane dataScreeningPane, ChartCollection collection) {
        NormalTableDataDefinition detailedDefinition = BoxTableDefinitionHelper.getBoxTableDetailedDefinition(collection);

        if (detailedDefinition != null) {
            dataScreeningPane.updateDefinition(detailedDefinition);
        }
    }
}