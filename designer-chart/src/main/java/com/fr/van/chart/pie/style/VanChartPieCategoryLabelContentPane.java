package com.fr.van.chart.pie.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.style.ChartTextAttrPane;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSummaryValueFormat;
import com.fr.plugin.chart.pie.attr.PieCategoryLabelContent;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SummaryValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-10
 */
public class VanChartPieCategoryLabelContentPane extends VanChartLabelContentPane {

    private SummaryValueFormatPaneWithCheckBox summaryValueFormatPane;

    public VanChartPieCategoryLabelContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        super(parent, showOnPane, inCondition);
    }

    @Override
    protected JPanel getLabelContentPane(JPanel contentPane) {
        return contentPane;
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        setCategoryNameFormatPane(new CategoryNameFormatPaneWithCheckBox(parent, showOnPane));
        summaryValueFormatPane = new SummaryValueFormatPaneWithCheckBox(parent, showOnPane);
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartPieCategoryRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Summary_Value")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipCategoryFormat(),
                new AttrTooltipSummaryValueFormat()
        };
    }

    @Override
    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{summaryValueFormatPane, null},
        };
    }

    @Override
    protected double[] getRowSize(double p) {
        return new double[]{p, p};
    }

    public JPanel createCommonStylePane() {
        setTextAttrPane(new ChartTextAttrPane());
        return getTextAttrPane();
    }

    @Override
    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        PieCategoryLabelContent pieCategoryLabelContent = (PieCategoryLabelContent) attrTooltipContent;
        super.populateFormatPane(pieCategoryLabelContent);
        summaryValueFormatPane.populate(pieCategoryLabelContent.getSummaryValueFormat());
    }

    protected void updateTooltipFormat(AttrTooltipContent target, AttrTooltipContent source) {
        super.updateTooltipFormat(target, source);

        if (target instanceof PieCategoryLabelContent && source instanceof PieCategoryLabelContent) {
            PieCategoryLabelContent targetPieCategory = (PieCategoryLabelContent) target;
            PieCategoryLabelContent sourcePieCategory = (PieCategoryLabelContent) source;

            targetPieCategory.setRichTextSummaryValueFormat(sourcePieCategory.getRichTextSummaryValueFormat());
        }
    }

    @Override
    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        PieCategoryLabelContent pieCategoryLabelContent = (PieCategoryLabelContent) attrTooltipContent;
        super.updateFormatPane(pieCategoryLabelContent);
        summaryValueFormatPane.update(pieCategoryLabelContent.getSummaryValueFormat());
    }

    @Override
    public void setDirty(boolean isDirty) {
        getCategoryNameFormatPane().setDirty(isDirty);
        summaryValueFormatPane.setDirty(isDirty);
    }

    @Override
    public boolean isDirty() {
        return getCategoryNameFormatPane().isDirty() || summaryValueFormatPane.isDirty();
    }

    protected AttrTooltipContent createAttrTooltip() {
        return new PieCategoryLabelContent();
    }
}
