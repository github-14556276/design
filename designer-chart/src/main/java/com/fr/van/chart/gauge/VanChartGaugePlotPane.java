package com.fr.van.chart.gauge;

import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Plot;
import com.fr.chartx.data.AbstractDataDefinition;
import com.fr.chartx.data.ChartDataDefinitionProvider;
import com.fr.chartx.data.field.AbstractColumnFieldCollection;
import com.fr.chartx.data.field.diff.GaugeColumnFieldCollection;
import com.fr.chartx.data.field.diff.MultiCategoryColumnFieldCollection;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.attr.GaugeDetailStyle;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.plugin.chart.gauge.GaugeIndependentVanChart;
import com.fr.plugin.chart.gauge.VanChartGaugePlot;
import com.fr.plugin.chart.type.GaugeStyle;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.van.chart.designer.type.AbstractVanChartTypePane;

/**
 * Created by Mitisky on 15/11/27.
 */
public class VanChartGaugePlotPane extends AbstractVanChartTypePane {

    private static final long serialVersionUID = -4599483879031804911L;

    @Override
    protected String[] getTypeIconPath() {
        return new String[]{"/com/fr/van/chart/gauge/images/pointer.png",
                "/com/fr/van/chart/gauge/images/pointer_180.png",
                "/com/fr/van/chart/gauge/images/ring.png",
                "/com/fr/van/chart/gauge/images/slot.png",
                "/com/fr/van/chart/gauge/images/cuvette.png",
        };
    }

    protected Plot getSelectedClonedPlot() {
        VanChartGaugePlot newPlot = null;
        Chart[] GaugeChart = GaugeIndependentVanChart.GaugeVanChartTypes;
        for (int i = 0, len = GaugeChart.length; i < len; i++) {
            if (typeDemo.get(i).isPressing) {
                newPlot = (VanChartGaugePlot) GaugeChart[i].getPlot();
            }
        }

        Plot cloned = null;
        try {
            if (newPlot == null) {
                throw new IllegalArgumentException("newPlot con not be null");
            } else {
                cloned = (Plot) newPlot.clone();
            }
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error("Error In GaugeChart");
        }
        return cloned;
    }

    /**
     * 保存界面属性
     */
    public void updateBean(VanChart chart) {
        boolean oldISMulti = chart.getPlot() instanceof VanChartGaugePlot && ((VanChartGaugePlot) chart.getPlot()).isMultiPointer();
        super.updateBean(chart);
        boolean newISMulti = chart.getPlot() instanceof VanChartGaugePlot && ((VanChartGaugePlot) chart.getPlot()).isMultiPointer();
        if (oldISMulti != newISMulti) {
            chart.setFilterDefinition(null);
        }
    }

    @Override
    protected VanChartPlot cloneOldPlot2New(VanChartPlot oldPlot, VanChartPlot newPlot) {
        try {
            VanChartGaugePlot vanChartNewPlot = (VanChartGaugePlot) newPlot;
            VanChartGaugePlot vanChartOldPlot = (VanChartGaugePlot) oldPlot;
            if (vanChartNewPlot.isMultiPointer() != vanChartOldPlot.isMultiPointer()) {
                return super.cloneOldPlot2New(oldPlot, newPlot);
            }

            VanChartGaugePlot clonePlot = (VanChartGaugePlot) vanChartOldPlot.clone();
            clonePlot.setGaugeStyle(vanChartNewPlot.getGaugeStyle());
            //都是多指针仪表盘,所有属性都一样
            if (clonePlot.isMultiPointer()) {
                return clonePlot;
            }
            //超链和系列中的样式、布局不保留
            clonePlot.setHotHyperLink(vanChartNewPlot.getHotHyperLink());
            clonePlot.setRadius(vanChartNewPlot.getRadius());
            reductionDetail(clonePlot.getGaugeDetailStyle(), vanChartNewPlot.getGaugeDetailStyle());

            //如果切换试管型仪表盘，部分属性不保留
            if (clonePlot.getGaugeStyle() == GaugeStyle.THERMOMETER ||
                    vanChartOldPlot.getGaugeStyle() == GaugeStyle.THERMOMETER) {
                clonePlot.setGaugeAxis(vanChartNewPlot.getGaugeAxis());
                clonePlot.getGaugeDetailStyle().setHorizontalLayout(vanChartNewPlot.getGaugeDetailStyle().isHorizontalLayout());
                clonePlot.getGaugeDetailStyle().setHotAreaColor(vanChartNewPlot.getGaugeDetailStyle().getHotAreaColor());

                AttrLabel cloneLabel = clonePlot.getAttrLabelFromConditionCollection();
                AttrLabel newLabel = vanChartNewPlot.getAttrLabelFromConditionCollection();
                cloneLabel.getAttrLabelDetail().setPosition(newLabel.getAttrLabelDetail().getPosition());
                cloneLabel.getAttrLabelDetail().setAlign(newLabel.getAttrLabelDetail().getAlign());
                cloneLabel.getAttrLabelDetail().setTextAttr(newLabel.getAttrLabelDetail().getTextAttr());
                cloneLabel.getGaugeValueLabelDetail().setPosition(newLabel.getGaugeValueLabelDetail().getPosition());
                cloneLabel.getGaugeValueLabelDetail().setAlign(newLabel.getGaugeValueLabelDetail().getAlign());
                cloneLabel.getGaugeValueLabelDetail().setTextAttr(newLabel.getGaugeValueLabelDetail().getTextAttr());
            }
            return clonePlot;

        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error("Error in change plot");
            return newPlot;
        }
    }

    private void reductionDetail(GaugeDetailStyle cloneDetail, GaugeDetailStyle newDetail) {
        cloneDetail.setNeedleColor(newDetail.getNeedleColor());
        cloneDetail.setPaneBackgroundColor(newDetail.getPaneBackgroundColor());
        cloneDetail.setSlotBackgroundColor(newDetail.getSlotBackgroundColor());
        cloneDetail.setAntiClockWise(newDetail.isAntiClockWise());
        cloneDetail.setInnerPaneBackgroundColor(newDetail.getInnerPaneBackgroundColor());
        cloneDetail.setThermometerWidth(newDetail.getThermometerWidth());
        cloneDetail.setChutePercent(newDetail.getChutePercent());
        cloneDetail.setPaneBackgroundColorAuto(newDetail.isPaneBackgroundColorAuto());
        cloneDetail.setSlotBackgroundColorAuto(newDetail.isSlotBackgroundColorAuto());
    }

    protected void cloneOldConditionCollection(Plot oldPlot, Plot newPlot) throws CloneNotSupportedException {
    }

    public Chart getDefaultChart() {
        return GaugeIndependentVanChart.GaugeVanChartTypes[0];
    }

    @Override
    protected void cloneHotHyperLink(Plot oldPlot, Plot newPlot) throws CloneNotSupportedException {
        if (oldPlot instanceof VanChartGaugePlot && newPlot instanceof VanChartGaugePlot) {
            if (((VanChartGaugePlot) oldPlot).isMultiPointer() == ((VanChartGaugePlot) newPlot).isMultiPointer()) {
                super.cloneHotHyperLink(oldPlot, newPlot);
            }
        }
    }

    @Override
    protected boolean acceptDefinition(ChartDataDefinitionProvider definition, VanChartPlot vanChartPlot) {
        if (definition instanceof AbstractDataDefinition) {
            AbstractColumnFieldCollection columnFieldCollection = ((AbstractDataDefinition) definition).getColumnFieldCollection();
            GaugeStyle gaugeStyle = ((VanChartGaugePlot) vanChartPlot).getGaugeStyle();
            switch (gaugeStyle) {
                case RING:
                case SLOT:
                case THERMOMETER:
                    return columnFieldCollection instanceof GaugeColumnFieldCollection;
                case POINTER:
                case POINTER_SEMI:
                default:
                    return columnFieldCollection instanceof MultiCategoryColumnFieldCollection;
            }
        }
        return false;
    }
}