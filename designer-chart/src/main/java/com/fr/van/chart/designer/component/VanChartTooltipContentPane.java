package com.fr.van.chart.designer.component;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.chart.gui.style.ChartTextAttrPane;
import com.fr.design.ui.ModernUIPane;
import com.fr.general.ComparatorUtils;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.AttrTooltipRichText;
import com.fr.plugin.chart.base.TableFieldCollection;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.stable.StringUtils;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.VanChartFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorPane;
import com.fr.van.chart.designer.component.richText.VanChartRichTextDialog;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * 数据点提示内容界面，含有通用设置、富文本编辑器、自定义JS界面
 */
public class VanChartTooltipContentPane extends BasicBeanPane<AttrTooltipContent> {

    private static final long serialVersionUID = 8825929000117843641L;

    // 字符样式button
    private static final int TEXT_ATTR_AUTO_INDEX = 0;
    private static final int TEXT_ATTR_CUSTOM_INDEX = 1;
    // 标签内容button
    public static final int COMMON_INDEX = 0;
    public static final int RICH_EDITOR_INDEX = 1;
    public static final int CUSTOM_INDEX_WITH_RICH_EDITOR = 2;
    public static final int CUSTOM_INDEX_WITHOUT_RICH_EDITOR = 1;

    private UIButtonGroup<Integer> content;

    private CategoryNameFormatPaneWithCheckBox categoryNameFormatPane;
    private SeriesNameFormatPaneWithCheckBox seriesNameFormatPane;
    private ValueFormatPaneWithCheckBox valueFormatPane;
    private PercentFormatPaneWithCheckBox percentFormatPane;

    //监控刷新时，自动数据点提示使用
    private ChangedValueFormatPaneWithCheckBox changedValueFormatPane;
    private ChangedPercentFormatPaneWithCheckBox changedPercentFormatPane;

    private JPanel centerPanel;
    private JPanel commonPanel;
    private JPanel editorPanel;
    private JPanel htmlPanel;
    private JPanel stylePanel;
    private UIButtonGroup<Integer> styleButton;
    private ChartTextAttrPane textAttrPane;
    private VanChartHtmlLabelPane htmlLabelPane;

    private VanChartStylePane parent;
    private JPanel showOnPane;

    private AttrTooltipContent richTextTooltipContent;
    private boolean inCondition;

    public VanChartTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        this(parent, showOnPane, false);
    }

    public VanChartTooltipContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        this.inCondition = inCondition;
        this.parent = parent;
        this.showOnPane = showOnPane;

        this.richTextTooltipContent = createAttrTooltip();

        initFormatPane(parent, showOnPane);

        this.setLayout(new BorderLayout());
        this.add(createLabelContentPane(), BorderLayout.CENTER);
    }

    public boolean isInCondition() {
        return inCondition;
    }

    public ChartTextAttrPane getTextAttrPane() {
        return textAttrPane;
    }

    public void setTextAttrPane(ChartTextAttrPane textAttrPane) {
        this.textAttrPane = textAttrPane;
    }

    public CategoryNameFormatPaneWithCheckBox getCategoryNameFormatPane() {
        return categoryNameFormatPane;
    }

    public void setCategoryNameFormatPane(CategoryNameFormatPaneWithCheckBox categoryNameFormatPane) {
        this.categoryNameFormatPane = categoryNameFormatPane;
    }

    public SeriesNameFormatPaneWithCheckBox getSeriesNameFormatPane() {
        return seriesNameFormatPane;
    }

    public void setSeriesNameFormatPane(SeriesNameFormatPaneWithCheckBox seriesNameFormatPane) {
        this.seriesNameFormatPane = seriesNameFormatPane;
    }

    public ValueFormatPaneWithCheckBox getValueFormatPane() {
        return valueFormatPane;
    }

    public void setValueFormatPane(ValueFormatPaneWithCheckBox valueFormatPane) {
        this.valueFormatPane = valueFormatPane;
    }

    public PercentFormatPaneWithCheckBox getPercentFormatPane() {
        return percentFormatPane;
    }

    public void setPercentFormatPane(PercentFormatPaneWithCheckBox percentFormatPane) {
        this.percentFormatPane = percentFormatPane;
    }

    public ChangedValueFormatPaneWithCheckBox getChangedValueFormatPane() {
        return changedValueFormatPane;
    }

    public void setChangedValueFormatPane(ChangedValueFormatPaneWithCheckBox changedValueFormatPane) {
        this.changedValueFormatPane = changedValueFormatPane;
    }

    public ChangedPercentFormatPaneWithCheckBox getChangedPercentFormatPane() {
        return changedPercentFormatPane;
    }

    public void setChangedPercentFormatPane(ChangedPercentFormatPaneWithCheckBox changedPercentFormatPane) {
        this.changedPercentFormatPane = changedPercentFormatPane;
    }

    public UIButtonGroup<Integer> getContent() {
        return content;
    }

    public AttrTooltipRichText getRichTextAttr() {
        return this.richTextTooltipContent.getRichTextAttr();
    }

    public void setRichTextAttr(AttrTooltipRichText richText) {
        this.richTextTooltipContent.setRichTextAttr(richText);
    }

    public void setRichTextTooltipContent(AttrTooltipContent richTextTooltipContent) {
        this.richTextTooltipContent = richTextTooltipContent;
    }

    private JPanel createLabelContentPane() {
        initDetailPane();
        initCenterPane();

        JPanel content = new JPanel(new BorderLayout());

        content.add(createButtonPane(), BorderLayout.NORTH);
        content.add(centerPanel, BorderLayout.CENTER);
        content.add(stylePanel, BorderLayout.SOUTH);

        initContentListener();

        return getLabelContentPane(content);
    }

    private void initDetailPane() {
        commonPanel = createCommonFormatPanel();
        editorPanel = createRichEditorPanel();
        htmlPanel = createHtmlPane();
        stylePanel = createCommonStylePane();
    }

    private void initCenterPane() {
        if (supportRichEditor()) {
            initCenterPaneWithRichEditor();
        } else {
            initCenterPaneWithoutRichEditor();
        }
    }

    private void initCenterPaneWithRichEditor() {
        centerPanel = new JPanel(new CardLayout()) {
            public Dimension getPreferredSize() {
                if (content.getSelectedIndex() == COMMON_INDEX) {
                    return commonPanel.getPreferredSize();
                } else if (content.getSelectedIndex() == RICH_EDITOR_INDEX) {
                    return editorPanel.getPreferredSize();
                } else {
                    return new Dimension(editorPanel.getPreferredSize().width, htmlLabelPane.getPreferredSize().height);
                }
            }
        };

        centerPanel.add(commonPanel, Toolkit.i18nText("Fine-Design_Chart_Common"));
        centerPanel.add(editorPanel, Toolkit.i18nText("Fine-Design_Chart_Rich_Text"));
        centerPanel.add(htmlPanel, Toolkit.i18nText("Fine-Design_Chart_Custom"));
    }

    private void initCenterPaneWithoutRichEditor() {
        centerPanel = new JPanel(new CardLayout()) {
            public Dimension getPreferredSize() {
                if (content.getSelectedIndex() == COMMON_INDEX) {
                    return commonPanel.getPreferredSize();
                } else {
                    return new Dimension(commonPanel.getPreferredSize().width, htmlLabelPane.getPreferredSize().height);
                }
            }
        };

        centerPanel.add(commonPanel, Toolkit.i18nText("Fine-Design_Chart_Common"));
        centerPanel.add(htmlPanel, Toolkit.i18nText("Fine-Design_Chart_Custom"));
    }

    protected String getLabelContentTitle() {
        return Toolkit.i18nText("Fine-Design_Report_Text");
    }

    protected JPanel getLabelContentPane(JPanel contentPane) {
        return createTableLayoutPaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Content"), contentPane);
    }

    protected boolean supportRichEditor() {
        return true;
    }

    protected boolean hasTextStylePane() {
        return true;
    }

    private JPanel createButtonPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;

        double[] column = {f, p};
        double[] row = {p, p};

        if (supportRichEditor()) {
            content = new UIButtonGroup<>(new String[]{
                    Toolkit.i18nText("Fine-Design_Chart_Common"),
                    Toolkit.i18nText("Fine-Design_Chart_Rich_Text"),
                    Toolkit.i18nText("Fine-Design_Chart_Custom")
            });
        } else {
            content = new UIButtonGroup<>(new String[]{
                    Toolkit.i18nText("Fine-Design_Chart_Common"),
                    Toolkit.i18nText("Fine-Design_Chart_Custom")
            });
        }

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{content, null}
        };

        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, row, column);
    }

    protected JPanel createCommonFormatPanel() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        JPanel formatContent = TableLayoutHelper.createTableLayoutPane(getPaneComponents(), getRowSize(p), new double[]{f, p});

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(getLabelContentTitle()), formatContent}
        };

        return TableLayoutHelper.createTableLayoutPane(components, new double[]{p, p}, new double[]{f, e});
    }

    public JPanel createCommonStylePane() {
        styleButton = new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Automatic"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")
        });

        textAttrPane = new ChartTextAttrPane() {
            protected Component[][] getComponents(JPanel buttonPane) {
                return new Component[][]{
                        new Component[]{null, null},
                        new Component[]{null, getFontNameComboBox()},
                        new Component[]{null, buttonPane}
                };
            }
        };

        JPanel buttonPane = TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Widget_Style"), styleButton);

        JPanel stylePanel = new JPanel(new BorderLayout());
        stylePanel.add(buttonPane, BorderLayout.CENTER);
        stylePanel.add(textAttrPane, BorderLayout.SOUTH);

        initStyleButtonListener();

        return stylePanel;
    }

    private void initStyleButtonListener() {
        styleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkStylePane();
            }
        });
    }

    private void checkStylePane() {
        if (hasTextStylePane()) {
            stylePanel.setVisible(true);
            if (styleButton != null) {
                textAttrPane.setVisible(styleButton.getSelectedIndex() == TEXT_ATTR_CUSTOM_INDEX);
            }
        } else {
            stylePanel.setVisible(false);
        }
    }

    private JPanel createRichEditorPanel() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p};

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Content_Style")), createRichEditorButton()}
        };

        return TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
    }

    private JComponent createRichEditorButton() {
        UIButton contentTextArea = new UIButton(Toolkit.i18nText("Fine-Design_Chart_Rich_Text_Edit"));

        contentTextArea.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                fireRichEditor();
            }
        });

        return contentTextArea;
    }

    private void fireRichEditor() {
        AttrTooltipRichText richText = this.richTextTooltipContent.getRichTextAttr();
        refreshTableFieldParams(richText.getParams());
        ModernUIPane<VanChartRichEditorModel> richEditorPane = VanChartRichEditorPane.createRichEditorPane(richText);
        VanChartRichTextPane richTextPane = this.createRichTextPane(richEditorPane);

        BasicDialog richTextDialog = new VanChartRichTextDialog(DesignerContext.getDesignerFrame(), richTextPane);

        richTextPane.populateBean(this.richTextTooltipContent);
        richEditorPane.populate(VanChartRichEditorPane.getRichEditorModel(richText));

        richTextDialog.addDialogActionListener(new DialogActionAdapter() {

            public void doOk() {
                AttrTooltipContent temporary = richTextPane.updateBean();
                VanChartRichEditorModel model = richEditorPane.update();

                String content = model.getContent();
                updateLocalRichText(content, model.isAuto());

                TableFieldCollection fieldCollection = temporary.getFieldCollection();
                if (fieldCollection != null) {
                    fieldCollection.checkFieldDefinition(content);
                }

                updateTooltipFormat(richTextTooltipContent, temporary);

                SwingUtilities.getWindowAncestor(richEditorPane).setVisible(false);
                VanChartRichEditorPane.checkDispose();
            }

            public void doCancel() {
                SwingUtilities.getWindowAncestor(richEditorPane).setVisible(false);
                VanChartRichEditorPane.checkDispose();
            }
        });

        richTextDialog.setVisible(true);

        if (parent != null) {
            parent.attributeChanged();
        }
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        return new VanChartRichTextPane(richEditorPane);
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Percent")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipCategoryFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }

    private void refreshRichTextParams(AttrTooltipRichText richText) {
        String[] fieldNames = getRichTextFieldNames();
        AttrTooltipFormat[] fieldFormats = getRichTextFieldFormats();

        Map<String, String> params = new LinkedHashMap<>();

        for (int i = 0, len = fieldNames.length; i < len; i++) {
            params.put(fieldNames[i], fieldFormats[i].getJs());
        }

        refreshTableFieldParams(params);

        richText.setParams(params);
    }

    private void refreshTableFieldParams(Map<String, String> params) {
        List<String> tableFieldNames = VanChartRichEditorPane.getFieldNames();
        List<String> defaultParams = Arrays.asList(getRichTextFieldNames());

        if (tableFieldNames == null || params == null) {
            return;
        }

        for (String fieldName : tableFieldNames) {
            // 富文本默认参数和数据集字段重名时，显示默认参数
            if (!defaultParams.contains(fieldName)) {
                params.put(fieldName, "${" + fieldName + "_" + fieldName.hashCode() + "}");
            }
        }
    }

    private JPanel createHtmlPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;

        double[] column = {f, p};
        double[] row = {p, p};

        htmlLabelPane = createHtmlLabelPane();
        htmlLabelPane.setParent(parent);

        Component[][] htmlComponents = new Component[][]{
                new Component[]{null, null},
                new Component[]{htmlLabelPane, null}
        };

        return TableLayout4VanChartHelper.createGapTableLayoutPane(htmlComponents, row, column);
    }

    protected VanChartHtmlLabelPane createHtmlLabelPane() {
        return new VanChartHtmlLabelPaneWithOutWidthAndHeight();
    }

    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        categoryNameFormatPane = new CategoryNameFormatPaneWithCheckBox(parent, showOnPane);
        seriesNameFormatPane = new SeriesNameFormatPaneWithCheckBox(parent, showOnPane);
        valueFormatPane = new ValueFormatPaneWithCheckBox(parent, showOnPane);
        percentFormatPane = new PercentFormatPaneWithCheckBox(parent, showOnPane);
    }

    protected JPanel createTableLayoutPaneWithTitle(String title, JPanel panel) {
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(title, panel);
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{categoryNameFormatPane, null},
                new Component[]{seriesNameFormatPane, null},
                new Component[]{valueFormatPane, null},
                new Component[]{percentFormatPane, null},
        };
    }

    private void initContentListener() {
        content.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCardPane();
            }
        });
    }


    protected void checkCardPane() {
        if (supportRichEditor()) {
            checkCardPaneWithRichEditor();
        } else {
            checkCardPaneWithoutRichEditor();
        }
    }

    private void checkCardPaneWithRichEditor() {
        CardLayout cardLayout = (CardLayout) centerPanel.getLayout();

        if (content.getSelectedIndex() == CUSTOM_INDEX_WITH_RICH_EDITOR) {
            cardLayout.show(centerPanel, Toolkit.i18nText("Fine-Design_Chart_Custom"));
            checkCustomPane();
        } else if (content.getSelectedIndex() == RICH_EDITOR_INDEX) {
            cardLayout.show(centerPanel, Toolkit.i18nText("Fine-Design_Chart_Rich_Text"));
        } else {
            cardLayout.show(centerPanel, Toolkit.i18nText("Fine-Design_Chart_Common"));
        }

        stylePanel.setVisible(hasTextStylePane() && content.getSelectedIndex() != RICH_EDITOR_INDEX);
    }

    private void checkCardPaneWithoutRichEditor() {
        CardLayout cardLayout = (CardLayout) centerPanel.getLayout();

        if (content.getSelectedIndex() == CUSTOM_INDEX_WITHOUT_RICH_EDITOR) {
            cardLayout.show(centerPanel, Toolkit.i18nText("Fine-Design_Chart_Custom"));
            checkCustomPane();
        } else {
            cardLayout.show(centerPanel, Toolkit.i18nText("Fine-Design_Chart_Common"));
        }

        stylePanel.setVisible(hasTextStylePane());
    }

    private void checkCustomPane() {
        if (isDirty()) {
            setCustomFormatterText();
            setDirty(false);
        }
    }

    protected void setCustomFormatterText() {
        htmlLabelPane.setCustomFormatterText(updateBean().getFormatterTextFromCommon());
    }

    public boolean isDirty() {

        return categoryNameFormatPane.isDirty()
                || seriesNameFormatPane.isDirty()
                || valueFormatPane.isDirty()
                || percentFormatPane.isDirty()
                || (changedValueFormatPane != null && changedValueFormatPane.isDirty())
                || (changedValueFormatPane != null && changedPercentFormatPane.isDirty());
    }

    public void setDirty(boolean isDirty) {
        categoryNameFormatPane.setDirty(isDirty);
        seriesNameFormatPane.setDirty(isDirty);
        valueFormatPane.setDirty(isDirty);
        percentFormatPane.setDirty(isDirty);

        if (changedValueFormatPane != null) {
            changedValueFormatPane.setDirty(isDirty);
        }
        if (changedPercentFormatPane != null) {
            changedPercentFormatPane.setDirty(isDirty);
        }
    }

    @Override
    protected String title4PopupWindow() {
        return "";
    }


    @Override
    public void populateBean(AttrTooltipContent attrTooltipContent) {
        if (attrTooltipContent == null) {
            return;
        }

        populateTypeButton(attrTooltipContent);
        populateFormatPane(attrTooltipContent);
        if (supportRichEditor()) {
            populateRichEditor(attrTooltipContent);
        }
        if (hasTextStylePane()) {
            if (styleButton != null) {
                if (attrTooltipContent.isCustom()) {
                    styleButton.setSelectedIndex(TEXT_ATTR_CUSTOM_INDEX);
                } else {
                    styleButton.setSelectedIndex(TEXT_ATTR_AUTO_INDEX);
                }
            }
            this.textAttrPane.populate(attrTooltipContent.getTextAttr());
        }

        htmlLabelPane.populate(attrTooltipContent.getHtmlLabel());
        if (!(attrTooltipContent.isCommon() || attrTooltipContent.isRichText())) {
            setDirty(false);
        }

        checkStylePane();
        checkCardPane();
    }

    private void populateTypeButton(AttrTooltipContent attrTooltipContent) {
        if (supportRichEditor()) {
            if (attrTooltipContent.isCommon()) {
                content.setSelectedIndex(COMMON_INDEX);
            } else if (attrTooltipContent.isRichText()) {
                content.setSelectedIndex(RICH_EDITOR_INDEX);
            } else {
                content.setSelectedIndex(CUSTOM_INDEX_WITH_RICH_EDITOR);
            }

            return;
        }

        if (attrTooltipContent.isCommon()) {
            content.setSelectedIndex(COMMON_INDEX);
        } else {
            content.setSelectedIndex(CUSTOM_INDEX_WITHOUT_RICH_EDITOR);
        }
    }

    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        VanChartFormatPaneWithCheckBox[] formatPaneGroup = new VanChartFormatPaneWithCheckBox[]{
                categoryNameFormatPane,
                seriesNameFormatPane,
                valueFormatPane,
                percentFormatPane,
                changedValueFormatPane,
                changedPercentFormatPane
        };

        AttrTooltipFormat[] formatGroup = new AttrTooltipFormat[]{
                attrTooltipContent.getCategoryFormat(),
                attrTooltipContent.getSeriesFormat(),
                attrTooltipContent.getValueFormat(),
                attrTooltipContent.getPercentFormat(),
                attrTooltipContent.getChangedValueFormat(),
                attrTooltipContent.getChangedPercentFormat()
        };

        for (int i = 0, len = formatPaneGroup.length; i < len; i++) {
            VanChartFormatPaneWithCheckBox formatPane = formatPaneGroup[i];
            AttrTooltipFormat format = formatGroup[i];

            if (formatPane != null && format != null) {
                formatPane.populate(format);
            }
        }
    }

    protected void populateRichEditor(AttrTooltipContent attrTooltipContent) {
        if (parent != null) {
            parent.refreshTableFieldNames();
        }

        updateTooltipFormat(richTextTooltipContent, attrTooltipContent);
        refreshRichTextParams(richTextTooltipContent.getRichTextAttr());
        populateRichText(attrTooltipContent.getRichTextAttr());
        checkRichEditorState(attrTooltipContent);
    }

    protected void populateRichText(AttrTooltipRichText tooltipRichText) {
        if (tooltipRichText != null) {
            updateLocalRichText(tooltipRichText.getContent(), tooltipRichText.isAuto());
            richTextTooltipContent.getRichTextAttr().setInitParamsContent(tooltipRichText.getInitParamsContent());
        }
    }

    protected void checkRichEditorState(AttrTooltipContent attrTooltipContent) {
        AttrTooltipRichText richText = this.richTextTooltipContent.getRichTextAttr();

        if (ComparatorUtils.equals(richText.getInitParamsContent(), StringUtils.EMPTY)) {
            richText.setContent(attrTooltipContent.getRichTextDefaultContent());
            richText.setInitParamsContent(attrTooltipContent.getRichTextDefaultParams());
        }
    }

    public AttrTooltipContent updateBean() {
        AttrTooltipContent attrTooltipContent = createAttrTooltip();

        updateLabelType(attrTooltipContent);
        updateFormatPane(attrTooltipContent);
        if (supportRichEditor()) {
            updateTooltipFormat(attrTooltipContent, this.richTextTooltipContent);
            updateTooltipRichText(attrTooltipContent);
        }
        updateTextAttr(attrTooltipContent);

        htmlLabelPane.update(attrTooltipContent.getHtmlLabel());

        return attrTooltipContent;
    }

    public void updateTextAttr(AttrTooltipContent attrTooltipContent) {
        if (hasTextStylePane()) {
            if (styleButton != null) {
                attrTooltipContent.setCustom(styleButton.getSelectedIndex() == TEXT_ATTR_CUSTOM_INDEX);
            }
            attrTooltipContent.setTextAttr(this.textAttrPane.update());
        }
    }

    private void updateLabelType(AttrTooltipContent attrTooltipContent) {
        if (supportRichEditor()) {
            attrTooltipContent.setCommon(content.getSelectedIndex() == COMMON_INDEX);
            attrTooltipContent.setRichText(content.getSelectedIndex() == RICH_EDITOR_INDEX);

            return;
        }

        attrTooltipContent.setCommon(content.getSelectedIndex() == COMMON_INDEX);
        attrTooltipContent.setRichText(false);
    }

    protected AttrTooltipContent createAttrTooltip() {
        return new AttrTooltipContent();
    }

    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        if (categoryNameFormatPane != null) {
            categoryNameFormatPane.update(attrTooltipContent.getCategoryFormat());
        }
        if (seriesNameFormatPane != null) {
            seriesNameFormatPane.update(attrTooltipContent.getSeriesFormat());
        }
        if (valueFormatPane != null) {
            valueFormatPane.update(attrTooltipContent.getValueFormat());
        }
        if (percentFormatPane != null) {
            percentFormatPane.update(attrTooltipContent.getPercentFormat());
        }
        if (changedValueFormatPane != null) {
            changedValueFormatPane.update(attrTooltipContent.getChangedValueFormat());
        }
        if (changedPercentFormatPane != null) {
            changedPercentFormatPane.update(attrTooltipContent.getChangedPercentFormat());
        }
    }

    protected void updateTooltipFormat(AttrTooltipContent target, AttrTooltipContent source) {
        if (target == null || source == null) {
            return;
        }

        // 更新富文本字段格式
        target.setRichTextCategoryFormat(source.getRichTextCategoryFormat());
        target.setRichTextSeriesFormat(source.getRichTextSeriesFormat());
        target.setRichTextValueFormat(source.getRichTextValueFormat());
        target.setRichTextPercentFormat(source.getRichTextPercentFormat());
        // 更新新增字段格式和汇总
        target.setFieldCollection(source.getFieldCollection());
    }

    private void updateLocalRichText(String content, boolean isAuto) {
        AttrTooltipRichText richText = this.richTextTooltipContent.getRichTextAttr();

        richText.setContent(content);
        richText.setAuto(isAuto);
    }

    private void updateTooltipRichText(AttrTooltipContent attrTooltipContent) {
        if (attrTooltipContent != null) {
            AttrTooltipRichText targetRichText = attrTooltipContent.getRichTextAttr();
            AttrTooltipRichText localRichText = this.richTextTooltipContent.getRichTextAttr();

            targetRichText.setContent(localRichText.getContent());
            targetRichText.setInitParamsContent(localRichText.getInitParamsContent());
            targetRichText.setAuto(localRichText.isAuto());
        }
    }
}
