package com.fr.van.chart.map.designer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

public class VanChartMapRichTextFieldListPane extends VanChartFieldListPane {

    public VanChartMapRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void initDefaultFieldButton() {
        VanChartFieldListener fieldListener = getFieldListener();

        VanChartFieldButton categoryNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Area_Name"),
                new AttrTooltipAreaNameFormat(), false, fieldListener);

        VanChartFieldButton seriesNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                new AttrTooltipSeriesFormat(), false, fieldListener);

        VanChartFieldButton valueButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                new AttrTooltipValueFormat(), false, fieldListener);

        VanChartFieldButton percentButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Percent"),
                new AttrTooltipPercentFormat(), false, fieldListener);

        setCategoryNameButton(categoryNameButton);
        setSeriesNameButton(seriesNameButton);
        setValueButton(valueButton);
        setPercentButton(percentButton);
    }
}
