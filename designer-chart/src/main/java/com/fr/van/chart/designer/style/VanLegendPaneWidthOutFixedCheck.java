package com.fr.van.chart.designer.style;

import javax.swing.JPanel;

/**
 * Created by eason on 2016/12/14.
 */
public class VanLegendPaneWidthOutFixedCheck extends VanChartPlotLegendPane{

    public VanLegendPaneWidthOutFixedCheck(){

    }

    public VanLegendPaneWidthOutFixedCheck(VanChartStylePane parent){
        super(parent);
    }

    protected JPanel createLegendPane(){
        return this.createLegendPaneWithoutFixedCheck();
    }
}
