package com.fr.van.chart.map.designer.style.series;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPx;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

public class VanChartMapAnchorMarkerPane extends BasicBeanPane<VanChartAttrMarker> {

    private UISpinner anchorSize;

    public VanChartMapAnchorMarkerPane() {
        anchorSize = new UISpinnerWithPx(0, Double.MAX_VALUE, 0.5, 22);

        Component[][] components = new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Height")), anchorSize}
        };

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.SECOND_EDIT_AREA_WIDTH;
        double[] row = {p};
        double[] col = {f, e};

        JPanel content = TableLayoutHelper.createTableLayoutPane(components, row, col);
        content.setBorder(TableLayout4VanChartHelper.SECOND_EDIT_AREA_BORDER);

        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        this.add(content, BorderLayout.CENTER);
    }

    public void populateBean(VanChartAttrMarker marker) {
        if (marker == null) {
            marker = new VanChartAttrMarker();
            marker.setCommon(false);
            marker.setMarkerType(MarkerType.MARKER_AUTO);
        }

        this.anchorSize.setValue(marker.getAnchorSize());
    }

    public VanChartAttrMarker updateBean() {
        VanChartAttrMarker marker = new VanChartAttrMarker();

        marker.setCommon(false);
        marker.setMarkerType(MarkerType.MARKER_AUTO);
        marker.setAnchorSize(this.anchorSize.getValue());

        return marker;
    }

    protected String title4PopupWindow() {
        return "anchorMarker";
    }
}
