package com.fr.van.chart.box.data.table;

import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.mainframe.chart.gui.UIEditLabel;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemListener;

public abstract class UIComboBoxWithEditLabel extends JPanel implements UIObserver {

    private UIEditLabel editLabel;
    private UIComboBox comboBox;

    protected UIObserverListener listener;

    public UIComboBoxWithEditLabel(String text) {
        editLabel = new UIEditLabel(text, SwingConstants.LEFT) {
            protected void doAfterMousePress() {
                clearAllBackground();
            }

            protected boolean appendOriginalLabel() {
                return false;
            }
        };

        editLabel.setPreferredSize(new Dimension(80, 20));
        comboBox = new UIComboBox();
        comboBox.setPreferredSize(new Dimension(100, 20));

        this.setLayout(new BorderLayout(4, 0));
        this.add(editLabel, BorderLayout.WEST);
        this.add(comboBox, BorderLayout.CENTER);
    }

    public UIComboBox getComboBox() {
        return comboBox;
    }

    public void setComboBox(UIComboBox comboBox) {
        this.comboBox = comboBox;
    }

    public void addItem(String value) {
        comboBox.addItem(value);
    }

    protected void addItemListener(ItemListener aListener) {
        comboBox.addItemListener(aListener);
    }

    protected abstract void clearAllBackground();

    public void clearBackGround() {
        editLabel.resetNomalrBackground();
    }

    public boolean shouldResponseChangeListener() {
        return true;
    }

    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;
        editLabel.registerChangeListener(listener);
    }

    public void populateComboBox(String value) {
        if (comboBox != null) {
            comboBox.setEditable(true);
            comboBox.setSelectedItem(value);
            comboBox.setEditable(false);
        }
    }

    public Object updateComboBox() {
        return comboBox.getSelectedItem();
    }

    public String getHeaderName() {
        return editLabel.getText();
    }

    public void setHeaderName(String text) {
        editLabel.setText(text);
    }
}
