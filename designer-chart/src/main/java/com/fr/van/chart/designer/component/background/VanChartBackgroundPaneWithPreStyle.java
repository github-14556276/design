package com.fr.van.chart.designer.component.background;

import com.fr.chart.base.BackgroundWithPreStyle;
import com.fr.chart.chartglyph.GeneralInfo;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.backgroundpane.ColorBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.ImageBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.NullBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.VanChartGradientPane;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-08
 */
public class VanChartBackgroundPaneWithPreStyle extends VanChartBackgroundPane {

    private static final int PREDEFINED_STYLE = 0;

    @Override
    protected void initList() {
        paneList.add(new NullBackgroundQuickPane() {
            public String title4PopupWindow() {
                return Toolkit.i18nText("Fine-Design_Chart_Predefined");
            }
        });
        paneList.add(new NullBackgroundQuickPane());
        paneList.add(new ColorBackgroundQuickPane());
        paneList.add(new ImageBackgroundQuickPane(false));
        paneList.add(new VanChartGradientPane());
    }

    public void populateBackground(GeneralInfo attr, int begin) {
        BackgroundWithPreStyle backgroundWithPreStyle = attr.getBackgroundWithPreStyle();
        if (backgroundWithPreStyle.isPredefinedStyle()) {
            typeComboBox.setSelectedIndex(PREDEFINED_STYLE);
            return;
        }
        super.populateBackground(attr, begin + 1);
    }

    public void updateBackground(GeneralInfo attr) {
        if (typeComboBox.getSelectedIndex() == PREDEFINED_STYLE) {
            attr.getBackgroundWithPreStyle().setPredefinedStyle(true);
            return;
        }
        attr.getBackgroundWithPreStyle().setPredefinedStyle(false);
        super.updateBackground(attr);
    }

}
