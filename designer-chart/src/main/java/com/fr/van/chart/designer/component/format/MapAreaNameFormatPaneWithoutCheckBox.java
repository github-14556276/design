package com.fr.van.chart.designer.component.format;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class MapAreaNameFormatPaneWithoutCheckBox extends CategoryNameFormatPaneWithoutCheckBox {

    public MapAreaNameFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Area_Name");
    }
}
