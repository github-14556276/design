package com.fr.van.chart.designer.component.format;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class PercentFormatPaneWithoutCheckBox extends VanChartFormatPaneWithoutCheckBox {

    public PercentFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Use_Percent");
    }

    protected boolean isPercent() {
        return true;
    }
}
