package com.fr.van.chart.designer.style.label;

import com.fr.chart.chartattr.Plot;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.van.chart.designer.PlotFactory;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VanChartPlotLabelPane extends BasicPane {
    private static final long serialVersionUID = -1701936672446232396L;
    private UICheckBox isLabelShow;

    private VanChartPlotLabelDetailPane labelDetailPane;

    private VanChartStylePane parent;
    private Plot plot;

    private JPanel labelPane;

    private boolean inCondition;

    public VanChartPlotLabelPane(Plot plot, VanChartStylePane parent) {
        this(plot, parent, false);
    }

    public VanChartPlotLabelPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        this.parent = parent;
        this.plot = plot;
        this.inCondition = inCondition;
        isLabelShow = new UICheckBox(Toolkit.i18nText("Fine-Design_Chart_Use_Label"));
        labelPane = new JPanel(new BorderLayout(0, 4));
        createLabelPane();
        addComponents();
    }

    public boolean isInCondition() {
        return inCondition;
    }

    public VanChartPlotLabelDetailPane getLabelDetailPane() {
        return labelDetailPane;
    }

    public void setLabelDetailPane(VanChartPlotLabelDetailPane labelDetailPane) {
        this.labelDetailPane = labelDetailPane;
    }

    public VanChartStylePane getParentPane() {
        return parent;
    }

    public Plot getPlot() {
        return plot;
    }

    public JPanel getLabelPane() {
        return labelPane;
    }

    public void setLabelPane(JPanel labelPane) {
        this.labelPane = labelPane;
    }

    protected void createLabelPane() {
        labelDetailPane = createPlotLabelDetailPane();
        labelPane.add(labelDetailPane, BorderLayout.CENTER);
    }

    protected void addComponents() {
        this.setLayout(new BorderLayout());
        this.add(getCenterPane(), BorderLayout.CENTER);
    }

    private JPanel getCenterPane() {
        if (inCondition) {
            return labelPane;
        }
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] columnSize = {f};
        double[] rowSize = {p, p};
        Component[][] components = new Component[][]{
                new Component[]{isLabelShow},
                new Component[]{labelPane}
        };
        isLabelShow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkBoxUse();
            }
        });

        JPanel panel = TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
        return panel;
    }


    @Override
    protected String title4PopupWindow() {
        return null;
    }

    private void checkBoxUse() {
        labelPane.setVisible(isLabelShow.isSelected());
        if (checkEnabled4Large()) {
            isLabelShow.setEnabled(!PlotFactory.largeDataModel(plot));
        }
    }

    protected boolean checkEnabled4Large() {
        return true;
    }

    public void populate(AttrLabel attr) {
        if (attr == null) {
            attr = ((VanChartPlot) this.plot).getDefaultAttrLabel();
        }

        isLabelShow.setSelected(inCondition ? true : attr.isEnable());

        labelDetailPane.populate(attr.getAttrLabelDetail());

        checkBoxUse();
    }

    public AttrLabel update() {
        //刪除返回null,否則無法保存不顯示標籤的屬性
        AttrLabel attrLabel = ((VanChartPlot) this.plot).getDefaultAttrLabel();
        attrLabel.setEnable(isLabelShow.isSelected());

        labelDetailPane.update(attrLabel.getAttrLabelDetail());

        return attrLabel;
    }

    public VanChartPlotLabelDetailPane createPlotLabelDetailPane() {
        return PlotFactory.createPlotLabelDetailPane(plot, parent, inCondition);
    }
}