package com.fr.van.chart.scatter;

import com.fr.van.chart.designer.component.VanChartHtmlLabelPane;
import com.fr.van.chart.designer.component.VanChartHtmlLabelPaneWithBackGroundLabel;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * 散点图标签界面
 */
public class  VanChartScatterLabelContentPane extends VanChartScatterTooltipContentPane{
    private static final long serialVersionUID = 5595016643808487922L;

    public VanChartScatterLabelContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        super(parent, showOnPane, inCondition);
    }

    @Override
    protected VanChartHtmlLabelPane createHtmlLabelPane() {
        return new VanChartHtmlLabelPaneWithBackGroundLabel();
    }

    //TODO Bjorn 散点图标签预定义样式
   /* public JPanel createCommonStylePane() {
        if (isInCondition()) {
            return super.createCommonStylePane();
        }
        setTextAttrPane(new ChartTextAttrPaneWithPreStyle());

        JPanel stylePanel = new JPanel(new BorderLayout());
        stylePanel.add(getTextAttrPane(), BorderLayout.CENTER);

        return stylePanel;
    }

    public void updateTextAttr(AttrTooltipContent attrTooltipContent) {
        if (isInCondition()) {
            super.updateTextAttr(attrTooltipContent);
            return;
        }
        if (hasTextStylePane()) {
            this.getTextAttrPane().update(attrTooltipContent.getTextAttr());
            if (!attrTooltipContent.getTextAttr().isPredefinedStyle()) {
                attrTooltipContent.setCustom(true);
            }
        }
    }*/
}
