package com.fr.van.chart.designer.component.tooltip;

import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by mengao on 2017/6/11.
 */
public class RefreshTooltipContentPaneWithOutSeries extends TooltipContentPaneWithOutSeries {
    public RefreshTooltipContentPaneWithOutSeries(VanChartStylePane parent, JPanel showOnPane) {
        super(null, showOnPane);
    }

    protected boolean supportRichEditor() {
        return false;
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);

        setChangedValueFormatPane(new ChangedValueFormatPaneWithCheckBox(parent, showOnPane) {
            @Override
            protected String getCheckBoxText() {
                return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Pointer_Change_Value");
            }
        });
        setChangedPercentFormatPane(new ChangedPercentFormatPaneWithCheckBox(parent, showOnPane));
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{getTargetValueFormatPane(), null},
                new Component[]{getChangedValueFormatPane(), null},
                new Component[]{getPercentFormatPane(), null},
                new Component[]{getChangedPercentFormatPane(), null},
        };
    }
}
