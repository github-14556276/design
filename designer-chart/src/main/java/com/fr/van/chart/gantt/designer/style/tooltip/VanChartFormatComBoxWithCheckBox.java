package com.fr.van.chart.gantt.designer.style.tooltip;

import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.format.AttrTooltipDurationFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.IntervalTimeFormat;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;

public class VanChartFormatComBoxWithCheckBox extends JPanel {
    private static final String LABEL_TEXT = Toolkit.i18nText("Fine-Design_Chart_Duration_Time");

    private UICheckBox isSelectedBox;
    private UIComboBox formatComBox;

    private boolean isDirty;

    public VanChartFormatComBoxWithCheckBox() {
        this.setLayout(new BorderLayout());
        isSelectedBox = new UICheckBox(LABEL_TEXT);
        formatComBox = new UIComboBox(IntervalTimeFormat.getFormats());
        isSelectedBox.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                isDirty = true;
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        formatComBox.setPreferredSize(new Dimension(40, 20));

        if (showSelectBox()) {
            this.add(isSelectedBox, BorderLayout.CENTER);
        } else {
            this.add(new UILabel(LABEL_TEXT), BorderLayout.CENTER);
        }

        this.add(formatComBox, BorderLayout.EAST);
    }

    protected boolean showSelectBox() {
        return true;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }

    public void populate(AttrTooltipFormat tooltipFormat) {
        if (tooltipFormat instanceof AttrTooltipDurationFormat) {
            this.isSelectedBox.setSelected(tooltipFormat.isEnable());
            formatComBox.setSelectedItem(((AttrTooltipDurationFormat) tooltipFormat).getIntervalTimeFormat());
        }
    }

    public void update(AttrTooltipFormat tooltipFormat) {
        if (tooltipFormat instanceof AttrTooltipDurationFormat) {
            tooltipFormat.setEnable(isSelectedBox.isSelected());
            ((AttrTooltipDurationFormat) tooltipFormat).setIntervalTimeFormat((IntervalTimeFormat) formatComBox.getSelectedItem());
        }
    }

    public void updateFormatParams(Map<String, String> paramMap, String value) {
        String key = LABEL_TEXT;

        if (paramMap != null && !paramMap.containsKey(key)) {
            paramMap.put(key, value);
        }
    }
}
