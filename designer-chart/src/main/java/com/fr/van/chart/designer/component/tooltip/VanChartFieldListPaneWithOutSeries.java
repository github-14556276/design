package com.fr.van.chart.designer.component.tooltip;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipTargetValueFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.gauge.attr.GaugeValueTooltipContent;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartFieldListPaneWithOutSeries extends VanChartFieldListPane {

    private VanChartFieldButton targetValueButton;

    public VanChartFieldListPaneWithOutSeries(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void initDefaultFieldButton() {
        super.initDefaultFieldButton();

        VanChartFieldListener listener = getFieldListener();
        setValueButton(new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Value_Pointer"),
                new AttrTooltipValueFormat(), false, listener));
        targetValueButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Target_Value"),
                new AttrTooltipTargetValueFormat(), false, listener);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getCategoryNameButton());
        fieldPane.add(getValueButton());
        fieldPane.add(targetValueButton);
        fieldPane.add(getPercentButton());
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getCategoryNameButton());
        fieldButtonList.add(getValueButton());
        fieldButtonList.add(targetValueButton);
        fieldButtonList.add(getPercentButton());

        return fieldButtonList;
    }

    public void populateDefaultField(AttrTooltipContent tooltipContent) {
        super.populateDefaultField(tooltipContent);

        if (tooltipContent instanceof GaugeValueTooltipContent) {
            GaugeValueTooltipContent gaugeValueTooltipContent = (GaugeValueTooltipContent) tooltipContent;
            populateButtonFormat(targetValueButton, gaugeValueTooltipContent.getRichTextTargetValueFormat());
        }
    }

    public void updateDefaultField(AttrTooltipContent tooltipContent) {
        super.updateDefaultField(tooltipContent);
        GaugeValueTooltipContent gaugeValueTooltipContent = (GaugeValueTooltipContent) tooltipContent;
        updateButtonFormat(targetValueButton, gaugeValueTooltipContent.getRichTextTargetValueFormat());
    }
}
