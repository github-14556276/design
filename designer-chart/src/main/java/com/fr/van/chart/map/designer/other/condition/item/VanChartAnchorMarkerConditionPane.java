package com.fr.van.chart.map.designer.other.condition.item;

import com.fr.chart.base.DataSeriesCondition;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.van.chart.designer.other.condition.item.AbstractNormalMultiLineConditionPane;
import com.fr.van.chart.map.designer.style.series.VanChartMapAnchorMarkerPane;

import javax.swing.JPanel;

public class VanChartAnchorMarkerConditionPane extends AbstractNormalMultiLineConditionPane {
    private VanChartMapAnchorMarkerPane anchorMarkerPane;

    public VanChartAnchorMarkerConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane);
    }

    protected String getItemLabelString() {
        return Toolkit.i18nText("Fine-Design_Chart_Marker");
    }

    protected JPanel initContentPane() {
        anchorMarkerPane = new VanChartMapAnchorMarkerPane();
        return anchorMarkerPane;
    }

    public String nameForPopupMenuItem() {
        return Toolkit.i18nText("Fine-Design_Chart_Marker");
    }

    public void populate(DataSeriesCondition condition) {
        if (condition instanceof VanChartAttrMarker) {
            anchorMarkerPane.populateBean((VanChartAttrMarker) condition);
        }
    }

    public DataSeriesCondition update() {
        return anchorMarkerPane.updateBean();
    }
}

