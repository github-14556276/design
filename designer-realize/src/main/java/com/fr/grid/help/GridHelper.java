package com.fr.grid.help;

/**
 * 韩文unicode编码范围
 * http://www.ch2ko.com/hanguoyu/hanwen-unicode/
 *
 * 韩文音节：AC00-D7AF
 * Character.UnicodeBlock.HANGUL_SYLLABLES
 * https://www.unicode.org/charts/PDF/UAC00.pdf
 *
 *
 * 韩文兼容字母：3130-318F
 * Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO
 * https://www.unicode.org/charts/PDF/U3130.pdf
 *
 *
 * 韩文字母：1100-11FF
 * Character.UnicodeBlock.HANGUL_JAMO
 * https://www.unicode.org/charts/PDF/U1100.pdf
 *
 *
 * 韩文字母扩展A
 * Character.UnicodeBlock.HANGUL_JAMO_EXTENDED_A
 * https://www.unicode.org/charts/PDF/UA960.pdf
 *
 *
 * 韩文字母扩展B
 * haracter.UnicodeBlock.HANGUL_JAMO_EXTENDED_B
 * https://www.unicode.org/charts/PDF/UD7B0.pdf
 * 
 *
 * 使用java内部的韩文unicode集判断
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/8/7
 */
public class GridHelper {
    /**
     * 韩国货币符号
     */
    private static final char HEX_20A9 = 0x20A9;

    public static boolean isKoreanCharacter(char value) {
        Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(value);
        return unicodeBlock == Character.UnicodeBlock.HANGUL_JAMO
                || unicodeBlock == Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO
                || unicodeBlock == Character.UnicodeBlock.HANGUL_SYLLABLES
                || unicodeBlock == Character.UnicodeBlock.HANGUL_JAMO_EXTENDED_A
                || unicodeBlock == Character.UnicodeBlock.HANGUL_JAMO_EXTENDED_B
                || value == HEX_20A9;
    }
}
