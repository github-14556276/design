package com.fr.design.mainframe.app;

import com.fr.base.Parameter;
import com.fr.design.mainframe.App;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.JWorkBook;
import com.fr.design.worker.open.OpenResult;
import com.fr.design.worker.open.OpenWorker;
import com.fr.file.FILE;
import com.fr.log.FineLoggerFactory;
import com.fr.main.impl.WorkBook;
import com.fr.report.worksheet.WorkSheet;

import java.util.concurrent.Callable;

/**
 * Created by juhaoyu on 2018/6/27.
 */
abstract class AbstractWorkBookApp implements App<WorkBook> {


    @Override
    public int currentAPILevel() {

        return CURRENT_LEVEL;
    }

    @Override
    public JTemplate<WorkBook, ?> openTemplate(FILE tplFile) {
        JWorkBook emptyTemplate = new JWorkBook(new WorkBook(new WorkSheet()), tplFile);
        OpenWorker<OpenResult<WorkBook, Parameter[]>> worker = new OpenWorker<>(
                new Callable<OpenResult<WorkBook, Parameter[]>>() {
                    @Override
                    public OpenResult<WorkBook, Parameter[]> call() {
                        WorkBook workBook = asIOFile(tplFile);
                        DesignerAppUtils.dealWithTemplateIOError(tplFile.getPath());
                        return new OpenResult<>(workBook, workBook.getParameters());
                    }
                }, emptyTemplate);
        worker.addCallBack(new Callable<JTemplate<?, ?>>() {
            @Override
            public JTemplate<?, ?> call() throws Exception {
                OpenResult<WorkBook, Parameter[]> result = worker.getResult();
                return new JWorkBook(result.getBaseBook(), tplFile, result.getRef());
            }
        });
        worker.start(tplFile.getPath());
        FineLoggerFactory.getLogger().info(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Template_Opening_And_Waiting", tplFile.getName()) + "...");
        OpenResult<WorkBook, Parameter[]> result = worker.getResult();
        if (result != null) {
            return new JWorkBook(result.getBaseBook(), tplFile);
        }
        return emptyTemplate;
    }

    @Override
    public String mark4Provider() {

        return getClass().getName();
    }

    @Override
    public void process() {

    }

    @Override
    public void undo() {

    }
}