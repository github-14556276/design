package com.fr.design.mainframe.app;

import com.fr.base.FRContext;
import com.fr.json.JSONArray;
import com.fr.json.JSONException;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.context.PluginMarker;
import com.fr.stable.StringUtils;
import com.fr.value.AtomicClearableLazyValue;
import com.fr.workspace.WorkContext;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 获取远程插件情况，定时刷新
 *
 * @author vito
 * @version 10.0
 * Created by vito on 2021/6/1
 */
public class PluginRemote {

    private static final PluginRemote INSTANCE = new PluginRemote();
    private final AtomicClearableLazyValue<Map<String, PluginStatus>> lazyValue = new AtomicClearableLazyValue<Map<String, PluginStatus>>() {
        @NotNull
        @Override
        protected Map<String, PluginStatus> compute() {
            return getRemoteStatus();
        }
    };

    /**
     * 时间间隔两分钟
     */
    private static final int INTERVAL = 2;

    private ScheduledExecutorService executor;

    public static PluginRemote getInstance() {

        return INSTANCE;
    }

    /**
     * 获取远程插件状态
     * @return 远程插件状态
     */
    public Map<String, PluginStatus> getPluginRemoteStatus() {
        return lazyValue.getValue();
    }

    private Map<String, PluginStatus> getRemoteStatus() {

        JSONArray statusArray = FRContext.getCommonOperator().getPluginStatus();
        JSONObject status;
        String id;
        String version;
        boolean running;
        Map<String, PluginStatus> statusMap = new HashMap<>();
        for (int i = 0; i < statusArray.length(); i++) {
            try {
                status = statusArray.getJSONObject(i);
                id = status.getString("id");
                version = status.getString("version");
                running = status.getBoolean("running");
                if(StringUtils.isBlank(id)){
                    continue;
                }
                PluginStatus pluginStatus = PluginStatus.create(id, version, running);
                if (pluginStatus != null) {
                    statusMap.put(id, pluginStatus);
                }
            } catch (JSONException e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        return statusMap;
    }


    public void start() {
        // 远程环境下开始同步插件情况
        if (!WorkContext.getCurrent().isLocal()) {
            executor = Executors.newSingleThreadScheduledExecutor();
            executor.scheduleAtFixedRate(() -> {
                lazyValue.drop();
                lazyValue.getValue();
            }, 0, INTERVAL, TimeUnit.MINUTES);
        }
    }

    public void stop() {

        if (executor != null) {
            executor.shutdown();
            executor = null;
            lazyValue.drop();
        }
    }

    public static class PluginStatus {

        private final PluginMarker marker;

        private final boolean running;

        private PluginStatus(PluginMarker marker, boolean running) {

            this.marker = marker;
            this.running = running;
        }

        static PluginStatus create(String id, String version, boolean running) {

            PluginMarker marker = PluginMarker.create(id, version);
            if (marker != null && marker.valid()) {
                return new PluginStatus(marker, running);
            }
            return null;
        }

        public PluginMarker getMarker() {

            return marker;
        }

        public boolean isRunning() {

            return running;
        }
    }
}
