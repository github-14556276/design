package com.fr.design.mainframe;

import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.util.UIUtil;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/14
 */
public class TemplateLockedHandler {

    public static void generateTipAndRefresh() {
        UIUtil.invokeLaterIfNeeded(new Runnable() {
            @Override
            public void run() {
                FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(),
                                                  Toolkit.i18nText("Fine_Designer_Remote_Design_Locked_Message"),
                                                  Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                                                  FineJOptionPane.WARNING_MESSAGE);
                DesignerFrameFileDealerPane.getInstance().refresh();
            }
        });
    }

}
