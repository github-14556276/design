package com.fr.design.share.effect.source;


import com.fr.report.cell.DefaultTemplateCellElement;

public class CellSourceNode extends BaseSourceNode<DefaultTemplateCellElement> {

    public CellSourceNode(DefaultTemplateCellElement cellElement) {
        this(cellElement, null);
    }

    public CellSourceNode(DefaultTemplateCellElement cellElement, SourceNode parent) {
        this(cellElement, cellElement.toString(), parent);
    }

    public CellSourceNode(DefaultTemplateCellElement cellElement, String path, SourceNode parent) {
        this.setTarget(cellElement);
        this.setPath(path);
        this.setParent(parent);
    }
}
