package com.fr.design.share.ui.generate;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrame;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.design.mainframe.share.Bean.ComponentGenerateInfo;
import com.fr.design.mainframe.share.action.ShareUIAspect;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.exception.LackOfValueException;
import com.fr.design.mainframe.share.generate.ComponentGeneratorCenter;
import com.fr.design.mainframe.share.select.ComponentTransformerFactory;
import com.fr.design.mod.ContentObjectManager;
import com.fr.form.share.exception.NetWorkFailedException;
import com.fr.form.share.group.DefaultShareGroup;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.design.share.effect.EffectItemGroup;
import com.fr.design.share.utils.EffectItemUtils;
import com.fr.design.share.utils.ShareDialogUtils;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.config.ComponentReuseConfigManager;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.form.share.record.ShareWidgetInfoManager;
import com.fr.form.ui.Widget;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import java.util.HashSet;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 * created by Harrison on 2020/04/20
 **/
public class ShareGeneratePane extends BasicPane {

    private static final Dimension DIALOG_SIZE = new Dimension(670, 760);
    private static final Dimension DIALOG_NORMAL_SIZE = new Dimension(670, 610);
    private static final Border DIALOG_BORDER = BorderFactory.createEmptyBorder(0, 6, 4, 6);
    private static final double MAX_WIDTH = 500.0;
    private static final double MAX_HEIGHT = 260.0;

    private JPanel mainPane = null;
    private ShareMainPane uploadPane = null;
    private ShareMainPane simplePane = null;

    private UICheckBox uploadCheckbox = null;

    private BasicDialog dialog;

    private final JTemplate<?, ?> jt;
    //报表块控件
    private final Widget shareWidget;
    //报表块数据集相关的参数
    private final HashMap<String, Object> paraMap;

    private List<EffectItemGroup> effectItemGroups;

    private final ShareUIAspect aspect;

    private final boolean supportUpload = ComponentReuseConfigManager.getInstance().supportUploadReu();

    public ShareGeneratePane(JTemplate<?, ?> jt, Widget shareWidget, Rectangle rec, Image shareCover, HashMap<String, Object> paraMap, ShareUIAspect aspect) {
        this.jt = jt;
        this.shareWidget = shareWidget;
        effectItemGroups = EffectItemUtils.getEffectItemGroupsByWidget(shareWidget);
        this.paraMap = paraMap;
        this.aspect = aspect;

        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(DIALOG_BORDER);
        this.setPreferredSize(getDialogSize());

        JPanel guidePane = initGuidePane(shareCover, rec);
        this.add(guidePane, BorderLayout.CENTER);
    }

    private JPanel initGuidePane(Image shareCover, Rectangle rec) {

        JPanel pane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        this.mainPane = FRGUIPaneFactory.createCardLayout_S_Pane();
        boolean needContentTip = needContentTip();
        this.simplePane = new ShareMainPane(shareCover, rec, false, effectItemGroups, needContentTip);
        this.uploadPane = new ShareMainPane(shareCover, rec, true, effectItemGroups, needContentTip);
        //暂时换一下，目前不用上传
        this.mainPane.add(simplePane, ShareUIUtils.convertStateChange(ItemEvent.DESELECTED));
        this.mainPane.add(uploadPane, ShareUIUtils.convertStateChange(ItemEvent.SELECTED));

        simplePane.getNameField().getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void changedUpdate(DocumentEvent e) {
                validInput();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                validInput();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                validInput();
            }
        });

        uploadPane.getNameField().getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void changedUpdate(DocumentEvent e) {
                validInput();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                validInput();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                validInput();
            }
        });

        //后面创建，因为需要依赖 mainPane 的创建
        JPanel uploadCheckBox = createUploadCheckBox();
        if (supportUpload) {
            pane.add(uploadCheckBox, BorderLayout.NORTH);
        }
        pane.add(mainPane, BorderLayout.CENTER);
        return pane;
    }

    private void validInput() {
        String userInput = getSelectMainPane().getNameField().getText().trim();

        if (StringUtils.isEmpty(userInput)) {
            dialog.setButtonEnabled(false);
        } else {
            dialog.setButtonEnabled(true);
        }
    }


    private boolean needContentTip() {
        long start = System.currentTimeMillis();
        XCreator xCreator = ComponentTransformerFactory.getInstance().transform(WidgetPropertyPane.getInstance().getEditingFormDesigner().getSelectionModel().getSelection()).getMiddle();
        Set<XCreator> xCreators = new HashSet<>();
        xCreator.traversalNameRelatedXCreators(xCreators);
        Set<String> nameSet = new HashSet<>();
        for (XCreator creator : xCreators) {
            nameSet.add(creator.toData().getWidgetName());
        }
        boolean result = ContentObjectManager.getInstance().needContentTip(shareWidget, nameSet);
        FineLoggerFactory.getLogger().debug("needContentTip spend {} ms", (System.currentTimeMillis() - start));
        return result;
    }

    private JPanel createUploadCheckBox() {

        JPanel panel = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane();
        String label = Toolkit.i18nText("Fine-Design_Share_Upload_Meanwhile");
        this.uploadCheckbox = new UICheckBox(label);
        uploadCheckbox.addItemListener(e -> {
            int stateChange = e.getStateChange();
            CardLayout cl = (CardLayout) (mainPane.getLayout());
            cl.show(mainPane, ShareUIUtils.convertStateChange(stateChange));
            repaint();
        });
        uploadCheckbox.setSelected(false);

        UILabel tipsLabel = ShareUIUtils.createTipsLabel(Toolkit.i18nText("Fine-Design_Share_Upload_Meanwhile_Tips"));
        panel.add(uploadCheckbox);
        panel.add(tipsLabel);
        panel.setPreferredSize(new Dimension(585, 30));
        return panel;
    }

    /* show 相关 */
    @Override
    public void show() {
        final DesignerFrame designerFrame = DesignerContext.getDesignerFrame();
        dialog = this.showWindowWithCustomSize(designerFrame, null, getDialogSize());
        dialog.setButtonEnabled(false);
        ShareDialogUtils.getInstance().setShareDialog(dialog);
        dialog.addDialogActionListener(new DialogActionAdapter() {

            @Override
            public void doOk() {
                ShareMainPane mainPane = getSelectMainPane();
                if (doCheck(mainPane)) {
                    dialog.setVisible(false);
                    doOk0(mainPane);
                } else {
                    // 抛出，从而防止页面关闭
                    throw new LackOfValueException();
                }
            }
        });
        dialog.setVisible(true);
    }

    private ShareMainPane getSelectMainPane() {

        ShareMainPane mainPane;
        if (uploadCheckbox.isSelected()) {
            mainPane = uploadPane;
        } else {
            mainPane = simplePane;
        }
        return mainPane;
    }

    private boolean doCheck(ShareMainPane mainPane) {

        return mainPane.check();
    }

    private void doOk0(final ShareMainPane mainPane) {
        for(EffectItemGroup effectItemGroup : effectItemGroups) {
            effectItemGroup.save();
        }
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                DefaultSharableWidget provider = mainPane.generate();
                Group selectGroup = mainPane.getSelectGroup();
                boolean success = false;
                try {
                    success = generateModule(provider);
                    if (success) {
                        //先读文件然后移动到指定分组
                        Group defaultGroup = DefaultShareGroupManager.getInstance().getGroup(DefaultShareGroup.GROUP_NAME);
                        if (defaultGroup != null) {
                            defaultGroup.loadModule(provider.getFileName());
                            defaultGroup.moveModule(selectGroup, Stream.of(provider.getId()).collect(Collectors.toList()));
                        }
                        collect(provider);
                        ShareWidgetInfoManager.getInstance().addCompCreateInfo(provider);
                    }
                } catch (NetWorkFailedException exception) {
                    FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), ShareUIUtils.formatWidthString(Toolkit.i18nText("Fine-Design_Share_NetWorkError"), 200),
                            Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
                    return null;
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                } finally {
                    //成功失败，都要处理
                    aspect.afterOk();
                }
                if (!success) {
                    FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), Toolkit.i18nText("Fine-Design_Share_Module_Failed"),
                            Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
                }
                return null;
            }
        };
        worker.execute();
    }

    private void collect(DefaultSharableWidget provider) {

        ComponentCollector.getInstance().collectGenerateCmpNumber();
        ComponentCollector.getInstance().collectGenerateCmpRecord(provider);
        if (uploadCheckbox.isSelected()) {
            ComponentCollector.getInstance().collectUploadCmpNumber();
        }
    }

    private boolean generateModule(DefaultSharableWidget info) throws Exception {

        DefaultSharableWidget transformInfo = transform(info);
        ComponentGenerateInfo generateInfo = new ComponentGenerateInfo(uploadCheckbox.isSelected(), jt, paraMap, shareWidget, transformInfo);
        ComponentGeneratorCenter center = new ComponentGeneratorCenter(generateInfo);
        return center.generate();
    }

    private DefaultSharableWidget transform(DefaultSharableWidget info) {
        confineSize(info);

        //先屏蔽
        //if (shareWidget instanceof AbstractBorderStyleWidget) {
        //    AbstractBorderStyleWidget styleWidget = (AbstractBorderStyleWidget) shareWidget;
        //    ExtendSharableAttrMark attrMark = styleWidget.getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
        //    if (attrMark != null) {
        //        String shareId = attrMark.getShareId();
        //        info.setId(shareId);
        //    }
        //}
        return info;
    }


    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Share_Module_Msg");
    }

    private Dimension getDialogSize() {
        return effectItemGroups.size() > 0 ? DIALOG_SIZE : DIALOG_NORMAL_SIZE;
    }


    private void confineSize(DefaultSharableWidget info) {
        double width = info.getWidth();
        double height = info.getHeight();
        if (width > 0 && height > 0 && (width > MAX_WIDTH || height > MAX_HEIGHT)) {
            double aspectRatio = width / height;
            if (width / height > MAX_WIDTH / MAX_HEIGHT) {
                info.setWidth((int) MAX_WIDTH);
                info.setHeight((int) (MAX_WIDTH / aspectRatio));
            } else {
                info.setHeight((int) MAX_HEIGHT);
                info.setWidth((int) (MAX_HEIGHT * aspectRatio));
            }
        }
    }
}
