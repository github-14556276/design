package com.fr.design.share.ui.effect;

import com.fr.base.BaseUtils;
import com.fr.base.DynamicUnitList;
import com.fr.base.ScreenResolution;
import com.fr.base.chart.BaseChartCollection;
import com.fr.base.chart.chartdata.CallbackEvent;
import com.fr.common.inputevent.InputEventBaseOnOS;
import com.fr.design.cell.bar.DynamicScrollBar;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.ElementCasePane;
import com.fr.design.mainframe.FormSelection;
import com.fr.design.mainframe.JForm;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.form.FormElementCaseDesigner;
import com.fr.design.mainframe.share.ui.base.ChartIcon;
import com.fr.design.utils.DesignUtils;
import com.fr.form.main.Form;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.Widget;
import com.fr.general.IOUtils;
import com.fr.grid.Grid;
import com.fr.grid.GridUtils;
import com.fr.grid.selection.CellSelection;
import com.fr.report.ReportHelper;
import com.fr.report.worksheet.FormElementCase;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

public class PreviewPane extends JPanel implements CallbackEvent{
    public static final Cursor SCALE_CURSOR = Toolkit.getDefaultToolkit().createCustomCursor(
            IOUtils.readImage("com/fr/base/images/share/scale.png"),
            new Point(16, 16), "ScaleCursor");
    private static final Icon PLUS_ICON = BaseUtils.readIcon("com/fr/base/images/share/add.png");
    private static final Icon PLUS_CLICK_ICON = BaseUtils.readIcon("com/fr/base/images/share/add_click.png");
    private static final Icon PLUS_HOVER_ICON = BaseUtils.readIcon("com/fr/base/images/share/add_hover.png");
    private static final Icon PLUS_DISABLED_ICON = BaseUtils.readIcon("com/fr/base/images/share/add_disabled.png");
    private static final Icon MINUS_CLICK_ICON = BaseUtils.readIcon("com/fr/base/images/share/minus_click.png");
    private static final Icon MINUS_HOVER_ICON = BaseUtils.readIcon("com/fr/base/images/share/minus_hover.png");
    private static final Icon MINUS_ICON = BaseUtils.readIcon("com/fr/base/images/share/minus.png");
    private static final Icon MINUS_DISABLED_ICON = BaseUtils.readIcon("com/fr/base/images/share/minus_disabled.png");
    private static final Dimension BUTTON_SIZE = new Dimension(20, 20);
    private static final Dimension PREVIEW_SIZE = new Dimension(373, 310);
    private static final int MAX = 400;
    private static final int HUND = 100;
    private static final int MIN = 10;
    private static final int DIR = 10;
    private static final double MIN_TIME = 0.4;

    private Widget widget;
    private UIButton plusButton;
    private UIButton minusButton;
    private JPanel preview;
    private int scaleValue = 100;
    private ElementCasePane elementCasePane;
    private Image previewImage;
    private int resolution;
    private Dimension previewSize;
    private boolean enabled;

    public PreviewPane(Widget widget) {
        this(widget, PREVIEW_SIZE, true);
    }

    public PreviewPane(Widget widget, Dimension previewSize, boolean enabled) {
        this.widget = widget;

        this.enabled = enabled;
        this.previewSize = previewSize;
        this.resolution = ScreenResolution.getScreenResolution();
        initComponent();
    }


    private void initComponent() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());

        if (enabled) {
            this.add(createTopToolbarPane(), BorderLayout.NORTH);
        } else {
            this.add(createRightToolbarPane(), BorderLayout.EAST);
        }
        this.add(createViewPane(), BorderLayout.CENTER);

    }

    private JPanel createTopToolbarPane() {
        JPanel toolbarPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        FlowLayout layout = new FlowLayout(FlowLayout.RIGHT, 10 ,0);
        toolbarPane.setLayout(layout);
        toolbarPane.setPreferredSize(new Dimension(previewSize.width, 22));
        addScaleButton(toolbarPane);
        return toolbarPane;
    }

    private JPanel createRightToolbarPane() {
        JPanel toolbarPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT, 10 ,0);
        toolbarPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));
        toolbarPane.setLayout(layout);
        addScaleButton(toolbarPane);
        return toolbarPane;
    }

    private void addScaleButton(JPanel toolbarPane) {
        if (isElementCaseEditor()) {
            plusButton = new UIButton(PLUS_ICON);
            plusButton.setDisabledIcon(PLUS_DISABLED_ICON);
            plusButton.setPressedIcon(PLUS_CLICK_ICON);
            plusButton.setRolloverIcon(PLUS_HOVER_ICON);
            plusButton.setPreferredSize(BUTTON_SIZE);
            plusButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addScale(10);
                }
            });

            minusButton = new UIButton(MINUS_ICON);
            minusButton.setDisabledIcon(MINUS_DISABLED_ICON);
            minusButton.setPressedIcon(MINUS_CLICK_ICON);
            minusButton.setRolloverIcon(MINUS_HOVER_ICON);
            minusButton.setPreferredSize(BUTTON_SIZE);
            minusButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addScale(-10);
                }
            });

            toolbarPane.add(minusButton);
            toolbarPane.add(plusButton);
        }
    }

    private JPanel createViewPane() {
        preview = FRGUIPaneFactory.createBorderLayout_S_Pane();
        preview.setBorder(BorderFactory.createEmptyBorder(5, 0 , 0 , 0));
        preview.setPreferredSize(this.previewSize);
        preview.setSize(previewSize);

        if (isElementCaseEditor()) {
            elementCasePreview();
        } else if (isChartEditor()) {
            chartPreview();
        } else {
            widgetPreview();
        }

        return preview;
    }

    private void elementCasePreview() {
        ElementCaseEditor editor = (ElementCaseEditor) widget;
        FormElementCase elementCase = (FormElementCase) editor.getElementCase();
        FormElementCaseDesigner designer = new FormElementCaseDesigner(elementCase, new Form());
        designer.setForeground(Color.BLACK);
        designer.setBackground(new Color(245, 245, 247));
        designer.setFont(DesignUtils.getDefaultGUIFont().applySize(12));
        elementCasePane = designer.getEditingElementCasePane();

        elementCasePane.setPreferredSize(new Dimension(previewSize.width, getInnerPreviewSize().height));
        elementCasePane.setSelection(new CellSelection(0, 0, 0, 0));

        Grid grid = elementCasePane.getGrid();
        grid.setEditable(false);
        grid.removeMouseWheelListener(grid.getGridMouseAdapter());
        grid.removeMouseMotionListener(grid.getGridMouseAdapter());
        grid.removeMouseListener(grid.getGridMouseAdapter());

        grid.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (!InputEventBaseOnOS.isControlDown(e)) {
                    ElementCasePane reportPane = grid.getElementCasePane();
                    if (reportPane.isVerticalScrollBarVisible()) {
                        JScrollBar scrollBar = reportPane.getVerticalScrollBar();
                        int maxValue = scrollBar.getModel().getMaximum();
                        int newValue = reportPane.getVerticalScrollBar().getValue() + e.getWheelRotation();
                        int extendValue =  GridUtils.getExtentValue(newValue, elementCase.getRowHeightList_DEC(), grid.getHeight(), getResolution());
                        if (extendValue <= maxValue) {
                            reportPane.getVerticalScrollBar().setValue(newValue);
                        }
                    }
                } else {
                    int dir = e.getWheelRotation();
                    addScale( - dir * DIR);
                    grid.setCursor(SCALE_CURSOR);
                }
            }
        });

        grid.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                grid.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });

        elementCasePane.getGridColumn().setEnabled(false);
        elementCasePane.getGridRow().setEnabled(false);

        preview.add(designer, BorderLayout.CENTER);
        preview.add(elementCasePane.getHorizontalScrollBar(), BorderLayout.SOUTH);
    }

    private Dimension getInnerPreviewSize() {
        if (isElementCaseEditor()) {
            int width = previewSize.width - (enabled ? elementCasePane.getVerticalScrollBar().getPreferredSize().width : 0);
            int height = previewSize.height - (enabled ? elementCasePane.getHorizontalScrollBar().getPreferredSize().height : 0);
            return new Dimension(width, height);
        }
        return previewSize;
    }

    private void chartPreview() {
        ChartEditor editor = (ChartEditor) widget;
        BaseChartCollection chartCollection = editor.getChartCollection();
        ChartIcon chartIcon = new ChartIcon(chartCollection, previewSize.width, previewSize.height);
        chartIcon.registerCallBackEvent(PreviewPane.this);
        UILabel label = new UILabel(chartIcon);
        preview.add(label, BorderLayout.CENTER);
    }

    private void widgetPreview(){
        JPanel widgetView = new JPanel(FRGUIPaneFactory.createBorderLayout());
        widgetView.setPreferredSize(previewSize);
        widgetView.setBackground(Color.WHITE);

        XCreator xCreator = getWidgetXCreator();
        if (xCreator != null) {
            previewImage = new BufferedImage(xCreator.getWidth(), xCreator.getHeight(), BufferedImage.TYPE_INT_RGB);
            xCreator.paint(previewImage.getGraphics());
            Icon icon = new ImageIcon(previewImage);
            UILabel label = new UILabel(icon);
            label.setAlignmentY(SwingConstants.CENTER);
            widgetView.add(label, BorderLayout.CENTER);
        }

        preview.add(widgetView, BorderLayout.CENTER);
    }

    private XCreator getWidgetXCreator() {
        JTemplate<?, ?> jt = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        FormSelection selection = ((JForm) jt).getFormDesign().getSelectionModel().getSelection();
        XCreator[] xCreators = selection.getSelectedCreators();
        return getWidgetXCreator(xCreators, widget);
    }
    private XCreator getWidgetXCreator(Component[] components, Widget widget) {
        for (Component component : components) {
            if (component instanceof XCreator) {
                XCreator xCreator = (XCreator) component;
                if (xCreator.toData().getWidgetName() == widget.getWidgetName()) {
                    return xCreator;
                }
                XCreator subXCreator = getWidgetXCreator(xCreator.getComponents(), widget);
                if (subXCreator != null) {
                    return subXCreator;
                }
            }
        }
        return null;
    }

    @Override
    public void callback() {
        this.repaint();
    }

    private void setScale(int resolution) {
        //网格线
        if (resolution < ScreenResolution.getScreenResolution() * MIN_TIME) {
            elementCasePane.getGrid().setShowGridLine(false);
        } else {
            elementCasePane.getGrid().setShowGridLine(true);
        }
        elementCasePane.setResolution(resolution);
        elementCasePane.getGrid().getGridMouseAdapter().setResolution(resolution);
        elementCasePane.getGrid().setResolution(resolution);
        //更新Grid
        Grid grid = elementCasePane.getGrid();
        DynamicUnitList rowHeightList = ReportHelper.getRowHeightList(elementCasePane.getEditingElementCase());
        DynamicUnitList columnWidthList = ReportHelper.getColumnWidthList(elementCasePane.getEditingElementCase());
        grid.setVerticalExtent(GridUtils.getExtentValue(0, rowHeightList, grid.getHeight(), resolution));
        grid.setHorizontalExtent(GridUtils.getExtentValue(0, columnWidthList, grid.getWidth(), resolution));
        elementCasePane.getGrid().updateUI();
        //更新Column和Row
        ((DynamicScrollBar) elementCasePane.getVerticalScrollBar()).setDpi(resolution);
        ((DynamicScrollBar) elementCasePane.getHorizontalScrollBar()).setDpi(resolution);
        elementCasePane.getGridColumn().setResolution(resolution);
        elementCasePane.getGridColumn().updateUI();
        elementCasePane.getGridRow().setResolution(resolution);
        elementCasePane.getGridRow().updateUI();
        elementCasePane.repaint();
    }

    private void addScale(int step) {
        scaleValue += step;
        scaleValue = fixedScale(scaleValue);
        plusButton.setEnabled(scaleValue < MAX);
        minusButton.setEnabled(scaleValue > MIN);
        setScale(getResolution());
    }

    private int fixedScale(int value) {
        if (value < MIN) {
            value = MIN;
        }
        if (value > MAX) {
            value = MAX;
        }
        return value;
    }

    private int getResolution() {
        return (int) (resolution * scaleValue / HUND);
    }

    public boolean isElementCaseEditor() {
        return widget instanceof ElementCaseEditor;
    }

    public boolean isChartEditor() {
        return widget instanceof ChartEditor;
    }

    public ElementCasePane getElementCasePane() {
        return elementCasePane;
    }
}
