package com.fr.design.share.ui.generate;

import com.fr.base.background.ImageBackground;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.ui.base.ImageBackgroundPane;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.ColorUIResource;
import java.awt.BorderLayout;
import java.awt.Image;

/**
 * 上传封面的面板
 * Coder: zack
 * Date: 2016/10/11
 * Time: 21:36
 */
public class CoverUploadPane extends BasicBeanPane<Image> {
    private static final ColorUIResource BORDER_COLOR = new ColorUIResource(168, 172, 180);
    private static final Border DIALOG_BORDER = BorderFactory.createEmptyBorder(0, 6, 4, 6);
    private ImageBackgroundPane backgroundPane;

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Share_Select_Upload_Cover");
    }

    public CoverUploadPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        backgroundPane = new ImageBackgroundPane();
        backgroundPane.setBorder(new MatteBorder(1, 1, 1, 1, BORDER_COLOR));
        this.add(backgroundPane, BorderLayout.CENTER);
        this.setBorder(DIALOG_BORDER);
    }

    @Override
    public void populateBean(Image ob) {
        if (ob != null) {
            backgroundPane.populate(new ImageBackground(ob));
        }
    }

    @Override
    public Image updateBean() {
        ImageBackground imageBackground = new ImageBackground();
        try {
            imageBackground = (ImageBackground) backgroundPane.update();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        //返回原图
        return imageBackground.getImage();
    }
}
