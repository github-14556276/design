package com.fr.design.share.ui.generate;

import com.fr.design.DesignerEnvManager;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.extra.LoginWebBridge;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.icombocheckbox.UIComboCheckBox;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.login.DesignerLoginHelper;
import com.fr.design.login.DesignerLoginSource;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.reuse.ComponentReuseNotificationInfo;
import com.fr.design.mainframe.share.constants.ComponentType;
import com.fr.design.mainframe.share.constants.ComponentTypes;
import com.fr.design.mainframe.share.constants.DisplayDevice;
import com.fr.design.mainframe.share.constants.StyleTheme;
import com.fr.design.mainframe.share.ui.base.DictionaryComboCheckBox;
import com.fr.design.mainframe.share.ui.base.LeftWordsTextArea;
import com.fr.design.mainframe.share.ui.base.PlaceholderTextArea;
import com.fr.design.mainframe.share.ui.base.ui.PlaceHolderUI;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.design.share.effect.EffectItemGroup;
import com.fr.design.share.utils.ShareDialogUtils;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.bean.StyleThemeBean;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.json.JSON;
import com.fr.json.JSONArray;
import com.fr.json.JSONFactory;
import com.fr.plugin.context.PluginContext;
import com.fr.plugin.manage.PluginFilter;
import com.fr.plugin.manage.PluginManager;
import com.fr.stable.ArrayUtils;
import com.fr.stable.ProductConstants;
import com.fr.stable.StringUtils;
import com.fr.stable.collections.combination.Pair;
import com.fr.stable.pinyin.PinyinHelper;
import java.util.HashMap;
import org.jetbrains.annotations.NotNull;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.ItemSelectable;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

/**
 * created by Harrison on 2020/04/20
 **/
public class ShareMainPane extends JPanel {

    private static final int COLUMN_ITEM_SIZE = 60;
    private static final int COLUMN_FIELD_WIDTH = 555;
    private static final int TEXT_FIELD_WIDTH = 160;
    private static final int TEXT_FIELD_HEIGHT = 21;
    private static final int NAME_MAX_LENGTH = 50;
    private static final int GROUP_WIDTH = 160;
    private static final int COMBO_WIDTH = 90;
    private static final int COMBO_WIDTH_PLUS = 150;
    private static final int COMBO_HEIGHT = 20;
    private static final int BTN_WIDTH = 70;
    private static final int BTN_HEIGHT = 20;
    private static final int BASEPANE_VERTICAL_GAP = 2;

    private UIScrollPane mainPane = null;

    private UICheckBox pluginCheckBox = new UICheckBox(Toolkit.i18nText("Fine-Design_Share_Make_Relate_To_Plugin"));
    private PriceExpectPane priceExpectPane = null;

    private UIComboBox parentClassify = null;
    private UIComboBox childClassify = null;

    private UIComboBox localGroup = null;

    private UILabel loginLabel = ShareUIUtils.createCenterRightUILabel(StringUtils.EMPTY);

    private UICheckBox pc = new UICheckBox(Toolkit.i18nText("Fine-Design_Share_PC_Device"));
    private UICheckBox mobile = new UICheckBox(Toolkit.i18nText("Fine-Design_Share_Mobile_Device"));

    private UIComboCheckBox pluginComboCheckBox = null;

    private UIComboBox styleComboBox = null;

    private UITextField nameField = new UITextField();

    private PlaceholderTextArea content = new LeftWordsTextArea();

    private PlaceholderTextArea help = new PlaceholderTextArea();
    private UILabel cover;
    private UIButton uploadBtn;
    //控件的缩略图
    private Image shareCover;
    private Rectangle rec;
    private boolean upload;

    private List<EffectItemGroup> effectItemGroups;
    private final boolean needContentTip;

    public ShareMainPane(Image shareCover, Rectangle rec, boolean upload, List<EffectItemGroup> effectItemGroups, boolean needContentTip) {

        this.shareCover = shareCover;
        this.rec = rec;
        this.upload = upload;
        this.effectItemGroups = effectItemGroups;
        this.needContentTip = needContentTip;

        initCover(shareCover);
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder());

        this.mainPane = createMainPane(upload);
        this.add(this.mainPane, BorderLayout.CENTER);
    }

    private void initCover(Image shareCover) {
        this.cover = new UILabel(getShowIcon(shareCover));
        MatteBorder matteBorder = BorderFactory.createMatteBorder(3, 3, 3, 3, Color.WHITE);
        this.cover.setBorder(matteBorder);
        this.cover.setHorizontalAlignment(JLabel.CENTER);
    }

    private Icon getShowIcon(Image image) {
        Image scaleImage = image.getScaledInstance(ShareComponentConstants.SHARE_THUMB_WIDTH, ShareComponentConstants.SHARE_THUMB_HEIGHT, Image.SCALE_SMOOTH);
        return new ImageIcon(scaleImage);
    }


    @NotNull
    private UIScrollPane createMainPane(boolean upload) {
        JPanel mainPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UIScrollPane scrollPane = new UIScrollPane(mainPane);
        JPanel overviewPane = createOverviewPane(upload);
        JPanel basePane = createBasePane(upload);
        mainPane.add(basePane, BorderLayout.NORTH);
        mainPane.add(overviewPane, BorderLayout.CENTER);
        return scrollPane;
    }

    private JPanel createOverviewPane(boolean upload) {
        JPanel componentPane = createComponentOverviewPane(upload);
        String title = Toolkit.i18nText("Fine-Design_Share_Overview");
        JPanel overviewPane = FRGUIPaneFactory.createTopVerticalTitledBorderPane(title);
        if (needContentTip) {
            UILabel tipsLabel = ShareUIUtils.createTipsLabel(Toolkit.i18nText("Fine-Design_Share_Generate_Content_Tip"));
            tipsLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
            JPanel panel = new JPanel(FRGUIPaneFactory.createBorderLayout());
            panel.add(tipsLabel);
            overviewPane.add(panel, BorderLayout.NORTH);
        }
        overviewPane.add(componentPane, BorderLayout.CENTER);

        return overviewPane;

    }

    private JPanel createBasePane(boolean upload) {
        // 提示
        UILabel tipsLabel = ShareUIUtils.createTipsLabel(Toolkit.i18nText("Fine-Design_Share_Generate_Help_Tips"));
        tipsLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        // 组件名称
        UILabel nameLabel = ShareUIUtils.createTopRightUILabel(Toolkit.i18nText("Fine-Design_Share_Name"));
        JPanel symbolTextField = createNameFiledPane();
        //显示封面
        UILabel coverLabel = ShareUIUtils.createTopRightUILabel(Toolkit.i18nText("Fine-Design_Share_Show_Cover"));
        JPanel coverImagePane = getCoverImagePane();
        //制作者
        UILabel vendorLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Share_Vendor"));
        JPanel vendorPane = createVendorPane();
        //适用终端
        UILabel deviceLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Device"));
        JPanel devicePane = createDevicePane();
        //组件分类
        UILabel classifyLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Classify"));
        JPanel classifyPane = createClassifyPane();

        //样式风格
        UILabel styleThemeLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Style_Theme"));
        JPanel styleThemePane = createStyleThemePane();
        //本地分组
        UILabel localGroupLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Local_Group"));
        JPanel localGroupPane = createLocalGroupPane();
        //使用插件
        UILabel pluginLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Use_Plugin"));
        JPanel pluginPane = createPluginPane();
        //期望价格
        UILabel priceLabel = ShareUIUtils.createCenterRightUILabel(Toolkit.i18nText("Fine-Design_Share_Expect_Price"));
        JPanel pricePane = createPricePane();

        double p = TableLayout.PREFERRED;

        JPanel innerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel infoPane;
        if (upload) {
            Component[][] components = new Component[][]{
                    new Component[]{nameLabel, symbolTextField},
                    new Component[]{coverLabel, coverImagePane},
                    new Component[]{vendorLabel, vendorPane},
                    new Component[]{deviceLabel, devicePane},
                    new Component[]{classifyLabel, classifyPane},
                    new Component[]{styleThemeLabel, styleThemePane},
                    new Component[]{pluginLabel, pluginPane},
                    new Component[]{priceLabel, pricePane},
            };
            double[] rowSize = {p, p, p, p, p, p, p, p};
            double[] columnSize = {COLUMN_ITEM_SIZE, p};
            int[][] rowCount = {{1, 1}, {1, 1}, {1, 1}, {1, 1}, {1, 1}, {1, 1},{1, 1}, {1, 1}};
            infoPane = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, LayoutConstants.HGAP_SMALL, BASEPANE_VERTICAL_GAP);
        } else {
            Component[][] components = new Component[][]{
                    new Component[]{nameLabel, symbolTextField},
                    new Component[]{coverLabel, coverImagePane},
                    new Component[]{deviceLabel, devicePane},
                    new Component[]{classifyLabel, classifyPane},
                    new Component[]{localGroupLabel, localGroupPane}
            };
            double[] rowSize = {p, p, p, p, p};
            double[] columnSize = {COLUMN_ITEM_SIZE, p};
            int[][] rowCount = {{1, 1}, {1, 1}, {1, 1},{1, 1}, {1, 1}};
            infoPane = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, LayoutConstants.HGAP_SMALL, BASEPANE_VERTICAL_GAP);
        }

        String title = Toolkit.i18nText("Fine-Design_Share_Base_Info");
        JPanel overviewPane = FRGUIPaneFactory.createTitledBorderPane(title);

        innerPane.add(tipsLabel, BorderLayout.NORTH);
        innerPane.add(infoPane, BorderLayout.CENTER);
        overviewPane.add(innerPane, BorderLayout.CENTER);

        return overviewPane;
    }

    private JPanel createEffectPane(List<EffectItemGroup> effectItemGroups) {
        return new EffectPane(effectItemGroups);
    }

    private JPanel createPricePane() {

        JPanel pane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane();
        this.priceExpectPane = new PriceExpectPane();
        pane.add(priceExpectPane);
        return pane;
    }

    private JPanel createStyleThemePane() {
        JPanel pane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane();
        PlaceHolderUI<UIComboBox> placeHolderUI = new PlaceHolderUI<UIComboBox>(
                Toolkit.i18nText("Fine-Design_Share_Style_Theme_Place_Holder")) {
            @Override
            protected boolean validate(UIComboBox uiComboBox) {
                return styleComboBox.getSelectedItem() != null;
            }
        };
        this.styleComboBox = ShareUIUtils.wrapUI(placeHolderUI, new UIComboBox());
        this.styleComboBox.refreshBoxItems(StyleTheme.getStyleThemeTypeInfo());
        styleComboBox.setPreferredSize(new Dimension(COMBO_WIDTH, COMBO_HEIGHT));
        pane.add(styleComboBox);
        return pane;
    }

    private JPanel createPluginPane() {

        JPanel pane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane();

        List<PluginContext> contexts = PluginManager.getContexts(new PluginFilter() {
            @Override
            public boolean accept(PluginContext pluginContext) {

                return pluginContext.isRunning();
            }
        });
        String[] displays = new String[contexts.size()];
        String[] values = new String[contexts.size()];
        Pair<String, String>[] pairs = new Pair[contexts.size()];
        for (int i = 0; i < contexts.size(); i++) {
            PluginContext context = contexts.get(i);
            String display = context.getName();
            String value = context.getID() + "-" + context.getVersion();
            pairs[i] = new Pair<>(display, value);
        }
        //按照顺序展示
        //转换一下中文为拼音
        Arrays.sort(pairs, new Comparator<Pair<String, String>>() {
            @Override
            public int compare(Pair<String, String> pair1, Pair<String, String> pair2) {
                String o1 = pair1.getFirst();
                String o2 = pair2.getFirst();
                String pinyin1 = PinyinHelper.getShortPinyin(o1);
                String pinyin2 = PinyinHelper.getShortPinyin(o2);
                return pinyin1.compareToIgnoreCase(pinyin2);
            }
        });
        for (int i = 0; i < pairs.length; i++) {
            displays[i] = pairs[i].getFirst();
            values[i] = pairs[i].getSecond();
        }
        this.pluginComboCheckBox = new DictionaryComboCheckBox(values, displays, "Fine-Design_Share_Select_Plugin");

        pluginComboCheckBox.setPopupMaxDisplayNumber(10);
        pluginComboCheckBox.setVisible(false);

        pluginCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int stateChange = e.getStateChange();
                pluginComboCheckBox.setVisible(stateChange == ItemEvent.SELECTED);
                pluginComboCheckBox.repaint();
            }
        });

        pane.add(pluginCheckBox);
        pane.add(pluginComboCheckBox);
        return pane;
    }

    private JPanel createClassifyPane() {

        JPanel pane = new JPanel();
        //距离远一点
        pane.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 5));
        this.parentClassify = new UIComboBox(ComponentTypes.values());
        this.parentClassify.setPreferredSize(new Dimension(COMBO_WIDTH, COMBO_HEIGHT));

        List<String> children = ComponentTypes.CHART.children(-1);
        final String placeHolderText = Toolkit.i18nText("Fine-Design_Share_Select_Type");
        PlaceHolderUI<UIComboBox> placeHolderUI = new PlaceHolderUI<UIComboBox>(placeHolderText) {
            @Override
            protected boolean validate(UIComboBox comboBox) {
                return childClassify.getSelectedItem() != null;
            }
        };
        this.childClassify = ShareUIUtils.wrapUI(placeHolderUI, new UIComboBox());
        this.childClassify.refreshBoxItems(children);
        this.childClassify.setPreferredSize(new Dimension(COMBO_WIDTH_PLUS, COMBO_HEIGHT));
        this.childClassify.setBorder(BorderFactory.createEmptyBorder());

        parentClassify.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {

                ItemSelectable itemSelectable = e.getItemSelectable();
                ComponentType selectedItem = (ComponentType) ((UIComboBox) itemSelectable).getSelectedItem();
                if (selectedItem != null) {
                    int device = displayDevice();
                    List<String> children = selectedItem.children(device);
                    childClassify.clearBoxItems();
                    childClassify.refreshBoxItems(children);
                }
            }
        });

        //设置默认值
        this.parentClassify.setSelectedItem(ComponentTypes.CHART);

        pane.add(parentClassify);
        pane.add(childClassify);
        return pane;
    }

    private JPanel createLocalGroupPane() {

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 2, 2));
        this.localGroup = new UIComboBox(DefaultShareGroupManager.getInstance().getAllGroup());
        this.localGroup.setPreferredSize(new Dimension(GROUP_WIDTH, COMBO_HEIGHT));

        panel.add(localGroup);
        return panel;
    }

    private JPanel createDevicePane() {

        JPanel pane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane();
        pc.setSelected(true);
        mobile.setSelected(true);

        ItemListener parentClassifyRefresh = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                parentClassify.setSelectedItem(ComponentTypes.CHART);
            }
        };

        pc.addItemListener(parentClassifyRefresh);

        mobile.addItemListener(parentClassifyRefresh);
        pane.add(pc);
        pane.add(mobile);
        return pane;
    }

    @NotNull
    private JPanel createVendorPane() {

        final JPanel vendorPane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane();

        String bbsUsername = DesignerEnvManager.getEnvManager().getDesignerLoginUsername();
        if (StringUtils.isEmpty(bbsUsername)) {
            UILabel unLoginLabel = ShareUIUtils.createHyperlinkLabel(Toolkit.i18nText("Fine-Design_Share_Click_Login"));

            final UILabel hidden = new ShareLoginLabel(vendorPane, unLoginLabel, loginLabel);

            unLoginLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    LoginWebBridge.getHelper().setUILabel(hidden);
                    Dialog shareDialog = ShareDialogUtils.getInstance().getShareDialog();
                    //必须这样创建，不然窗口优先级不对
                    DesignerLoginHelper.showLoginDialog(DesignerLoginSource.NORMAL, new HashMap<>(), shareDialog);
                }
            });
            vendorPane.add(unLoginLabel);
        } else {
            loginLabel.setText(bbsUsername);
            vendorPane.add(loginLabel);
        }

        return vendorPane;
    }

    @NotNull
    private JPanel createComponentOverviewPane(boolean upload) {
        //功能介绍
        UILabel effectLabel = ShareUIUtils.createTopRightUILabel(Toolkit.i18nText("Fine-Design_Share_Special_Effects"));
        JPanel effectPane = createEffectPane(effectItemGroups);

        content.setDocument(
                new LimitedDocument(200, 500)
        );
        String contentTip = Toolkit.i18nText("Fine-Design_Share_Content_Introduction_Placeholder");
        content.setAutoscrolls(true);
        content.setPlaceholder(contentTip);
        UIScrollPane contentPane = new UIScrollPane(content);
        contentPane.setPreferredSize(new Dimension(COLUMN_FIELD_WIDTH, 50));
        contentPane.setBorder(null);

        help.setDocument(
                new LimitedDocument(1000)
        );
        String helpTip = Toolkit.i18nText("Fine-Design_Share_Help_Msg_Placeholder");
        help.setPlaceholder(helpTip);
        UIScrollPane helpPane = new UIScrollPane(help);
        helpPane.setPreferredSize(new Dimension(COLUMN_FIELD_WIDTH, 50));
        helpPane.setBorder(null);

        UILabel helpLabel = ShareUIUtils.createTopRightUILabel(Toolkit.i18nText("Fine-Design_Share_Help_Msg"));
        UILabel contentLabel = ShareUIUtils.createTopRightUILabel(Toolkit.i18nText("Fine-Design_Share_Content_Introduction"));


        double p = TableLayout.PREFERRED;
        int[] row = {1, 1};
        double[] columnSize = {COLUMN_ITEM_SIZE, p};
        List<Component[]> componentList= new LinkedList<Component[]>();
        List<Double> rowSizeList = new LinkedList<Double>();
        List<int[]> rowCountList = new LinkedList<int[]>();

        if (effectItemGroups.size() > 0 ) {
            componentList.add(new Component[]{effectLabel, effectPane});
        }

        componentList.add(new Component[]{helpLabel, helpPane});

        if (upload) {
            componentList.add(new Component[]{contentLabel, contentPane});
        }
        for (int i = 0; i < componentList.size(); i++) {
            rowSizeList.add(p);
            rowCountList.add(row);
        }

        return TableLayoutHelper.createGapTableLayoutPane(
                componentList.toArray(new Component[componentList.size()][]),
                ArrayUtils.toPrimitive(rowSizeList.toArray(new Double[rowSizeList.size()])),
                columnSize,
                rowCountList.toArray(new int[rowCountList.size()][]),
                LayoutConstants.HGAP_SMALL, LayoutConstants.VGAP_MEDIUM);
    }

    private JPanel createNameFiledPane() {
        LimitedDocument nameLimited = new LimitedDocument(NAME_MAX_LENGTH);
        nameField.setPlaceholder(Toolkit.i18nText("Fine-Design_Share_Name_Placeholder"));
        nameField.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));
        nameField.setDocument(nameLimited);
        JPanel symbolTextFiled = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel validSymbol = new UILabel(" *");
        symbolTextFiled.add(nameField, BorderLayout.CENTER);
        symbolTextFiled.add(validSymbol, BorderLayout.EAST);

        return symbolTextFiled;
    }


    private void initUploadCoverBtn() {

        this.uploadBtn = new UIButton(Toolkit.i18nText("Fine-Design_Share_Select_Upload_Cover"));
        this.uploadBtn.setPreferredSize(new Dimension(BTN_WIDTH, BTN_HEIGHT));
        uploadBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final CoverUploadPane pane = new CoverUploadPane();
                BasicDialog basicDialog = pane.showWindow(DesignerContext.getDesignerFrame(), true);
                pane.setSize(BasicDialog.MEDIUM);
                pane.populateBean(shareCover);
                basicDialog.addDialogActionListener(new DialogActionAdapter() {
                    @Override
                    public void doOk() {
                        shareCover = pane.updateBean();
                        cover.setIcon(getShowIcon(shareCover));
                        cover.repaint();
                    }
                });
                basicDialog.setVisible(true);
            }
        });
    }

    private JPanel getCoverImagePane() {

        initUploadCoverBtn();
        JPanel coverImagePane = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
        cover.setPreferredSize(new Dimension(ShareComponentConstants.SHARE_THUMB_WIDTH, ShareComponentConstants.SHARE_THUMB_HEIGHT));
        coverImagePane.add(cover);

        JPanel uploadBtnPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        uploadBtnPane.setPreferredSize(new Dimension(BTN_WIDTH, ShareComponentConstants.SHARE_THUMB_HEIGHT));
        uploadBtnPane.add(uploadBtn, BorderLayout.SOUTH);
        coverImagePane.add(uploadBtnPane);

        return coverImagePane;

    }

    /* 校验 */

    public boolean check() {

        Dialog shareDialog = ShareDialogUtils.getInstance().getShareDialog();
        String name = nameField.getText();
        if (StringUtils.isEmpty(name)) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Lack_Name"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        if (upload && StringUtils.isEmpty(content.getText())) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Lack_Content"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        if (upload && StringUtils.isEmpty(loginLabel.getText())) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Lack_Login"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        if (upload && styleComboBox.getSelectedItem() == null) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Style_Theme_Tip"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        if (!pc.isSelected() && !mobile.isSelected()) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Lack_Device"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        if (pluginCheckBox.isSelected() && ArrayUtils.isEmpty(pluginComboCheckBox.getSelectedValues())) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Lack_Plugins"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        if (!priceExpectPane.check()) {
            FineJOptionPane.showMessageDialog(shareDialog, Toolkit.i18nText("Fine-Design_Share_Lack_Price"),
                    Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
            return false;
        }
        return true;
    }

    /* 生成 */

    public DefaultSharableWidget generate() {

        String uuid = UUID.randomUUID().toString();
        DefaultSharableWidget provider = new DefaultSharableWidget(uuid, nameField.getText(), help.getText(), shareCover, rec.width, rec.height);
        provider.setDisplayDevice(displayDevice());
        provider.setParentClassify(classify(parentClassify.getSelectedItem()));
        provider.setChildClassify(classify(childClassify.getSelectedItem()));
        provider.setDesignerVersion(ProductConstants.VERSION);
        provider.setVendor(loginLabel.getText());
        provider.setFileName(provider.getNameWithID());
        provider.setVendorUid(DesignerEnvManager.getEnvManager().getDesignerLoginUid());
        provider.setCreateTime(System.currentTimeMillis());
        JSONArray historyCreatedReuses = JSONFactory.createJSON(
                JSON.ARRAY,
                ComponentReuseNotificationInfo.getInstance().getHistoryCreatedReuses()
        );
        historyCreatedReuses.add(uuid);
        ComponentReuseNotificationInfo.getInstance().setHistoryCreatedReuses(historyCreatedReuses.toString());
        DesignerEnvManager.getEnvManager().saveXMLFile();

        if (upload) {
            provider.setSummary(content.getText());
            Object selectStyle = styleComboBox.getSelectedItem();
            provider.setStyleTheme(selectStyle == null ? null : ((StyleThemeBean) selectStyle).getId());
            provider.setInvolvePlugins(pluginComboCheckBox.getText());
            provider.setPriceExpect(priceExpectPane.getPrice());
        }

        return provider;
    }

    public Group getSelectGroup() {
        return (Group) localGroup.getSelectedItem();
    }

    public UITextField getNameField() {
        return nameField;
    }

    private String classify(Object classify) {

        return classify == null ? StringUtils.EMPTY : classify.toString();
    }

    private int displayDevice() {

        List<DisplayDevice> types = new ArrayList<>(8);
        if (pc.isSelected()) {
            types.add(DisplayDevice.PC);
        }
        if (mobile.isSelected()) {
            types.add(DisplayDevice.MOBILE);
        }
        return DisplayDevice.buildDevice(types);
    }

    private class LimitedDocument extends PlainDocument {
        private static final long serialVersionUID = 1L;
        private int maxLength = -1;// 允许的最大长度
        private int errorLength = -1;
        private int loop = 5;
        private String allowCharAsString = null;// 允许的字符串格式（0123456789）

        public LimitedDocument(int maxLength) {
            this(-1, maxLength);
        }

        public LimitedDocument(int errorLength, int maxLength) {
            super();
            this.errorLength = errorLength;
            this.maxLength = maxLength;
        }

        @Override
        public void insertString(int offset, String str, AttributeSet attrSet) throws BadLocationException {
            if (str == null) {
                return;
            }
            if (allowCharAsString != null && str.length() == 1) {
                if (allowCharAsString.indexOf(str) == -1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();// 发出一个警告声
                    return;// 不是所要求的字符格式，就直接返回，不进行下面的添加
                }
            }
            char[] charVal = str.toCharArray();
            String strOldValue = getText(0, getLength());
            char[] tmp = strOldValue.toCharArray();
            int currentLength = tmp.length + charVal.length;
            if (errorLength != -1 && currentLength > errorLength && ++loop >= 5) {
                this.loop = 0;
                FineJOptionPane.showMessageDialog(ShareDialogUtils.getInstance().getShareDialog(),
                        ShareUIUtils.formatWidthString(Toolkit.i18nText("Fine-Design_Share_Input_Errors", String.valueOf(errorLength)), 200),
                        Toolkit.i18nText("Fine-Design_Basic_Error"), ERROR_MESSAGE, UIManager.getIcon("OptionPane.errorIcon"));
                //只提示，不报错
            }
            if (maxLength != -1 && (currentLength > maxLength)) {
                java.awt.Toolkit.getDefaultToolkit().beep();// 发出一个警告声
                return;// 长度大于指定的长度maxLength，也直接返回，不进行下面的添加
            }
            super.insertString(offset, str, attrSet);
        }

        public void setAllowChar(String str) {
            allowCharAsString = str;
        }
    }

}
