package com.fr.design.share.ui.config.table;

import com.fr.base.BaseUtils;
import com.fr.design.i18n.Toolkit;
import com.fr.locale.InterProviderFactory;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;

public class ConfigItemRender extends DefaultTableCellRenderer {
    static final private Color PLACEHOLDER_COLOR = new Color(193, 193, 193);
    static final private Color NO_EDITABLE_BG = new Color(240, 240, 241);

    static final private Border DEFAULT_NO_FOCUS_BORDER = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(1, 5, 1, 5), BorderFactory.createEmptyBorder(0, 4, 0, 4));
    static final private Border SELECTED_BORDER = BorderFactory.createLineBorder(new Color(160,190,240));

    private final Icon editIcon = BaseUtils.readIcon("com/fr/base/images/share/edit.png");

    public ConfigItemRender() {
        super();
        setBorder(DEFAULT_NO_FOCUS_BORDER);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setText(StringUtils.EMPTY);
        setToolTipText(null);
        setBorder(DEFAULT_NO_FOCUS_BORDER);
        setBackground(null);
        setForeground(null);
        setIcon(null);
        setAlignmentX(JComponent.LEFT_ALIGNMENT);

        if (column == 2) {
            setIcon(editIcon);
            setAlignmentX(JComponent.CENTER_ALIGNMENT);
            return this;
        }

        if (value == null) {
            return this;
        }

        if (StringUtils.isNotEmpty(value.toString())) {
            setValue(value);
        }
        setForeground(Color.black);

        if (column == 1) {
            if (!table.isCellEditable(row, column)) {
                setForeground(PLACEHOLDER_COLOR);
                setBackground(NO_EDITABLE_BG);
                setToolTipText(Toolkit.i18nText("Fine-Design_Share_Not_Support_Rename"));
            }
            if (StringUtils.isEmpty(value.toString())) {
                setValue(Toolkit.i18nText("Fine-Design_Share_Need_Rename"));
                setForeground(PLACEHOLDER_COLOR);
            }
        }

        if (table.getSelectedRow() == row && table.getSelectedColumn() == column && table.isCellEditable(row, column) ) {
            setBorder(SELECTED_BORDER);
            setBackground(null);
            table.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
        } else {
            if (isSelected) {
                setBackground(table.getSelectionBackground());
            }
            table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        return this;
    }


    public void setValue(Object value) {
        super.setValue(value);
        setToolTipText((value == null) ? null : value.toString());
    }
}
