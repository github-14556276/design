package com.fr.design.share.ui.generate;

import com.fr.design.DesignerEnvManager;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.bbs.UserInfoPane;
import com.fr.stable.StringUtils;

import javax.swing.JPanel;

/**
 * created by Harrison on 2020/04/20
 **/
public class ShareLoginLabel extends UILabel {
    
    private JPanel vendorPane;
    
    private UILabel unLoginLabel;
    
    private UILabel loginLabel;
    
    public ShareLoginLabel(JPanel vendorPane, UILabel unLoginLabel, UILabel loginLabel) {
        this.vendorPane = vendorPane;
        this.unLoginLabel = unLoginLabel;
        this.loginLabel = loginLabel;
        setVisible(false);
    }
    
    @Override
    public void setText(String text) {
        super.setText(text);
        if (StringUtils.isNotEmpty(text)) {
            tryGetLogin();
        }
    }
    
    private void tryGetLogin() {
        
        String bbsUsername = DesignerEnvManager.getEnvManager().getDesignerLoginUsername();
        if (StringUtils.isNotEmpty(bbsUsername)) {
            vendorPane.remove(unLoginLabel);
            loginLabel.setText(bbsUsername);
            vendorPane.add(loginLabel);
            // 像这种 remove ， 再 add 的， 需要 validate()
            vendorPane.validate();
            vendorPane.repaint();
            //同步刷新页面
            UserInfoPane.getInstance().updateBBSUserInfo();
        }
    }
}
