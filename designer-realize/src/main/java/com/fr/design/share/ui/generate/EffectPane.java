package com.fr.design.share.ui.generate;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.ui.base.PageableButton;
import com.fr.design.share.effect.EffectItemGroup;
import com.fr.form.ui.Widget;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class EffectPane extends JPanel {
    private List<EffectItemGroup> effectItemGroups;
    private JPanel contentPane;
    private int selectIndex = 0;
    private BasicArrowButton previewButton;
    private BasicArrowButton nextButton;
    private UILabel nameLabel;
    private List<JPanel> contents = new ArrayList<>();


    public EffectPane(List<EffectItemGroup> effectItemGroups) {
        this.effectItemGroups = effectItemGroups;
        if (effectItemGroups.size() > 0) {
            initComponent();
        }
    }

    private void initComponent() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        createContentPane();

        JPanel rightPane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        JPanel headerPane = createHeaderPane();
        contentPane = createContentPane();
        addContent();
        rightPane.add(headerPane, BorderLayout.NORTH);
        rightPane.add(contentPane, BorderLayout.CENTER);
        this.add(rightPane, BorderLayout.CENTER);
    }

    private JPanel createHeaderPane() {
        JPanel headerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel naviPane = createNaviPane();
        headerPane.add(naviPane, BorderLayout.EAST);
        return headerPane;
    }

    private JPanel createNaviPane() {
        JPanel naviPane = new JPanel(FRGUIPaneFactory.createRightFlowLayout());
        nameLabel = new UILabel("");
        nameLabel.setPreferredSize(new Dimension(nameLabel.getFont().getSize() * 20, 15));
        nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);

        previewButton = new PageableButton(SwingConstants.WEST) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(15, 15);
            }
        };
        previewButton.setEnabled(hasPreview());
        previewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectIndex--;
                EffectPane.this.TogglePage();
            }
        });

        nextButton = new PageableButton(SwingConstants.EAST) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(15, 15);
            }
        };
        nextButton.setEnabled(hasNext());
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectIndex++;
                EffectPane.this.TogglePage();
            }
        });

        naviPane.add(nameLabel);
        naviPane.add(previewButton);
        naviPane.add(nextButton);
        return naviPane;
    }

    private JPanel createContentPane() {
        JPanel contentPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 0 , 0, 0));
        return contentPane;
    }

    private void addContent() {
        JPanel content = new EffectContent(getSelectEffectItemGroup());
        contents.add(selectIndex, content);
        updateContent();
    }

    private Widget getSelectWidget() {
        return getSelectEffectItemGroup().getWidget();
    }

    private EffectItemGroup getSelectEffectItemGroup() {
        return effectItemGroups.get(selectIndex);
    }

    private boolean hasPreview() {
        return selectIndex > 0;
    }

    private boolean hasNext() {
        return selectIndex < effectItemGroups.size() - 1;
    }

    private void TogglePage() {
        previewButton.setEnabled(hasPreview());
        nextButton.setEnabled(hasNext());
        if (selectIndex < contents.size()) {
            updateContent();
        } else {
            addContent();
        }
    }

    private void updateContent() {
        // 添加contentPane
        contentPane.removeAll();
        contentPane.add(contents.get(selectIndex));
        nameLabel.setText(getSelectWidget().getWidgetName());
        nameLabel.setToolTipText(getSelectWidget().getWidgetName());
        this.repaint();
    }
}
