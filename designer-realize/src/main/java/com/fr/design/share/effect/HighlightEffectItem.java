package com.fr.design.share.effect;

import com.fr.design.condition.HighLightConditionAttributesPane;
import com.fr.design.gui.controlpane.NameObjectCreator;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.report.cell.cellattr.highlight.DefaultHighlight;

public class HighlightEffectItem extends BaseEffectItem<DefaultHighlight>{
    private DefaultHighlight highlight;

    public HighlightEffectItem(DefaultHighlight highlight, SourceNode sourceNode) {
        this.highlight = highlight;
        this.setSourceNode(sourceNode);
        this.init();
    }

    private void init() {
        DefaultHighlight object;
        try {
            object = (DefaultHighlight) highlight.clone();
        } catch (CloneNotSupportedException e) {
            object = new DefaultHighlight();
            object.setName(highlight.getName());
            object.setCondition(highlight.getCondition());
            for (int i = 0; i < highlight.actionCount(); i++) {
                object.addHighlightAction(highlight.getHighlightAction(i));
            }
        }
        setObject(object);

        this.setName(highlight.getName());
        this.setNameableCreator(new NameObjectCreator(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Condition_Attributes"), DefaultHighlight.class, HighLightConditionAttributesPane.class));
    }

    public void setName(String name) {
        super.setName(name);
        getObject().setName(name);
    }

    @Override
    public void updateBean(Object bean) {
        DefaultHighlight highlight = (DefaultHighlight) bean;
        highlight.setName(this.getName());
        setObject(highlight);
    }

    @Override
    public void save() {
        highlight.setCondition(getObject().getCondition());
        highlight.setName(getObject().getName());
        highlight.removeActions();
        for (int i = 0; i < getObject().actionCount(); i++) {
            highlight.addHighlightAction(getObject().getHighlightAction(i));
        }
    }
}
