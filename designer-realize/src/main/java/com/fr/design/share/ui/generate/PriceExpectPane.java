package com.fr.design.share.ui.generate;

import com.fr.design.gui.ibutton.UIRadioButton;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.stable.StringUtils;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kerry on 2020-06-23
 */
public class PriceExpectPane extends JPanel {
    private static final int TEXT_FILED_WIDTH = 160;
    private static final int TEXT_FILED_HEIGHT = 20;
    private static final String FREE_PRICE = "0";
    private static final String PRICE_FORMAT = "%.2f";
    private UITextField priceFiled;
    private UIRadioButton freeRadio;
    private UIRadioButton chargeRadio;

    public PriceExpectPane() {
        freeRadio = new UIRadioButton(Toolkit.i18nText("Fine-Design_Share_Price_Free"));
        chargeRadio = new UIRadioButton(Toolkit.i18nText("Fine-Design_Share_Price_Charge"));
        priceFiled = new UITextField();
        priceFiled.setPlaceholder(Toolkit.i18nText("Fine-Design_Share_Price_Charge_Tip"));
        ButtonGroup radioGroup = new ButtonGroup();
        radioGroup.add(freeRadio);
        radioGroup.add(chargeRadio);
        this.setLayout(FRGUIPaneFactory.createBoxFlowLayout());
        this.add(freeRadio);
        this.add(chargeRadio);
        final JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        panel.add(priceFiled);
        this.add(panel);
        panel.setPreferredSize(new Dimension(TEXT_FILED_WIDTH, TEXT_FILED_HEIGHT));
        panel.setVisible(false);
        this.freeRadio.setSelected(true);
        freeRadio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.setVisible(false);

            }
        });
        chargeRadio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel.setVisible(true);
            }
        });
    }

    public String getPrice() {
        if (this.freeRadio.isSelected()) {
            return FREE_PRICE;
        }
        float priceNumber = Float.parseFloat(this.priceFiled.getText());
        return String.format(PRICE_FORMAT, priceNumber);
    }


    public boolean check() {
        if (freeRadio.isSelected()) {
            return true;
        }
        String price = this.priceFiled.getText();
        if (StringUtils.isEmpty(price)) {
            return false;
        }
        try {
            float priceNumber = Float.parseFloat(price);
            if (priceNumber <= 0) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

}
