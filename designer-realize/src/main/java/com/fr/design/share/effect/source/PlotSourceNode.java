package com.fr.design.share.effect.source;

import com.fr.chart.chartattr.Plot;
import com.fr.design.ChartTypeInterfaceManager;
import com.fr.stable.StringUtils;

public class PlotSourceNode extends BaseSourceNode<Plot> {

    public PlotSourceNode(Plot plot) {
        this(plot, StringUtils.EMPTY, null);
    }

    public PlotSourceNode(Plot plot, SourceNode parent) {
        this(plot, StringUtils.EMPTY, parent);
    }

    public PlotSourceNode(Plot plot, String chartName) {
        this(plot, chartName, null);
    }

    public PlotSourceNode(Plot plot, String chartName, SourceNode parent) {
        String path = ChartTypeInterfaceManager.getInstance().getName(plot.getPlotID());
        if (StringUtils.isNotEmpty(chartName)) {
            path = path + "-" + chartName;
        }
        this.setPath(path);
        this.setTarget(plot);
        this.setParent(parent);
    }

}
