package com.fr.design.share.effect;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.form.javascript.FormEmailPane;
import com.fr.design.fun.HyperlinkProvider;
import com.fr.design.gui.controlpane.NameObjectCreator;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.module.DesignModuleFactory;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.general.ComparatorUtils;
import com.fr.js.EmailJavaScript;
import com.fr.js.JavaScript;
import com.fr.js.NameJavaScript;
import com.fr.stable.ListMap;

import java.util.Map;
import java.util.Set;

public class HyperlinkEffectItem extends BaseEffectItem<NameJavaScript>{
    private NameJavaScript javaScript;

    public HyperlinkEffectItem(NameJavaScript javaScript, SourceNode sourceNode) {
        this.javaScript = javaScript;
        this.setSourceNode(sourceNode);
        this.init();
    }

    private void init() {
        NameJavaScript object;
        try {
            object = (NameJavaScript) javaScript.clone();
        } catch (CloneNotSupportedException e) {
            object = new NameJavaScript(javaScript.getName(), javaScript.getJavaScript());
        }
        setObject(object);

        this.setName(this.javaScript.getName());
        NameableCreator[] creators = createNameableCreator();
        for (NameableCreator creator : creators) {
            if (creator.acceptObject2Populate(javaScript.getJavaScript()) != null) {
                this.setNameableCreator(creator);
                return;
            }
        }
    }

    public void setName(String name) {
        super.setName(name);
        getObject().setName(name);
    }

    private NameableCreator[] createNameableCreator() {
        Map<String, NameableCreator> nameCreators = new ListMap<>();
        NameableCreator[] creators = DesignModuleFactory.getHyperlinkGroupType().getHyperlinkCreators();
        for (NameableCreator creator : creators) {
            nameCreators.put(creator.menuName(), creator);
        }
        Set<HyperlinkProvider> providers = ExtraDesignClassManager.getInstance().getArray(HyperlinkProvider.XML_TAG);
        for (HyperlinkProvider provider : providers) {
            NameableCreator nc = provider.createHyperlinkCreator();
            nameCreators.put(nc.menuName(), nc);
        }

        creators = nameCreators.values().toArray(new NameableCreator[nameCreators.size()]);
        for (int i = 0; i < creators.length; i++) {
            if (ComparatorUtils.equals(creators[i].menuName(), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Email"))) {
                creators[i] = new NameObjectCreator(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Email"), EmailJavaScript.class, FormEmailPane.class);
                break;
            }
        }

        return creators;
    }

    @Override
    public Object getPopulateBean() {
        return getObject().getObject();
    }

    @Override
    public void updateBean(Object bean) {
        JavaScript javaScript = (JavaScript) bean;
        getObject().setJavaScript(javaScript);
        getObject().setObject(bean);
    }

    @Override
    public void save() {
        javaScript.setName(getObject().getName());
        javaScript.setObject(getObject().getObject());
        javaScript.setJavaScript(getObject().getJavaScript());
    }
}
