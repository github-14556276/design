package com.fr.design.actions.insert.flot;

import com.fr.base.BaseUtils;
import com.fr.design.gui.chart.MiddleChartDialog;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ElementCasePane;
import com.fr.design.menu.MenuKeySet;
import com.fr.design.module.DesignModuleFactory;

import javax.swing.KeyStroke;
import java.awt.Window;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-28
 */
public class AutoChartFloatAction extends ChartFloatAction {

    /**
     * 构造函数 图表插入悬浮元素
     */
    public AutoChartFloatAction(ElementCasePane t) {
        super(t);
    }

    protected void init() {
        this.setMenuKeySet(FLOAT_INSERT_AUTO_CHART);
        this.setName(getMenuKeySet().getMenuKeySetName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/m_insert/auto_chart.png"));
    }

    public static final MenuKeySet FLOAT_INSERT_AUTO_CHART = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'M';
        }

        @Override
        public String getMenuName() {
            return Toolkit.i18nText("Fine-Design_Report_M_Insert_Auto_Chart");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

    protected MiddleChartDialog getMiddleChartDialog(Window window) {
        return DesignModuleFactory.getAutoChartDialog(window);
    }
}
