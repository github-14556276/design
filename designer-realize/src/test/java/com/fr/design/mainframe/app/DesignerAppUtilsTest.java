package com.fr.design.mainframe.app;

import com.fr.invoke.Reflect;
import com.fr.plugin.context.PluginMarker;
import com.fr.plugin.context.PluginMarkerAdapter;
import com.fr.stable.TemplateIOErrorContextHolder;
import com.fr.third.guava.collect.Multimap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author vito
 * @version 10.0
 * Created by vito on 2021/5/31
 */
@RunWith(PowerMockRunner.class)
public class DesignerAppUtilsTest {
    @Test
    public void testDealWithErrorDetailMultiLineAndCache() {
        TemplateIOErrorContextHolder.registerPluginNameMap(new HashMap<String, String>() {{
            put("2", "好用的插件");
        }}, new HashSet<>());
        TemplateIOErrorContextHolder.addNeedEnablePlugin(PluginMarkerAdapter.create("1", "1.0", "1插件"));
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("2", "1.0"));
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("3", "1.0"));
        String log = DesignerAppUtils.dealWithErrorDetailMultiLineAndCache("template1");
        Assert.assertTrue(log.contains("1插件"));
        Assert.assertTrue(log.contains("好用的插件"));
        Assert.assertTrue(log.contains("3"));
        Multimap<String, PluginMarkerAdapter> map = DesignerAppUtils.popPluginInfoMap("template1");
        Assert.assertEquals(3, map.size());
        Assert.assertNull(DesignerAppUtils.popPluginInfoMap("template1"));
    }

    @Test
    public void testInvalidatePlugins() {
        TemplateIOErrorContextHolder.registerPluginNameMap(new HashMap<String, String>() {{
            put("2", "好用的插件");
        }}, new HashSet<>());
        TemplateIOErrorContextHolder.addNeedEnablePlugin(PluginMarkerAdapter.create("1", "1.0", "1插件"));
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("2", "1.0"));
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("3", "1.0"));
        String log = DesignerAppUtils.dealWithErrorDetailMultiLineAndCache("template1");
        Assert.assertTrue(log.contains("1插件"));
        Assert.assertTrue(log.contains("好用的插件"));
        Assert.assertTrue(log.contains("3"));
        DesignerAppUtils.invalidatePlugins("template1");
        Assert.assertNull(DesignerAppUtils.popPluginInfoMap("template1"));
    }

    @Test
    public void testRearrange() {
        // 远程插件模拟注册
//        PluginRemoteSync pluginRemoteSync = EasyMock.createMock(PluginRemoteSync.class);
//        EasyMock.expect(pluginRemoteSync.getPluginRemoteStatusByIdIndex()).andReturn(new HashMap<String, PluginRemoteSync.PluginStatus>(){{
//            put("com.fr.plugin1", Reflect.on(PluginRemoteSync.PluginStatus.class).call("create","com.fr.plugin1","1",true).get());
//            put("com.fr.plugin2", Reflect.on(PluginRemoteSync.PluginStatus.class).call("create","com.fr.plugin2","1",true).get());
//            put("com.fr.plugin3", Reflect.on(PluginRemoteSync.PluginStatus.class).call("create","com.fr.plugin3","1",false).get());
//            put("com.fr.plugin4", Reflect.on(PluginRemoteSync.PluginStatus.class).call("create","com.fr.plugin4","1",false).get());
//        }}).anyTimes();
//        EasyMock.replay(pluginRemoteSync);
//        PowerMock.mockStaticPartial(PluginRemoteSync.class, "getInstance");
//        EasyMock.expect(PluginRemoteSync.getInstance()).andReturn(pluginRemoteSync).anyTimes();
//        PowerMock.replay(PluginRemoteSync.class);

        // 本地插件模拟检查
        TemplateIOErrorContextHolder.registerPluginNameMap(new HashMap<String, String>() {{
            put("com.fr.plugin1", "好用的插件1");
            put("com.fr.plugin2", "好用的插件2");
            put("com.fr.plugin3", "好用的插件3");
            put("com.fr.plugin4", "好用的插件4");
            put("com.fr.plugin5", "好用的插件5");
        }}, new HashSet<>());
        // unknown
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("com.fr.plugin7", "1"));
        // disable
        TemplateIOErrorContextHolder.addNeedEnablePlugin(PluginMarkerAdapter.create("com.fr.plugin5", "1", "plugin5"));
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("com.fr.plugin3", "1"));
        // not install
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("com.fr.plugin1", "1"));
        TemplateIOErrorContextHolder.addNeedInstallPlugin(PluginMarker.create("com.fr.plugin4", "1"));

        Multimap<String, PluginMarkerAdapter> pendingPlugins = TemplateIOErrorContextHolder.getPendingPlugin();

        Reflect.on(DesignerAppUtils.class).call("rearrange", pendingPlugins).get();
        Assert.assertEquals(1, pendingPlugins.get(TemplateIOErrorContextHolder.UNKNOWN_PLUGIN).size());
        Collection<PluginMarkerAdapter> pluginMarkerAdapters = pendingPlugins.get(TemplateIOErrorContextHolder.DISABLE_PLUGIN);
        Assert.assertEquals(2, pluginMarkerAdapters.size());
        pluginMarkerAdapters.contains(PluginMarker.create("com.fr.plugin3", "1"));
        pluginMarkerAdapters.contains(PluginMarker.create("com.fr.plugin4", "1"));
        Collection<PluginMarkerAdapter> pluginMarkerAdapters1 = pendingPlugins.get(TemplateIOErrorContextHolder.NOT_INSTALLED_PLUGIN);
        Assert.assertEquals(1, pluginMarkerAdapters1.size());
        pluginMarkerAdapters1.contains(PluginMarker.create("com.fr.plugin5", "1"));
    }
}