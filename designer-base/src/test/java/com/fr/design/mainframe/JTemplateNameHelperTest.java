package com.fr.design.mainframe;

import junit.framework.TestCase;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/5/8
 */
public class JTemplateNameHelperTest extends TestCase {

    public void testNewTemplateNameByIndex() {

        String name = JTemplateNameHelper.newTemplateNameByIndex("TEST");

        assertEquals("TEST1", name);

        String name1 = JTemplateNameHelper.newTemplateNameByIndex("TEST");

        assertEquals("TEST2", name1);

    }
}
