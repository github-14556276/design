package com.fr.design.mod;

import junit.framework.TestCase;
import org.junit.Assert;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class ContentReplaceUtilTest extends TestCase {

    public void testReplaceContent() {
        String text = "setTimeout(function() {\n" +
                "\n" +
                "\t$(\"div[widgetname=DATEEDITOR0]\").css({\n" +
                "\n" +
                "\t\t'opacity': '0.3'\n" +
                "\n" +
                "\t});\n" +
                "\n" +
                "}, 50);\n" +
                "var a = \"dateEditor0\"";
        String oldName = "dateEditor0";
        String newName = "dateEditor00";
        String result = "setTimeout(function() {\n" +
                "\n" +
                "\t$(\"div[widgetname=DATEEDITOR00]\").css({\n" +
                "\n" +
                "\t\t'opacity': '0.3'\n" +
                "\n" +
                "\t});\n" +
                "\n" +
                "}, 50);\n" +
                "var a = \"dateEditor00\"";
        Assert.assertEquals(result, ContentReplaceUtil.replaceContent(text, oldName, newName));

        String text1 = "setInterval(function() {\n" +
                "\t//获取当前body中tab的索引位置\n" +
                "    var aa = _g().getWidgetByName(\"tabpane00\").getShowIndex();\n" +
                "    //根据tab索引轮播tab块,索引从0开始,到最后一个tab块后跳转到第一个\n" +
                "\tif(aa == TAB轮播-测试-2) {\n" +
                "        _g().getWidgetByName('tabpane00').showCardByIndex(0);\n" +
                "    } else {\n" +
                "\n" +
                "        _g().getWidgetByName('tabpane00').showCardByIndex(aa + TAB轮播-测试-1);\n" +
                "    }\n" +
                "}, TAB轮播-测试-2000);";
        String oldName1 = "tabpane00";
        String newName1 = "tabpane0";
        String result1 = "setInterval(function() {\n" +
                "\t//获取当前body中tab的索引位置\n" +
                "    var aa = _g().getWidgetByName(\"tabpane0\").getShowIndex();\n" +
                "    //根据tab索引轮播tab块,索引从0开始,到最后一个tab块后跳转到第一个\n" +
                "\tif(aa == TAB轮播-测试-2) {\n" +
                "        _g().getWidgetByName('tabpane0').showCardByIndex(0);\n" +
                "    } else {\n" +
                "\n" +
                "        _g().getWidgetByName('tabpane0').showCardByIndex(aa + TAB轮播-测试-1);\n" +
                "    }\n" +
                "}, TAB轮播-测试-2000);";
        Assert.assertEquals(result1, ContentReplaceUtil.replaceContent(text1, oldName1, newName1));
    }

}