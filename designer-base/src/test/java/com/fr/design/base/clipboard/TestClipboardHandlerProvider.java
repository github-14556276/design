package com.fr.design.base.clipboard;

import com.fr.design.fun.impl.AbstractClipboardHandlerProvider;

/**
 * created by Harrison on 2020/05/15
 **/
class TestClipboardHandlerProvider<T> extends AbstractClipboardHandlerProvider<T> {
    
    @Override
    public T cut(T selection) {
        return selection;
    }
    
    @Override
    public T copy(T selection) {
        return selection;
    }
    
    @Override
    public T paste(T selection) {
        return null;
    }
    
    @Override
    public boolean support(Object selection) {
        return true;
    }
}
