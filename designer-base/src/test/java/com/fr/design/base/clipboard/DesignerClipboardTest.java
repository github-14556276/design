package com.fr.design.base.clipboard;

import com.fr.design.fun.ClipboardHandlerProvider;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.dnd.ArrayListTransferable;
import com.fr.form.main.ExtraFormClassManager;
import com.fr.plugin.injectable.PluginModule;
import com.fr.stable.fun.mark.Mutable;
import com.fr.third.guava.collect.Lists;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.swing.JPanel;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@PrepareForTest(PluginModule.class)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.swing.*")
public class DesignerClipboardTest {
    
    @Before
    public void setUp() throws Exception {
        
        Set<Mutable> providers = new HashSet<>();
        
        ExtraFormClassManager formClassManager = EasyMock.mock(ExtraFormClassManager.class);
        EasyMock.expect(formClassManager.getArray(ClipboardHandlerProvider.XML_TAG))
                .andReturn(providers)
                .anyTimes();
        EasyMock.replay(formClassManager);
        
        PowerMock.mockStatic(PluginModule.class);
        EasyMock.expect(PluginModule.getAgent(PluginModule.ExtraForm))
                .andReturn(formClassManager)
                .anyTimes();
        PowerMock.replayAll();
    }
    
    @After
    public void tearDown() throws Exception {
        
        PowerMock.resetAll();
    }
    
    @Test
    public void testClipboard() throws Exception {
        
        JPanel panel = new JPanel();
        Clipboard clipboard = DesignerContext.getClipboard(panel);
        
        ArrayList<String> transferData = Lists.newArrayList("test", "test2");
        ArrayListTransferable transferable = new ArrayListTransferable(transferData);
        clipboard.setContents(transferable, null);
        
        Transferable filterTransferable = clipboard.getContents(null);
        DataFlavor[] flavors = transferable.getTransferDataFlavors();
        ArrayList<String> transferData2 = (ArrayList<String>) filterTransferable.getTransferData(flavors[0]);
        
        Assert.assertEquals(transferData.get(0), transferData2.get(0));
    }
    
}