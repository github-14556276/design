package com.fr.design.i18n;

import com.fr.general.GeneralContext;
import com.fr.invoke.Reflect;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.awt.Dimension;
import java.util.Locale;

/**
 * Created by kerry on 2/24/21
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(GeneralContext.class)
public class DesignSizeI18nManagerTest {
    @Test
    public void testI18nDimension() {
        Dimension dimension = DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.report.ReportColumnsPane");
        validDimension(dimension, 660, 600);

        PowerMock.mockStatic(GeneralContext.class);
        EasyMock.expect(GeneralContext.getLocale()).andReturn(Locale.ENGLISH).times(3);
        PowerMock.replayAll();

        dimension = DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.report.ReportColumnsPane");
        validDimension(dimension, 800, 600);
    }

    @Test
    public void testContainKey() {
        PowerMock.mockStatic(GeneralContext.class);
        EasyMock.expect(GeneralContext.getLocale()).andReturn(Locale.ENGLISH).times(3);
        PowerMock.replayAll();

        boolean result = Reflect.on(DesignSizeI18nManager.getInstance()).call("containKey", "testKey").get();
        Assert.assertFalse(result);

        result = Reflect.on(DesignSizeI18nManager.getInstance()).call("containKey", "com.fr.design.report.ReportColumnsPane").get();
        Assert.assertTrue(result);

    }

    @Test
    public void testParseDimensionFromText() {
        String dimensionText = "800*600";
        Dimension result = Reflect.on(DesignSizeI18nManager.getInstance()).call("parseDimensionFromText", dimensionText).get();
        validDimension(result, 800, 600);

        dimensionText = "800* 600";
        result = Reflect.on(DesignSizeI18nManager.getInstance()).call("parseDimensionFromText", dimensionText).get();
        validDimension(result, 660, 600);

        dimensionText = " 800*600";
        result = Reflect.on(DesignSizeI18nManager.getInstance()).call("parseDimensionFromText", dimensionText).get();
        validDimension(result, 660, 600);

        dimensionText = "800*600s";
        result = Reflect.on(DesignSizeI18nManager.getInstance()).call("parseDimensionFromText", dimensionText).get();
        validDimension(result, 660, 600);

        dimensionText = "800s*600";
        result = Reflect.on(DesignSizeI18nManager.getInstance()).call("parseDimensionFromText", dimensionText).get();
        validDimension(result, 660, 600);

        dimensionText = "800-600";
        result = Reflect.on(DesignSizeI18nManager.getInstance()).call("parseDimensionFromText", dimensionText).get();
        validDimension(result, 660, 600);
    }

    private void validDimension(Dimension dimension, int width, int height) {
        Assert.assertEquals(width, dimension.width);
        Assert.assertEquals(height, dimension.height);
    }
}
