package com.fr.design.data.datapane;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author Yyming
 * @version 10.0
 * Created by Yyming on 2020/9/29
 */
public class TableDataPaneListPaneTest {

    @Test
    public void rename() {
        TableDataPaneListPane listPane = new TableDataPaneListPane();
        listPane.rename("111", "222");
        listPane.rename("222", "333");
        Map<String, String> dsNameChangedMap = listPane.getDsNameChangedMap();
        assertEquals(1, dsNameChangedMap.size());
        listPane.rename("333","111");
        assertEquals(0, dsNameChangedMap.size());
    }
}