package com.fr.design.upm;

import com.fr.config.ServerPreferenceConfig;
import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Locale;
import java.util.Map;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2021/1/5
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ServerPreferenceConfig.class, CloudCenter.class})
@SuppressStaticInitializationFor("com.fr.general.CloudCenter")
public class UpmUtilsTest {

    @Test
    public void testRenderMap() {

        ServerPreferenceConfig serverPreferenceConfig = EasyMock.mock(ServerPreferenceConfig.class);
        EasyMock.expect(serverPreferenceConfig.getOptimizedUPMVersion()).andReturn("1.0").anyTimes();
        PowerMock.mockStatic(ServerPreferenceConfig.class);
        EasyMock.expect(ServerPreferenceConfig.getInstance()).andReturn(serverPreferenceConfig).anyTimes();

        CloudCenter cloudCenter = EasyMock.mock(CloudCenter.class);
        EasyMock.expect(cloudCenter.acquireUrlByKind("upm.script.version")).andReturn("2.0").anyTimes();
        PowerMock.mockStatic(CloudCenter.class);
        EasyMock.expect(CloudCenter.getInstance()).andReturn(cloudCenter).anyTimes();

        EasyMock.replay(serverPreferenceConfig, cloudCenter);
        PowerMock.replayAll();

        GeneralContext.setLocale(Locale.CHINA);

        Map<String, String> map4Tpl = UpmUtils.renderMap();
        Assert.assertEquals(map4Tpl.size(), 3);
        Assert.assertEquals(map4Tpl.get("version"), "1.0");
        Assert.assertEquals(map4Tpl.get("new_version"), "2.0");
        Assert.assertEquals(map4Tpl.get("language"), "zh_CN");


        EasyMock.verify(serverPreferenceConfig, cloudCenter);
        PowerMock.verifyAll();

    }
}
