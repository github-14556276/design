package com.fr.base.svg;

import com.fr.cert.token.lang.Assert;
import junit.framework.TestCase;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2020/12/25
 */
public class IconUtilsTest extends TestCase {

    public void testReadIcon() {
        String resource = "com/fr/design/images/m_edit/copy.png";

        Icon pngIcon = IconUtils.readIcon(resource);
        assertTrue(pngIcon instanceof ImageIcon);

        resource = "com/fr/design/images/m_edit/copy_normal.svg";
        Icon suffixIcon = IconUtils.readIcon(resource);
        assertTrue(suffixIcon instanceof SVGIcon);

        resource = "com/fr/design/images/m_edit/copy";
        Icon noSuffixIcon = IconUtils.readIcon(resource);
        assertTrue(noSuffixIcon instanceof SVGIcon);
    }

    public void testReadSVGIcon() {
        String resource = "com/fr/design/images/m_edit/copy.png";

        Icon suffixIcon = IconUtils.readSVGIcon(resource, IconUtils.ICON_TYPE_NORMAL);
        assertTrue(suffixIcon instanceof SVGIcon);

        resource = "com/fr/design/images/m_edit/copy";
        Icon noSuffixIcon = IconUtils.readSVGIcon(resource, IconUtils.ICON_TYPE_NORMAL);
        assertTrue(noSuffixIcon instanceof SVGIcon);
    }
}
