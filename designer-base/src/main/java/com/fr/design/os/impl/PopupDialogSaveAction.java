package com.fr.design.os.impl;

import com.fr.design.gui.controlpane.UIControlPane;
import com.fr.stable.os.OperatingSystem;
import com.fr.stable.os.support.OSBasedAction;

import java.awt.*;

/**
 * linux下超链弹窗等保存问题
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/7/21
 */
public class PopupDialogSaveAction implements OSBasedAction {

    private UIControlPane currentControlPane;
    private Window popupDialog;

    @Override
    public void execute(Object... objects) {
        boolean canSave = OperatingSystem.isLinux() && popupDialog != null && popupDialog.isVisible() && currentControlPane != null;
        if (canSave) {
            currentControlPane.saveSettings();
        }
    }

    public void register(UIControlPane currentControlPane, Window popupDialog) {
        this.currentControlPane = currentControlPane;
        this.popupDialog = popupDialog;
    }

    public void unregister() {
        this.currentControlPane = null;
        this.popupDialog = null;
    }

}
