package com.fr.design.data.datapane.connect;

import com.fr.base.GraphHelper;
import com.fr.data.impl.JDBCDatabaseConnection;
import com.fr.data.pool.DBCPConnectionPoolAttr;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.BasicPane;
import com.fr.design.editor.editor.IntegerEditor;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DBCPAttrPane extends BasicPane {
    public static final int TIME_MULTIPLE = 1000;
    private static final Pattern FETCHSIZE_PATTERN = Pattern.compile("^0$|^[1-9][\\d]*[\\d]*$");;
    private static final Pattern CHAR_NEED_ESCAPE = Pattern.compile("[?|$^*\\\\\\[\\](){}.+]");
    private static final int MAX_FETCHSIZE = 1000000;
    private static final Map<String, Integer> DEFAULT_FETCHSIZE_MAP = new HashMap<>();
    private static final int ORACLE_DEFAULT_FETCHSIZE = 128;
    private static final int DB2_DEFAULT_FETCHSIZE = 50;
    private static final int POSTGRE_DEFAULT_FETCHSIZE = 10000;
    private static final int EMPTY_FETCHSIZE = -2;
    // carl:DBCP的一些属性
    private IntegerEditor DBCP_INITIAL_SIZE = new IntegerEditor();
    private IntegerEditor DBCP_MAX_ACTIVE = new IntegerEditor();
    private IntegerEditor DBCP_MAX_IDLE = new IntegerEditor();
    private IntegerEditor DBCP_MIN_IDLE = new IntegerEditor();
    private IntegerEditor DBCP_MAX_WAIT = new IntegerEditor();
    private UITextField DBCP_VALIDATION_QUERY = new UITextField();

    private UIComboBox DBCP_TESTONBORROW = new UIComboBox(new String[]{Toolkit.i18nText("Fine-Design_Basic_No"), Toolkit.i18nText("Fine-Design_Basic_Yes")});
    private UIComboBox DBCP_TESTONRETURN = new UIComboBox(new String[]{Toolkit.i18nText("Fine-Design_Basic_No"), Toolkit.i18nText("Fine-Design_Basic_Yes")});
    private UIComboBox DBCP_TESTWHILEIDLE = new UIComboBox(new String[]{Toolkit.i18nText("Fine-Design_Basic_No"), Toolkit.i18nText("Fine-Design_Basic_Yes")});

    private IntegerEditor DBCP_TIMEBETWEENEVICTIONRUNSMILLS = new IntegerEditor();
    private IntegerEditor DBCP_NUMTESTSPEREVICTIONRUN = new IntegerEditor();
    private IntegerEditor DBCP_MINEVICTABLEIDLETIMEMILLIS = new IntegerEditor();

    private UITextField FETCHSIZE = new UITextField();

    private JPanel defaultPane;
    private JPanel northFlowPane;
    private JPanel southFlowPane;

    static {
        DEFAULT_FETCHSIZE_MAP.put("Oracle", ORACLE_DEFAULT_FETCHSIZE);
        DEFAULT_FETCHSIZE_MAP.put("DB2", DB2_DEFAULT_FETCHSIZE);
        DEFAULT_FETCHSIZE_MAP.put("Postgre", POSTGRE_DEFAULT_FETCHSIZE);
    }

    public DBCPAttrPane() {
        defaultPane = this;

        // JPanel northFlowPane
        northFlowPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Basic_ConnectionPool_Attr"));
        northFlowPane.setPreferredSize(new Dimension(630, 320));
        defaultPane.add(northFlowPane, BorderLayout.NORTH);

        DBCP_VALIDATION_QUERY.setColumns(15);
        // ContextPane

        double f = TableLayout.FILL;
        // double p = TableLayout.PREFERRED;
        double[] rowSize = {f, f, f, f, f, f, f, f, f, f, f, f};
        double[] columnSize = {f, f};
        Component[][] comps = {
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Initial_Size") + ":", SwingConstants.RIGHT), DBCP_INITIAL_SIZE},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Max_Active") + ":", SwingConstants.RIGHT), DBCP_MAX_ACTIVE},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Max_Idle") + ":", SwingConstants.RIGHT), DBCP_MAX_IDLE},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Min_Idle") + ":", SwingConstants.RIGHT), DBCP_MIN_IDLE},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Connection_Pool_Max_Wait_Time") + ":", SwingConstants.RIGHT), DBCP_MAX_WAIT},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Validation_Query") + ":", SwingConstants.RIGHT), DBCP_VALIDATION_QUERY},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Test_On_Borrow") + ":", SwingConstants.RIGHT), DBCP_TESTONBORROW},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Test_On_Return") + ":", SwingConstants.RIGHT), DBCP_TESTONRETURN},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Test_While_Idle") + ":", SwingConstants.RIGHT), DBCP_TESTWHILEIDLE},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Connection_Pool_Evictionruns_millis") + ":", SwingConstants.RIGHT),
                        DBCP_TIMEBETWEENEVICTIONRUNSMILLS},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Dbcp_Num_Test_Per_Evction_Run") + ":", SwingConstants.RIGHT), DBCP_NUMTESTSPEREVICTIONRUN},
                {new UILabel(Toolkit.i18nText("Fine-Design_Basic_Connection_Pool_Mix_Evictable_Idle_Time_Millis") + ":", SwingConstants.RIGHT),
                        DBCP_MINEVICTABLEIDLETIMEMILLIS}};

        JPanel contextPane = TableLayoutHelper.createGapTableLayoutPane(comps, rowSize, columnSize, 10, 4);
        northFlowPane.add(contextPane);
    }

    public void populate(JDBCDatabaseConnection jdbcDatabase) {
        DBCPConnectionPoolAttr dbcpAttr = jdbcDatabase.getDbcpAttr();
        if (dbcpAttr == null) {
            dbcpAttr = new DBCPConnectionPoolAttr();
            jdbcDatabase.setDbcpAttr(dbcpAttr);
        }
        this.DBCP_INITIAL_SIZE.setValue(dbcpAttr.getInitialSize());
        this.DBCP_MAX_ACTIVE.setValue(dbcpAttr.getMaxActive());
        this.DBCP_MAX_IDLE.setValue(dbcpAttr.getMaxIdle());
        this.DBCP_MAX_WAIT.setValue(dbcpAttr.getMaxWait());
        this.DBCP_MIN_IDLE.setValue(dbcpAttr.getMinIdle());
        this.DBCP_VALIDATION_QUERY.setText(dbcpAttr.getValidationQuery());
        this.DBCP_TESTONBORROW.setSelectedIndex(dbcpAttr.isTestOnBorrow() ? 1 : 0);
        this.DBCP_TESTONRETURN.setSelectedIndex(dbcpAttr.isTestOnReturn() ? 1 : 0);
        this.DBCP_TESTWHILEIDLE.setSelectedIndex(dbcpAttr.isTestWhileIdle() ? 1 : 0);
        this.DBCP_MINEVICTABLEIDLETIMEMILLIS.setValue(dbcpAttr.getMinEvictableIdleTimeMillis() / TIME_MULTIPLE);
        this.DBCP_NUMTESTSPEREVICTIONRUN.setValue(dbcpAttr.getNumTestsPerEvictionRun());
        this.DBCP_TIMEBETWEENEVICTIONRUNSMILLS.setValue(dbcpAttr.getTimeBetweenEvictionRunsMillis());
        Integer fetchSize = DEFAULT_FETCHSIZE_MAP.get(JDBCConnectionDef.getInstance().getDatabaseName());
        if (fetchSize != null) {
            if (jdbcDatabase.getFetchSize() == EMPTY_FETCHSIZE) {
                this.FETCHSIZE.setText(StringUtils.EMPTY);
            } else {
                this.FETCHSIZE.setText(jdbcDatabase.getFetchSize() == -1 ? String.valueOf(fetchSize) : String.valueOf(jdbcDatabase.getFetchSize()));
            }
        }
    }

    public void update(JDBCDatabaseConnection jdbcDatabase) {
        DBCPConnectionPoolAttr dbcpAttr = jdbcDatabase.getDbcpAttr();
        if (dbcpAttr == null) {
            dbcpAttr = new DBCPConnectionPoolAttr();
            jdbcDatabase.setDbcpAttr(dbcpAttr);
        }
        dbcpAttr.setInitialSize(this.DBCP_INITIAL_SIZE.getValue().intValue());
        dbcpAttr.setMaxActive(this.DBCP_MAX_ACTIVE.getValue().intValue());
        dbcpAttr.setMaxIdle(this.DBCP_MAX_IDLE.getValue().intValue());
        dbcpAttr.setMaxWait(this.DBCP_MAX_WAIT.getValue().intValue());
        dbcpAttr.setMinIdle(this.DBCP_MIN_IDLE.getValue().intValue());
        dbcpAttr.setValidationQuery(this.DBCP_VALIDATION_QUERY.getText());
        dbcpAttr.setTestOnBorrow(this.DBCP_TESTONBORROW.getSelectedIndex() == 0 ? false : true);
        dbcpAttr.setTestOnReturn(this.DBCP_TESTONRETURN.getSelectedIndex() == 0 ? false : true);
        dbcpAttr.setTestWhileIdle(this.DBCP_TESTWHILEIDLE.getSelectedIndex() == 0 ? false : true);
        dbcpAttr.setMinEvictableIdleTimeMillis(((Number) this.DBCP_MINEVICTABLEIDLETIMEMILLIS.getValue()).intValue() * TIME_MULTIPLE);
        dbcpAttr.setNumTestsPerEvictionRun(((Number) this.DBCP_NUMTESTSPEREVICTIONRUN.getValue()).intValue());
        dbcpAttr.setTimeBetweenEvictionRunsMillis(((Number) this.DBCP_TIMEBETWEENEVICTIONRUNSMILLS.getValue()).intValue());
        Integer fetchSize = DEFAULT_FETCHSIZE_MAP.get(JDBCConnectionDef.getInstance().getDatabaseName());
        if (fetchSize != null) {
            if (StringUtils.isEmpty(this.FETCHSIZE.getText())) {
                jdbcDatabase.setFetchSize(EMPTY_FETCHSIZE);
            } else {
                jdbcDatabase.setFetchSize(Integer.parseInt(this.FETCHSIZE.getText()));
            }
        }
    }

    @Override
    public BasicDialog showWindow(Window window) {
        String databaseName = JDBCConnectionDef.getInstance().getDatabaseName();
        if (showOtherConfig(databaseName)) {
            southFlowPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Report_Other"));
            southFlowPane.setPreferredSize(new Dimension(630, 200));
            double f = TableLayout.FILL;
            double[] rowSize = {f};
            double otherColumnSize = GraphHelper.getWidth(Toolkit.i18nText("Fine-Design_Basic_Connection_Pool_Evictionruns_millis")) + 6;
            double[] columnSize = {otherColumnSize, otherColumnSize};
            FETCHSIZE.addKeyListener(fetchSizeKeyListener);
            FETCHSIZE.addInputMethodListener(fetchSizeInputMethodListener);
            FETCHSIZE.setHorizontalAlignment(JTextField.RIGHT);
            Component[][] comps = {
                    {new UILabel("Fetchsize:", SwingConstants.RIGHT), FETCHSIZE}
            };
            JPanel otherConfigPane = TableLayoutHelper.createGapTableLayoutPane(comps, rowSize, columnSize, 10, 4);
            southFlowPane.add(otherConfigPane);

            defaultPane.removeAll();
            defaultPane.add(northFlowPane, BorderLayout.NORTH);
            defaultPane.add(southFlowPane, BorderLayout.SOUTH);
        } else {
            if (southFlowPane != null) {
                defaultPane.remove(southFlowPane);
            }
        }
        return this.showWindow(window, null);
    }

    private boolean showOtherConfig(Object dbType) {
        return ComparatorUtils.equals(dbType, "Oracle") || ComparatorUtils.equals(dbType, "DB2") || ComparatorUtils.equals(dbType, "Postgre");
    }

    KeyListener fetchSizeKeyListener = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            String fetchSize = FETCHSIZE.getText();
            if (!isFetchSizeValid(fetchSize)) {
                FETCHSIZE.setText(fetchSize.replaceAll(getCharNeedReplace(e.getKeyChar()), ""));
                if (!isFetchSizeValid(FETCHSIZE.getText())) {
                    FETCHSIZE.setText(StringUtils.EMPTY);
                }
            } else if (FETCHSIZE.getText().length() > String.valueOf(MAX_FETCHSIZE).length() || Long.parseLong(FETCHSIZE.getText()) > MAX_FETCHSIZE) {
                FETCHSIZE.setText(String.valueOf(MAX_FETCHSIZE));
            }
        }
    };

    InputMethodListener fetchSizeInputMethodListener = new InputMethodListener() {
        @Override
        public void inputMethodTextChanged(InputMethodEvent event) {
            if (null == event.getText()) {
                return;
            }
            char ch = event.getText().current();
            if (!(ch >= '0' && ch <= '9')) {
                event.consume();
            }
        }

        @Override
        public void caretPositionChanged(InputMethodEvent event) {

        }
    };

    private boolean isFetchSizeValid(String fetchSize) {
        return FETCHSIZE_PATTERN.matcher(fetchSize).find();
    }

    private String getCharNeedReplace(char c) {
        String charNeedReplace = c + "";
        Matcher matcher = CHAR_NEED_ESCAPE.matcher(charNeedReplace);
        if (matcher.find()) {
            charNeedReplace = "\\" + charNeedReplace;
        }
        return charNeedReplace;
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Basic_Advanced_Setup");
    }
}
