package com.fr.design.data.datapane.connect;

import com.fr.base.GraphHelper;
import com.fr.data.impl.JDBCDatabaseConnection;
import com.fr.design.border.UITitledBorder;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.ActionLabel;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ipasswordfield.UIPasswordFieldWithFixedLength;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.utils.BrowseUtils;
import com.fr.file.filter.ChooseFileFilter;
import com.fr.general.ComparatorUtils;
import com.fr.i18n.UrlI18nManager;
import com.fr.stable.ArrayUtils;
import com.fr.stable.EncodeConstants;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JDBCDefPane extends JPanel {
    public static final String DRIVER_TYPE = "driver_type";
    public static final String USER_NAME = "user_name";
    private static final String OTHER_DB = "Others";
    private static final Pattern ORACLE_URL = Pattern.compile("^jdbc:oracle:thin:@[/]*([-0-9a-zA-Z_\\.\\\\]+)(:([0-9]+|port))?([:/](.*))?.*", Pattern.CASE_INSENSITIVE);
    private static final Pattern GENERAL_URL = Pattern.compile("^jdbc:(mysql|sqlserver|db2|derby|postgresql|inceptor|inceptor2|hive2)://([-0-9a-zA-Z_\\.\\\\]+)(:([0-9]+|port))?((/|;DatabaseName=)(.*))?.*", Pattern.CASE_INSENSITIVE);
    private static final Pattern PORT = Pattern.compile("^0$|^[1-9][\\d]*[\\d]*$");
    private static final Pattern CHAR_NEED_ESCAPE = Pattern.compile("[?|$^*\\\\\\[\\](){}.+]");
    // 编码转换.
    private String originalCharSet = null;
    private static Map<String, DriverURLName[]> jdbcMap = new HashMap<String, DriverURLName[]>();

    static {
        jdbcMap.put(OTHER_DB, new DriverURLName[]{new DriverURLName("sun.jdbc.odbc.JdbcOdbcDriver", "jdbc:odbc:"),
                new DriverURLName("org.hsqldb.jdbcDriver", "jdbc:hsqldb:file:[PATH_TO_DB_FILES]"), new DriverURLName("com.inet.tds.TdsDriver", "jdbc:inetdae7:localhost:1433/"),
                new DriverURLName("COM.cloudscape.JDBCDriver", "jdbc:cloudscape:/cloudscape/"),
                new DriverURLName("com.internetcds.jdbc.tds.Driver", "jdbc:freetds:sqlserver://localhost/"),
                new DriverURLName("com.fr.swift.jdbc.Driver", "jdbc:swift:emb://default")});
        jdbcMap.put("Inceptor", new DriverURLName[]{new DriverURLName("org.apache.hive.jdbc.HiveDriver", "jdbc:inceptor2://localhost:port/databaseName"),
                new DriverURLName("org.apache.hadoop.hive.jdbc.HiveDriver", "jdbc:inceptor://localhost:port/databaseName")});
        jdbcMap.put("Oracle", new DriverURLName[]{new DriverURLName("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@localhost:port:databaseName")});
        jdbcMap.put("DB2", new DriverURLName[]{new DriverURLName("com.ibm.db2.jcc.DB2Driver", "jdbc:db2://localhost:port/databaseName")});
        jdbcMap.put("SQL Server", new DriverURLName[]{new DriverURLName("com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver://localhost:port;databaseName=databaseName")});
        jdbcMap.put("MySQL", new DriverURLName[]{new DriverURLName("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:port/databaseName"),
                new DriverURLName("org.gjt.mm.mysql.Driver", "jdbc:mysql://localhost:port/databaseName")});
        jdbcMap.put("Sybase", new DriverURLName[]{new DriverURLName("com.sybase.jdbc2.jdbc.SybDriver", "jdbc:sybase:Tds:localhost:port/databaseName")});
        jdbcMap.put("Derby", new DriverURLName[]{new DriverURLName("org.apache.derby.jdbc.ClientDriver", "jdbc:derby://localhost:port/databaseName")});
        jdbcMap.put("Postgre", new DriverURLName[]{new DriverURLName("org.postgresql.Driver", "jdbc:postgresql://localhost:port/databaseName")});

        jdbcMap.put("Access", new DriverURLName[]{new DriverURLName("sun.jdbc.odbc.JdbcOdbcDriver", "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=")});
        jdbcMap.put("SQLite", new DriverURLName[]{new DriverURLName("org.sqlite.JDBC", "jdbc:sqlite://${ENV_HOME}/../help/FRDemo.db")});
    }

    private UIButton dbtypeButton;
    private UIComboBox dbtypeComboBox;
    private UIComboBox driverComboBox;
    private UITextField dbNameTextField;
    private UITextField hostTextField;
    private UITextField portTextField;
    private UITextField urlTextField;
    private UITextField userNameTextField;
    private JPasswordField passwordTextField;
    private UIComboBox charSetComboBox;
    private ActionLabel odbcTipsLink;
    private JPanel centerPanel;
    private Component[][] allComponents;
    private Component[][] partComponents;
    // 请不要改动dbtype,只应该最后添加
    private final String[] dbtype = {"Oracle", "DB2", "SQL Server", "MySQL", "Sybase", "Access", "Derby", "Postgre", "SQLite", "Inceptor", OTHER_DB};


    private JDBCDatabaseConnection jdbcDatabase;
    private boolean needRefresh = true;

    public JDBCDefPane() {
        this.setBorder(UITitledBorder.createBorderWithTitle("JDBC" + ":"));
        this.setLayout(FRGUIPaneFactory.createLabelFlowLayout());
        JPanel innerthis = FRGUIPaneFactory.createY_AXISBoxInnerContainer_L_Pane();
        innerthis.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.add(innerthis);
        dbtypeComboBox = new UIComboBox();
        dbtypeComboBox.setName(DRIVER_TYPE);
        for (int i = 0; i < dbtype.length; i++) {
            dbtypeComboBox.addItem(dbtype[i]);
        }
        dbtypeComboBox.addActionListener(dbtypeActionListener);
        dbtypeComboBox.setMaximumRowCount(10);

        driverComboBox = new UIComboBox();
        driverComboBox.setEditable(true);
        driverComboBox.addActionListener(driverListener);
        dbNameTextField = new UITextField(15);
        hostTextField = new UITextField(15);
        portTextField = new UITextField(15);
        portTextField.addInputMethodListener(portInputMethodListener);
        portTextField.addKeyListener(portKeyListener);
        urlTextField = new UITextField(15);
        urlTextField.getDocument().addDocumentListener(updateParaListener);
        enableSubDocListener();
        userNameTextField = new UITextField(15);
        userNameTextField.setName(USER_NAME);
        passwordTextField = new UIPasswordFieldWithFixedLength(15);
        dbtypeButton = new UIButton(".");
        dbtypeButton.setToolTipText(Toolkit.i18nText("Fine-Design_Basic_Click_Get_Default_URL"));
        dbtypeButton.addActionListener(dbtypeButtonActionListener);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        JPanel dbtypePane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        dbtypePane.add(new UILabel((Toolkit.i18nText("Fine-Design_Basic_Database") + ":")));
        Component[][] dbtypeComComponents = {{dbtypeComboBox}};
        double[] dbtypeRowSize = {p};
        double[] dbtypeColumnSize = {p};
        JPanel dbtypeComPane = TableLayoutHelper.createTableLayoutPane(dbtypeComComponents, dbtypeRowSize, dbtypeColumnSize);

        JPanel driverPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        driverPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Basic_Driver") + ":"));
        // 选择ODBC数据源的时候的提示链接
        JPanel odbcTipsPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        odbcTipsLink = new ActionLabel(Toolkit.i18nText("Fine-Design_Basic_Odbc_Tips")) {
            @Override
            public void paintComponent(Graphics _gfx) {
                super.paintComponent(_gfx);
                _gfx.setColor(Color.blue);
                _gfx.drawLine(0, this.getHeight() - 1, GraphHelper.getWidth(this.getText()), this.getHeight() - 1);
            }
        };
        odbcTipsPane.add(odbcTipsLink);
        odbcTipsLink.setPreferredSize(new Dimension(GraphHelper.getWidth(Toolkit.i18nText("Fine-Design_Basic_Odbc_Tips")), odbcTipsLink.getPreferredSize().height));
        odbcTipsLink.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String url = UrlI18nManager.getInstance().getI18nUrl("odbc.help");
                BrowseUtils.browser(url);
            }
        });

        JPanel driverComboBoxAndTips = new JPanel(new BorderLayout());
        driverComboBoxAndTips.add(driverComboBox, BorderLayout.WEST);
        driverComboBoxAndTips.add(odbcTipsPane, BorderLayout.CENTER);

        JPanel hostPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        hostPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Basic_Host") + ":"));
        Component[][] hostComComponents = {{hostTextField}};
        double[] hostRowSize = {p};
        double[] hostColumnSize = {p};
        JPanel hostComPane = TableLayoutHelper.createTableLayoutPane(hostComComponents, hostRowSize, hostColumnSize);

        JPanel portPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        portPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Basic_Port") + ":"));
        Component[][] portComComponents = {{portTextField}};
        double[] portRowSize = {p};
        double[] portColumnSize = {p};
        JPanel portComPane = TableLayoutHelper.createTableLayoutPane(portComComponents, portRowSize, portColumnSize);

        JPanel dbNamePane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        dbNamePane.add(new UILabel(Toolkit.i18nText("Fine-Design_Basic_DatabaseName") + ":"));
        Component[][] dbNameComComponents = {{dbNameTextField}};
        double[] dbNameRowSize = {p};
        double[] dbNameColumnSize = {p};
        JPanel dbNameComPane = TableLayoutHelper.createTableLayoutPane(dbNameComComponents, dbNameRowSize, dbNameColumnSize);

        JPanel urlPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        urlPane.add(new UILabel("URL:"));
        Component[][] urlComComponents = {{urlTextField, dbtypeButton}};
        double[] urlRowSize = {p};
        double[] urlColumnSize = {f, 21};
        JPanel urlComPane = TableLayoutHelper.createCommonTableLayoutPane(urlComComponents, urlRowSize, urlColumnSize, 4);

        JPanel userPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        userPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Report_UserName") + ":"));
        Component[][] userComComponents = {{userNameTextField, new UILabel(Toolkit.i18nText("Fine-Design_Basic_Password") + ":"), passwordTextField}};
        double[] userRowSize = {p};
        double[] userColumnSize = {f, p, f};
        JPanel userComPane = TableLayoutHelper.createCommonTableLayoutPane(userComComponents, userRowSize, userColumnSize, 4);

        String[] defaultEncode = new String[]{Toolkit.i18nText("Fine-Design_Encode_Auto")};
        charSetComboBox = new UIComboBox(ArrayUtils.addAll(defaultEncode, EncodeConstants.ENCODING_ARRAY));
        JPanel chartSetPane = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        chartSetPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Basic_Datasource_Charset") + ":"));
        Component[][] charSetComComponents = {{charSetComboBox}};
        double[] charSetRowSize = {p};
        double[] charSetColumnSize = {f};
        JPanel charSetComPane = TableLayoutHelper.createTableLayoutPane(charSetComComponents, charSetRowSize, charSetColumnSize);

        //这边调整的话注意下面的changePane
        allComponents = new Component[][]{{dbtypePane, dbtypeComPane}, {driverPane, driverComboBoxAndTips}, {hostPane, hostComPane},
                {portPane, portComPane}, {dbNamePane, dbNameComPane}, {userPane, userComPane},
                {chartSetPane, charSetComPane}, {urlPane, urlComPane}};
        partComponents = new Component[][]{{dbtypePane, dbtypeComPane}, {driverPane, driverComboBoxAndTips}, {urlPane, urlComPane},
                {userPane, userComPane}, {chartSetPane, charSetComPane}};
        double[] rowSize = {p, p, p, p, p, p, p, p};
        double[] columnSize = {p, f, 22};
        // REPORT-41450 Windows环境的jdk11下dpi为125%时会因为缩放导致显示问题，因此加个水平gap值
        centerPanel = TableLayoutHelper.createGapTableLayoutPane(allComponents, rowSize, columnSize, 6, 6);
        innerthis.add(centerPanel);
    }

    public void populate(JDBCDatabaseConnection jdbcDatabase) {
        needRefresh = false;
        if (jdbcDatabase == null) {
            jdbcDatabase = new JDBCDatabaseConnection();
        }
        this.jdbcDatabase = jdbcDatabase;
        if (ComparatorUtils.equals(jdbcDatabase.getDriver(), "sun.jdbc.odbc.JdbcOdbcDriver")
                && jdbcDatabase.getURL().startsWith("jdbc:odbc:Driver={Microsoft")) {
            this.dbtypeComboBox.setSelectedItem("Access");
        } else {
            Iterator<Entry<String, DriverURLName[]>> jdbc = jdbcMap.entrySet().iterator();
            boolean out = false;
            while (jdbc.hasNext()) {
                Entry<String, DriverURLName[]> entry = jdbc.next();
                DriverURLName[] dus = entry.getValue();
                for (int i = 0, len = dus.length; i < len; i++) {
                    if (ComparatorUtils.equals(dus[i].getDriver(), jdbcDatabase.getDriver())) {
                        this.dbtypeComboBox.setSelectedItem(entry.getKey());
                        out = true;
                        break;
                    }
                }
                if (out) {
                    break;
                }
            }
            if (!out) {
                this.dbtypeComboBox.setSelectedItem(OTHER_DB);
            }
        }
        this.driverComboBox.setSelectedItem(jdbcDatabase.getDriver());
        this.urlTextField.setText(jdbcDatabase.getURL());
        this.userNameTextField.setText(jdbcDatabase.getUser());
        this.passwordTextField.setText(jdbcDatabase.getPassword());
        this.originalCharSet = jdbcDatabase.getOriginalCharsetName();
        if (StringUtils.isBlank(originalCharSet)) {
            this.charSetComboBox.setSelectedItem(Toolkit.i18nText("Fine-Design_Encode_Auto"));
        } else {
            this.charSetComboBox.setSelectedItem(jdbcDatabase.getOriginalCharsetName());
        }
        needRefresh = false;
    }

    protected JDBCDatabaseConnection getJDBCDatabase() {
        return this.jdbcDatabase;
    }

    private void changePane(Object dbType) {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] columnSize = {p, f, 22};
        if (ComparatorUtils.equals(dbType, OTHER_DB) || ComparatorUtils.equals(dbType, "Access") || ComparatorUtils.equals(dbType, "SQLite")) {
            if (this.centerPanel.getComponentCount() != partComponents.length * 2) {
                centerPanel.removeAll();
                TableLayoutHelper.addComponent2ResultPane(partComponents, new double[]{p, p, p, p, p}, columnSize, centerPanel);
            }
        } else if (this.centerPanel.getComponentCount() != allComponents.length * 2) {
            centerPanel.removeAll();
            TableLayoutHelper.addComponent2ResultPane(allComponents, new double[]{p, p, p, p, p, p, p, p}, columnSize, centerPanel);
        }
    }

    public JDBCDatabaseConnection update() {
        if (jdbcDatabase == null) {
            jdbcDatabase = new JDBCDatabaseConnection();
        }
        Object driveItem = this.driverComboBox.getSelectedItem();
        jdbcDatabase.setDriver(driveItem == null ? null : driveItem.toString().trim());
        jdbcDatabase.setURL(this.urlTextField.getText().trim());
        jdbcDatabase.setUser(this.userNameTextField.getText().trim());
        jdbcDatabase.setPassword(new String(this.passwordTextField.getPassword()).trim());
        jdbcDatabase.setOriginalCharsetName(this.originalCharSet);
        if (this.charSetComboBox.getSelectedIndex() == 0) {
            jdbcDatabase.setNewCharsetName(null);
            jdbcDatabase.setOriginalCharsetName(null);
        } else {
            jdbcDatabase.setNewCharsetName(EncodeConstants.ENCODING_GBK);
            jdbcDatabase.setOriginalCharsetName(((String) this.charSetComboBox.getSelectedItem()));
        }
        return jdbcDatabase;
    }

    ActionListener dbtypeActionListener = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {

            urlTextField.setText(StringUtils.EMPTY);
            driverComboBox.removeAllItems();
            if (ComparatorUtils.equals(dbtypeComboBox.getSelectedItem(), StringUtils.EMPTY)) {
                driverComboBox.setSelectedItem(StringUtils.EMPTY);
                return;
            }

            DriverURLName[] dus = jdbcMap.get(dbtypeComboBox.getSelectedItem());
            for (int i = 0, len = dus.length; i < len; i++) {
                driverComboBox.addItem(dus[i].getDriver());
                if (i == 0) {
                    driverComboBox.setSelectedItem(dus[i].getDriver());
                    urlTextField.setText(dus[i].getURL());
                }
            }
            // 更改数据库类型后 数据库名称置空和之前逻辑保持一致
            if (needRefresh) {
                jdbcDatabase.setDatabase(StringUtils.EMPTY);
            }
            changePane(dbtypeComboBox.getSelectedItem());
            JDBCConnectionDef.getInstance().setConnection((String) dbtypeComboBox.getSelectedItem(), jdbcDatabase);
            DatabaseConnectionPane.JDBC.getAdvancedAttrPane().populate(jdbcDatabase);
        }
    };

    ActionListener driverListener = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if (driverComboBox.getSelectedItem() == null || ComparatorUtils.equals(driverComboBox.getSelectedItem(), StringUtils.EMPTY)) {
                return;
            }
            odbcTipsLink.setVisible(ComparatorUtils.equals("sun.jdbc.odbc.JdbcOdbcDriver", driverComboBox.getSelectedItem())); // 选择的如果是ODBC就显示提示
            Iterator<Entry<String, DriverURLName[]>> jdbc = jdbcMap.entrySet().iterator();
            while (jdbc.hasNext()) {
                Entry<String, DriverURLName[]> entry = jdbc.next();
                DriverURLName[] dus = entry.getValue();
                for (int i = 0, len = dus.length; i < len; i++) {
                    if (ComparatorUtils.equals(dus[i].getDriver(), (driverComboBox.getSelectedItem()))) {
                        urlTextField.setText(dus[i].getURL());
                        return;
                    }
                }
            }
        }

    };

    ActionListener dbtypeButtonActionListener = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
            if (ComparatorUtils.equals(dbtypeComboBox.getSelectedItem(), StringUtils.EMPTY)) {
                return;
            }
            DriverURLName[] dus = jdbcMap.get(dbtypeComboBox.getSelectedItem());
            for (int i = 0, len = dus.length; i < len; i++) {
                if (ComparatorUtils.equals(driverComboBox.getSelectedItem(), (dus[i].getDriver()))) {
                    urlTextField.setText(dus[i].getURL());
                    if (ComparatorUtils.equals(dbtypeComboBox.getSelectedItem(), ("Access"))) {
                        // ben:这个能不能换种处理方案- -
                        JFileChooser filechooser = new JFileChooser();
                        filechooser.setDialogTitle(Toolkit.i18nText("Fine-Design_Basic_Open"));
                        filechooser.setMultiSelectionEnabled(false);
                        filechooser.addChoosableFileFilter(new ChooseFileFilter(new String[]{"accdb", "mdb"}, "Microsoft Office Access"));
                        int result = filechooser.showOpenDialog(DesignerContext.getDesignerFrame());
                        File selectedfile = null;

                        if (result == JFileChooser.APPROVE_OPTION) {
                            selectedfile = filechooser.getSelectedFile();
                            if (selectedfile != null) {
                                String selectedName = selectedfile.getPath().substring(selectedfile.getPath().lastIndexOf('.') + 1);
                                if (selectedName.equalsIgnoreCase("mdb") || selectedName.equalsIgnoreCase("accdb")) {
                                    urlTextField.setText(urlTextField.getText() + selectedfile.getPath());
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    };

    InputMethodListener portInputMethodListener = new InputMethodListener() {
        @Override
        public void inputMethodTextChanged(InputMethodEvent event) {
            if (null == event.getText()) {
                return;
            }
            char ch = event.getText().current();
            if (!(ch >= '0' && ch <= '9')) {
                event.consume();
            }
        }

        @Override
        public void caretPositionChanged(InputMethodEvent event) {

        }
    };

    DocumentListener updateParaListener = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            updatePara();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updatePara();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updatePara();
        }
    };

    private void updatePara() {
        String dbType = dbtypeComboBox.getSelectedItem().toString();
        if (ComparatorUtils.equals(dbType, OTHER_DB) || ComparatorUtils.equals(dbType, "Access") || ComparatorUtils.equals(dbType, "SQLite")) {
            return;
        }
        disableSubDocListener();
        String url = urlTextField.getText().trim();
        Matcher matcher = ORACLE_URL.matcher(url);
        if (matcher.find()) {
            if (matcher.group(1) != null) {
                hostTextField.setText(matcher.group(1));
            } else {
                hostTextField.setText(StringUtils.EMPTY);
            }
            if (matcher.group(3) != null) {
                portTextField.setText(matcher.group(3));
            } else {
                portTextField.setText(StringUtils.EMPTY);
            }
            if (matcher.group(5) != null) {
                dbNameTextField.setText(matcher.group(5));
            } else {
                dbNameTextField.setText(StringUtils.EMPTY);
            }
            enableSubDocListener();
            return;
        }
        matcher = GENERAL_URL.matcher(url);
        if (matcher.find()) {
            if (matcher.group(2) != null) {
                hostTextField.setText(matcher.group(2));
            } else {
                hostTextField.setText(StringUtils.EMPTY);
            }
            if (matcher.group(4) != null) {
                portTextField.setText(matcher.group(4));
            } else {
                portTextField.setText(StringUtils.EMPTY);
            }
            if (matcher.group(7) != null) {
                dbNameTextField.setText(matcher.group(7));
            } else {
                dbNameTextField.setText(StringUtils.EMPTY);
            }
            enableSubDocListener();
            return;
        }
        hostTextField.setText(StringUtils.EMPTY);
        portTextField.setText(StringUtils.EMPTY);
        dbNameTextField.setText(StringUtils.EMPTY);
        enableSubDocListener();
    }

    private void enableSubDocListener() {
        this.dbNameTextField.getDocument().addDocumentListener(updateURLListener);
        this.hostTextField.getDocument().addDocumentListener(updateURLListener);
    }

    private void disableSubDocListener() {
        this.dbNameTextField.getDocument().removeDocumentListener(updateURLListener);
        this.hostTextField.getDocument().removeDocumentListener(updateURLListener);
    }

    KeyListener portKeyListener = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            String port = portTextField.getText();
            if (isPortValid(port)) {
                updateURL();
            } else {
                portTextField.setText(port.replaceAll(getCharNeedReplace(e.getKeyChar()), ""));
                if (!isPortValid(portTextField.getText())) {
                    portTextField.setText(StringUtils.EMPTY);
                    updateURL();
                }
            }
        }
    };

    private boolean isPortValid(String port) {
        return PORT.matcher(port).find();
    }

    private String getCharNeedReplace(char c) {
        String charNeedReplace = c + "";
        Matcher matcher = CHAR_NEED_ESCAPE.matcher(charNeedReplace);
        if (matcher.find()) {
            charNeedReplace = "\\" + charNeedReplace;
        }
        return charNeedReplace;
    }

    DocumentListener updateURLListener = new DocumentListener() {

        @Override
        public void insertUpdate(DocumentEvent e) {
            updateURL();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateURL();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateURL();
        }
    };

    private void updateURL() {
        String dbType = dbtypeComboBox.getSelectedItem().toString();
        if (!ComparatorUtils.equals(dbType, OTHER_DB) && !ComparatorUtils.equals(dbType, "Access") && !ComparatorUtils.equals(dbType, "SQLite")) {
            urlTextField.getDocument().removeDocumentListener(updateParaListener);
            DriverURLName[] driverURLNames = jdbcMap.get(dbType);
            String defaultURL = null;
            for (DriverURLName driverURLName : driverURLNames) {
                if (ComparatorUtils.equals(driverURLName.getDriver(), driverComboBox.getSelectedItem().toString())) {
                    defaultURL = driverURLName.getURL();
                }
            }
            if (defaultURL != null) {
                String host = hostTextField.getText().trim();
                String port = portTextField.getText().trim();
                String dbName = dbNameTextField.getText().trim();
                defaultURL = defaultURL.replace("localhost", host).replace(":port", ComparatorUtils.equals(port, "") ? "" : ":" + port);
                if (defaultURL.startsWith("jdbc:sqlserver")) {
                    defaultURL = defaultURL.replace("=databaseName", "=" + dbName);
                } else {
                    defaultURL = defaultURL.replace("databaseName", dbName);
                }
            }
            urlTextField.setText(defaultURL);
            urlTextField.getDocument().addDocumentListener(updateParaListener);
        }
    }

    private static class DriverURLName {
        public DriverURLName(String driver, String url) {
            this.driver = driver;
            this.url = url;
        }

        public String getDriver() {
            return this.driver;
        }

        public String getURL() {
            return this.url;
        }

        private String driver;
        private String url;
    }
}
