package com.fr.design.update.actions;

import com.fr.decision.update.SyncExecutor;
import com.fr.decision.update.info.UpdateCallBack;
import com.fr.decision.update.info.UpdateProgressCallBack;
import com.fr.design.i18n.Toolkit;
import com.fr.design.versioncheck.VersionCheckUtils;
import com.fr.env.SyncFailedPluginsDialog;
import com.fr.json.JSONArray;
import com.fr.log.FineLoggerFactory;
import java.util.concurrent.ExecutionException;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

public abstract class SyncFileProcess extends SwingWorker<Boolean, Void> {
    private String buildNo;
    private JProgressBar bar;
    private SyncFailedPluginsDialog syncFailedPluginsDialog;

    public SyncFileProcess(JProgressBar bar, String buildNo, SyncFailedPluginsDialog syncFailedPluginsDialog) {
        this.bar = bar;
        this.buildNo = buildNo;
        this.syncFailedPluginsDialog = syncFailedPluginsDialog;
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        UpdateCallBack callBack = new UpdateProgressCallBack(bar);
        bar.setValue(0);
        bar.setString(Toolkit.i18nText("Fine-Design_Basic_Sync_Plugins"));
        JSONArray syncFailedPlugins = VersionCheckUtils.syncPlugins(VersionCheckUtils.checkLocalAndRemotePlugin());
        if (syncFailedPlugins.size() > 0) {
            syncFailedPluginsDialog.showSyncFailedPluginsInfo(syncFailedPlugins);
        }
        RecoverForDesigner recoverForDesigner = new RecoverForDesigner();
        if (!recoverForDesigner.backup()) {
            return false;
        }
        boolean result = SyncExecutor.getInstance().execute(callBack, buildNo);
        if (!result) {
            recoverForDesigner.recover();
        }
        return result;
    }

    @Override
    protected void done() {
        boolean success = false;
        try {
            success = get();
        } catch (InterruptedException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        if (success) {
            onDownloadSuccess();
        } else {
            onDownloadFailed();
        }
    }


    /**
     * 下载成功
     */
    public abstract void onDownloadSuccess();

    /**
     * 下载失败
     */
    public abstract void onDownloadFailed();
}
