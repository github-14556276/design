package com.fr.design.update.factory;

import com.fr.decision.update.data.UpdateConstants;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StableUtils;

import java.io.File;;

/**
 * @author Bryant
 * @version 10.0
 * Created by Bryant on 2020-09-29
 */
public class UpdateFileFactory {

    private UpdateFileFactory() {
    }

    public static File[] getBackupVersions() {
        File[] versions = null;
        try {
            File backupDir = new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR));
            StableUtils.mkdirs(backupDir);
            versions = backupDir.listFiles();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return versions;
    }

    public static boolean isBackupVersionsValid(String version) {
        boolean designerValid = false;
        boolean envValid = false;
        try {
            File designerLib = new File(StableUtils.pathJoin(version, UpdateConstants.DESIGNERBACKUPPATH));
            File[] jars = designerLib.listFiles();
            if (jars != null && jars.length > 0) {
                designerValid = true;
            }
            File envLib = new File(StableUtils.pathJoin(version, UpdateConstants.BACKUPPATH));
            jars = envLib.listFiles();
            if (jars != null && jars.length > 0) {
                envValid = true;
            }
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return designerValid && envValid;
    }
}
