package com.fr.design.notification;


import com.fr.design.notification.ui.NotificationCenterPane;
import java.util.ArrayList;
import java.util.List;

public class NotificationCenter {
    private static NotificationCenter notificationCenter = new NotificationCenter();
    private List<Notification> notifications;
    private NotificationCenter(){
        notifications = new ArrayList<>();
    }

    public static NotificationCenter getInstance(){
        return notificationCenter;
    }

    public void addNotification(Notification message){
        notifications.add(message);
        NotificationCenterPane.getNotificationCenterPane().refreshButton();
    }

    public int getNotificationsCount(){
        return notifications.size();
    }

    public void removeNotification(int index){
        notifications.remove(index);
        NotificationCenterPane.getNotificationCenterPane().refreshButton();
    }

    public Notification getNotification(int index){
        return notifications.get(index);
    }

    public void clearAllNotifications(){
        notifications.clear();
        NotificationCenterPane.getNotificationCenterPane().refreshButton();
    }

}
