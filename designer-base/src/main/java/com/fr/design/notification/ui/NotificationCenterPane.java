package com.fr.design.notification.ui;

import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.notification.NotificationCenter;
import com.fr.general.IOUtils;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class NotificationCenterPane extends BasicPane {
    private static NotificationCenterPane notificationCenterPane = new NotificationCenterPane();
    private static UIButton notificationCenterButton;

    private NotificationCenterPane() {
        setPreferredSize(new Dimension(24, 24));
        setLayout(new BorderLayout());
        notificationCenterButton = new UIButton();
        notificationCenterButton.setIcon(IOUtils.readIcon("/com/fr/design/notification/ui/notificationCenter.png"));
        notificationCenterButton.setToolTipText(Toolkit.i18nText("Fine-Design_Basic_Show_Notification"));
        notificationCenterButton.set4ToolbarButton();
        notificationCenterButton.setRolloverEnabled(false);
        this.add(notificationCenterButton);
        notificationCenterButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                NotificationCenterDialog notificationCenterDialog = new NotificationCenterDialog(DesignerContext.getDesignerFrame());
                notificationCenterDialog.showDialog();
            }
        });
        this.setBackground(UIConstants.TEMPLATE_TAB_PANE_BACKGROUND);
    }

    public static NotificationCenterPane getNotificationCenterPane() {
        return notificationCenterPane;
    }

    public void refreshButton() {
        if (NotificationCenter.getInstance().getNotificationsCount() > 0) {
            notificationCenterButton.setIcon(IOUtils.readIcon("/com/fr/design/notification/ui/notificationCenterDot.png"));
        } else {
            notificationCenterButton.setIcon(IOUtils.readIcon("/com/fr/design/notification/ui/notificationCenter.png"));
        }
    }

    @Override
    protected String title4PopupWindow() {
        return "NotificationCenter";
    }
}
