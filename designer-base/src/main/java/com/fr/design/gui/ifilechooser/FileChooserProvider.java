package com.fr.design.gui.ifilechooser;

import java.awt.*;
import java.io.File;

public interface FileChooserProvider {
    File[] getSelectedFiles();

    File getSelectedFile();

    int showDialog(Component parent);


    default int showOpenDialog(Component parent, String approveButtonText) {
        return -1;
    }

    default void setMultiSelectionEnabled(boolean multiple) {

    }

    default void setCurrentDirectory(File file) {

    }
}
