package com.fr.design.gui.frpane;

import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPercent;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-01-21
 */
public class UINumberDragPaneWithPercent extends UINumberDragPane {

    public UINumberDragPaneWithPercent(double minValue, double maxValue) {
        super(minValue, maxValue);
    }

    public UINumberDragPaneWithPercent(double minValue, double maxValue, double dierta) {
        super(minValue, maxValue, dierta);
    }

    protected UISpinner createUISpinner(double minValue, double maxValue, double dierta) {
        return new UISpinnerWithPercent(minValue, maxValue, dierta, minValue);
    }
}
