package com.fr.design.gui.icombobox;

import com.fr.log.FineLoggerFactory;

import javax.swing.JTree;
import javax.swing.SwingWorker;
import javax.swing.tree.TreeCellRenderer;
import java.util.concurrent.FutureTask;

/**
 * 模糊搜索前需执行完前置任务的TreeComboBox
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2021/4/14
 */
public class SearchPreTaskTreeComboBox extends FRTreeComboBox {

    /**
     * 模糊搜索前任务
     */
    private FutureTask<Void> preSearchTask;

    public SearchPreTaskTreeComboBox(JTree tree, TreeCellRenderer renderer, boolean editable) {
        super(tree, renderer, editable);
    }

    public FutureTask<Void> getPreSearchTask() {
        return preSearchTask;
    }

    public void setPreSearchTask(FutureTask<Void> preSearchTask) {
        this.preSearchTask = preSearchTask;
    }

    protected UIComboBoxEditor createEditor() {
        return new SearchPreTaskComboBoxEditor(this);
    }

    private class SearchPreTaskComboBoxEditor extends FrTreeSearchComboBoxEditor {

        public SearchPreTaskComboBoxEditor(FRTreeComboBox comboBox) {
            super(comboBox);
        }

        protected void changeHandler() {
            if (isSetting()) {
                return;
            }
            setPopupVisible(true);
            new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    FutureTask<Void> task = getPreSearchTask();
                    try {
                        // 确保模糊搜索前任务执行完成后，再进行模糊搜索
                        if (task != null) {
                            task.get();
                        }
                    } catch (Exception e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                    if (task != null) {
                        // 任务执行后置空，否则会被别的操作重复触发
                        setPreSearchTask(null);
                    }
                    return null;
                }

                @Override
                protected void done() {
                    // 模糊搜索
                    search();
                }
            }.execute();
        }
    }
}
