package com.fr.design.gui.ibutton;

import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.JTemplate;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.plaf.ButtonUI;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/10
 */
public class UISaveForbiddenButton extends UIButton {
    public UISaveForbiddenButton() {
    }

    public UISaveForbiddenButton(String string) {
        super(string);
    }

    public UISaveForbiddenButton(Icon icon) {
        super(icon);
    }

    public UISaveForbiddenButton(Action action) {
        super(action);
    }

    public UISaveForbiddenButton(String text, Icon icon) {
        super(text, icon);
    }

    public UISaveForbiddenButton(Icon normal, Icon rollOver, Icon pressed) {
        super(normal, rollOver, pressed);
    }

    public UISaveForbiddenButton(String resource, boolean needSetDisabledIcon) {
        super(resource, needSetDisabledIcon);
    }

    @Override
    public boolean isEnabled() {
        JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        boolean enabled = true;
        if (template != null) {
            enabled = !template.isSaving();
        }
        return enabled;
    }

    @Override
    public ButtonUI getUI() {
        return new UISaveForbiddenButtonUI();
    }
}
