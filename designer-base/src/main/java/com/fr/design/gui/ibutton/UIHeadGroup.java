package com.fr.design.gui.ibutton;

import com.fr.base.BaseUtils;
import com.fr.design.constants.UIConstants;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.design.utils.gui.UIComponentUtils;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

// fanglei：不是原作者，只是优化如下问题：代码冗余，无法拓展（例如我想加个enable属性没法加），甚至还有数组越界的问题。
public class UIHeadGroup extends JPanel {
    private static final int MIN_HEIGHT = 25;
    private List<UIHead> uiHeads = new ArrayList<>();
    protected List<UIToggleButton> labelButtonList;
    private boolean isNeedLeftRightOutLine = true;
    protected int selectedIndex = -1;

    protected void tabChanged(int newSelectedIndex) {
        // do nothing
    }

    public UIHeadGroup(String[] textArray) {
        for (int i = 0; i < textArray.length; i++) {
            uiHeads.add(new UIHead(textArray[i], i));
        }
        initUIHeadGroup(uiHeads);
    }

    public UIHeadGroup(Icon[] iconArray) {
        for (int i = 0; i < iconArray.length; i++) {
            uiHeads.add(new UIHead(iconArray[i], i));
        }
        initUIHeadGroup(uiHeads);
    }

    public UIHeadGroup(Icon[] iconArray, String[] textArray) {
        int length = Math.min(textArray.length, iconArray.length);
        for (int i = 0; i < length; i++) {
            uiHeads.add(new UIHead(textArray[i], iconArray[i], i));
        }
        initUIHeadGroup(uiHeads);
    }

    public UIHeadGroup(List<UIHead> uiHeads) {
        initUIHeadGroup(uiHeads);
    }

    public void initUIHeadGroup(List<UIHead> uiHeads) {
        if (uiHeads != null) {
            labelButtonList = new ArrayList<UIToggleButton>(uiHeads.size());
            this.setLayout(new GridLayout(0, uiHeads.size(), 1, 0));

            for (UIHead head : uiHeads) {
                if (head.isOnlyText()) {
                    this.setBackground(UIConstants.TREE_BACKGROUND);
                } else {
                    this.setBackground(UIConstants.NORMAL_BACKGROUND);
                }
                initButton(createUIToggleButton(head));
            }
            setSelectedIndex(0);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dim = super.getPreferredSize();
        if (dim.height < MIN_HEIGHT) {
            dim.height = MIN_HEIGHT;
        }
        return dim;
    }

    private void initButton(UIToggleButton labelButton) {
        labelButton.setRoundBorder(false);
        labelButton.setBorderPainted(false);
        labelButton.setPressedPainted(false);
        UIComponentUtils.setLineWrap(labelButton);
        labelButtonList.add(labelButton);
        this.add(labelButton);
    }

    public void setSelectedIndex(int newSelectedIndex) {
        selectedIndex = newSelectedIndex;
        for (int i = 0; i < labelButtonList.size(); i++) {
            UIToggleButton button = labelButtonList.get(i);
            if (i == selectedIndex) {
                button.setNormalPainted(false);
                button.setBackground(new Color(240, 240, 243));
                button.setOpaque(true);
                button.setSelected(true);
            } else {
                button.setOpaque(false);
                button.setNormalPainted(true);
                button.setSelected(false);
            }
        }

        tabChanged(newSelectedIndex);
    }

    public void setNeedLeftRightOutLine(boolean isNeedLeftRightOutLine) {
        this.isNeedLeftRightOutLine = isNeedLeftRightOutLine;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public UIToggleButton createUIToggleButton(final UIHead head) {
        UIToggleButton uiToggleButton = new UIToggleButton(head.getText(), head.getIcon()) {
            @Override
            protected MouseListener getMouseListener() {
                return new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        if (head.isEnable()) {
                            setSelectedIndex(head.getIndex());
                            UIHeadGroup.this.repaint();
                        }
                    }
                };
            }
        };

        uiToggleButton.setEnabled(head.isEnable());
        return uiToggleButton;
    }

    public static void main(String... args) {
        JFrame jf = new JFrame("test");
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel content = (JPanel) jf.getContentPane();
        content.setLayout(null);
        Icon[] a1 = {BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_left_normal.png"), BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_center_normal.png"),
                BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_right_normal.png")};
        UIHeadGroup bb = new UIHeadGroup(a1);
        bb.setBounds(20, 20, bb.getPreferredSize().width, bb.getPreferredSize().height);
        bb.setSelectedIndex(0);
        content.add(bb);
        GUICoreUtils.centerWindow(jf);
        jf.setSize(400, 400);
        jf.setVisible(true);
    }
}