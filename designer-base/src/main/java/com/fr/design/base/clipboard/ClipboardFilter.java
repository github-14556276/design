package com.fr.design.base.clipboard;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.fun.ClipboardHandlerProvider;
import com.fr.plugin.injectable.PluginModule;

import java.util.HashSet;
import java.util.Set;

/**
 * created by Harrison on 2020/05/14
 **/
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class ClipboardFilter {
    public static Set<ClipboardHandlerProvider> clipboardHandlerProviders = new HashSet();

    public static void registerClipboardHandler(ClipboardHandlerProvider provider) {
        if (!clipboardHandlerProviders.contains(provider)) {
            clipboardHandlerProviders.add(provider);
        }
    }

    public static void removeClipboardHandler(ClipboardHandlerProvider provider) {
        if (clipboardHandlerProviders.contains(provider)) {
            clipboardHandlerProviders.remove(provider);
        }
    }
    
    public static <T> T cut(T selection) {
        for (ClipboardHandlerProvider provider : getClipboardHandlerProviders()) {
            if (provider.support(selection)) {
                selection = ((ClipboardHandlerProvider<T>) provider).cut(selection);
            }
        }
        return selection;
    }
    
    public static <T> T copy(T selection) {
        for (ClipboardHandlerProvider provider : getClipboardHandlerProviders()) {
            if (provider.support(selection)) {
                selection = ((ClipboardHandlerProvider<T>) provider).copy(selection);
            }
        }
        return selection;
    }
    
    public static <T> T paste(T selection) {
        for (ClipboardHandlerProvider provider : getClipboardHandlerProviders()) {
            if (provider.support(selection)) {
                selection = ((ClipboardHandlerProvider<T>) provider).paste(selection);
            }
        }
        return selection;
    }

    private static Set<ClipboardHandlerProvider> getClipboardHandlerProviders() {
        Set<ClipboardHandlerProvider> providers = new HashSet<>();

        for (ClipboardHandlerProvider clipboardHandlerProvider : clipboardHandlerProviders) {
            providers.add(clipboardHandlerProvider);
        }

        ExtraDesignClassManager manager = PluginModule.getAgent(PluginModule.ExtraDesign);
        Set<ClipboardHandlerProvider> pluginProviders = manager.getArray(ClipboardHandlerProvider.XML_TAG);
        for (ClipboardHandlerProvider clipboardHandlerProvider : pluginProviders) {
            providers.add(clipboardHandlerProvider);
        }

        return providers;
    }
}
