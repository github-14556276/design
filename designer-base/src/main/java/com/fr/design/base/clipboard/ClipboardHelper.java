package com.fr.design.base.clipboard;

import java.util.List;

public class ClipboardHelper {
    public static String formatExcelString(List<List<Object>> table) {
        StringBuffer stringBuffer = new StringBuffer();

        for (int row = 0; row < table.size(); row++) {
            List<Object> rowValue = table.get(row);
            for (int col = 0; col < rowValue.size(); col++) {
                Object cell = rowValue.get(col);
                stringBuffer.append(cell);
                if (col != rowValue.size() - 1) {
                    stringBuffer.append("\t");
                }
            }
            if (row != table.size() - 1) {
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }
}
