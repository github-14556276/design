package com.fr.design.extra.exe;

import com.fr.design.DesignerEnvManager;
import com.fr.design.extra.PluginConstants;
import com.fr.design.extra.Process;
import com.fr.general.CloudCenter;
import com.fr.general.http.HttpToolbox;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import java.util.Locale;

/**
 * Created by vito on 16/5/16.
 */
public class GetPluginCategoriesExecutor implements Executor {
    private String result = "[]";

    @Override
    public String getTaskFinishMessage() {
        return result;
    }

    @Override
    public Command[] getCommands() {
        return new Command[]{
                new Command() {
                    @Override
                    public String getExecuteMessage() {
                        return null;
                    }

                    @Override
                    public void run(Process<String> process) {
                        Locale locale = DesignerEnvManager.getEnvManager().getLanguage();
                        String url = CloudCenter.getInstance().acquireUrlByKind("shop.plugin.category") + "&locale=" + locale.toString();
                        if (StringUtils.isNotEmpty(url)) {
                            try {
                                result = HttpToolbox.get(url);
                                return;
                            } catch (Exception e) {
                                FineLoggerFactory.getLogger().error(e.getMessage(), e);
                            }
                        }
                        result = PluginConstants.CONNECTION_404;
                    }
                }
        };
    }
}
