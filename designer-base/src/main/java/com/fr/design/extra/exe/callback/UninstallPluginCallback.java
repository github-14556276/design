package com.fr.design.extra.exe.callback;

import com.fr.design.bridge.exec.JSCallback;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.extra.PluginOperateUtils;
import com.fr.design.i18n.Toolkit;

import com.fr.log.FineLoggerFactory;
import com.fr.plugin.context.PluginMarker;
import com.fr.plugin.error.PluginErrorCode;
import com.fr.plugin.manage.PluginManager;
import com.fr.plugin.manage.control.PluginTaskResult;

/**
 * Created by ibm on 2017/5/27.
 */
public class UninstallPluginCallback extends AbstractPluginTaskCallback {
    private JSCallback jsCallback;

    public UninstallPluginCallback(PluginMarker pluginMarker, JSCallback jsCallback){
        this.jsCallback = jsCallback;
        this.pluginMarker = pluginMarker;
    }

    @Override
    public void done(PluginTaskResult result) {
        String pluginInfo = PluginOperateUtils.getSuccessInfo(result);
        if (result.isSuccess()) {
            jsCallback.execute("success");
            String successInfo = pluginInfo + Toolkit.i18nText("Fine-Design_Basic_Plugin_Delete_Success");
            FineLoggerFactory.getLogger().info(successInfo);
            FineJOptionPane.showMessageDialog(null, successInfo);
        }else if (result.errorCode() == PluginErrorCode.NeedUninstallDependingPluginFirst) {
            int rv = FineJOptionPane.showConfirmDialog(
                    null,
                    Toolkit.i18nText("Fine-Design_Basic_Plugin_Delete_Dependence"),
                    Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"),
                    FineJOptionPane.OK_CANCEL_OPTION,
                    FineJOptionPane.INFORMATION_MESSAGE
            );
            if (rv == FineJOptionPane.OK_OPTION) {
                PluginManager.getController().uninstall(pluginMarker, true, new UninstallPluginCallback(pluginMarker, jsCallback));
            }
        } else {
            jsCallback.execute("failed");
            FineLoggerFactory.getLogger().info(Toolkit.i18nText("Fine-Design_Basic_Plugin_Delete_Failed"));
            FineJOptionPane.showMessageDialog(null, pluginInfo, Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"), FineJOptionPane.ERROR_MESSAGE);
        }
    }
}
