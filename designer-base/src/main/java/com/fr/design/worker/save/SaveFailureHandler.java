package com.fr.design.worker.save;

import com.fr.common.exception.ThrowableHandler;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.general.IOUtils;
import com.fr.workspace.exception.DiskSpaceFullException;
import java.awt.Frame;
import javax.swing.JOptionPane;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/12/7
 */
public class SaveFailureHandler implements ThrowableHandler {

    private static final SaveFailureHandler INSTANCE = new SaveFailureHandler();

    public static SaveFailureHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean process(Throwable e) {
        for (Handler handler : Handler.values()) {
            if (handler.process(e)) {
                break;
            }
        }
        return true;
    }

    public enum Handler implements ThrowableHandler {

        FullDisk {
            @Override
            public boolean process(Throwable e) {
                if (e.getCause() instanceof DiskSpaceFullException
                        || e instanceof DiskSpaceFullException
                        || e.getCause().getCause() instanceof DiskSpaceFullException) {
                    FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(),
                                                      Toolkit.i18nText("Fine_Design_Template_Save_Failed_By_Full_Disk"),
                                                      Toolkit.i18nText("Fine-Design_Basic_Alert"),
                                                      JOptionPane.WARNING_MESSAGE,
                                                      IOUtils.readIcon("/com/fr/design/images/warnings/warning32.png"));
                    return true;
                }
                return false;
            }
        },

        Other {
            @Override
            public boolean process(Throwable e) {
                boolean minimized = (DesignerContext.getDesignerFrame().getExtendedState() & Frame.ICONIFIED ) != 0;
                FineJOptionPane.showMessageDialog(
                        minimized ? null : DesignerContext.getDesignerFrame(),
                        Toolkit.i18nText("Fine-Design-Basic_Save_Failure"),
                        Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                        JOptionPane.ERROR_MESSAGE);
                return true;
            }
        };



    }
}