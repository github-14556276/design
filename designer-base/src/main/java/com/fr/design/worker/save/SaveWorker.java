package com.fr.design.worker.save;

import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrameFileDealerPane;
import com.fr.design.mainframe.EastRegionContainerPane;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.worker.WorkerManager;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import java.awt.Frame;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 * 模板保存的worker
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/1
 */
public class SaveWorker extends SwingWorker<Boolean, Void> {

    private static final int TIME_OUT = 400;

    private final Callable<Boolean> callable;

    private String taskName;

    private final JTemplate<?, ?> template;

    protected boolean success;

    private boolean slowly;

    public SaveWorker(Callable<Boolean> callable, JTemplate<?, ?> template) {
        this.callable = callable;
        this.template = template;
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        return callable.call();
    }

    @Override
    protected void done() {
        try {
           success = get();
        } catch (Exception e) {
            processResult();
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            SaveFailureHandler.getInstance().process(e);
            return;
        }
        processResult();
    }

    private void processResult() {
        this.template.setSaving(false);
        // 恢复界面
        if (slowly && ComparatorUtils.equals(this.template.getName(), HistoryTemplateListCache.getInstance().getCurrentEditingTemplate().getName())) {
            DesignerContext.getDesignerFrame().refreshUIToolBar();
            DesignerContext.getDesignerFrame().getCenterTemplateCardPane().hideCover();
        }
        DesignerFrameFileDealerPane.getInstance().stateChange();
        WorkerManager.getInstance().removeWorker(taskName);
    }

    public void start(String taskName) {
        this.taskName = taskName;
        this.template.setSaving(true);
        this.execute();
        // worker纳入管理
        WorkerManager.getInstance().registerWorker(taskName, this);
        try {
            this.get(TIME_OUT, TimeUnit.MILLISECONDS);
        } catch (TimeoutException timeoutException) {
            slowly = true;
            // 开始禁用
            EastRegionContainerPane.getInstance().updateAllPropertyPane();
            DesignerContext.getDesignerFrame().getCenterTemplateCardPane().showCover();
            DesignerFrameFileDealerPane.getInstance().stateChange();
        } catch (Exception exception) {
            FineLoggerFactory.getLogger().error(exception.getMessage(), exception);
            WorkerManager.getInstance().removeWorker(taskName);
        }
    }
}
