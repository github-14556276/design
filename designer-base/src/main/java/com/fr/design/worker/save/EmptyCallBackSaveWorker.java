package com.fr.design.worker.save;

import com.fr.design.mainframe.JTemplate;
import java.util.concurrent.Callable;

/**
 * 空实现
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/9
 */
public class EmptyCallBackSaveWorker extends CallbackSaveWorker {

    public EmptyCallBackSaveWorker(Callable<Boolean> callable, JTemplate<?, ?> template) {
        super(callable, template);
    }

    public EmptyCallBackSaveWorker() {
        this(null, null);
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        return false;
    }

    @Override
    protected void done() {
        // do nothing
    }

    @Override
    public void start(String taskName) {
        // do nothing
    }
}
