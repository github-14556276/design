package com.fr.design.worker.open;

import com.fr.base.chart.exception.ChartNotFoundException;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrameFileDealerPane;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.utils.DesignUtils;
import com.fr.design.worker.WorkerManager;
import com.fr.exception.DecryptTemplateException;
import com.fr.file.FILE;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;

import com.fr.stable.StringUtils;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * 模板打开的worker
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/9
 */
public class OpenWorker<T> extends SwingWorker<T, Void> {

    private static final int TIME_OUT = 400;

    private final Callable<T> callable;

    private final JTemplate<?, ?> template;

    private Callable<JTemplate<?, ?>> templateCallable;

    private boolean slowly = false;

    private String taskName;

    private T result;

    public OpenWorker(Callable<T> callable, JTemplate<?, ?> template) {
        this.callable = callable;
        this.template = template;
    }

    @Override
    protected T doInBackground() throws Exception {
        return this.callable.call();
    }

    @Override
    protected void done() {
        try {
         result = get();
        } catch (CancellationException ignored) {
          return;
        } catch (Throwable t) {
            processFailed(StringUtils.EMPTY);
            Throwable cause = t.getCause();
            if (cause instanceof DecryptTemplateException) {
                FineJOptionPane.showMessageDialog(
                        DesignerContext.getDesignerFrame(),
                        Toolkit.i18nText("Fine-Design_Encrypt_Decrypt_Exception"),
                        Toolkit.i18nText("Fine-Design_Basic_Alert"),
                        JOptionPane.WARNING_MESSAGE,
                        UIManager.getIcon("OptionPane.errorIcon")
                );
            }
            if (cause instanceof ChartNotFoundException) {
                FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(),
                                                  Toolkit.i18nText("Fine-Design_Chart_Not_Found_Exception"),
                                                  Toolkit.i18nText("Fine-Design_Basic_Error"),
                                                  JOptionPane.ERROR_MESSAGE,
                                                  UIManager.getIcon("OptionPane.errorIcon"));
            }
            FineLoggerFactory.getLogger().error(t.getMessage(), t);
            return;
        }
        // 后续动作
        processResult();
    }

    private void processResult() {
        this.template.setOpening(false);
        if (slowly && templateCallable != null) {
            try {
                JTemplate<?, ?> book = templateCallable.call();
                if (inValidDesigner(book)) {
                    String text = Toolkit.i18nText("Fine-Design_Report_Template_Version_Not_Match", DesignUtils.parseVersion(book.getTarget().getDesignerVersion()));
                    processFailed(text);
                    return;
                }
                FILE tplFile = book.getEditingFILE();
                JTemplate<?, ?> currentTemplate =  HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
                // 当前tab页是正在打开的模板
                if (ComparatorUtils.equals(currentTemplate.getEditingFILE(), tplFile)) {
                    currentTemplate.whenClose();
                    DesignerContext.getDesignerFrame().addAndActivateJTemplate(book);
                    HistoryTemplateListCache.getInstance().replaceCurrentEditingTemplate(book);
                } else {
                    // 当前tab页是其他模板
                    for (int i = 0, len = HistoryTemplateListCache.getInstance().getHistoryCount(); i < len; i++) {
                        JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getTemplate(i);
                        if (ComparatorUtils.equals(template.getEditingFILE(), book.getEditingFILE())) {
                            template.whenClose();
                            HistoryTemplateListCache.getInstance().getHistoryList().set(i, book);
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        DesignerFrameFileDealerPane.getInstance().stateChange();
        WorkerManager.getInstance().removeWorker(taskName);
    }

    private boolean inValidDesigner(JTemplate<?, ?> jt) {
        return jt.isOldDesigner(false) || (!jt.isJWorkBook() && jt.isNewDesigner(false));
    }

    private void processFailed(String text) {
        this.template.setOpenFailed(true);
        this.template.setOpening(false);
        JTemplate<?, ?> currentTemplate =  HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        // 需要判断当前打开的模板是不是异步执行后失败的模板 是的话立即展示失败后的提示内容 否则只设置下失败的提示内容
        if (ComparatorUtils.equals(currentTemplate.getEditingFILE().getName(), this.template.getEditingFILE().getName())) {
            DesignerContext.getDesignerFrame().getCenterTemplateCardPane().showOpenFailedCover(text);
            DesignerFrameFileDealerPane.getInstance().stateChange();
        } else {
            this.template.setTemplateOpenFailedTip(text);
        }
        WorkerManager.getInstance().removeWorker(taskName);

    }

    public void addCallBack(Callable<JTemplate<?, ?>> templateCallable) {
        this.templateCallable = templateCallable;
    }

    public void start(String taskName) {
        this.taskName = taskName;
        this.template.setOpening(true);
        this.execute();
        WorkerManager.getInstance().registerWorker(taskName, this);
    }

    public T getResult() {
        if (result != null) {
            return result;
        }
        try {
           return this.get(TIME_OUT, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            slowly = true;
        } catch (Exception exception) {
            FineLoggerFactory.getLogger().error(exception.getMessage(), exception);
            WorkerManager.getInstance().removeWorker(taskName);
        }
        return null;
    }
}
