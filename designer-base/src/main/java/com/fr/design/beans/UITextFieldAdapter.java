package com.fr.design.beans;

import com.fr.design.gui.itextfield.UITextField;

import javax.swing.JComponent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class UITextFieldAdapter implements ErrorMsgTextFieldAdapter {
    private final UITextField uiTextField = new UITextField();

    public UITextFieldAdapter(){
        addDocumentListener();
    }
    @Override
    public void setText(String str) {
        uiTextField.setText(str);
    }

    @Override
    public String getText() {
        return uiTextField.getText();
    }

    public void addDocumentListener() {
        uiTextField.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
                uiTextField.setToolTipText(uiTextField.getText());
            }

            public void insertUpdate(DocumentEvent e) {
                uiTextField.setToolTipText(uiTextField.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                uiTextField.setToolTipText(uiTextField.getText());
            }
        });
    }

    @Override
    public JComponent getErrorMsgTextField() {
        return uiTextField;
    }
}