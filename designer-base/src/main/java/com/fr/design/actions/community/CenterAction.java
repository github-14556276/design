package com.fr.design.actions.community;

import com.fr.design.menu.MenuKeySet;
import com.fr.general.CloudCenter;

import javax.swing.KeyStroke;

/**
 * Created by XINZAI on 2018/8/23.
 */
public class CenterAction extends UpAction {
    public CenterAction() {
        this.setMenuKeySet(CENTER);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/center");
    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.center", "http://bbs.fanruan.com/events/");
    }

    public static final MenuKeySet CENTER = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'C';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Commuinity_Center");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };
}
