package com.fr.design.actions.community;

import com.fr.base.BaseUtils;
import com.fr.design.actions.UpdateAction;
import com.fr.design.locale.impl.TechSupportMark;
import com.fr.design.menu.MenuKeySet;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.locale.LocaleCenter;
import com.fr.general.locale.LocaleMark;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/7/28
 */
public class TechSupportAction extends UpdateAction {
    public TechSupportAction() {
        this.setMenuKeySet(TechSupport);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/bbs/support.png"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LocaleMark<String> localeMark = LocaleCenter.getMark(TechSupportMark.class);
        String str=localeMark.getValue();
        BrowseUtils.browser(localeMark.getValue());
    }

    public static final MenuKeySet TechSupport = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 0;
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_TechSupport");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };
}
