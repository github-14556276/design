package com.fr.design.dialog.link;

import com.fr.design.gui.ilable.UILabel;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.net.URI;

/**
 * 用来构建JOptionPane带超链的消息提示
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/10/23
 */
public class MessageWithLink extends JEditorPane {

    private static final UILabel LABEL = new UILabel();


    public MessageWithLink(String message, String linkName, String link) {
        this(message, linkName, link, LABEL.getBackground(), LABEL.getFont());
    }

    public MessageWithLink(String linkName, String link ) {
        this(StringUtils.EMPTY, linkName, link);
    }

    public MessageWithLink(String message, String linkName, String link, Color color) {
        this(message, linkName, link, color, LABEL.getFont());
    }

    public MessageWithLink(String frontMessage, String linkName, String link, String backMessage) {
        this(frontMessage, linkName, link, backMessage, LABEL.getBackground(), LABEL.getFont());
    }

    public MessageWithLink(String message, String linkName, String link, Color color, Font font) {
        this(message, linkName, link, StringUtils.EMPTY, color, font);
    }

    public MessageWithLink(String frontMessage, String linkName, String link, String backMessage, Color color, Font font) {
        super("text/html", "<html><body style=\"" + generateStyle(color, font) + "\">" + frontMessage + "<a href=\"" + link + "\">" + linkName + "</a>" + backMessage + "</body></html>");
        initListener(link);
        setEditable(false);
        setBorder(null);
    }

    protected void initListener(String link) {
        addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                    try {
                        Desktop.getDesktop().browse(URI.create(link));
                    } catch (Exception exception) {
                        FineLoggerFactory.getLogger().error(exception.getMessage(), exception);
                    }
                }
            }
        });
    }

    private static StringBuilder generateStyle(Color color, Font font) {
        // 构建相同风格样式
        StringBuilder style = new StringBuilder("font-family:" + font.getFamily() + ";");
        style.append("font-weight:").append(font.isBold() ? "bold" : "normal").append(";");
        style.append("font-size:").append(font.getSize()).append("pt;");
        style.append("background-color: rgb(").append(color.getRed()).append(",").append(color.getGreen()).append(",").append(color.getBlue()).append(");");

        return style;
    }

}
