package com.fr.design.dialog;

import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextarea.UITextArea;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * 可展开对话框
 *
 * <pre>
 *  UIExpandDialog.Builder()
 *                  .owner(jf)
 *                  .title("title")
 *                  .messageType(UIExpandDialog.WARNING_MESSAGE)
 *                  .message("message text")
 *                  .detail("detail")
 *                  .expand(false)
 *                  .modal(false)
 *                  .dialogActionListener(new DialogActionAdapter(){
 *                      public void doOk() {
 *                          System.out.println("OK");
 *                      }
 *                  }).build().setVisible(true);
 *  </pre>
 *
 * @author vito
 * @version 10.0
 * Created by vito on 2021/5/19
 */
public class UIExpandDialog extends UIDialog {
    public static final int MARGIN = 10;
    public static final int TEXT_AREA_ROW = 5;
    public static final int GAP = 5;

    public static final int ERROR_MESSAGE = 0;
    public static final int INFORMATION_MESSAGE = 1;
    public static final int WARNING_MESSAGE = 2;
    public static final int QUESTION_MESSAGE = 3;

    public static final String HTML_TAG_1 = "<html>";
    public static final String HTML_TAG_2 = "</html>";

    private final JPanel foldBar = new JPanel();
    private final JPanel expandableContentPane = new JPanel();
    private final UILabel narrow = new UILabel();
    private final UILabel narrowHit = new UILabel();
    private final UIButton buttonOK;
    private UIButton buttonCancel = null;
    private JLabel msg;
    private final UITextArea textArea = new UITextArea();

    public UIExpandDialog(Frame owner, String title, boolean isModal,
                          int messageType, String message, String detail,
                          String okText, String cancelText, boolean isExpand) {
        super(owner);
        buttonOK = new UIButton(okText);
        if (!StringUtils.isEmpty(cancelText)) {
            buttonCancel = new UIButton(cancelText);
        }
        setTitle(title);
        setModal(isModal);
        initComponents(messageType, message, detail, isModal, isExpand, null);
    }

    public UIExpandDialog(Builder builder) {
        super(builder.owner);
        buttonOK = new UIButton(builder.okText);
        if (!StringUtils.isEmpty(builder.cancelText)) {
            buttonCancel = new UIButton(builder.cancelText);
        }
        setTitle(builder.title);
        setModal(builder.modal);
        initComponents(builder.messageType, builder.message, builder.detail,
                builder.modal, builder.expand, builder.dialogActionListener);
    }

    public void initComponents(int messageType, String message, String detail,
                               boolean isModal, boolean isExpand, DialogActionListener l) {
        applyClosingAction();
        setLayout(new BorderLayout(GAP, GAP));
        setResizable(false);
        setModal(isModal);
        getRootPane().setDefaultButton(buttonOK);

        // 标题面板
        UILabel icon = new UILabel(getIconForType(messageType));
        msg = new JLabel(HTML_TAG_1 + message + HTML_TAG_2);
        msg.setPreferredSize(new Dimension(300, 50));
        JPanel mainMsg = new JPanel();
        mainMsg.setLayout(new FlowLayout(FlowLayout.LEFT, MARGIN, MARGIN));
        mainMsg.setPreferredSize(new Dimension(380, 60));
        mainMsg.add(icon);
        mainMsg.add(msg);
        add(mainMsg, BorderLayout.NORTH);

        // 内容面板
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new BorderLayout(GAP, GAP));
        foldBar.setLayout(new FlowLayout(FlowLayout.LEFT, MARGIN, 0));
        foldBar.add(narrow);
        foldBar.add(narrowHit);
        contentPanel.add(foldBar, BorderLayout.NORTH);

        textArea.setEditable(false);
        textArea.setRows(TEXT_AREA_ROW);
        textArea.setMargin(new Insets(GAP, GAP, GAP, GAP));
        textArea.setEditable(false);
        textArea.setText(detail);
        UIScrollPane scrollPane = new UIScrollPane(textArea);
        expandableContentPane.setLayout(new BorderLayout());
        expandableContentPane.setBorder(BorderFactory.createEmptyBorder(0, MARGIN, 0, MARGIN));
        expandableContentPane.add(scrollPane, BorderLayout.CENTER);
        changeExpand(isExpand);
        contentPanel.add(expandableContentPane, BorderLayout.CENTER);
        add(contentPanel, BorderLayout.CENTER);


        // 操作面板
        JPanel actionPanel = new JPanel();
        actionPanel.setLayout(new FlowLayout(FlowLayout.CENTER, MARGIN, MARGIN));
        if (buttonCancel != null) {
            actionPanel.add(buttonCancel);
        }
        actionPanel.add(buttonOK);
        add(actionPanel, BorderLayout.SOUTH);

        initListener();
        if (l != null) {
            addDialogActionListener(l);
        }
        pack();
        if (getOwner() != null) {
            GUICoreUtils.setWindowCenter(getOwner(), this);
        }

    }

    private void changeExpand(boolean isExpand) {
        if (isExpand) {
            expandableContentPane.setVisible(true);
            narrow.setIcon(UIManager.getIcon("OptionPane.narrow.down"));
            narrowHit.setText(Toolkit.i18nText("Fine_Designer_Hide_Detail"));
        } else {
            expandableContentPane.setVisible(false);
            narrow.setIcon(UIManager.getIcon("OptionPane.narrow.right"));
            narrowHit.setText(Toolkit.i18nText("Fine_Designer_Look_Detail"));
        }
    }

    private void initListener() {

        foldBar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changeExpand(!expandableContentPane.isShowing());
                pack();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                foldBar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                foldBar.setCursor(Cursor.getDefaultCursor());
            }
        });
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doOK();
            }
        });

        if (buttonCancel != null) {
            buttonCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    doCancel();
                }
            });
        }
    }

    protected Icon getIconForType(int messageType) {
        if (messageType < 0 || messageType > 3)
            return null;
        String propertyName;
        switch (messageType) {
            case 0:
                propertyName = "OptionPane.errorIcon";
                break;
            case 1:
            default:
                propertyName = "OptionPane.informationIcon";
                break;
            case 2:
                propertyName = "OptionPane.warningIcon";
                break;
            case 3:
                propertyName = "OptionPane.questionIcon";
                break;
        }
        return UIManager.getIcon(propertyName);
    }

    /**
     * 设置对话框主消息
     *
     * @param message 消息内容
     */
    public void setMessage(String message) {
        msg.setText(HTML_TAG_1 + message + HTML_TAG_2);
    }

    /**
     * 设置对话框消息详情
     *
     * @param detail 消息详情
     */
    public void setDetail(String detail) {
        textArea.setText(detail);
    }

    /**
     * 设置详情面板展开关闭
     *
     * @param expand 展开或关闭
     */
    public void setExpand(boolean expand) {
        changeExpand(expand);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
    }

    public static Builder Builder() {
        return new UIExpandDialog.Builder();
    }

    public static final class Builder {

        public int messageType = ERROR_MESSAGE;
        public String title;
        public String message;
        public String detail;
        public String okText = Toolkit.i18nText("Fine-Design_Report_OK");
        public String cancelText = Toolkit.i18nText("Fine-Design_Basic_Cancel");
        public boolean modal = true;
        public boolean expand = true;
        public Frame owner = null;
        public DialogActionListener dialogActionListener = null;

        private Builder() {
        }

        public UIExpandDialog build() {
            return new UIExpandDialog(this);
        }

        public Builder owner(Frame owner) {
            this.owner = owner;
            return this;
        }

        public Builder messageType(int messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public Builder okText(String okText) {
            this.okText = okText;
            return this;
        }

        public Builder cancelText(String cancelText) {
            this.cancelText = cancelText;
            return this;
        }

        public Builder modal(boolean modal) {
            this.modal = modal;
            return this;
        }

        public Builder expand(boolean expand) {
            this.expand = expand;
            return this;
        }

        public Builder dialogActionListener(DialogActionListener dialogActionListener) {
            this.dialogActionListener = dialogActionListener;
            return this;
        }
    }

    @Override
    public void checkValid() throws Exception {

    }
}
