package com.fr.design.event;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/7/29
 */
public interface StateChangeListener {
    public void stateChange();
}
