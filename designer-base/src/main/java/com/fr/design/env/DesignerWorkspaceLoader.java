package com.fr.design.env;

import com.fr.workspace.WorkContext;
import com.fr.workspace.Workspace;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/11
 */
public class DesignerWorkspaceLoader {

    public static void init() {
        Workspace workspace = WorkContext.getCurrent();
        if (workspace.isLocal()) {
            return;
        }
        workspace.isWarDeploy();
        workspace.isCluster();
    }
}
