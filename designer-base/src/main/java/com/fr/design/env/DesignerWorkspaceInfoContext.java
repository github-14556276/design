package com.fr.design.env;

/**
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/24
 */
public class DesignerWorkspaceInfoContext {

    private static DesignerWorkspaceInfo workspaceInfo;

    public static DesignerWorkspaceInfo getWorkspaceInfo() {
        return workspaceInfo;
    }

    public static void setWorkspaceInfo(DesignerWorkspaceInfo workspaceInfo) {
        DesignerWorkspaceInfoContext.workspaceInfo = workspaceInfo;
    }
}
