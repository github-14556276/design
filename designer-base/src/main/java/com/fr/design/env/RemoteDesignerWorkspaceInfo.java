package com.fr.design.env;

import com.fr.design.DesignerEnvManager;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.security.SecurityToolbox;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;
import com.fr.stable.project.ProjectConstants;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;
import com.fr.workspace.WorkContext;
import com.fr.workspace.connect.WorkspaceConnectionInfo;

import java.util.HashSet;
import java.util.Set;

public class RemoteDesignerWorkspaceInfo implements DesignerWorkspaceInfo {

    private static final Set<String> FILTER_SET = new HashSet<>();
    private static final String HTTPS = "https://";
    private static final String HTTP = "http://";
    private static final String DEFAULT_SERVLET_NAME = "decision";
    private static final String PATH = ProjectConstants.FORWARD_SLASH + ProjectConstants.WEBAPP_NAME + ProjectConstants.FORWARD_SLASH + DEFAULT_SERVLET_NAME;
    private static final String HOST_NAME = "127.0.0.1";
    private static final String LOCAL_HOTS_NAME = "localhost";
    private static final String QUOTATION = ":";
    private static String port = Integer.toString(DesignerEnvManager.getEnvManager().getEmbedServerPort());

    static {
        initFilerSet(port);
    }

    private static void initFilerSet(String newPort) {
        FILTER_SET.clear();
        FILTER_SET.add(HTTP + HOST_NAME + QUOTATION + newPort + PATH);
        FILTER_SET.add(HTTPS + HOST_NAME + QUOTATION + newPort + PATH);
        FILTER_SET.add(HTTP + LOCAL_HOTS_NAME + QUOTATION + newPort + PATH);
        FILTER_SET.add(HTTPS + LOCAL_HOTS_NAME + QUOTATION + newPort + PATH);
        port = newPort;
    }

    private String name;

    private String remindTime;

    private WorkspaceConnectionInfo connection;

    /**
     * 标记下新创建的远程工作目录 兼容存留的远程目录客户升级后再回退 读取为密文
     * 仅保证当前新增是加密的
     */
    private boolean newCreated;

    public static RemoteDesignerWorkspaceInfo create(WorkspaceConnectionInfo connection) {
        RemoteDesignerWorkspaceInfo info = new RemoteDesignerWorkspaceInfo();
        info.connection = connection;
        return info;
    }

    @Override
    public DesignerWorkspaceType getType() {

        return DesignerWorkspaceType.Remote;
    }

    @Override
    public String getName() {

        return name;
    }

    @Override
    public String getPath() {

        return null;
    }

    @Override
    public WorkspaceConnectionInfo getConnection() {

        return connection;
    }

    public void setRemindTime(String remindTime){
        this.remindTime = remindTime;
    }

    @Override
    public String getRemindTime(){
        return remindTime;
    }

    public boolean isNewCreated() {
        return newCreated;
    }

    public void setNewCreated(boolean newCreated) {
        this.newCreated = newCreated;
    }

    @Override
    public void readXML(XMLableReader reader) {

        if (reader.isAttr()) {
            this.name = reader.getAttrAsString("name", StringUtils.EMPTY);
            this.remindTime = reader.getAttrAsString("remindTime", StringUtils.EMPTY);
            this.newCreated = reader.getAttrAsBoolean("newCreated", false);
        }
        if (reader.isChildNode()) {
            String tagName = reader.getTagName();
            if ("Connection".equals(tagName)) {
                String url = reader.getAttrAsString("url", StringUtils.EMPTY);
                String username = reader.getAttrAsString("username", StringUtils.EMPTY);
                //密码解密
                String password = SecurityToolbox.defaultDecrypt(reader.getAttrAsString("password", StringUtils.EMPTY).replaceAll(StringUtils.BLANK, "\r\n"));
                String certPath = reader.getAttrAsString("certPath", StringUtils.EMPTY);
                String certSecretKey = readCertSecretKey(reader);
                boolean rememberPwd = reader.getAttrAsBoolean("rememberPwd", true);
                this.connection = new WorkspaceConnectionInfo(url, username, password, certPath, certSecretKey, rememberPwd);
            }
        }
    }

    private String readCertSecretKey(XMLableReader reader) {
        if (isNewCreated()) {
            return SecurityToolbox.defaultDecrypt(reader.getAttrAsString("certSecretKey", StringUtils.EMPTY).replaceAll(StringUtils.BLANK, "\r\n"));
        } else {
            return reader.getAttrAsString("certSecretKey", StringUtils.EMPTY);
        }
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {

        writer.attr("name", name);
        writer.attr("remindTime", remindTime);
        writer.attr("newCreated", isNewCreated());
        if (this.connection != null) {
            writer.startTAG("Connection");
            writer.attr("url", connection.getUrl());
            writer.attr("username", connection.getUserName());
            writer.attr("password", SecurityToolbox.defaultEncrypt(connection.getPassword()));
            writer.attr("certPath", connection.getCertPath());
            writeCertSecretKey(writer);
            writer.attr("rememberPwd", connection.isRememberPwd());
            writer.end();
        }
    }

    private void writeCertSecretKey(XMLPrintWriter writer) {
        if (isNewCreated()) {
            writer.attr("certSecretKey", SecurityToolbox.defaultEncrypt(connection.getCertSecretKey()));
        } else {
            writer.attr("certSecretKey", connection.getCertSecretKey());
        }
    }

    @Override
    @SuppressWarnings("squid:S2975")
    public Object clone() throws CloneNotSupportedException {

        RemoteDesignerWorkspaceInfo object = (RemoteDesignerWorkspaceInfo) super.clone();

        object.connection = (WorkspaceConnectionInfo) StableUtils.cloneObject(this.connection);
        return object;
    }


    @Override
    public boolean checkValid() throws Exception {
        String newPort = Integer.toString(DesignerEnvManager.getEnvManager().getEmbedServerPort());
        if (!ComparatorUtils.equals(port, newPort)) {
            // 使用过程中 更改了内置服务器端口 重新初始化下
            initFilerSet(newPort);
        }
        if (FILTER_SET.contains(connection.getUrl())) {
            FineLoggerFactory.getLogger().error("url is same with local designer");
            return false;
        }
        WorkContext.getConnector().validateVT(connection);
        return true;
    }
}
