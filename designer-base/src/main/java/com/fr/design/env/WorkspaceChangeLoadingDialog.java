package com.fr.design.env;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.utils.gui.GUICoreUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JDialog;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/10
 */
public class WorkspaceChangeLoadingDialog extends JDialog {

    private static final ImageIcon LOADING_ICON = new ImageIcon(WorkspaceChangeLoadingDialog.class.getResource("/com/fr/web/images/loading-local.gif"));

    private static WorkspaceChangeLoadingDialog dialog;

    public WorkspaceChangeLoadingDialog() {
        super(DesignerContext.getDesignerFrame());
        setLayout(new BorderLayout());
        this.getContentPane().setBackground(Color.WHITE);
        this.setResizable(false);
        this.setUndecorated(true);
        this.setAlwaysOnTop(true);
        this.setModal(false);
        this.setSize(new Dimension(400, 100));
        this.add(new UILabel(LOADING_ICON, UILabel.CENTER), BorderLayout.NORTH);
        this.add(new UILabel(Toolkit.i18nText("Fine-Design_Change_Workspace_Tip"), UILabel.CENTER), BorderLayout.CENTER);
        GUICoreUtils.centerWindow(this);
    }


    public static void showDialog() {
        dialog = new WorkspaceChangeLoadingDialog();
        dialog.setVisible(true);
    }

    public static void hideDialog() {
        dialog.dispose();
    }
}
