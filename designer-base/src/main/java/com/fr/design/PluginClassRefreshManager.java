package com.fr.design;

import com.fr.design.constants.DesignerLaunchStatus;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.fun.HyperlinkProvider;
import com.fr.design.fun.TableDataDefineProvider;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.ui.util.UIUtil;
import com.fr.plugin.observer.PluginEvent;
import com.fr.plugin.observer.PluginEventListener;
import com.fr.plugin.observer.PluginEventType;
import com.fr.plugin.observer.PluginListenerRegistration;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/17
 */
public class PluginClassRefreshManager {


    private static final PluginClassRefreshManager INSTANCE = new PluginClassRefreshManager();

    private final Set<String> context = new HashSet<>();

    private final PluginEventListener pluginAfterRunEventListener = new PluginEventListener() {
        @Override
        public void on(PluginEvent event) {
            // 重载模版之前 触发下hide
            fireTabChange();
            // 兼容之前版本特性
            for (String tag : context) {
                if (event.getContext().contain(tag)) {
                    HistoryTemplateListCache.getInstance().reloadAllEditingTemplate();
                    return;
                }
            }
            // 重新载入模板xml内容 到 Workbook/Form对象中
            HistoryTemplateListCache.getInstance().reloadAllEditingTemplateByPlugin(event.getContext());
        }
    };

    private final PluginEventListener beforeAllPluginActive = new PluginEventListener() {
        @Override
        public void on(PluginEvent event) {
            PluginListenerRegistration.getInstance().stopListen(pluginAfterRunEventListener);
        }
    };

    private final PluginEventListener afterAllPluginsActive = new PluginEventListener() {
        @Override
        public void on(PluginEvent event) {
            PluginListenerRegistration.getInstance().listen(PluginEventType.AfterRun, pluginAfterRunEventListener);
            if (DesignerLaunchStatus.getStatus() != DesignerLaunchStatus.WORKSPACE_INIT_COMPLETE) {
                fireTabChange();
                HistoryTemplateListCache.getInstance().reloadAllEditingTemplate();
            }
        }
    };

    public void fireTabChange() {
        JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (template != null) {
            UIUtil.invokeLaterIfNeeded(new Runnable() {
                @Override
                public void run() {
                    template.fireTabChange();
                }
            });
        }
    }


    public static PluginClassRefreshManager getInstance() {
        return INSTANCE;
    }

    public void load() {
        context.add(TableDataDefineProvider.XML_TAG);
        context.add(HyperlinkProvider.XML_TAG);
    }

    private PluginClassRefreshManager() {
        PluginListenerRegistration.getInstance().listen(PluginEventType.BeforeAllActive, beforeAllPluginActive);
        PluginListenerRegistration.getInstance().listen(PluginEventType.AfterAllActive, afterAllPluginsActive);
    }

    public void removePluginListener() {
        PluginListenerRegistration.getInstance().stopListen(this.pluginAfterRunEventListener);
        PluginListenerRegistration.getInstance().stopListen(this.beforeAllPluginActive);
        PluginListenerRegistration.getInstance().stopListen(this.afterAllPluginsActive);
    }

    public void addPluginListener() {
        PluginListenerRegistration.getInstance().listen(PluginEventType.AfterRun, this.pluginAfterRunEventListener);
        PluginListenerRegistration.getInstance().listen(PluginEventType.BeforeAllActive, beforeAllPluginActive);
        PluginListenerRegistration.getInstance().listen(PluginEventType.AfterAllActive, afterAllPluginsActive);
    }

}
