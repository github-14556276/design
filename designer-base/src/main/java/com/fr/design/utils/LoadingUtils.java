package com.fr.design.utils;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.general.IOUtils;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

public class LoadingUtils {
    private static final ImageIcon LOADING_ICON = new ImageIcon(IOUtils.readImage("/com/fr/design/images/mainframe/loading.png"));
    private static final Color TIP_COLOR = new Color(108, 174, 235);
    private static final int Y_GAP = 50;
    private static final int X_GAP = 10;

    public static JPanel createLoadingPane() {
        JPanel jPanel = new JPanel();
        UILabel loadingLabel = new UILabel(LOADING_ICON);
        UILabel tipLabel = new UILabel(Toolkit.i18nText("Fine-Design_Loading"));
        tipLabel.setForeground(TIP_COLOR);
        jPanel.setLayout(new LayoutManager() {
            @Override
            public void removeLayoutComponent(Component comp) {
            }

            @Override
            public Dimension preferredLayoutSize(Container parent) {
                return parent.getPreferredSize();
            }

            @Override
            public Dimension minimumLayoutSize(Container parent) {
                return null;
            }

            @Override
            public void layoutContainer(Container parent) {
                int width = parent.getParent().getWidth();
                int height = parent.getParent().getHeight();
                int loadingLabelWidth = loadingLabel.getPreferredSize().width;
                int loadingLabelHeight = loadingLabel.getPreferredSize().height;
                int loadingLabelX = (width - loadingLabelWidth) / 2;
                int loadingLabelY = (height - loadingLabelHeight) / 2;
                int tipLabelWidth = tipLabel.getPreferredSize().width;
                int tipLabelHeight = tipLabel.getPreferredSize().height;
                int tipLabelX = (width - tipLabelWidth) / 2 + X_GAP;
                int tipLabelY = loadingLabelY + loadingLabelHeight - Y_GAP;
                loadingLabel.setBounds(loadingLabelX, loadingLabelY, loadingLabelWidth, loadingLabelHeight);
                tipLabel.setBounds(tipLabelX, tipLabelY, tipLabelWidth, tipLabelHeight);
            }

            @Override
            public void addLayoutComponent(String name, Component comp) {
            }
        });

        jPanel.setBackground(Color.WHITE);
        jPanel.add(loadingLabel);
        jPanel.add(tipLabel);
        return jPanel;
    }
}