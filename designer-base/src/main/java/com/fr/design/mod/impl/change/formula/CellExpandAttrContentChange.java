package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.report.cell.cellattr.CellExpandAttr;
import java.util.HashMap;
import java.util.Map;

/**
 * 扩展后排序公式
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class CellExpandAttrContentChange implements ContentChange<CellExpandAttr> {

    private final Map<ChangeItem, ContentReplacer<CellExpandAttr>> map;

    public CellExpandAttrContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.CellExpandAttr4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.CellExpandAttr4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return CellExpandAttr.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<CellExpandAttr>> changeInfo() {
        return map;
    }
}
