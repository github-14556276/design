package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.report.cell.cellattr.CellGUIAttr;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/3
 */
public class CellGUIAttrContentChange implements ContentChange<CellGUIAttr> {

    private Map<ChangeItem, ContentReplacer<CellGUIAttr>> map;

    public CellGUIAttrContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.CellGUIAttr4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.CellGUIAttr4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return CellGUIAttr.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<CellGUIAttr>> changeInfo() {
        return map;
    }
}
