package com.fr.design.mod.impl.repalce;

import com.fr.data.SimpleDSColumn;
import com.fr.design.mod.ContentReplacer;
import com.fr.general.ComparatorUtils;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class SimpleDSColumnContentReplacer implements ContentReplacer<SimpleDSColumn> {

    @Override
    public void replace(SimpleDSColumn simpleDSColumn, String oldName, String newName) {
        if (ComparatorUtils.equals(simpleDSColumn.getDsName(), oldName)) {
            simpleDSColumn.setDsName(newName);
        }
    }
}
