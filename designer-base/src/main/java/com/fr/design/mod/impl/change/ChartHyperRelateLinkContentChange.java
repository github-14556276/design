package com.fr.design.mod.impl.change;

import com.fr.chart.web.ChartHyperRelateLink;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.ChartHyperRelateLink4WidgetNameContentReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/27
 */
public abstract class ChartHyperRelateLinkContentChange implements ContentChange<ChartHyperRelateLink> {

    private final Map<ChangeItem, ContentReplacer<ChartHyperRelateLink>> map;

    public ChartHyperRelateLinkContentChange() {
        this.map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, new ChartHyperRelateLink4WidgetNameContentReplacer());
    }

    @Override
    public Map<ChangeItem, ContentReplacer<ChartHyperRelateLink>> changeInfo() {
        return map;
    }

}
