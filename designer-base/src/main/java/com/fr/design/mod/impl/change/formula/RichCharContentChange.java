package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.report.cell.cellattr.core.RichChar;
import java.util.HashMap;
import java.util.Map;

/**
 * 富文本
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class RichCharContentChange implements ContentChange<RichChar> {

    private final Map<ChangeItem, ContentReplacer<RichChar>> map;

    public RichCharContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.RichChar4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.RichChar4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return RichChar.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<RichChar>> changeInfo() {
        return map;
    }
}
