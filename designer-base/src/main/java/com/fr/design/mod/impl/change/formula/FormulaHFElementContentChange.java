package com.fr.design.mod.impl.change.formula;

import com.fr.base.headerfooter.FormulaHFElement;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * 页面/页脚——公式
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */

public class FormulaHFElementContentChange implements ContentChange<FormulaHFElement> {

    private final Map<ChangeItem, ContentReplacer<FormulaHFElement>> map;

    public FormulaHFElementContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.FormulaHFElement4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.FormulaHFElement4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return FormulaHFElement.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<FormulaHFElement>> changeInfo() {
        return map;
    }
}
