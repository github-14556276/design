package com.fr.design.mod.impl.change;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.JavaScriptContentReplacer;
import com.fr.js.JavaScriptImpl;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/27
 */
public class JavaScriptContentChange implements ContentChange<JavaScriptImpl> {

    private final Map<ChangeItem, ContentReplacer<JavaScriptImpl>> map;

    public JavaScriptContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, new JavaScriptContentReplacer());
        map.put(ChangeItem.TABLE_DATA_NAME, new JavaScriptContentReplacer());
    }

    @Override
    public String type() {
        return JavaScriptImpl.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<JavaScriptImpl>> changeInfo() {
        return map;
    }

}
