package com.fr.design.mod.event;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class WidgetNameModifyEvent extends ModifyEvent {

    public static final WidgetNameModifyEvent INSTANCE = new WidgetNameModifyEvent();

}
