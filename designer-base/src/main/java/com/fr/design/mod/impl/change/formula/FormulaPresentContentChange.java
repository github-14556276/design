package com.fr.design.mod.impl.change.formula;

import com.fr.base.present.FormulaPresent;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * 公式形态
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */

public class FormulaPresentContentChange implements ContentChange<FormulaPresent> {

    private final Map<ChangeItem, ContentReplacer<FormulaPresent>> map;

    public FormulaPresentContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.FormulaPresent4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.FormulaPresent4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return FormulaPresent.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<FormulaPresent>> changeInfo() {
        return map;
    }
}
