package com.fr.design.mod;

import com.fr.stable.Filter;
import java.util.HashSet;
import java.util.Set;

/**
 * 联动修改的类过滤器
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/31
 */
public class ModClassFilter implements Filter<String> {

    private static final Set<String> FILTER_SET = new HashSet<>();

    private static final Filter<String> INSTANCE = new ModClassFilter();

    private static final String DESIGN_PREFIX = "com.fr.design";

    public static Filter<String> getInstance() {
        return INSTANCE;
    }

    static {
        FILTER_SET.add("java.awt.image.BufferedImage");
        FILTER_SET.add("sun.awt.AppContext");
        FILTER_SET.add("com.fr.poly.creator.ECBlockCreator");
        FILTER_SET.add("io.netty.channel.nio.SelectedSelectionKeySet");
        FILTER_SET.add("com.fr.form.ui.ElementCaseImage");
        FILTER_SET.add("this$0");
    }

    @Override
    public boolean accept(String s) {
        return FILTER_SET.contains(s) || s.startsWith(DESIGN_PREFIX);
    }
}
