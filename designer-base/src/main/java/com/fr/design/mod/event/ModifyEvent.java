package com.fr.design.mod.event;

import com.fr.design.mod.bean.ContentChangeItem;
import com.fr.event.Event;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class ModifyEvent implements Event<ContentChangeItem> {

}
