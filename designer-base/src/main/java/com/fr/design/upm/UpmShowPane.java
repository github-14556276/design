package com.fr.design.upm;

import com.fr.design.dialog.BasicPane;
import com.fr.design.ui.ModernUIPane;
import com.fr.design.ui.compatible.ModernUIPaneFactory;
import com.fr.design.upm.event.DownloadEvent;
import com.fr.event.Event;
import com.fr.event.EventDispatcher;
import com.fr.event.Listener;
import com.teamdev.jxbrowser.browser.callback.InjectJsCallback;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.events.ScriptContextAdapter;
import com.teamdev.jxbrowser.chromium.events.ScriptContextEvent;

import com.teamdev.jxbrowser.js.JsObject;
import java.awt.*;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-04-12
 * Update Plugin Manager容器
 */
public class UpmShowPane extends BasicPane {

    private ModernUIPane<Object> modernUIPane;

    @Override
    protected String title4PopupWindow() {
        return "UPM";
    }

    UpmShowPane() {
        setLayout(new BorderLayout());
        modernUIPane = ModernUIPaneFactory.modernUIPaneBuilder()
                .prepareForV6(new ScriptContextAdapter() {
                    @Override
                    public void onScriptContextCreated(ScriptContextEvent event) {
                        // 6.x
                        JSValue window = event.getBrowser().executeJavaScriptAndReturnValue("window");
                        window.asObject().setProperty("PluginHelper", UpmBridge.getBridge(event.getBrowser()));
                    }
                })
                .prepareForV7(params -> {
                    // 7.x
                    JsObject window = params.frame().executeJavaScript("window");
                    if (window != null) {
                        window.putProperty("PluginHelper", NewUpmBridge.getBridge(window));
                    }
                    return InjectJsCallback.Response.proceed();
                })
                .withURL(UpmFinder.getMainResourcePath(), UpmUtils.renderMap())
                .build();
        EventDispatcher.listen(DownloadEvent.UPDATE, new Listener<String>() {
            @Override
            public void on(Event event, String param) {
                modernUIPane.redirect(UpmFinder.getMainResourcePath(), UpmUtils.renderMap());
            }
        });
        add(modernUIPane, BorderLayout.CENTER);
    }
}
