package com.fr.design.upm.exec;

import com.fr.design.bridge.exec.JSExecutor;
import com.teamdev.jxbrowser.js.JsFunction;
import com.teamdev.jxbrowser.js.JsObject;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-04-18
 */
public class NewUpmBrowserExecutor implements JSExecutor {

    public static NewUpmBrowserExecutor create(JsObject window, JsFunction callback) {
        return new NewUpmBrowserExecutor(window, callback);
    }

    private final JsFunction callback;
    private final JsObject window;

    private NewUpmBrowserExecutor(JsObject window, JsFunction callback) {
        this.window = window;
        this.callback = callback;
    }

    @Override
    public void executor(String newValue) {
        callback.invoke(window, newValue);
    }
}
