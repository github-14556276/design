package com.fr.design.report.fit.provider;


import com.fr.design.report.fit.FitAttrModel;
import com.fr.stable.fun.mark.Mutable;


public interface FitAttrModelProvider extends Mutable, FitAttrModel {
    String XML_TAG = "FitAttrModelProvider";
    int CURRENT_LEVEL = 1;
}
