package com.fr.design.report.fit;

import com.fr.design.mainframe.JTemplate;
import com.fr.report.fit.ReportFitAttr;

public interface FitAttrModel {
    /**
     * @Description 名称，比如：普通报表、决策报表等
     **/
    String getModelName();

    /**
     * @Description 自适应选项的名称，比如返回：表格
     **/
    String getFitName();

    /**
     * @Description 自适应选项
     **/
    FitType[] getFitTypes();


    /**
     * @Description 获取全局的自适应属性
     **/
    ReportFitAttr getGlobalReportFitAttr();

    /**
     * @Description 设置全局的自适应属性
     * @param: reportFitAttr
     **/
    void setGlobalReportFitAttr(ReportFitAttr reportFitAttr);

    /**
     * @Description 优先级
     **/
    int getPriority();

    /**
     * @Description 是否可用
     * @param: jTemplate
     **/
    boolean isAvailable(JTemplate jTemplate);
}
