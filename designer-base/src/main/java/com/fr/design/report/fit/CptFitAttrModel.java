package com.fr.design.report.fit;


import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.JTemplate;
import com.fr.report.fit.ReportFitAttr;
import com.fr.report.fit.ReportFitConfig;


public class CptFitAttrModel implements FitAttrModel {

    @Override
    public FitType[] getFitTypes() {
        return new FitType[]{
                FitType.HORIZONTAL_FIT,
                FitType.DOUBLE_FIT,
                FitType.NOT_FIT
        };
    }

    @Override
    public String getFitName() {
        return Toolkit.i18nText("Fine-Designer_Fit-Element");
    }


    @Override
    public String getModelName() {
        return Toolkit.i18nText("Fine-Design_Basic_Plain_Report");
    }

    @Override
    public ReportFitAttr getGlobalReportFitAttr() {
        return ReportFitConfig.getInstance().getCptFitAttr();
    }

    @Override
    public void setGlobalReportFitAttr(ReportFitAttr reportFitAttr) {
        ReportFitConfig.getInstance().setCptFitAttr(reportFitAttr);
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public boolean isAvailable(JTemplate jTemplate) {
        return jTemplate.isJWorkBook();
    }
}
