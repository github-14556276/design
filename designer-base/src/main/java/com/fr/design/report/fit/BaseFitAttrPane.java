package com.fr.design.report.fit;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ibutton.UIRadioButton;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.DesignSizeI18nManager;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.report.fit.menupane.FitPreviewPane;
import com.fr.design.report.fit.menupane.FitRadioGroup;
import com.fr.design.report.fit.menupane.FontRadioGroup;
import com.fr.design.report.fit.provider.FitAttrModelProvider;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.ComparatorUtils;
import com.fr.report.fit.ReportFitAttr;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

import static com.fr.design.i18n.Toolkit.i18nText;

public abstract class BaseFitAttrPane extends BasicBeanPane<ReportFitAttr> {

    protected JPanel contentJPanel;
    protected UILabel belowSetLabel;
    protected UIComboBox itemChoose;
    protected java.util.List<FitAttrModel> fitAttrModelList = new ArrayList<>();

    public FontRadioGroup fontRadioGroup;
    public FitRadioGroup adaptRadioGroup;
    public JPanel attrJPanel;
    public FitPreviewPane previewJPanel;
    public FitAttrModel fitAttrModel;
    private static final int BELOW_SET_COMPONENT_HSPACE = 8;


    protected BaseFitAttrPane() {
        initFitAttrModel();
    }

    private void initFitAttrModel() {
        fitAttrModelList.add(new FrmFitAttrModel());
        fitAttrModelList.add(new CptFitAttrModel());

        Set<FitAttrModelProvider> fitAttrModelProviders = ExtraDesignClassManager.getInstance().getArray(FitAttrModelProvider.XML_TAG);

        for (FitAttrModelProvider fitAttrModelProvider : fitAttrModelProviders) {
            fitAttrModelList.add(fitAttrModelProvider);
        }

        fitAttrModelList = fitAttrModelList.stream().sorted(Comparator.comparing(FitAttrModel::getPriority).reversed()).collect(Collectors.toList());
    }

    protected void populateModel(FitAttrModel fitAttrModel) {
        this.fitAttrModel = fitAttrModel;
        if (attrJPanel != null) {
            contentJPanel.remove(attrJPanel);
        }
        if (previewJPanel != null) {
            contentJPanel.remove(previewJPanel);
        }

        fontRadioGroup = new FontRadioGroup();
        adaptRadioGroup = new FitRadioGroup();
        initAttrJPanel();
        initPreviewJPanel();
    }


    protected void initAttrJPanel() {
        int colCount = fitAttrModel.getFitTypes().length + 1;
        Component[][] components = new Component[2][colCount];
        initFitRadioGroup(fontRadioGroup, i18nText("Fine-Designer_Fit-Font"), new String[]{i18nText("Fine-Designer_Fit"), i18nText("Fine-Designer_Fit-No")}, components[0]);
        initFitRadioGroup(adaptRadioGroup, fitAttrModel.getFitName(), Arrays.stream(fitAttrModel.getFitTypes()).map(FitType::description).toArray(String[]::new), components[1]);

        double[] rowSize = new double[2];
        double[] columnSize = new double[colCount];
        for (int i = 0; i < rowSize.length; i++) {
            rowSize[i] = 20;
        }
        for (int i = 0; i < columnSize.length; i++) {
            if (i == 0) {
                columnSize[i] = DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.report.fit.firstColumn").getWidth();
            } else {
                columnSize[i] = DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.report.fit.column").getWidth();
            }
        }

        attrJPanel = TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
        attrJPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 10, 0));
        contentJPanel.add(attrJPanel);
    }

    private void initFitRadioGroup(FitRadioGroup fitRadioGroup, String name, String[] options, Component[] components) {
        components[0] = new UILabel(name);
        for (int i = 0; i < options.length; i++) {

            if (options[i] != null) {
                UIRadioButton fontFitRadio = new UIRadioButton(options[i]);
                fitRadioGroup.add(fontFitRadio);
                components[i + 1] = fontFitRadio;
            } else {
                components[i + 1] = null;
            }
        }
        fitRadioGroup.addActionListener(getPreviewActionListener());
    }

    protected ActionListener getPreviewActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshPreviewJPanel();
            }
        };
    }

    protected void refreshPreviewJPanel() {
        String previewIndex = getPreviewIndex();
        previewJPanel.refreshPreview(previewIndex, fontRadioGroup.isEnabled());
    }

    protected String getPreviewIndex() {
        return getStateInPC(adaptRadioGroup.getSelectRadioIndex()) + "" + fontRadioGroup.getSelectRadioIndex();
    }

    protected void initPreviewJPanel() {
        previewJPanel = new FitPreviewPane();
        previewJPanel.setBorder(BorderFactory.createEmptyBorder(0, getPreviewJPanelLeft(), 0, 0));
        contentJPanel.add(previewJPanel);
    }

    private int getPreviewJPanelLeft() {
        int left = 0;
        if (belowSetLabel.getPreferredSize() != null) {
            left = belowSetLabel.getPreferredSize().width + BELOW_SET_COMPONENT_HSPACE;
        }
        return left;
    }

    protected int getStateInPC(int index) {
        FitType[] fitTypes = fitAttrModel.getFitTypes();
        return fitTypes[index].getState();
    }

    protected int getOptionIndex(int state) {
        FitType[] fitTypes = fitAttrModel.getFitTypes();
        for (int i = 0; i < fitTypes.length; i++) {
            if (ComparatorUtils.equals(state, fitTypes[i].getState())) {
                return i;
            }
        }
        return 0;
    }


    @Override
    public void populateBean(ReportFitAttr ob) {
        fontRadioGroup.selectIndexButton(ob.isFitFont() ? 0 : 1);
        adaptRadioGroup.selectIndexButton(getOptionIndex(ob.fitStateInPC()));
        refreshPreviewJPanel();
    }

    @Override
    public ReportFitAttr updateBean() {
        ReportFitAttr reportFitAttr = new ReportFitAttr();
        reportFitAttr.setFitFont(fontRadioGroup.isFontFit());
        reportFitAttr.setFitStateInPC(getStateInPC(adaptRadioGroup.getSelectRadioIndex()));
        return reportFitAttr;
    }


    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        fontRadioGroup.setEnabled(enabled);
        adaptRadioGroup.setEnabled(enabled);
        refreshPreviewJPanel();
    }

    @Override
    protected String title4PopupWindow() {
        return i18nText("Fine-Designer_PC_Fit_Attr");
    }

    protected abstract String[] getItemNames();

    protected void initComponents() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        contentJPanel = FRGUIPaneFactory.createVerticalFlowLayout_Pane(false, FlowLayout.LEFT, 0, 0);
        this.add(contentJPanel);
        initItemChoose();
        initPrompt();
    }


    private void initItemChoose() {
        JPanel chooseJPanel = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
        ItemListener itemListener = getItemListener();
        itemChoose = new UIComboBox(getItemNames());
        itemChoose.addItemListener(itemListener);
        belowSetLabel = new UILabel(i18nText("Fine-Design_Report_Blow_Set"));
        JPanel hSpaceLabel = new JPanel();
        hSpaceLabel.setSize(BELOW_SET_COMPONENT_HSPACE, 0);
        JPanel buttonPane = GUICoreUtils.createFlowPane(new Component[]{
                belowSetLabel, hSpaceLabel, itemChoose}, FlowLayout.LEFT);
        chooseJPanel.add(buttonPane);
        chooseJPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
        contentJPanel.add(chooseJPanel);
    }


    protected abstract ItemListener getItemListener();


    public void populate(ReportFitAttr reportFitAttr) {

    }

    protected void initPrompt() {
    }

    protected void refresh() {
        validate();
        repaint();
        revalidate();
    }
}
