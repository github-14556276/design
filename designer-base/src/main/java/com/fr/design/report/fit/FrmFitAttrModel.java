package com.fr.design.report.fit;


import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.JTemplate;
import com.fr.report.fit.ReportFitAttr;
import com.fr.report.fit.ReportFitConfig;


public class FrmFitAttrModel implements FitAttrModel {


    @Override
    public String getModelName() {
        return Toolkit.i18nText("Fine-Design_Basic_Decision_Report");
    }

    @Override
    public String getFitName() {
        return Toolkit.i18nText("Fine-Designer_Fit-Element");
    }

    public FitType[] getFitTypes() {
        return new FitType[]{
                FitType.DEFAULT,
                FitType.HORIZONTAL_FIT,
                FitType.DOUBLE_FIT,
                FitType.NOT_FIT
        };
    }

    @Override
    public ReportFitAttr getGlobalReportFitAttr() {
        return ReportFitConfig.getInstance().getFrmFitAttr();
    }

    @Override
    public void setGlobalReportFitAttr(ReportFitAttr reportFitAttr) {
        ReportFitConfig.getInstance().setFrmFitAttr(reportFitAttr);
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public boolean isAvailable(JTemplate jTemplate) {
        return !jTemplate.isJWorkBook();
    }


}
