package com.fr.design.fun;

/**
 * @author richie
 * @date 2015-03-23
 * @since 8.0
 * 表单控件
 */
public interface FormWidgetOptionProvider extends ParameterWidgetOptionProvider {

    String XML_TAG = "FormWidgetOptionProvider";

    /**
     * 组件是否是布局容器
     * @return 是布局容器则返回true，否则返回false
     */
    boolean isContainer();

    /**
     * 如果是布局容器要实现粘贴到容器中的操作
     * @param t
     * @param <T> 泛型参数 表示选中的组件 一般为FormSelection
     */
    <T> void paste2Container(T t);

    /**
     * 往扩展容器本身中添加粘贴内容
     * @param r
     * @param <T> 粘贴板选中的组件
     * @param <R> 容器布局适配器
     */
    <T, R> void formWidgetPaste(T t, R r, int x, int y);

}