package com.fr.design.fun.impl;

import com.fr.design.fun.DesignerStartClassProcessor;
import com.fr.stable.fun.mark.API;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/5/7
 */
@API(level = DesignerStartClassProcessor.CURRENT_LEVEL)
public abstract class AbstractDesignerStartClassProcessorProcessor implements DesignerStartClassProcessor {

    @Override
    public String mark4Provider() {
        return getClass().getName();
    }

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }
}
