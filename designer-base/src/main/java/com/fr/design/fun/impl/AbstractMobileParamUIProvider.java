package com.fr.design.fun.impl;

import com.fr.design.fun.MobileParamUIProvider;
import com.fr.stable.fun.impl.AbstractProvider;
import com.fr.stable.fun.mark.API;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/30
 */
@API(level = MobileParamUIProvider.CURRENT_LEVEL)
public abstract class AbstractMobileParamUIProvider extends AbstractProvider implements MobileParamUIProvider {

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public String mark4Provider() {
        return getClass().getName();
    }

}
