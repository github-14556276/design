package com.fr.design.fun;

import com.fr.stable.fun.mark.Immutable;
import java.awt.event.MouseEvent;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/14
 */
public interface TemplateTreeDefineProcessor extends Immutable {

    String XML_TAG = "TemplateTreeDefineProcessor";

    int CURRENT_LEVEL = 1;

    void  rightClickAction(MouseEvent mouseEvent);

}
