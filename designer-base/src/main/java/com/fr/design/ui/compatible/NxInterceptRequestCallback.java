package com.fr.design.ui.compatible;

import com.fr.base.TemplateUtils;
import com.fr.general.IOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ArrayUtils;
import com.fr.stable.EncodeConstants;
import com.fr.stable.StringUtils;
import com.fr.third.org.apache.commons.codec.net.URLCodec;
import com.teamdev.jxbrowser.net.HttpHeader;
import com.teamdev.jxbrowser.net.HttpStatus;
import com.teamdev.jxbrowser.net.UrlRequest;
import com.teamdev.jxbrowser.net.UrlRequestJob;

import com.teamdev.jxbrowser.net.callback.InterceptUrlRequestCallback;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2020/3/25
 */
public class NxInterceptRequestCallback implements InterceptUrlRequestCallback {

    Map<String, String> map;

    public NxInterceptRequestCallback() {
    }

    public NxInterceptRequestCallback(Map<String, String> map) {
        this.map = map;
    }

    @Override
    public Response on(Params params) {
        UrlRequest urlRequest = params.urlRequest();
        String path = urlRequest.url();
        if (path.startsWith("file:")) {
            Optional<UrlRequestJob> optional = generateFileProtocolUrlRequestJob(params, path);
            if (optional.isPresent()) {
                return Response.intercept(optional.get());
            }
        } else {
            return next(params, path);
        }
        return Response.proceed();
    }

    Response next(Params params, String path) {
        return Response.proceed();
    }

    private Optional<UrlRequestJob> generateFileProtocolUrlRequestJob(Params params, String path) {
        try {
            String url = new URLCodec().decode(path);
            String filePath = TemplateUtils.renderParameter4Tpl(url, map);
            File file = new File(URI.create(filePath).getPath());
            InputStream inputStream = IOUtils.readResource(file.getAbsolutePath());
            String mimeType = getMimeType(path);
            byte[] bytes;
            if (isPlainText(mimeType)) {
                String text = IOUtils.inputStream2String(inputStream, EncodeConstants.ENCODING_UTF_8);
                text = TemplateUtils.renderParameter4Tpl(text, map);
                bytes = text.getBytes(StandardCharsets.UTF_8);
            } else {
                bytes = IOUtils.inputStream2Bytes(inputStream);
            }
            return Optional.of(generateBasicUrlRequestJob(params, mimeType, bytes));
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    private boolean isPlainText(String mimeType) {
        return ArrayUtils.contains(new String[]{"text/html", "text/javascript", "text/css"}, mimeType);
    }

    UrlRequestJob generateBasicUrlRequestJob(Params params, String mimeType, byte[] bytes) {
        UrlRequestJob.Options options = UrlRequestJob.Options
                .newBuilder(HttpStatus.OK)
                .addHttpHeader(HttpHeader.of("Content-Type", mimeType))
                .build();
        UrlRequestJob urlRequestJob = params.newUrlRequestJob(options);
        urlRequestJob.write(bytes);
        urlRequestJob.complete();
        return urlRequestJob;
    }

    String getMimeType(String path) {
        // 去除 xxx?xxx 后面部分
        int index = path.indexOf("?");
        if (index != -1) {
            path = path.substring(0, path.indexOf("?"));
        }
        if (StringUtils.isBlank(path)) {
            return "text/html";
        }
        if (path.endsWith(".html")) {
            return "text/html";
        }
        if (path.endsWith(".css")) {
            return "text/css";
        }
        if (path.endsWith(".js")) {
            return "text/javascript";
        }
        if (path.endsWith(".svg")) {
            return "image/svg+xml";
        }
        if (path.endsWith(".png")) {
            return "image/png";
        }
        if (path.endsWith(".jpeg")) {
            return "image/jpeg";
        }
        if (path.endsWith(".gif")) {
            return "image/gif";
        }
        if (path.endsWith(".woff")) {
            return "font/woff";
        }
        if (path.endsWith(".ttf")) {
            return "truetype";
        }
        if (path.endsWith(".eot")) {
            return "embedded-opentype";
        }
        Path file = new File(path).toPath();
        try {
            return Files.probeContentType(file);
        } catch (IOException e) {
            return "text/html";
        }
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}