package com.fr.design.ui.compatible;

import com.fr.design.ui.ModernUIPane;
import com.fr.stable.os.OperatingSystem;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/13
 */
public class ModernUIPaneFactory {

    public static <T> ModernUIPane.Builder<T> modernUIPaneBuilder() {

        if (isV7()) {
            return new NewModernUIPane.Builder<>();
        } else {
            return new ModernUIPane.Builder<>();
        }

    }

    public static boolean isV7() {

        // 7.15的class不存在时 走老版本
        boolean hasJxBrowserV7_15 = true;
        try {
            Class.forName("com.teamdev.jxbrowser.net.Scheme");
        } catch (ClassNotFoundException e) {
            hasJxBrowserV7_15 = false;
        }

        return OperatingSystem.isWindows() && hasJxBrowserV7_15;

    }
}
