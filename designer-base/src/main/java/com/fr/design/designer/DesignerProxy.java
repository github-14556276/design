package com.fr.design.designer;

/**
 * 暂时是空接口，标记一下模板设计时推出的代理其他 {@link TargetComponent} 的类
 */
public interface DesignerProxy {
}
