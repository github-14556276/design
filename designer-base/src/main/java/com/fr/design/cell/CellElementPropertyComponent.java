package com.fr.design.cell;

import com.fr.design.designer.TargetComponent;

import javax.swing.JPanel;

/**
 * 单元格属性配置面板接口
 * @author zack
 * @version 10.0
 * Created by zack on 2020/7/14
 */
public interface CellElementPropertyComponent {

    /**
     * 判断当前编辑的对象是否显示当前实现
     * @param tc
     * @return
     */
    boolean accept(TargetComponent tc);

    /**
     * 加载数据
     * @param tc
     */
    void populate(TargetComponent tc);

    /**
     * 返回当前属性面板,默认返回this
     * @return
     */
    JPanel toPanel();

}