package com.fr.design.selection;

import com.fr.design.designer.TargetComponent;

/**
 * 
 * @author zhou
 * @since 2012-7-26上午10:20:22
 */
public interface SelectableElement {

	/**
	 * 获取选中元素的快速编辑区域
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	QuickEditor getQuickEditor(TargetComponent tc);

	/**
	 * @Description 有些实现类中getQuickEditor会在获取editor后，再填充面板。这个方法只会获取editor，不会填充面板
	 * @param: tc
	 * @return com.fr.design.selection.QuickEditor
	 * @Author Henry.Wang
	 * @Date 2021/4/2 15:02
	 **/
	default QuickEditor getQuickEditorWithoutPopulate(TargetComponent tc){
		return null;
	}
}