package com.fr.design.mainframe.check;

import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.DesignSizeI18nManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.IOUtils;
import com.fr.i18n.UrlI18nManager;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * 字体缺失检测的具体结果对话框
 *
 */
public class CheckFontInfoDialog extends JDialog implements ActionListener {

    private JPanel topPanel;
    private JPanel upInTopPanel;
    private JPanel downInTopPanel;
    private JPanel hiddenPanel;
    private JPanel bottomPanel;

    private UILabel imageLabel;
    private UILabel directUiLabel;
    private UILabel detailLabel;

    public CheckFontInfoDialog(Frame parent, String areaText) {
        super(parent,true);
        //提示信息
        JPanel imagePanel = new JPanel();
        imageLabel = new UILabel(IOUtils.readIcon("com/fr/design/images/warnings/warning32.png"));
        imagePanel.add(imageLabel);

        JPanel messagePanel = FRGUIPaneFactory.createVerticalFlowLayout_S_Pane(true);
        MessageWithLink linkMessage = new MessageWithLink(Toolkit.i18nText("Fine_Designer_Check_Font_Message"),
                Toolkit.i18nText("Fine_Designer_Check_Font_Install_Font"),
                UrlI18nManager.getInstance().getI18nUrl("help.install.font")
                );
        linkMessage.setPreferredSize(DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.mainframe.check.CheckFontInfoDialog.messageWithLink"));
        messagePanel.add(linkMessage);

        // 查看详情按钮
        directUiLabel = new UILabel();
        directUiLabel.setIcon(UIManager.getIcon("OptionPane.narrow.right"));
        detailLabel = new UILabel();
        detailLabel.setText(Toolkit.i18nText("Fine_Designer_Look_Detail"));

        upInTopPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        upInTopPanel.add(imageLabel, BorderLayout.WEST);
        upInTopPanel.add(messagePanel, BorderLayout.CENTER);

        downInTopPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        downInTopPanel.add(directUiLabel, BorderLayout.WEST);
        downInTopPanel.add(detailLabel, BorderLayout.CENTER);

        topPanel = FRGUIPaneFactory.createVerticalFlowLayout_S_Pane(true);
        topPanel.add(upInTopPanel, BorderLayout.NORTH);
        topPanel.add(downInTopPanel, BorderLayout.SOUTH);

        //中间的详情展示(可隐藏)
        hiddenPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        hiddenPanel.setBorder(BorderFactory.createEmptyBorder(0,12,0,12));
        JScrollPane scrollPane = new JScrollPane();
        JTextArea checkArea = new JTextArea(areaText);
        scrollPane.setViewportView(checkArea);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        checkArea.setEnabled(false);
        hiddenPanel.add(scrollPane);
        hiddenPanel.setVisible(false);

        downInTopPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (hiddenPanel.isVisible()) {
                    hiddenPanel.setVisible(false);
                    CheckFontInfoDialog.this.setSize(DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.mainframe.check.CheckFontInfoDialog.collapse"));
                    detailLabel.setText(Toolkit.i18nText("Fine_Designer_Look_Detail"));
                    directUiLabel.setIcon(UIManager.getIcon("OptionPane.narrow.right"));
                } else {
                    CheckFontInfoDialog.this.setSize(DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.mainframe.check.CheckFontInfoDialog.unfold"));
                    hiddenPanel.setVisible(true);
                    detailLabel.setText(Toolkit.i18nText("Fine_Designer_Hide_Detail"));
                    directUiLabel.setIcon(UIManager.getIcon("OptionPane.narrow.down"));
                }
            }
        });

        //底部的按钮面板
        UIButton okButton = new UIButton(Toolkit.i18nText("Fine-Design_Report_OK"));
        okButton.addActionListener(this);
        bottomPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        bottomPanel.add(okButton, BorderLayout.EAST);

        this.setTitle(Toolkit.i18nText("Fine_Designer_Check_Font"));
        this.setResizable(false);

        this.add(topPanel, BorderLayout.NORTH);
        this.add(hiddenPanel, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);
        this.setSize(DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.mainframe.check.CheckFontInfoDialog.collapse"));

        GUICoreUtils.centerWindow(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.dispose();
    }
}
