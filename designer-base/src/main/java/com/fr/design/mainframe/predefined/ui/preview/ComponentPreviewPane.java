package com.fr.design.mainframe.predefined.ui.preview;

import com.fr.base.FRContext;
import com.fr.base.GraphHelper;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.general.Background;
import com.fr.general.FRFont;
import com.fr.general.act.BorderPacker;
import com.fr.general.act.TitlePacker;
import com.fr.stable.Constants;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by kerry on 2020-09-04
 */
public abstract class ComponentPreviewPane extends StyleSettingPreviewPane {
    private PredefinedStyle style = new PredefinedStyle();
    private static final int SMALL_GAP = 10;
    private static final int GAP = 10;
    private JPanel contentPane;
    private TitlePreviewPane titlePane;


    public ComponentPreviewPane() {
        this.setBackground(null);
        this.setOpaque(false);
        this.contentPane = createContentPane();
        this.titlePane = new TitlePreviewPane();
        this.titlePane.setPreferredSize(new Dimension(484, 35));
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));

        this.add(titlePane, BorderLayout.NORTH);
        this.add(contentPane, BorderLayout.CENTER);
    }

    protected abstract JPanel createContentPane();

    public void refresh() {
        this.repaint();
    }

    public void refresh(PredefinedStyle style) {
        this.style = style;
    }

    public void paintComponents(Graphics g) {
        BorderPacker borderStyle = style.getComponentStyle().getBorderStyle();

        updateBorders(g, borderStyle);

        paintTitle(g, borderStyle.getTitle());

        paintContentPane(g, borderStyle);

        super.paintComponents(g);
    }

    private void paintContentPane(Graphics g, BorderPacker borderStyle) {

        Graphics clipg;
        clipg = g.create(10, 40, this.contentPane.getWidth(), this.contentPane.getHeight());
        if (borderStyle.getBackground() != null) {
            borderStyle.getBackground().paint(clipg, clipg.getClipBounds());
        }
        clipg.dispose();
    }

    private void paintTitle(Graphics g, TitlePacker titlePacker) {
        Background background = titlePacker.getBackground();
        if (background != null) {
            background.paint(g, new Rectangle2D.Double(10, 5, this.titlePane.getWidth(), this.titlePane.getHeight()));
        }
        titlePane.setTitleObject(titlePacker);
        titlePane.paintComponent(g);
    }

    private void updateBorders(Graphics g, BorderPacker borderStyle) {
        if (borderStyle != null) {
            borderStyle.paint(g, new Rectangle2D.Double(SMALL_GAP, SMALL_GAP, getWidth() - GAP, getHeight() - SMALL_GAP - GAP));
        }
    }


    private class TitlePreviewPane extends JPanel {
        private FRFont frFont = null;
        private int titlePosition = Constants.LEFT;

        public TitlePreviewPane() {
            this.setBackground(null);
            this.setOpaque(false);
            frFont = FRContext.getDefaultValues().getFRFont();
        }

        @Override
        public void paint(Graphics g) {

        }

        public void paintComponent(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            Dimension d = getSize();
            if (frFont == null) {
                return;
            }
            FontMetrics fm = getFontMetrics(frFont);
            if (this.isEnabled()) {
                g2d.setColor(frFont.getForeground());
            } else {
                g2d.setColor(new Color(237, 237, 237));
            }
            g2d.setFont(frFont.applyResolutionNP(96));
            String paintText = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Preview_Title_Text");
            int startY = 0, startX = 0;
            startX = (d.width - fm.stringWidth(paintText)) / 2;
            startY = (d.height - fm.getHeight()) / 2 + fm.getAscent();
            if (this.titlePosition == Constants.LEFT) {
                startX = GAP;
            } else if (this.titlePosition == Constants.RIGHT) {
                startX = d.width - fm.stringWidth(paintText) - GAP - fm.getMaxAdvance();
            }
            GraphHelper.drawString(g2d, paintText, startX, startY);

        }

        public void setTitleObject(TitlePacker titlePacker) {
            this.frFont = titlePacker.getFrFont();
            this.titlePosition = titlePacker.getPosition();
        }
    }

}
