package com.fr.design.mainframe;

import com.fr.design.DesignState;
import com.fr.design.base.mode.DesignModeContext;
import com.fr.design.constants.UIConstants;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.file.MutilTempalteTabPane;
import com.fr.design.file.NewTemplatePane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.imenu.UIMenuHighLight;
import com.fr.design.gui.itoolbar.UILargeToolbar;
import com.fr.design.gui.itoolbar.UIToolbar;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.toolbar.ToolBarMenuDock;
import com.fr.design.mainframe.toolbar.ToolBarMenuDockPlus;
import org.jetbrains.annotations.Nullable;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.util.ArrayList;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/4/6
 */
public class CenterRegionContainerPane extends JPanel {

    private static volatile CenterRegionContainerPane THIS;

    private static final int LEFT_ALIGN_GAP = -5;

    private DesktopCardPane centerTemplateCardPane;

    private JPanel toolbarPane;//撤销重做 等工具栏 + 模板tab标签 + maybe have cpt 字体

    private JComponent toolbarComponent;//cpt 字体 等工具栏

    private JPanel eastPane;//=largeToolbar+eastCenterPane
    private UILargeToolbar largeToolbar;//预览

    private JPanel eastCenterPane;//=combineUp + templateTabPane

    private UIToolbar combineUp;//撤销重做 等工具栏

    private JPanel templateTabPane;//新建模板 + 模板tab标签
    private NewTemplatePane newWorkBookPane;//新建模板button


    public static CenterRegionContainerPane getInstance() {
        if (THIS == null) {
            synchronized (CenterRegionContainerPane.class) {
                if (THIS == null) {
                    THIS = new CenterRegionContainerPane();
                }
            }
        }
        return THIS;
    }

    public CenterRegionContainerPane() {

        toolbarPane = new JPanel() {

            @Override
            public Dimension getPreferredSize() {

                Dimension dim = super.getPreferredSize();
                // dim.height = TOOLBAR_HEIGHT;
                return dim;
            }
        };
        toolbarPane.setLayout(FRGUIPaneFactory.createBorderLayout());
        eastPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        eastPane.add(largeToolbar = getToolBarMenuDock().createLargeToolbar(), BorderLayout.WEST);
        eastCenterPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        combineUpTooBar();
        eastCenterPane.add(combineUp, BorderLayout.NORTH);
        templateTabPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        templateTabPane.add(newWorkBookPane = getToolBarMenuDock().getNewTemplatePane(), BorderLayout.WEST);
        templateTabPane.add(MutilTempalteTabPane.getInstance(), BorderLayout.CENTER);
        eastCenterPane.add(templateTabPane, BorderLayout.CENTER);

        eastPane.add(eastCenterPane, BorderLayout.CENTER);
        toolbarPane.add(eastPane, BorderLayout.NORTH);
        toolbarPane.add(new UIMenuHighLight(), BorderLayout.SOUTH);

        this.setLayout(new BorderLayout());
        this.add(centerTemplateCardPane = new DesktopCardPane(), BorderLayout.CENTER);
        this.add(toolbarPane, BorderLayout.NORTH);

    }

    private ToolBarMenuDock getToolBarMenuDock() {
        return DesignerContext.getDesignerFrame().getToolBarMenuDock();
    }

    /**
     * 创建上工具栏
     */
    private void combineUpTooBar() {
        combineUp = new UIToolbar(FlowLayout.LEFT);
        combineUp.setBorder(new MatteBorder(new Insets(0, LEFT_ALIGN_GAP, 1, 0), UIConstants.LINE_COLOR));
        combineUp.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 2));
        setUpUpToolBar(null);

    }


    /**
     * 重置上工具栏
     */
    private void resetCombineUpTooBar(JComponent[] toolbar4Form, ToolBarMenuDockPlus plus) {
        combineUp.removeAll();
        setUpUpToolBar(toolbar4Form);
        plus.insertToCombineUpToolbar(combineUp);
    }


    /**
     * 填充上工具栏的中的工具
     *
     * @param toolbar4Form 目标组件
     */
    private void setUpUpToolBar(@Nullable JComponent[] toolbar4Form) {
        UIButton[] fixButtons = getToolBarMenuDock().createUp();
        for (UIButton fixButton : fixButtons) {
            combineUp.add(fixButton);
        }
        if (!DesignModeContext.isAuthorityEditing()) {
            combineUp.addSeparator(new Dimension(2, 16));
            if (toolbar4Form != null) {
                for (JComponent jComponent : toolbar4Form) {
                    combineUp.add(jComponent);
                }
            }
            //添加检测按钮
            addCheckButton();
        }
        //添加分享按钮
        addShareButton();
        //添加插件中的按钮
        addExtraButtons();
    }


    private void addExtraButtons() {

        JTemplate<?, ?> jt = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (jt == null) {
            return;
        }


        UIButton[] extraButtons = jt.createExtraButtons();
        for (UIButton extraButton : extraButtons) {
            combineUp.add(extraButton);
        }
        if (extraButtons.length > 0) {
            combineUp.addSeparator(new Dimension(2, 16));
        }
    }

    private void addCheckButton() {
        JTemplate<?, ?> jt = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (jt == null) {
            return;
        }
        combineUp.addSeparator(new Dimension(2, 16));
        UIButton[] checkButtons = jt.createCheckButton();
        for (UIButton checkButton : checkButtons) {
            combineUp.add(checkButton);
        }
    }

    private void addShareButton() {

        JTemplate<?, ?> jt = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (jt == null) {
            return;
        }

        combineUp.addSeparator(new Dimension(2, 16));
        UIButton[] shareButtons = jt.createShareButton();
        for (UIButton shareButton : shareButtons) {
            combineUp.add(shareButton);
        }
    }

    /**
     * 检查
     *
     * @param flag 组件是否可见
     * @param al   组件名称
     */
    protected void checkCombineUp(boolean flag, ArrayList<String> al) {
        //Yvan: 检查当前是否为WORK_SHEET状态，因为只有WORK_SHEET中含有格式刷组件，此时是不需要进行checkComponentsByNames的
        JTemplate<?, ?> jTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (jTemplate != null) {
            // 第一个条件满足后还需要添加一重判断，判断是编辑报表块还是参数面板，编辑报表块时则直接return
            if (jTemplate.getMenuState() == DesignState.WORK_SHEET && !jTemplate.isUpMode()) {
                return;
            }
            combineUp.checkComponentsByNames(flag, al);
        }
    }


    /**
     * 重置相关的工具条.
     *
     * @param plus 工具条中相关信息
     */
    protected void resetToolkitByPlus(ToolBarMenuDockPlus plus, ToolBarMenuDock ad) {

        resetCombineUpTooBar(ad.resetUpToolBar(plus), plus);

        if (toolbarComponent != null) {
            toolbarPane.remove(toolbarComponent);
        }

        // 颜色，字体那些按钮的工具栏
        toolbarPane.add(toolbarComponent = ad.resetToolBar(toolbarComponent, plus), BorderLayout.CENTER);

        if (plus.hasToolBarPane()) {
            this.add(toolbarPane, BorderLayout.NORTH);
        } else {
            this.remove(toolbarPane);
        }

        resetByDesignMode();
    }

    private void resetByDesignMode() {
        if (DesignModeContext.isDuchampMode()) {
            eastPane.remove(largeToolbar);
            eastCenterPane.remove(templateTabPane);
            centerTemplateCardPane.refresh(HistoryTemplateListCache.getInstance().getCurrentEditingTemplate());
        } else {
            eastPane.add(largeToolbar, BorderLayout.WEST);
            eastCenterPane.add(templateTabPane, BorderLayout.CENTER);
        }

    }

    JComponent getToolbarComponent() {

        return this.toolbarComponent;
    }

    /**
     * 判断是否在权限编辑状态，若是在权限编辑状态，则需要有虚线框和关闭突变
     */
    protected void needToAddAuhtorityPaint() {
        newWorkBookPane.setButtonGray(DesignModeContext.isAuthorityEditing());

    }


    protected DesktopCardPane getCenterTemplateCardPane() {

        return centerTemplateCardPane;
    }

    protected void refreshUIToolBar() {
        if (toolbarComponent instanceof UIToolbar) {
            ((UIToolbar ) toolbarComponent).refreshUIToolBar();
        }
        combineUp.refreshUIToolBar();
        getToolBarMenuDock().updateEnable();
    }

}
