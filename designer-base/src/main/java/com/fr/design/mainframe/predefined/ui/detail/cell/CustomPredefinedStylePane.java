package com.fr.design.mainframe.predefined.ui.detail.cell;

import com.fr.base.ScreenResolution;
import com.fr.base.Style;
import com.fr.config.predefined.PredefinedCellStyle;
import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.AttrScrollPane;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.BasicScrollPane;
import com.fr.design.dialog.MultiTabPane;
import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.style.AbstractBasicStylePane;
import com.fr.design.gui.style.AlignmentPane;
import com.fr.design.gui.style.BorderPane;
import com.fr.design.gui.style.FormatPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;


/**
 * 哎，复杂的原型图导致复杂的画法。非我所愿也
 *
 * @author zhou
 * @since 2012-5-24上午10:36:10
 */
public class CustomPredefinedStylePane extends MultiTabPane<PredefinedCellStyle> {
    private PredefinedCellStyle cellStyle;
    private PreviewArea previewArea;
    private boolean populating;
    private AttributeChangeListener attributeChangeListener;


    public CustomPredefinedStylePane() {
        super();
        tabPane.setOneLineTab(true);
        tabPane.setDrawLine(false);
        tabPane.setBorder(BorderFactory.createLineBorder(UIConstants.SHADOW_GREY));
        tabPane.setLayout(new GridLayout(1, 3, 0, 0));
    }

    public void registerAttrChangeListener(AttributeChangeListener listener){
        this.attributeChangeListener = listener;
    }

    private void fireAttrChangeListener() {
        if (this.attributeChangeListener != null) {
            this.attributeChangeListener.attributeChange();
        }
    }

    /**
     * @return
     */
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Custom_Style");
    }


    /**
     *
     */
    public void reset() {
        populateBean(null);
    }

    @Override
    /**
     *
     */
    public void populateBean(PredefinedCellStyle ob) {
        this.populating = true;
        this.cellStyle = ob;
        for (int i = 0; i < paneList.size(); i++) {
            ((AbstractBasicStylePane) paneList.get(i)).populateBean(ob.getStyle());
            previewArea.preview(ob.getStyle());
        }
        this.populating = false;
    }

    @Override
    /**
     *
     */
    public PredefinedCellStyle updateBean() {
        AbstractBasicStylePane basicStylePane = (AbstractBasicStylePane) paneList.get(tabPane.getSelectedIndex());
        this.cellStyle.setStyle(basicStylePane.update(this.cellStyle.getStyle()));
        return this.cellStyle;
    }


    /**
     * @param ob
     * @return
     */
    public boolean accept(Object ob) {
        return ob instanceof PredefinedCellStyle;
    }

    @Override
    protected List<BasicPane> initPaneList() {
        paneList = new ArrayList<BasicPane>();
        paneList.add(new FormatPane());
        paneList.add(new BorderPane());
        paneList.add(new AlignmentPane());
        return paneList;
    }

    protected void initLayout() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 4));

        JPanel previewPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        previewArea = new PreviewArea();
        previewPane.setBorder(GUICoreUtils.createTitledBorder(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preview"), null));
        previewPane.add(previewArea, BorderLayout.CENTER);

        this.add(previewPane, BorderLayout.NORTH);

        this.add(jPanel, BorderLayout.CENTER);
        jPanel.add(tabPane, BorderLayout.NORTH);
        JPanel attrListenerPane = new AbstractAttrNoScrollPane() {

            @Override
            protected void initContentPane() {
                leftContentPane = createContentPane();
                this.add(leftContentPane, BorderLayout.CENTER);
            }

            @Override
            protected JPanel createContentPane() {
                this.addAttributeChangeListener(new AttributeChangeListener() {
                    @Override
                    public void attributeChange() {
                        if (populating) {
                            return;
                        }
                        PredefinedCellStyle cellStyle = updateBean();
                        if (cellStyle != null) {
                            previewArea.preview(cellStyle.getStyle());
                        }
                        fireAttrChangeListener();
                    }
                });
                BasicScrollPane basicScrollPane = new AttrScrollPane() {
                    @Override
                    protected JPanel createContentPane() {
                        return centerPane;
                    }
                };
                return basicScrollPane;
            }
        };
        jPanel.add(attrListenerPane, BorderLayout.CENTER);
    }

    @Override
    /**
     *
     */
    public void updateBean(PredefinedCellStyle ob) {
        return;
    }




    /**
     * 预览Style的面板
     *
     * @author richer
     */
    private static class PreviewArea extends JComponent {

        private String paintText = "Report";
        private Style style = Style.DEFAULT_STYLE;

        public PreviewArea() {
            setPreferredSize(new Dimension(40, 30));
        }

        public void preview(Style style) {
            this.style = style;
            repaint();
        }

        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            int resolution = ScreenResolution.getScreenResolution();

            if (style == Style.DEFAULT_STYLE) {
                // 如果是默认的style,就只写"Report"上去
                Style.paintContent(g2d, paintText, style, getWidth() - 3, getHeight() - 3, resolution);
                return;
            }

            Style.paintBackground(g2d, style, getWidth() - 3, getHeight() - 3);

            Style.paintContent(g2d, paintText, style, getWidth() - 3, getHeight() - 3, resolution);

            Style.paintBorder(g2d, style, getWidth() - 3, getHeight() - 3);
        }

        @Override
        public Dimension getMinimumSize() {
            return getPreferredSize();
        }
    }
}
