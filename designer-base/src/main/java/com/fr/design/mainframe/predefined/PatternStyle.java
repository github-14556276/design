package com.fr.design.mainframe.predefined;

import com.fr.config.predefined.PredefinedStyle;
//import com.fr.predefined.PredefinedPatternStyleManager;


/**
 * Created by kerry on 2020-08-31
 */
public enum PatternStyle {
    DARK_STYLE(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Dark_Pattern")) {
        @Override
        public PredefinedStyle getPredefinedStyle() {
//            return PredefinedPatternStyleManager.INSTANCE.getDarkMode();
            return new PredefinedStyle();
        }
    },
    LIGHT_STYLE(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Light_Pattern")) {
        @Override
        public PredefinedStyle getPredefinedStyle() {
//            return PredefinedPatternStyleManager.INSTANCE.getLightMode();
            return new PredefinedStyle();
        }
    };


    private String name;


    PatternStyle(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public abstract PredefinedStyle getPredefinedStyle();

}
