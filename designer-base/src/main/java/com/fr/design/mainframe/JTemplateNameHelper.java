package com.fr.design.mainframe;

import com.fr.design.file.TemplateTreePane;
import com.fr.design.gui.itree.filetree.TemplateFileTree;
import com.fr.stable.StringUtils;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/5/7
 */
public class JTemplateNameHelper {

    private static final int PREFIX_NUM = 3000;

    private static short currentIndex = 0;// 此变量用于多次新建模板时，让名字不重复

    public static String newTemplateNameByIndex(String prefix) {
        // 用于获取左侧模板的文件名，如左侧已包含"WorkBook1.cpt, WorkBook12.cpt, WorkBook177.cpt"
        // 那么新建的文件名将被命名为"WorkBook178.cpt",即取最大数+1
        TemplateFileTree tt = TemplateTreePane.getInstance().getTemplateFileTree();
        DefaultMutableTreeNode gen = (DefaultMutableTreeNode) tt.getModel().getRoot();
        String[] str = new String[gen.getChildCount()];

        List<Integer> reportNum = new ArrayList<>();
        for (int j = 0; j < gen.getChildCount(); j++) {
            str[j] = gen.getChildAt(j).toString();
            //返回文件名中的index(算法中没有再匹配文件后缀了，因为DefaultMutableTreeNode中已经匹配过了)
            Integer index = getFileNameIndex(prefix, str[j]);
            if (index != null) {
                reportNum.add(index);
            }
        }
        Collections.sort(reportNum);
        int idx = reportNum.size() > 0 ? reportNum.get(reportNum.size() - 1) + 1 : 1;

        idx = idx + currentIndex;
        currentIndex++;
        return prefix + idx;
    }

    /**
     * @return java.lang.Integer WorkBook11.cpt则返回11，如果没有找到index返回null
     * @Description 返回文件名中的index
     * @param: prefix 前缀
     * @param: fileName 文件名称全名
     * @Author Henry.Wang
     * @Date 2021/4/9 11:13
     **/
    private static Integer getFileNameIndex(String prefix, String fileName) {
		if (fileName.length() <= prefix.length()) {
            return null;
        }
        char[] chars = new char[fileName.length()];
        int i = 0;
        for (; i < fileName.length(); i++) {
            char c = fileName.charAt(i);
            //匹配前缀
            if (i < prefix.length()) {
                if (c != prefix.charAt(i)) {
                    return null;
                }
            } else {
                if (c == '.') {
                    break;
                } else {
                    //匹配0~9
                    if (c < 48 || c > 57) {
                        return null;
                    }
                    chars[i - prefix.length()] = c;
                }
            }
        }
        String s = new String(chars).substring(0, i - prefix.length());
        if (StringUtils.isBlank(s)) {
            return null;
        }
        return Integer.valueOf(s);
    }
}
