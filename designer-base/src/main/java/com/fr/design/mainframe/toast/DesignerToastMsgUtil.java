package com.fr.design.mainframe.toast;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.stable.Constants;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

/**
 * Created by kerry on 5/6/21
 */
public class DesignerToastMsgUtil {
    private static final int MIN_WIDTH = 134;
    private static final int MAX_WIDTH = 454;
    private static final Icon PROMPT_ICON = BaseUtils.readIcon("/com/fr/design/images/toast/toast_prompt.png");
    private static final Icon WARNING_ICON = BaseUtils.readIcon("/com/fr/design/images/toast/toast_warning.png");

    private DesignerToastMsgUtil() {

    }


    public static void toastPrompt(JPanel contendPane) {
        toastPane(PROMPT_ICON, contendPane, DesignerContext.getDesignerFrame());
    }

    public static void toastWarning(JPanel contendPane) {
        toastPane(WARNING_ICON, contendPane, DesignerContext.getDesignerFrame());
    }

    public static void toastWarning(JPanel contendPane, Window parent) {
        toastPane(WARNING_ICON, contendPane, parent);
    }

    public static void toastPrompt(String promptInfo) {
        toastPrompt(toastPane(promptInfo));
    }

    public static void toastWarning(String warningInfo) {
        toastWarning(toastPane(warningInfo));
    }

    public static void toastWarning(String warningInfo, Window parent) {
        toastWarning(toastPane(warningInfo), parent);
    }

    private static JPanel toastPane(String text) {
        UILabel promptLabel = new UILabel("<html>" + text + "</html>");
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        int width = promptLabel.getPreferredSize().width;
        if (width > MAX_WIDTH) {
            Dimension dimension = calculatePreferSize(text, promptLabel.getFont(), width);
            jPanel.setPreferredSize(dimension);
        }
        jPanel.add(promptLabel, BorderLayout.NORTH);
        return jPanel;
    }

    private static void toastPane(Icon icon, JPanel contendPane, Window parent) {
        JPanel pane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel uiLabel = new UILabel(icon);
        uiLabel.setVerticalAlignment(SwingConstants.TOP);
        uiLabel.setBorder(BorderFactory.createEmptyBorder(3, 0, 0, 0));
        pane.add(uiLabel, BorderLayout.WEST);
        pane.add(contendPane, BorderLayout.CENTER);
        pane.setBorder(BorderFactory.createEmptyBorder(8, 15, 8, 15));
        contendPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        ToastMsgDialog dialog;
        if (parent instanceof Dialog) {
            dialog = new ToastMsgDialog((Dialog) parent, pane);
        } else {
            dialog = new ToastMsgDialog((Frame) parent, pane);
        }
        dialog.setVisible(true);
    }


    private static Dimension calculatePreferSize(String text, Font font, int width) {
        int limitWidth = Math.max(MIN_WIDTH, width);
        limitWidth = Math.min(MAX_WIDTH, limitWidth);
        return new Dimension(limitWidth, getHtmlHeight(text, limitWidth, font));
    }


    private static int getHtmlHeight(String content, int width, Font font) {
        StringBuffer limitDiv = new StringBuffer("<div style='width:").append(width)
                .append("px;height:100%").append(getFontWrapStyle(font)).append(";'>");
        return getHtmlContentDimension(content, limitDiv).height;
    }


    private static Dimension getHtmlContentDimension(String content, StringBuffer limitDiv) {
        content = limitDiv.append(content).append("</div>").toString();
        JEditorPane editorPane = new JEditorPane();
        editorPane.setContentType("text/html");
        editorPane.setText(content);
        return editorPane.getPreferredSize();
    }

    private static String getFontWrapStyle(Font font) {
        double dpi96 = Constants.FR_PAINT_RESOLUTION;
        double dpi72 = Constants.DEFAULT_FONT_PAINT_RESOLUTION;
        return new StringBuilder()
                .append(";font-size:").append(font.getSize() * dpi96 / dpi72)
                .append("pt;font-family:").append(font.getFontName())
                .toString();
    }

}
