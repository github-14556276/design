package com.fr.design.mainframe;

import com.fr.design.DesignState;
import com.fr.design.DesignerEnvManager;
import com.fr.design.ExtraDesignClassManager;
import com.fr.design.fun.TitlePlaceProcessor;
import com.fr.design.gui.imenu.UIMenuHighLight;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.loghandler.LogMessageBar;
import com.fr.design.mainframe.toolbar.ToolBarMenuDock;
import com.fr.design.mainframe.toolbar.ToolBarMenuDockPlus;
import com.fr.design.menu.MenuManager;
import com.fr.design.os.impl.SupportOSImpl;
import com.fr.general.GeneralContext;
import com.fr.plugin.context.PluginContext;
import com.fr.plugin.injectable.PluginModule;
import com.fr.plugin.manage.PluginFilter;
import com.fr.plugin.observer.PluginEvent;
import com.fr.plugin.observer.PluginEventListener;
import com.fr.plugin.observer.PluginEventType;
import com.fr.stable.os.support.OSBasedAction;
import com.fr.stable.os.support.OSSupportCenter;

import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/4/6
 */
public class NorthRegionContainerPane extends JPanel {

    private static volatile NorthRegionContainerPane THIS;

    private JMenuBar menuBar;

    private PluginFilter pluginFilter = new PluginFilter() {

        @Override
        public boolean accept(PluginContext context) {

            return context.contain(PluginModule.ExtraDesign);
        }
    };

    private volatile boolean existDesignExtraPlugin;

    public static NorthRegionContainerPane getInstance() {
        if (THIS == null) {
            synchronized (NorthRegionContainerPane.class) {
                if (THIS == null) {
                    THIS = new NorthRegionContainerPane();
                }
            }
        }
        return THIS;
    }

    public NorthRegionContainerPane() {

        ToolBarMenuDock ad = DesignerContext.getDesignerFrame().getToolBarMenuDock();

        this.setLayout(new BorderLayout());
        this.add(new UIMenuHighLight(), BorderLayout.SOUTH);
        this.add(initNorthEastPane(ad), BorderLayout.EAST);
    }

    /**
     * @param ad 菜单栏
     * @return panel
     */
    protected JPanel initNorthEastPane(final ToolBarMenuDock ad) {
        //hugh: private修改为protected方便oem的时候修改右上的组件构成
        //顶部日志+登陆按钮
        final JPanel northEastPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        initPluginListener(northEastPane, ad);
        refreshNorthEastPane(northEastPane, ad);
        return northEastPane;
    }

    private void initPluginListener(JPanel northEastPane, ToolBarMenuDock ad) {
        //优先级为-1，保证最后全面刷新一次
        PluginEventListener pluginOnRunOrStopListener = new PluginEventListener(-1) {

            @Override
            public void on(PluginEvent event) {
                refreshAll(northEastPane, ad);
            }
        };

        // 在设计器启动时仅在最后一个插件启用时候进行刷新一次  如果插件启用过程中存在实现了设计器接口的插件
        PluginEventListener afterAllPluginsActiveListener = new PluginEventListener() {
            @Override
            public void on(PluginEvent event) {
                //优先级为-1，保证最后全面刷新一次
                GeneralContext.listenPluginRunningChanged(pluginOnRunOrStopListener, pluginFilter);
                // 在设计器启动时仅在最后一个插件启用时候进行刷新一次  如果插件启用过程中存在实现了设计器接口的插件
                boolean needRefresh = DesignerContext.getDesignerFrame() != null && DesignerContext.getDesignerFrame().isVisible() && existDesignExtraPlugin;
                if (needRefresh) {
                    refreshAll(northEastPane, ad);
                }
            }
        };
        PluginEventListener beforeAllPluginStopListener = new PluginEventListener() {
            @Override
            public void on(PluginEvent event) {
                GeneralContext.stopListenPlugin(pluginOnRunOrStopListener);
            }
        };
        PluginEventListener pluginEventListener = new PluginEventListener() {
            @Override
            public void on(PluginEvent event) {
                existDesignExtraPlugin = true;
            }
        };
        GeneralContext.listenPluginRunningChanged(pluginEventListener, pluginFilter);
        GeneralContext.listenPlugin(PluginEventType.AfterAllActive, afterAllPluginsActiveListener);
        GeneralContext.listenPlugin(PluginEventType.BeforeAllStop, beforeAllPluginStopListener);

    }

    private void refreshAll(JPanel northEastPane, ToolBarMenuDock ad) {
        refreshNorthEastPane(northEastPane, ad);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (DesignerContext.getDesignerFrame() == null) {
                    return;
                }
                DesignerContext.getDesignerFrame().refresh();
                DesignerContext.getDesignerFrame().repaint();
            }
        });
    }

    private void refreshNorthEastPane(final JPanel northEastPane, final ToolBarMenuDock ad) {

        northEastPane.removeAll();
        northEastPane.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        northEastPane.add(LogMessageBar.getInstance());
        TitlePlaceProcessor processor = ExtraDesignClassManager.getInstance().getSingle(TitlePlaceProcessor.MARK_STRING);
        if (processor != null) {
            final Component[] bbsLoginPane = {null};
            OSSupportCenter.buildAction(new OSBasedAction() {
                @Override
                public void execute(Object... objects) {
                    bbsLoginPane[0] = ad.createBBSLoginPane();
                }
            }, SupportOSImpl.USERINFOPANE);
            processor.hold(northEastPane, LogMessageBar.getInstance(), bbsLoginPane[0]);
        }
        northEastPane.add(ad.createAlphaFinePane());
        if (!DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().isEnabled()) {
            ad.createAlphaFinePane().setVisible(false);
        }
        northEastPane.add(ad.createNotificationCenterPane());

        OSSupportCenter.buildAction(new OSBasedAction() {
            @Override
            public void execute(Object... objects) {
                northEastPane.add(ad.createBBSLoginPane());
            }
        }, SupportOSImpl.USERINFOPANE);

    }

    /**
     * 重置相关的工具条.
     *
     * @param plus 工具条中相关信息
     */
    void resetToolkitByPlus(ToolBarMenuDockPlus plus, ToolBarMenuDock ad) {
        DesignState designState = new DesignState(plus);
        MenuManager.getInstance().setMenus4Designer(designState);
        if (menuBar == null) {
            this.add(menuBar = ad.createJMenuBar(plus), BorderLayout.CENTER);
        } else {
            ad.resetJMenuBar(menuBar, plus);
        }
    }
}
