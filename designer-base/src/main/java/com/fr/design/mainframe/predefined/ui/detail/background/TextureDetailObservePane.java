package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.base.background.TextureBackground;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.style.background.texture.TextureDetailPane;
import com.fr.general.Background;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

/**
 * Created by kerry on 2020-09-02
 */
public class TextureDetailObservePane extends AbstractBackgroundDetailPane<TextureBackground> implements UIObserver {
    private TextureDetailPane detailPane;

    private UIObserverListener listener;

    public TextureDetailObservePane() {

        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        detailPane = TextureDetailPane.createMiniTextureDetailPane(6);

        detailPane.setPreferredSize(new Dimension(160, 108));
        UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Texture"));
        label.setPreferredSize(new Dimension(24, 108));
        label.setVerticalAlignment(SwingConstants.TOP);
        JPanel jPanel = TableLayoutHelper.createGapTableLayoutPane(
                new Component[][]{new Component[]{label, detailPane}}, TableLayoutHelper.FILL_LASTCOLUMN, 33, 5);
        jPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 10));


        detailPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (listener != null) {
                    listener.doChange();
                }
            }
        });
        this.add(jPanel, BorderLayout.CENTER);
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;

    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    @Override
    public void populate(TextureBackground background) {
        this.detailPane.populate(background);
    }

    @Override
    public TextureBackground update() {
        try {
            return (TextureBackground) this.detailPane.update();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return null;
    }


    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Texture");
    }

    @Override
    public boolean accept(Background background) {
        return background instanceof TextureBackground;
    }


    @Override
    public void addChangeListener(ChangeListener changeListener) {

    }
}
