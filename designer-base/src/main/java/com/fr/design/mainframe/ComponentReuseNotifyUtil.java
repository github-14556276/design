package com.fr.design.mainframe;

import com.fr.design.DesignerEnvManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.reuse.ComponentReuseNotificationInfo;
import com.fr.design.mainframe.toast.DesignerToastMsgUtil;
import com.fr.design.notification.SnapChat;
import com.fr.design.notification.SnapChatFactory;
import com.fr.design.notification.SnapChatKey;

/**
 * Created by kerry on 5/8/21
 */
public class ComponentReuseNotifyUtil {
    private static final String COMPONENT_SNAP_CHAT_KEY = "com.fr.component.share-components";

    private ComponentReuseNotifyUtil() {

    }

    public static void enterWidgetLibExtraAction() {
        if (ComponentReuseNotificationInfo.getInstance().isClickedWidgetLib()) {
            return;
        }
        SnapChat snapChat = SnapChatFactory.createSnapChat(false, new SnapChatKey() {
            @Override
            public String calc() {
                return COMPONENT_SNAP_CHAT_KEY;
            }
        });
        if (snapChat.hasRead()) {
            DesignerToastMsgUtil.toastPrompt(Toolkit.i18nText("Fine-Design_Component_Reuse_Merge_Prompt"));
        }
        ComponentReuseNotificationInfo.getInstance().setClickedWidgetLib(true);
        DesignerEnvManager.getEnvManager().saveXMLFile();
    }


}
