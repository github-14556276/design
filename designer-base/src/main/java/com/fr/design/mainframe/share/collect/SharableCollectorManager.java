package com.fr.design.mainframe.share.collect;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.stable.StringUtils;
import com.fr.third.joda.time.DateTime;
import com.fr.third.joda.time.Days;
import com.fr.third.joda.time.format.DateTimeFormat;
import com.fr.third.joda.time.format.DateTimeFormatter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * created by Harrison on 2020/03/25
 **/
public class SharableCollectorManager {

    /**
     * 1 天
     */
    private static final int DELTA = 1;

    /**
     * 发送间隔 5 分钟
     */
    private static final long SEND_DELAY = 300 * 1000L;

    /**
     * 线程
     */
    private ScheduledExecutorService service;

    private static class ConfigManagerHolder {
        private static SharableCollectorManager instance = new SharableCollectorManager();
    }

    public static SharableCollectorManager getInstance() {

        return ConfigManagerHolder.instance;
    }

    public void execute() {

        service = Executors
                .newSingleThreadScheduledExecutor(new NamedThreadFactory("SharableComponentCollectorManager", true));
        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                sendCloudCenter();
            }
        }, SEND_DELAY, SEND_DELAY, TimeUnit.MILLISECONDS);
    }

    public void shutdown() {

        ComponentCollector.getInstance().saveInfo();
        if (service != null) {
            service.shutdown();
        }
    }

    private void sendCloudCenter() {

        String lastTime = ComponentCollector.getInstance().getLastTime();
        if (validate(lastTime)) {
            boolean sendSuccess = ComponentSender.send();
            String currentTime = DateTime.now().toString("yyyy-MM-dd");
            ComponentCollector.getInstance().setLastTime(currentTime);
            if (sendSuccess) {
                ComponentCollector.getInstance().clear();
            }
        }
        ComponentCollector.getInstance().saveInfo();
    }

    private boolean validate(String lastTime) {

        if (StringUtils.isEmpty(lastTime)) {
            return true;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime last = formatter.parseDateTime(lastTime);
        DateTime current = DateTime.now();
        return Days.daysBetween(last, current).getDays() >= DELTA;
    }


}
