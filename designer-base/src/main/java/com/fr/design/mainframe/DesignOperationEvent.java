package com.fr.design.mainframe;

import com.fr.event.Event;
import com.fr.event.Null;

/**
 * Created by kerry on 4/28/21
 */
public enum DesignOperationEvent implements Event<Null> {

    CELL_STYLE_MODIFY,

    CELL_IMAGE_VALUE_MODIFY

}
