package com.fr.design.mainframe.predefined.ui.preview;

import javax.swing.JPanel;

/**
 * Created by kerry on 2020-08-31
 */
public abstract class StyleSettingPreviewPane extends JPanel {
    public StyleSettingPreviewPane() {

    }


    public abstract void refresh();


}
