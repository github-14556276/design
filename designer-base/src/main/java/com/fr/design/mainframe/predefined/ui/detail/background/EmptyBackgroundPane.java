package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.general.Background;

/**
 * Created by kerry on 2020-09-01
 */
public class EmptyBackgroundPane extends AbstractBackgroundDetailPane {

    @Override
    public void populate(Background background) {

    }

    @Override
    public Background update() {
        return null;
    }

    /**
     * 名称
     *
     * @return 名称
     */
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Null");
    }

    public boolean accept(Background background) {
        return background == null;
    }


}
