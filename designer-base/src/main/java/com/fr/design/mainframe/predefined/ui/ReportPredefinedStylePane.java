package com.fr.design.mainframe.predefined.ui;

import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.JTemplate;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

/**
 * Created by kerry on 2020-08-28
 */
public class ReportPredefinedStylePane extends BasicPane {
    private PredefinedStyleSelectPane selectPane;
    private JTemplate currentTemplate;

    public ReportPredefinedStylePane(JTemplate jTemplate) {
        this.currentTemplate = jTemplate;
        initPane();
    }

    private void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel jPanel = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Select"));
        jPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        JPanel subPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        this.selectPane = new PredefinedStyleSelectPane(currentTemplate.getTemplatePredefinedStyle(), false);
        subPanel.add(this.selectPane, BorderLayout.CENTER);
        jPanel.add(subPanel, BorderLayout.CENTER);
        this.add(jPanel, BorderLayout.CENTER);
    }

    public void update() {
        PredefinedStyle style = selectPane.update();
        if (style != null) {
//            currentTemplate.resetPredefinedStyle(style.getStyleName());
        }
    }

    public void refresh() {
        this.selectPane.refreshPane();
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Template_Style");
    }
}
