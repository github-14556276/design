package com.fr.design.mainframe;

import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.i18n.Toolkit;
import com.fr.stable.StableUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/12
 */
public class TemplateSavingChecker {


    public static boolean check() {
        List<String> list = getSavingTemplate();
        if (!list.isEmpty()) {
            FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(),
                                              Toolkit.i18nText("Fine-Design_Close_Template_Tip", StableUtils.join(list, "、")),
                                              Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                                              FineJOptionPane.INFORMATION_MESSAGE);
            return false;
        }
        return true;
    }


    private static List<String> getSavingTemplate() {
        List<String> result = new ArrayList<>();
        for (JTemplate<?, ?> template : HistoryTemplateListCache.getInstance().getHistoryList()) {
            if (template.isSaving()) {
                result.add(template.getEditingFILE().getName());
            }
        }
        return result;
    }

}
