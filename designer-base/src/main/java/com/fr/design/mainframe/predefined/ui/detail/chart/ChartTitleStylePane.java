package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.i18n.Toolkit;

import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-16
 */
public class ChartTitleStylePane extends AbstractChartStylePane {

    //字体样式
    private ChartFontPane chartFontPane;

    protected void initComponents() {
        chartFontPane = new ChartFontPane() {
            public String getUILabelText() {
                return Toolkit.i18nText("Fine-Design_Chart_Title_Character");
            }
        };
    }

    protected Component[][] getComponent() {
        return new Component[][]{
                new Component[]{chartFontPane, null}
        };
    }

    protected double[] getRows(double p) {
        return new double[]{p};
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Report_Title");
    }

    public void populate(PredefinedChartStyle chartStyle) {
        chartFontPane.populate(chartStyle.getTitleFont());
    }


    public void update(PredefinedChartStyle chartStyle) {
        chartStyle.setTitleFont(chartFontPane.update());
    }
}
