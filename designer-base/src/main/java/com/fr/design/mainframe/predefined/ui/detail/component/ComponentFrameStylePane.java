package com.fr.design.mainframe.predefined.ui.detail.component;

import com.fr.base.GraphHelper;
import com.fr.base.Utils;
import com.fr.config.predefined.PredefinedComponentStyle;
import com.fr.design.gui.frpane.UINumberDragPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIButtonUI;
import com.fr.design.gui.ibutton.UIColorButton;
import com.fr.design.gui.icombobox.LineComboBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.style.BackgroundSpecialPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.backgroundpane.GradientBackgroundQuickPane;
import com.fr.general.act.BorderPacker;
import com.fr.stable.Constants;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.RoundRectangle2D;

/**
 * Created by kerry on 2020-09-01
 */
public class ComponentFrameStylePane extends ComponentStylePane {
    private final static String[] BORDER_STYLE = new String[]{com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Common"), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Shadow")};
    private static final double ALPHA_MAX_NUMBER = 100;
    private final static int[] BORDER_LINE_STYLE_ARRAY = new int[]{
            Constants.LINE_NONE,
            Constants.LINE_THIN, //1px
            Constants.LINE_MEDIUM, //2px
            Constants.LINE_THICK, //3px
    };

    //渲染风格
    private UIComboBox borderStyleCombo;
    //边框粗细
    private LineComboBox currentLineCombo;
    //边框圆角
    private UISpinner borderCornerSpinner;
    //边框颜色
    private UIColorButton currentLineColorPane;
    //主体背景
    private BackgroundSpecialPane backgroundPane;
    //透明度
    private UINumberDragPane numberDragPane;

    public ComponentFrameStylePane() {
        initPane();
    }

    protected void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        this.borderStyleCombo = new UIComboBox(BORDER_STYLE);
        this.currentLineCombo = new LineComboBox(BORDER_LINE_STYLE_ARRAY);
        this.currentLineColorPane = new UIColorButton(null);
        this.borderCornerSpinner = new UISpinner(0, 1000, 1, 0);
        currentLineColorPane.setUI(getButtonUI(currentLineColorPane));
        currentLineColorPane.set4ToolbarButton();
        currentLineColorPane.setPreferredSize(new Dimension(20, 20));
        JPanel buttonPane = new JPanel(new BorderLayout());
        buttonPane.add(currentLineColorPane, BorderLayout.WEST);
        backgroundPane = new BackgroundSpecialPane();
        JPanel transparencyPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        this.numberDragPane = new UINumberDragPane(0, 100);
        transparencyPane.add(numberDragPane, BorderLayout.CENTER);
        transparencyPane.add(new UILabel(" %"), BorderLayout.EAST);

        double p = TableLayout.PREFERRED;
        double[] rowSize = {p, p, p, p, p, p, p, p};
        double[] columnSize = {p, 157};
        JPanel rightTopContentPane = TableLayoutHelper.createCommonTableLayoutPane(new JComponent[][]{
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Render_Style")), borderStyleCombo},
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Border_Line")), currentLineCombo},
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Border_Color")), buttonPane},
                getBorderCornerSpinnerComp(),
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Body_Background")), backgroundPane},
                {new UILabel(""), new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Alpha"))},
                {new UILabel(""), transparencyPane},
        }, rowSize, columnSize, 10);
        UIScrollPane rightTopPane = new UIScrollPane(rightTopContentPane);
        rightTopPane.setBorder(BorderFactory.createEmptyBorder());
        this.add(rightTopPane, BorderLayout.CENTER);
    }


    private JComponent[] getBorderCornerSpinnerComp() {
        return new JComponent[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Radius") + ":"), borderCornerSpinner};
    }


    protected UIButtonUI getButtonUI(final UIColorButton uiColorButton) {
        return new UIButtonUI() {

            public void paint(Graphics g, JComponent c) {
                UIButton b = (UIButton) c;
                g.setColor(Color.black);
                GraphHelper.draw(g, new RoundRectangle2D.Double(1, 1, b.getWidth() - 2, b.getHeight() - 2, 0, 0), 1);

                if (b.getModel().isEnabled()) {
                    g.setColor(uiColorButton.getColor());
                } else {
                    g.setColor(new Color(Utils.filterRGB(uiColorButton.getColor().getRGB(), 50)));
                }
                g.fillRect(2, 2, b.getWidth() - 3, b.getHeight() - 3);
            }
        };
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Frame");
    }


    @Override
    public void populate(PredefinedComponentStyle componentStyle) {
        BorderPacker borderStyle = componentStyle.getBorderStyle();
        this.borderStyleCombo.setSelectedIndex(borderStyle.getBorderStyle());
        this.borderCornerSpinner.setValue(borderStyle.getBorderRadius());
        this.currentLineCombo.setSelectedLineStyle(borderStyle.getBorder());
        this.currentLineColorPane.setColor(borderStyle.getColor());
        this.backgroundPane.populateBean(borderStyle.getBackground());
        numberDragPane.populateBean(borderStyle.getAlpha() * ALPHA_MAX_NUMBER);
    }

    @Override
    public void update(PredefinedComponentStyle componentStyle) {
        BorderPacker style = componentStyle.getBorderStyle();
        style.setBorderStyle(borderStyleCombo.getSelectedIndex());
        style.setBorderRadius((int) borderCornerSpinner.getValue());
        style.setBorder(currentLineCombo.getSelectedLineStyle());
        style.setColor(currentLineColorPane.getColor());
        style.setBackground(backgroundPane.update());
        style.setAlpha((float) (numberDragPane.updateBean() / ALPHA_MAX_NUMBER));
    }
}
