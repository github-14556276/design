package com.fr.design.editlock;

import com.fr.report.LockItem;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 * 服务器数据集的checker
 */
public class ServerTableDataLockChangeChecker extends EditLockChangeChecker{
    private static class Holder {
        private static final ServerTableDataLockChangeChecker INSTANCE = new ServerTableDataLockChangeChecker();
    }

    public static ServerTableDataLockChangeChecker getInstance() {
        return ServerTableDataLockChangeChecker.Holder.INSTANCE;
    }

    public ServerTableDataLockChangeChecker() {
        this.lockItem = LockItem.SERVER_TABLE_DATA;
    }
}
