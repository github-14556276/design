package com.fr.design.editlock;

import java.util.EventListener;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 */
public interface EditLockChangeListener extends EventListener {
    /**
     * 锁定状态改变后执行的动作
     * @param event 事件
     */
    void updateLockedState(EditLockChangeEvent event);
}
