package com.fr.design.editlock;

import java.util.EventObject;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 */
public class EditLockChangeEvent extends EventObject {

    private boolean isLocked;

    /**
     * @param source 锁状态发生了改变，且当前锁状态就是source
     */
    public EditLockChangeEvent(boolean source) {
        super(source);
        this.isLocked = source;
    }

    public boolean isLocked() {
        return isLocked;
    }
}
