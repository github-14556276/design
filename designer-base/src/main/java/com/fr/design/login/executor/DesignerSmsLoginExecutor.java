package com.fr.design.login.executor;

import com.fr.design.extra.Process;
import com.fr.design.extra.exe.Command;
import com.fr.design.extra.exe.Executor;
import com.fr.design.login.service.DesignerPassportManager;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/28
 */
public class DesignerSmsLoginExecutor implements Executor {
    private String result = "[]";

    private String regionCode;
    private String phone;
    private String code;

    public DesignerSmsLoginExecutor(String regionCode, String phone, String code) {
        this.regionCode = regionCode;
        this.phone = phone;
        this.code = code;
    }

    @Override
    public String getTaskFinishMessage() {
        return result;
    }

    @Override
    public Command[] getCommands() {
        return new Command[]{
                new Command() {
                    @Override
                    public String getExecuteMessage() {
                        return null;
                    }

                    @Override
                    public void run(Process<String> process) {
                        result = DesignerPassportManager.getInstance().smsLogin(regionCode, phone, code);
                    }
                }
        };
    }
}
