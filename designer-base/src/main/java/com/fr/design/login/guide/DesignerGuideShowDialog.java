package com.fr.design.login.guide;

import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.UIDialog;
import com.fr.design.utils.gui.GUICoreUtils;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import javax.swing.JPanel;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerGuideShowDialog extends UIDialog {

    private static final Dimension DEFAULT = new Dimension(700, 577);

    public DesignerGuideShowDialog(Frame frame, BasicPane pane) {
        super(frame);
        setUndecorated(true);
        JPanel panel = (JPanel) getContentPane();
        panel.setLayout(new BorderLayout());
        add(pane, BorderLayout.CENTER);
        setSize(DEFAULT);
        GUICoreUtils.centerWindow(this);
        setResizable(false);
    }

    @Override
    public void checkValid() throws Exception {

    }
}
