package com.fr.design.login.task;

import com.fr.design.bridge.exec.JSCallback;
import com.fr.design.bridge.exec.JSUtils;
import com.fr.design.extra.Process;
import com.fr.design.extra.exe.Command;
import com.fr.design.extra.exe.Executor;
import com.fr.stable.StringUtils;
import javax.swing.SwingWorker;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/27
 */
public class DesignerLoginTaskWorker<V> extends SwingWorker<V, String> {

    private Executor executor;
    private JSCallback callback;

    public DesignerLoginTaskWorker(final JSCallback callback, final Executor executor) {
        this.executor = executor;
        this.callback = callback;
    }

    @Override
    protected V doInBackground() throws Exception {
        Command[] commands = executor.getCommands();
        for (Command command : commands) {
            String message = command.getExecuteMessage();
            if (StringUtils.isNotBlank(message)) {
                publish(message);
            }
            command.run(new Process<String>() {
                @Override
                public void process(String s) {
                    if (StringUtils.isNotBlank(s)) {
                        publish(JSUtils.trimText(s));
                    }
                }
            });
        }
        return null;
    }

    @Override
    protected void done() {
        String result = executor.getTaskFinishMessage();
        callback.execute(result);
    }
}
