package com.fr.design.login.message;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.DesignerEnvManager;
import com.fr.design.dialog.NotificationDialog;
import com.fr.design.dialog.NotificationDialogAction;
import com.fr.design.event.DesignerOpenedListener;
import com.fr.design.login.utils.DesignerLoginUtils;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.CloudCenter;
import com.fr.general.http.HttpToolbox;
import com.fr.json.JSON;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/11
 */
public class DesignerMessageHelper {

    private static final long DELAY = 7L;
    private static final String STATUS = "status";
    private static final String DATA = "data";
    private static final String SUCCESS = "success";
    private static final String MESSAGE_ID = "messageId";
    private static final String TITLE = "title";
    private static final String BODY = "body";
    private static final String JUMP_TYPE = "jumpType";
    private static final String JUMP_TO = "jumpTo";

    private static DesignerMessageHelper instance;

    private DesignerMessageHelper() {
    }

    public static DesignerMessageHelper getInstance() {
        if (instance == null) {
            instance = new DesignerMessageHelper();
        }
        return instance;
    }

    public void prepareShowMessage() {
        if (!DesignerLoginUtils.isOnline()) {
            return;
        }
        DesignerContext.getDesignerFrame().addDesignerOpenedListener(new DesignerOpenedListener() {
            @Override
            public void designerOpened() {
                ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("DesignerMessageHelper"));
                service.schedule(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            pullLatestMessage();
                        } catch (Exception e) {
                            FineLoggerFactory.getLogger().error(e.getMessage(), e);
                        }
                    }
                }, DELAY, TimeUnit.MINUTES);
                service.shutdown();
            }
        });
    }

    private void pullLatestMessage() throws Exception {
        String url = CloudCenter.getInstance().acquireUrlByKind("designer.message.push", "https://market.fanruan.com/api/v1/message/designer");
        Map<String, String> params = new HashMap<>();
        params.put("designerId", DesignerEnvManager.getEnvManager().getUUID());
        String result = HttpToolbox.post(url, params);
        JSONObject response = JSONFactory.createJSON(JSON.OBJECT, result);
        String status = response.optString(STATUS);
        if (SUCCESS.equals(status)) {
            parseLatestMessage(response.optJSONObject(DATA));
        }
    }

    private void parseLatestMessage(JSONObject data) {
        String messageId = data.optString(MESSAGE_ID);
        String title = data.optString(TITLE);
        String body = data.optString(BODY);
        int jumpType = data.optInt(JUMP_TYPE);
        String jumpTo = data.optString(JUMP_TO);
        if (StringUtils.isNotEmpty(messageId) && StringUtils.isNotEmpty(title) && StringUtils.isNotEmpty(body) && jumpType > 0 && StringUtils.isNotEmpty(jumpTo)) {
            NotificationJumpType notificationJumpType = NotificationJumpType.valueOf(jumpType);
            if (notificationJumpType == NotificationJumpType.WEB_URL) {
                NotificationDialog.defaultPopup(messageId, title, body, new NotificationDialogAction() {
                    @Override
                    public String name() {
                        return jumpTo;
                    }

                    @Override
                    public void doClick() {
                        String ssoUrl = DesignerLoginUtils.generateDesignerSSOUrl(jumpTo);
                        BrowseUtils.browser(ssoUrl);
                    }
                });
            } else if (notificationJumpType == NotificationJumpType.DESIGNER_MODULE) {
                NotificationActionType notificationActionType = NotificationActionType.valueOf(jumpTo);
                NotificationDialog.defaultPopup(messageId, title, body, notificationActionType.getAction());
            }
        }
    }
}
