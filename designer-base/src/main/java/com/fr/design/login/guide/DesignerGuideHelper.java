package com.fr.design.login.guide;

import com.fr.base.FRContext;
import com.fr.design.DesignerEnvManager;
import com.fr.design.dialog.UIDialog;
import com.fr.design.event.DesignerOpenedListener;
import com.fr.design.login.utils.DesignerLoginUtils;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.os.impl.SupportOSImpl;
import com.fr.design.update.push.DesignerPushUpdateManager;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import javax.swing.WindowConstants;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerGuideHelper {

    private static final String MAIN_RESOURCE_PATH = "/com/fr/design/login/guide.html";
    private static final String JXBROWSER = "com.teamdev.jxbrowser.browser.Browser";
    private static final long ONE_WEEK = 7 * 24 * 3600 * 1000L;
    private static final long ONE_MONTH = 30 * 24 * 3600 * 1000L;
    private static final long SIX_MONTH = 6 * ONE_MONTH;

    private static UIDialog dialog = null;

    public static String getMainResourcePath() {
        return MAIN_RESOURCE_PATH;
    }

    public static UIDialog getDialog() {
        return dialog;
    }

    public static void prepareShowGuideDialog() {
        // 如果存在更新升级的弹窗，则不显示引导页面
        if (!DesignerLoginUtils.isOnline()
                || !SupportOSImpl.DESIGNER_LOGIN.support()
                || !FRContext.isChineseEnv()
                || DesignerPushUpdateManager.getInstance().isShouldPopUp()
                || DesignerEnvManager.getEnvManager().isUseOldVersionLogin()) {
            return;
        }
        if (isActivatedForOneWeek()) {
            if (isLogin() && !isLoginForSixMonths()) {
                return;
            }
            if (selectedDoNotRemindInOneMonth()) {
                return;
            }
            DesignerContext.getDesignerFrame().addDesignerOpenedListener(new DesignerOpenedListener() {
                @Override
                public void designerOpened() {
                    try {
                        showGuideDialog();
                    } catch (Throwable t) {
                        FineLoggerFactory.getLogger().warn(t.getMessage(), t);
                    }
                }
            });
        }
    }

    /**
     * 激活满一周
     */
    private static boolean isActivatedForOneWeek() {
        DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
        String key = manager.getActivationKey();
        if (StringUtils.isEmpty(key)) {
            return false;
        }
        return (System.currentTimeMillis() - manager.getDesignerActivatedTime()) > ONE_WEEK;
    }

    /**
     * 已经登录
     */
    private static boolean isLogin() {
        return DesignerEnvManager.getEnvManager().getDesignerLoginUid() > 0;
    }

    /**
     * 已经登录满六个月
     */
    private static boolean isLoginForSixMonths() {
        return isLogin() && (System.currentTimeMillis() - DesignerEnvManager.getEnvManager().getDesignerLastLoginTime()) > SIX_MONTH;
    }

    /**
     * 一个月内不再提醒
     */
    private static boolean selectedDoNotRemindInOneMonth() {
        return DesignerEnvManager.getEnvManager().isDesignerLoginDoNotRemind()
                && (System.currentTimeMillis() - DesignerEnvManager.getEnvManager().getDesignerLoginDoNotRemindSelectedTime()) <= ONE_MONTH;
    }

    private static void showGuideDialog() {
        boolean hasJxBrowser = true;
        try {
            Class.forName(JXBROWSER);
        } catch (ClassNotFoundException e) {
            hasJxBrowser = false;
        }
        if (hasJxBrowser) {
            showGuidePane();
        }
    }

    private static void showGuidePane() {
        DesignerGuidePane designerGuidePane = new DesignerGuidePane();
        if (dialog == null) {
            dialog = new DesignerGuideShowDialog(DesignerContext.getDesignerFrame(), designerGuidePane);
        }
        dialog.setVisible(true);
    }

    public static void closeWindow() {
        if (dialog != null) {
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dialog.setVisible(false);
            dialog = null;
        }
    }

    public static void main(String[] args) {
        DesignerEnvManager.getEnvManager().setOpenDebug(true);
        showGuideDialog();
    }
}
