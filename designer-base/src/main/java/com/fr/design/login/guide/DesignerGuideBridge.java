package com.fr.design.login.guide;

import com.fr.design.DesignerEnvManager;
import com.fr.design.bridge.exec.JSBridge;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.login.DesignerLoginHelper;
import com.fr.design.login.DesignerLoginSource;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSObject;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerGuideBridge {

    public static DesignerGuideBridge getBridge(Browser browser) {
        return new DesignerGuideBridge(browser);
    }

    private JSObject window;

    private DesignerGuideBridge(Browser browser) {
        this.window = browser.executeJavaScriptAndReturnValue("window").asObject();
    }

    @JSBridge
    public String i18nText(String key) {
        return Toolkit.i18nText(key);
    }

    @JSBridge
    public void closeWindow(boolean doNotRemind, boolean login) {
        if (login) {
            DesignerGuideHelper.closeWindow();
            DesignerLoginHelper.showLoginDialog(DesignerLoginSource.GUIDE);
            checkDoNotRemind(doNotRemind);
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String[] options = new String[]{
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Quit"),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Return_Login")
                    };
                    int rv = FineJOptionPane.showConfirmDialog(
                            DesignerGuideHelper.getDialog(),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Quit_Tip"),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[1]
                    );
                    if (rv == JOptionPane.YES_OPTION) {
                        DesignerGuideHelper.closeWindow();
                        checkDoNotRemind(doNotRemind);
                    } else if (rv == JOptionPane.NO_OPTION) {
                        DesignerLoginHelper.showLoginDialog(DesignerLoginSource.GUIDE);
                        DesignerGuideHelper.closeWindow();
                        checkDoNotRemind(doNotRemind);
                    }
                }
            });
        }
    }

    /**
     * 用户勾选了一个月内不再提醒
     */
    private void checkDoNotRemind(boolean doNotRemind) {
        DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
        manager.setDesignerLoginDoNotRemind(doNotRemind);
        if (doNotRemind) {
            manager.setDesignerLoginDoNotRemindSelectedTime(System.currentTimeMillis());
        }
    }
}
