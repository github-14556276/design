package com.fr.design.plugin;

import com.fr.design.dialog.UIDialog;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/3/8
 */
public class DesignerPluginContext {

    private static UIDialog DIALOG;

    public static UIDialog getPluginDialog() {
        return DIALOG;
    }

    public static void setPluginDialog(UIDialog pluginDialog) {
        DesignerPluginContext.DIALOG = pluginDialog;
    }
}
