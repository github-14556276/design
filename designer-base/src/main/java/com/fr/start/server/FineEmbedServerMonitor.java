package com.fr.start.server;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.gui.iprogressbar.ProgressDialog;
import com.fr.design.i18n.Toolkit;
import com.fr.design.locale.impl.SupportLocaleImpl;
import com.fr.design.mainframe.DesignerContext;
import com.fr.event.Event;
import com.fr.event.EventDispatcher;
import com.fr.event.Listener;
import com.fr.event.Null;
import com.fr.general.FRFont;
import com.fr.general.locale.LocaleAction;
import com.fr.general.locale.LocaleCenter;

import javax.swing.plaf.ColorUIResource;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 内置服务器启动监视器
 *
 * @author zack
 * @date 2018/8/21
 */
public class FineEmbedServerMonitor {
    private int progress;
    private static final int STEP = 1;
    /**
     * 40ms更新进度
     */
    private static final int STEP_HEARTBEAT = 40;
    private static volatile FineEmbedServerMonitor monitor;
    private static ProgressDialog progressDialog = new ProgressDialog(DesignerContext.getDesignerFrame());
    //由于默认值的字体不支持韩文，所以要对韩文单独生成字体
    private FRFont font = null;
    private static final int FONT_RGB = 333334;
    private static final int FONT_SIZE = 14;
    private static final String FONT_NAME = "Dialog";

    private FineEmbedServerMonitor() {
    }

    static {
        EventDispatcher.listen(EmbedServerEvent.AfterStop, new Listener<Null>() {
            @Override
            public void on(Event event, Null aNull) {
                getInstance().reset();
                progressDialog.dispose();
            }
        });
    }

    public static FineEmbedServerMonitor getInstance() {
        if (monitor == null) {
            synchronized (FineEmbedServerMonitor.class) {
                if (monitor == null) {
                    monitor = new FineEmbedServerMonitor();
                }
            }
        }
        return monitor;
    }

    public int getProgress() {
        if (progress == progressDialog.getProgressMaximum()) {
            return progress;
        } else {
            progress += STEP;
            return progress;
        }
    }

    public void setComplete() {
        this.progress = progressDialog.getProgressMaximum();
    }

    public void reset() {
        this.progress = 0;
    }

    public boolean isComplete() {
        return this.progress >= progressDialog.getProgressMaximum();
    }

    public void monitor() {
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1,
                new NamedThreadFactory("FineEmbedServerMonitor"));
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (isComplete()) {
                    scheduler.shutdown();
                    progressDialog.dispose();
                    return;
                }
                if (!progressDialog.isVisible()) {
                    progressDialog = new ProgressDialog(DesignerContext.getDesignerFrame());
                    progressDialog.setVisible(true);
                    //如果为韩文则改变字体
                    LocaleCenter.buildAction(new LocaleAction() {
                        @Override
                        public void execute() {
                            font = FRFont.getInstance().applySize(FONT_SIZE).applyForeground(new ColorUIResource(FONT_RGB)).applyName(FONT_NAME);
                        }
                    }, SupportLocaleImpl.SUPPORT_KOREA);
                    String text = Toolkit.i18nText("Fine-Design_Basic_Loading_Embed_Server");
                    progressDialog.updateLoadingText(text, font);
                }
                progressDialog.setProgressValue(getProgress());
            }
        }, 0, STEP_HEARTBEAT, TimeUnit.MILLISECONDS);

    }
}
