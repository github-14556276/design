package com.fr.env;

import com.fr.design.versioncheck.VersionCheckUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralContext;
import com.fr.general.GeneralUtils;
import com.fr.general.IOUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * @author pengda
 * @version 10.0
 * Created  on 2021-06-02
 */
public class VersionCheckMessageDialog extends JDialog implements ActionListener {
    private UILabel imageLabel;
    private UILabel detailLabel;
    private JPanel centerPanel;
    private JPanel detailPanel;
    private JPanel body;
    private String envName;

    public VersionCheckMessageDialog(Frame parent, String message, String envName) {
        super(parent, true);
        this.envName = envName;
        init(message);
    }

    private void init(String message) {
        JPanel imagePanel = new JPanel();
        imageLabel = new UILabel(IOUtils.readIcon("com/fr/design/images/warnings/warning32.png"));
        imagePanel.add(imageLabel);
        JPanel messagePanel = FRGUIPaneFactory.createVerticalFlowLayout_S_Pane(true);
        UILabel messageText = new UILabel();
        messageText.setText("<html>" + message + "</html>");
        messagePanel.add(messageText);

        centerPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        centerPanel.setBorder(BorderFactory.createEmptyBorder(15, 10, 10, 10));
        centerPanel.add(imagePanel, BorderLayout.WEST);
        centerPanel.add(messagePanel, BorderLayout.CENTER);

        detailLabel = new UILabel();
        detailLabel.setText(Toolkit.i18nText("Fine_Designer_Look_Detail"));
        detailLabel.setForeground(Color.BLUE);

        detailPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        detailPanel.add(detailLabel, BorderLayout.EAST);

        String localBranch = GeneralUtils.readFullBuildNO();
        String remoteBranch = VersionCheckUtils.getRemoteBranch(envName);
        List<String> noExistServiceDescription;
        if(ComparatorUtils.equals(localBranch,remoteBranch)) {
            noExistServiceDescription = new ArrayList<>();
        }else{
            noExistServiceDescription = VersionCheckUtils.getNoExistServiceDescription(this.envName);
        }

        detailPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                hideDialog();
                CheckServiceDialog checkServiceDialog = new CheckServiceDialog(DesignerContext.getDesignerFrame(), localBranch, remoteBranch, noExistServiceDescription);
                checkServiceDialog.setVisible(true);
            }
        });

        body = FRGUIPaneFactory.createBorderLayout_S_Pane();
        body.add(centerPanel, BorderLayout.CENTER);
        body.add(detailPanel, BorderLayout.SOUTH);

        this.setTitle(Toolkit.i18nText("Fine-Design_Basic_Sync_Prompt"));
        this.setResizable(false);
        this.add(body, BorderLayout.NORTH);
        this.setSize(new Dimension(GeneralContext.getLocale().equals(Locale.US) ? 300 : 280, 135));
        GUICoreUtils.centerWindow(this);
    }

    private void hideDialog(){
        this.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        hideDialog();
    }
}
