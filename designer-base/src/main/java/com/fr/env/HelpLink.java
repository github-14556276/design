package com.fr.env;


import com.fr.i18n.UrlI18nManager;
import com.fr.locale.InterProviderFactory;
import com.fr.stable.StringUtils;
import java.util.HashMap;
import java.util.Map;

/**
 * 错误提示中的跳转链接管理
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/9
 */
public class HelpLink {

    public static String getLink(String solution) {
        Map<String, String> map = new HashMap<>();
        String link = UrlI18nManager.getInstance().getI18nUrl("help.remote.design");
        map.put(InterProviderFactory.getProvider().getLocText("Fine-Core_Remote_Design_Change_PassWord"), StringUtils.EMPTY);
        map.put(InterProviderFactory.getProvider().getLocText("Fine-Core_Remote_Design_Cert_Error_Solution"), link);
        map.put(InterProviderFactory.getProvider().getLocText("Fine-Core_Remote_Design_Connection_Unknown_Error_Solution"), link);
        map.put(InterProviderFactory.getProvider().getLocText("Fine-Core_Remote_Design_NetWork_Connection_Error_Solution"), link);
        return map.get(solution);
    }

}
