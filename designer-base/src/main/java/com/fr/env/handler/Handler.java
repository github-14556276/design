package com.fr.env.handler;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public interface Handler<T, R> {

    /**
     * @param t
     * @return 是否需要继续处理
     */
    R  handle(T t);

}
