package com.fr.env.handler.impl;

import com.fr.base.exception.ExceptionDescriptor;
import com.fr.env.handler.Handler;
import com.fr.env.handler.RefWrapper;
import com.fr.env.handler.ResultWrapper;
import com.fr.workspace.engine.convert.ExceptionConverter;

/**
 * 出现预料之外的情况异常处理器
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public class UnexpectedHandler implements Handler<RefWrapper, ResultWrapper> {

    @Override
    public ResultWrapper handle(RefWrapper wrapper) {
        Throwable e = wrapper.getThrowable();
        if (!(e instanceof ExceptionDescriptor)) {
            return new ResultWrapper(ExceptionConverter.getInstance().convert(e)) ;
        }
        return new ResultWrapper(e);
    }
}
