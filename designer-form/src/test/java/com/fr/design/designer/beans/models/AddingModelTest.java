package com.fr.design.designer.beans.models;

import com.fr.base.chart.BaseChartCollection;
import com.fr.config.dao.DaoContext;
import com.fr.config.dao.impl.LocalClassHelperDao;
import com.fr.config.dao.impl.LocalEntityDao;
import com.fr.config.dao.impl.LocalXmlEntityDao;
import com.fr.design.designer.creator.CRPropertyDescriptor;
import com.fr.design.designer.creator.XChartEditor;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.designer.creator.XWTitleLayout;
import com.fr.design.designer.creator.cardlayout.XWCardLayout;
import com.fr.design.gui.chart.MiddleChartComponent;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.module.DesignModuleFactory;
import com.fr.form.main.Form;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WCardLayout;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.stable.core.PropertyChangeListener;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.swing.JComponent;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.beans.IntrospectionException;

@PrepareForTest({DesignModuleFactory.class})
@PowerMockIgnore({"com.sun.*", "javax.*", "com.fr.jvm.assist.*"})
@RunWith(PowerMockRunner.class)
public class AddingModelTest {
    
    @Before
    public void setUp() {
        DaoContext.setXmlEntityDao(new LocalXmlEntityDao());
        DaoContext.setClassHelperDao(new LocalClassHelperDao());
        DaoContext.setEntityDao(new LocalEntityDao());
    }
    
    @After
    public void tearDown() {
        DaoContext.setXmlEntityDao(null);
        DaoContext.setClassHelperDao(null);
        DaoContext.setEntityDao(null);
    }
    
    /**
     * 默认名字 + i
     */
    @Test
    public void testInstantiateCreator() throws Exception {
        
        Dimension dimension = new Dimension(20, 20);
        
        ChartEditor chartEditor1 = new ChartEditor();
        XCreator xCreator1 = new DemoCreator(chartEditor1, dimension, "test");
        
        ChartEditor chartEditor2 = new ChartEditor();
        chartEditor2.setWidgetName("test02");
        XCreator xCreator2 = new DemoCreator(chartEditor2, dimension, "test02");
        xCreator1.add(xCreator2);
        
        ChartEditor chartEditor3 = new ChartEditor();
        chartEditor3.setWidgetName("test03");
        WAbsoluteLayout.BoundsWidget boundsWidget = new WAbsoluteLayout.BoundsWidget(chartEditor3, new Rectangle(dimension));
        WTitleLayout wTitleLayout03 = new WTitleLayout();
        wTitleLayout03.addWidget(boundsWidget);
        //需要和内部的 widget 一样
        wTitleLayout03.setWidgetName("test03");
        XWTitleLayout xCreator3 = new XWTitleLayout(wTitleLayout03, dimension);
        xCreator1.add(xCreator3);
        
        AddingModel addingModel = new AddingModel(xCreator1, 20, 20);
        
        Form form = EasyMock.mock(Form.class);
        EasyMock.expect(form.isNameExist("test0")).andReturn(true).once();
        EasyMock.expect(form.isNameExist("test03")).andReturn(true).once();
        EasyMock.expect(form.isNameExist(EasyMock.anyString())).andReturn(false).anyTimes();
        EasyMock.replay(form);
        
        FormDesigner mock = EasyMock.mock(FormDesigner.class);
        EasyMock.expect(mock.getTarget()).andReturn(form).anyTimes();
        EasyMock.replay(mock);
        
        addingModel.instantiateCreator(mock);
        //没有默认参数, 但已经存在 test
        Assert.assertEquals("test1", xCreator1.toData().getWidgetName());
        //直接返回
        Assert.assertEquals("test020", xCreator2.toData().getWidgetName());
        //已经存在，后接0
        Assert.assertEquals("test030", xCreator3.toData().getWidgetName());
    }
    
    @Test
    public void testInstantiateCreator_cardLayout() throws Exception {
        
        Form form = EasyMock.mock(Form.class);
        EasyMock.expect(form.isNameExist("cardlayout0")).andReturn(true).once();
        EasyMock.expect(form.isNameExist("cardlayout1")).andReturn(true).once();
        EasyMock.expect(form.isNameExist(EasyMock.anyString())).andReturn(false).anyTimes();
        EasyMock.replay(form);
        
        FormDesigner mock = EasyMock.mock(FormDesigner.class);
        EasyMock.expect(mock.getTarget()).andReturn(form).anyTimes();
        EasyMock.replay(mock);
    
        WCardLayout wCardLayout = new WCardLayout(20, 20);
        XWCardLayout xwCardLayout = new XWCardLayout(wCardLayout, new Dimension(40, 40));
        AddingModel addingModel = new AddingModel(mock, xwCardLayout);
        Assert.assertEquals("cardlayout2", xwCardLayout.toData().getWidgetName());
    
        //依赖于 cardlayout 创建 container
        XLayoutContainer parentLayOut = xwCardLayout.initCreatorWrapper(80);
        //组件默认名 tablelayout2
        AddingModel parentModel = new AddingModel(mock, parentLayOut);
        //经过处理 tablayout20
        Assert.assertEquals("tablayout20", parentLayOut.toData().getWidgetName());
        Assert.assertEquals("tabpane20", ((XCreator) (parentLayOut.getComponent(0))).getXCreator().toData().getWidgetName());
        Assert.assertEquals("cardlayout20", xwCardLayout.toData().getWidgetName());
    
    }
    
    @Test
    public void testInstantiateCreator_containsNotXCreator() throws Exception {
        
        Form form = EasyMock.mock(Form.class);
        EasyMock.expect(form.isNameExist(EasyMock.anyString())).andReturn(false).anyTimes();
        EasyMock.replay(form);
        
        FormDesigner mock = EasyMock.mock(FormDesigner.class);
        EasyMock.expect(mock.getTarget()).andReturn(form).anyTimes();
        EasyMock.replay(mock);
        
        PowerMock.mockStaticPartial(DesignModuleFactory.class, "getChartComponent");
        EasyMock.expect(DesignModuleFactory.getChartComponent(EasyMock.anyObject(BaseChartCollection.class))).andReturn(new MiddleChartComponent() {
            @Override
            public void populate(BaseChartCollection cc) {
            
            }
            
            @Override
            public BaseChartCollection update() {
                return null;
            }
            
            @Override
            public void reset() {
            
            }
            
            @Override
            public void addStopEditingListener(PropertyChangeListener list) {
            
            }
        }).anyTimes();
        PowerMock.replayAll();
        
        Dimension dimension = new Dimension(20, 20);
        
        ChartEditor chartEditor1 = new ChartEditor();
        XCreator xCreator1 = new XChartEditor(chartEditor1, dimension);
        
        
        AddingModel chartModel = new AddingModel(mock, xCreator1);
        Assert.assertEquals("chart0", xCreator1.toData().getWidgetName());
    }
    
    private static class DemoCreator extends XCreator {
        
        private String widgetName;
    
        public DemoCreator(Widget ob, Dimension initSize, String defaultName) {
            super(ob, initSize);
            this.widgetName = defaultName;
        }
        
        @Override
        public CRPropertyDescriptor[] supportedDescriptor() throws IntrospectionException {
            return new CRPropertyDescriptor[0];
        }
        
        @Override
        protected JComponent initEditor() {
            return null;
        }
        
        @Override
        protected void initXCreatorProperties() {
        
        }
        
        @Override
        public String createDefaultName() {
            return this.widgetName;
        }
    }
    
}