package com.fr.design.mainframe.share.ui.online.widgetpackage;

import com.fr.design.mainframe.share.ui.block.OnlineWidgetPackageBlock;
import com.fr.design.mainframe.share.ui.block.PreviewWidgetBlock;
import com.fr.design.mainframe.share.ui.online.OnlineWidgetSelectPane;
import com.fr.form.share.base.DataLoad;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.constants.ShareComponentConstants;

/**
 * Created by kerry on 2020-10-21
 */
public class OnlineWidgetPackageSelectPane extends OnlineWidgetSelectPane {
    private final OnlineWidgetPackagesShowPane parentPane;

    public OnlineWidgetPackageSelectPane(OnlineShareWidget[] providers, int widgetsPerNum, OnlineWidgetPackagesShowPane parentPane) {
        super(providers, widgetsPerNum);
        this.parentPane = parentPane;

    }

    public OnlineWidgetPackageSelectPane(DataLoad<OnlineShareWidget> dataLoad, int widgetsPerNum, OnlineWidgetPackagesShowPane parentPane) {
        super(dataLoad, widgetsPerNum);
        this.parentPane = parentPane;

    }

    public void showWidgetDetailPane(String id) {
        this.parentPane.showWidgetDetailPane(id);
    }

    protected PreviewWidgetBlock<OnlineShareWidget> createWidgetBlock(OnlineShareWidget provider) {
        return new OnlineWidgetPackageBlock(provider, this);
    }

    protected int getPaneHeight(int count) {
        return count * (ShareComponentConstants.SHARE_PACKAGE_BLOCK_HEIGHT + V_GAP);
    }

}
