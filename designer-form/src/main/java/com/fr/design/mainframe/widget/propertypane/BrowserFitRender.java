package com.fr.design.mainframe.widget.propertypane;

import com.fr.design.mainframe.widget.renderer.EncoderCellRenderer;

public class BrowserFitRender extends EncoderCellRenderer {

    public BrowserFitRender() {
        super(new BrowserFitWrapper());
    }
}
