package com.fr.design.mainframe;

import com.fr.base.GraphHelper;
import com.fr.design.form.util.XCreatorConstants;
import com.fr.design.icon.IconPathConstants;
import com.fr.general.IOUtils;

import com.fr.stable.Constants;

import java.awt.BasicStroke;
import java.awt.Stroke;
import javax.swing.JComponent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;


/**
 * Created with IntelliJ IDEA.
 * User: zx
 * Date: 14-7-24
 * Time: 上午9:09
 */
public class CoverPane extends JComponent {
    private AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);
    protected static final int BORDER_WIDTH = 2;
    public static final int EDIT_BTN_W = 75;
    public static final int EDIT_BTN_H = 20;
    private static final int BORDER_GAP = 2;
    private static final BasicStroke BORDER_STROKE = new BasicStroke(2f);

    public static void paintEditButton(Graphics g, Component component) {
        int x = 0;
        int y = 0;
        int w = component.getWidth();
        int h = component.getHeight();

        Graphics2D g2d = (Graphics2D) g;
        Composite oldComposite = g2d.getComposite();
        //画白色的编辑层
        paintCover(g, component);
        //画编辑按钮所在框
        FormDesigner formDesigner = WidgetPropertyPane.getInstance().getEditingFormDesigner();
        AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, formDesigner.getCursor().getType() != Cursor.DEFAULT_CURSOR ? 0.9f : 0.7f);
        g2d.setColor(XCreatorConstants.EDIT_COLOR);
        g2d.setComposite(alphaComposite);
        g2d.fillRoundRect((x + w / 2 - EDIT_BTN_W / 2), (y + h / 2 - EDIT_BTN_H / 2), EDIT_BTN_W, EDIT_BTN_H, 4, 4);
        g2d.setComposite(oldComposite);
        //画编辑按钮图标
        BufferedImage image = IOUtils.readImage(IconPathConstants.EDIT_ICON_PATH);
        g2d.drawImage(
                image,
                (x + w / 2 - 23),
                (y + h / 2 - image.getHeight() / 2),
                image.getWidth(),
                image.getHeight(),
                null
        );
        g2d.setColor(Color.WHITE);
        //画编辑文字
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.drawString(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Edit"), x + w / 2 - 2, y + h / 2 + 5);

        g.setColor(XCreatorConstants.FORM_BORDER_COLOR);
        GraphHelper.draw(g, new Rectangle(BORDER_WIDTH, BORDER_WIDTH, w - BORDER_WIDTH * 2, h - BORDER_WIDTH * 2), Constants.LINE_MEDIUM);
    }

    /**
     * 绘制悬浮层
     * @param g
     * @param component
     */
    public static void paintCover(Graphics g, Component component) {
        Graphics2D g2d = (Graphics2D) g;
        Composite oldComposite = g2d.getComposite();
        //画白色的编辑层
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 50 / 100.0F));
        g2d.setColor(XCreatorConstants.COVER_COLOR);
        g2d.fillRect(0, 0, component.getWidth(), component.getHeight());
        g2d.setComposite(oldComposite);
        g2d.setColor(XCreatorConstants.FORM_BORDER_COLOR);
        Stroke oldStroke = g2d.getStroke();
        g2d.setStroke(BORDER_STROKE);
        g2d.drawRect(BORDER_GAP, BORDER_GAP, component.getWidth() - BORDER_GAP * 2, component.getHeight() - BORDER_GAP * 2);
        g2d.setStroke(oldStroke);
    }

    public CoverPane() {
        setBackground(null);
        setOpaque(false);
    }

    public AlphaComposite getComposite() {
        return composite;
    }

    public void setComposite(AlphaComposite composite) {
        this.composite = composite;
    }


    public void paint(Graphics g) {
        super.paint(g);
        paintCover(g, this);
    }
}
