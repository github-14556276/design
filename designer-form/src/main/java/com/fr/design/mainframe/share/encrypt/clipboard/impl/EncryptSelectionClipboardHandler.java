package com.fr.design.mainframe.share.encrypt.clipboard.impl;

import com.fr.design.mainframe.FormSelection;
import com.fr.design.mainframe.share.encrypt.clipboard.CrossClipboardHandler;

/**
 * 组件选择
 * <p>
 * created by Harrison on 2020/05/18
 **/
public class EncryptSelectionClipboardHandler extends CrossClipboardHandler<FormSelection> {
    private static EncryptSelectionClipboardHandler selectionClipboardHandler;

    public static EncryptSelectionClipboardHandler getInstance() {
        if (selectionClipboardHandler == null) {
            selectionClipboardHandler = new EncryptSelectionClipboardHandler();
        }
        return selectionClipboardHandler;
    }
    
    public EncryptSelectionClipboardHandler() {
        
        super(new CrossTemplateClipBoardState(), new CrossLayoutClipBoardState());
    }
    
    @Override
    public boolean support(Object selection) {
        
        return selection instanceof FormSelection;
    }
}
