package com.fr.design.mainframe.share.ui.base;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

/**
 * Created by kerry on 2020-10-23
 */
public class LoadingPane extends JPanel {

    public LoadingPane() {
        this(Toolkit.i18nText("Fine-Design_Share_Online_Loading"));
    }

    public LoadingPane(String message) {
        JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel borderPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        borderPane.setPreferredSize(new Dimension(120, 120));
        ImagePanel imagePanel = new ImagePanel("/com/fr/base/images/share/loading.gif");
        imagePanel.setPreferredSize(new Dimension(45, 45));
        borderPane.setBorder(BorderFactory.createEmptyBorder(150, 95, 5, 75));
        borderPane.add(imagePanel);

        panel.add(borderPane, BorderLayout.CENTER);

        JPanel labelPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel topLabel = new UILabel(Toolkit.i18nText(message), SwingConstants.CENTER);
        topLabel.setForeground(Color.GRAY);
        labelPanel.add(topLabel, BorderLayout.CENTER);
        labelPanel.setPreferredSize(new Dimension(240, 20));
        panel.add(labelPanel, BorderLayout.SOUTH);

        panel.setPreferredSize(new Dimension(240, 230));


        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(100, 0, 0, 0));
        this.add(panel, BorderLayout.NORTH);
    }

}
