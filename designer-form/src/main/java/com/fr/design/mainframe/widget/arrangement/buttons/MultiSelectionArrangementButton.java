package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.mainframe.ArrangementType;

import javax.swing.Icon;
import java.awt.event.ActionListener;

public interface MultiSelectionArrangementButton {
    Icon getIcon();

    String getTipText();

    ActionListener getActionListener();

    ArrangementType getArrangementType();
}
