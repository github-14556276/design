package com.fr.design.mainframe.share.action;

import com.fr.design.actions.UpdateAction;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ifilechooser.ExtensionFilter;
import com.fr.design.gui.ifilechooser.FileChooserArgs;
import com.fr.design.gui.ifilechooser.FileChooserFactory;
import com.fr.design.gui.ifilechooser.FileChooserProvider;
import com.fr.design.gui.ifilechooser.FileSelectionMode;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.share.ui.base.FailureMessagePane;
import com.fr.design.mainframe.share.ui.local.LocalWidgetRepoPane;
import com.fr.design.mainframe.share.util.InstallUtils;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.form.share.record.ShareWidgetInfoManager;
import com.fr.form.share.utils.ReuxUtils;
import com.fr.log.FineLoggerFactory;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.SwingWorker;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/8
 */

public class InstallComponentAction extends UpdateAction {

    public InstallComponentAction() {
        this.putValue(Action.SMALL_ICON, null);
        this.setName(Toolkit.i18nText("Fine-Design_Share_Install"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        FileChooserProvider fileChooserProvider = FileChooserFactory.createFileChooser(
                    FileChooserArgs.newBuilder().
                        setFileSelectionMode(FileSelectionMode.MULTIPLE_FILE).
                        setMultiSelectionEnabled(true).
                        setTipText(Toolkit.i18nText("Fine-Design_Basic_Select")).
                        setFilters(new ExtensionFilter[] {
                                new ExtensionFilter("reu", "*.reu"),
                                new ExtensionFilter("reus", "*.reus"),
                                new ExtensionFilter("zip", "*.zip"),
                        }).build());
        int returnValue = fileChooserProvider.showDialog(null);
        installComponent(returnValue, fileChooserProvider.getSelectedFiles());
    }

    private void installComponent(int returnValue, File[] selectedFiles) {
        if (returnValue != JFileChooser.APPROVE_OPTION) {
            return;
        }
        LocalWidgetRepoPane.getInstance().switch2InstallingPane();
        final File[] chosenFiles = selectedFiles;
        new SwingWorker<InstallBackInfo, Void>() {
            @Override
            protected InstallBackInfo doInBackground() {
                return batchInstallZipFiles(chosenFiles);
            }

            @Override
            protected void done() {
                try {
                    InstallBackInfo info = get();
                    LocalWidgetRepoPane.getInstance().refreshAllGroupPane();
                    showMessageDialog(info);
                } catch (InterruptedException | ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e, e.getMessage());
                }
            }
        }.execute();
    }

    private InstallBackInfo batchInstallZipFiles(File[] chosenFiles) {
        try {
            long installTime = System.currentTimeMillis();
            boolean installStatus = true;
            //记录安装失败的组件
            List<String> failureList = new ArrayList<>();
            for (File file : chosenFiles) {
                installStatus &= installFromDiskZipFile(file, installTime, failureList);
            }
            ShareWidgetInfoManager.getInstance().saveXmlInfo();

            boolean needShowMessage = (chosenFiles.length > 1 && chosenFiles.length != failureList.size()) || containRues(chosenFiles);
            return new InstallBackInfo(installStatus, needShowMessage, failureList);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return new InstallBackInfo(false, false, new ArrayList<>());
        }
    }

    private void showMessageDialog(InstallBackInfo info) {
        if (info.success) {
            FineJOptionPane.showMessageDialog(null, Toolkit.i18nText("Fine-Design_Form_Share_Module_OK"));
            return;
        }
        if (info.needShowMessage) {
            final FailureMessagePane failureMessagePane = new FailureMessagePane(appendString(info.failureInfo));
            BasicDialog dialog = failureMessagePane.showSmallWindow(DesignerContext.getDesignerFrame(), new DialogActionAdapter() {
            });
            dialog.setVisible(true);
        } else {
            ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Share_Module_Install_Error"));
        }
    }

    private boolean containRues(File[] chosenFiles) {
        for (File file : chosenFiles) {
            if (ReuxUtils.isReusFile(file))
                return true;
        }
        return false;
    }

    private String appendString(List<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String str : list) {
            builder.append(str).append("\n");
        }
        return builder.toString();
    }

    /**
     * 安装选中文件
     */
    private boolean installFromDiskZipFile(File chosenFile, long installTime, List<String> failList) {
        if (chosenFile == null) {
            return false;
        }
        ShareComponentUtils.checkReadMe();
        return ReuxUtils.isReusFile(chosenFile) ? InstallUtils.installReusFile(chosenFile, installTime, failList) : InstallUtils.installReuFile(chosenFile, installTime, failList);
    }

    private static class InstallBackInfo {
        final boolean success;
        final boolean needShowMessage;
        final List<String> failureInfo;

        public InstallBackInfo(boolean success, boolean needShowMessage, List<String> failureInfo) {
            this.success = success;
            this.needShowMessage = needShowMessage;
            this.failureInfo = failureInfo;
        }
    }

}
