package com.fr.design.mainframe.share.generate.task;

import com.fr.config.MarketConfig;
import com.fr.design.i18n.Toolkit;
import com.fr.form.share.config.ComponentReuseConfigManager;
import com.fr.design.mainframe.share.generate.ComponentBanner;
import com.fr.io.utils.ResourceIOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.third.org.apache.http.HttpEntity;
import com.fr.third.org.apache.http.HttpStatus;
import com.fr.third.org.apache.http.client.config.CookieSpecs;
import com.fr.third.org.apache.http.client.config.RequestConfig;
import com.fr.third.org.apache.http.client.entity.UrlEncodedFormEntity;
import com.fr.third.org.apache.http.client.methods.CloseableHttpResponse;
import com.fr.third.org.apache.http.client.methods.HttpPost;
import com.fr.third.org.apache.http.conn.ssl.NoopHostnameVerifier;
import com.fr.third.org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import com.fr.third.org.apache.http.entity.mime.HttpMultipartMode;
import com.fr.third.org.apache.http.entity.mime.MultipartEntityBuilder;
import com.fr.third.org.apache.http.entity.mime.content.ByteArrayBody;
import com.fr.third.org.apache.http.impl.client.BasicCookieStore;
import com.fr.third.org.apache.http.impl.client.CloseableHttpClient;
import com.fr.third.org.apache.http.impl.client.HttpClients;
import com.fr.third.org.apache.http.message.BasicNameValuePair;
import com.fr.third.org.apache.http.ssl.SSLContextBuilder;
import com.fr.third.org.apache.http.ssl.TrustStrategy;
import com.fr.workspace.WorkContext;
import org.jetbrains.annotations.NotNull;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * 组件上传
 * <p>
 * created by Harrison on 2020/04/13
 **/
public class ComponentUploader implements ComponentBanner {

    //private static final String MARKET_LOGIN = CloudCenter.getInstance().acquireUrlByKind("market.login", "https://market.fanruan.com/ShopServer?pg=login&_=1590635085629");
    //private static final String MARKET_REU_FILE_UPLOAD = CloudCenter.getInstance().acquireUrlByKind("market.reuses.upload", "https://market.fanruan.com/reuses/upload");

    /**
     * 先用临时的。 暂时不上线
     */
    private static final String URL_MARKET_LOGIN = ComponentReuseConfigManager.getInstance().getMarketLoginUrl();
    private static final String URL_MARKET_REU_FILE_UPLOAD = ComponentReuseConfigManager.getInstance().getComponentUploadUrl();

    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_FILE = "file";

    public boolean upload(String path) {

        boolean success = false;
        try {
            success = upload0(path);
        } catch (Exception e) {
            WorkContext.getWorkResource().delete(path);
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return success;
    }

    @Override
    public String getLoadingText() {

        return Toolkit.i18nText("Fine-Design_Share_Encrypt");
    }

    private boolean upload0(String path) throws Exception {

        CloseableHttpClient client = createClient();
        if (login(client)) {
            HttpPost uploadRequest = new HttpPost(URL_MARKET_REU_FILE_UPLOAD);
            byte[] bytes = WorkContext.getWorkResource().readFully(path);
            String fileName = ResourceIOUtils.getName(path);
            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    .setCharset(StandardCharsets.UTF_8)
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                    // 相当于<input type="file" name="file"/>
                    .addPart(KEY_FILE, new ByteArrayBody(bytes, fileName))
                    .build();
            uploadRequest.setEntity(reqEntity);

            // 发起请求 并返回请求的响应
            CloseableHttpResponse uploadResponse = client.execute(uploadRequest);
            FineLoggerFactory.getLogger().info(uploadResponse.toString());
            return uploadResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
        }
        return false;
    }

    private boolean login(CloseableHttpClient client) throws IOException {

        String bbsUsername = MarketConfig.getInstance().getBbsUsername();
        String bbsPassword = MarketConfig.getInstance().getBbsPassword();
        List<BasicNameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair(KEY_USERNAME, bbsUsername));
        pairs.add(new BasicNameValuePair(KEY_PASSWORD, bbsPassword));

        HttpPost login = new HttpPost(URL_MARKET_LOGIN);
        login.setEntity(new UrlEncodedFormEntity(pairs, StandardCharsets.UTF_8));
        login.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36");
        login.addHeader("Connection", "keep-alive");
        CloseableHttpResponse loginResponse = client.execute(login);
        FineLoggerFactory.getLogger().info(loginResponse.toString());
        return loginResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
    }

    private static CloseableHttpClient createClient() throws Exception {

        SSLConnectionSocketFactory connectionFactory = createSSL();

        BasicCookieStore cookieStore = new BasicCookieStore();
        return HttpClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD)
                        .setSocketTimeout(2000)
                        .setConnectTimeout(5000)
                        .setConnectionRequestTimeout(5000)
                        .setAuthenticationEnabled(false)
                        .build())
                .setDefaultCookieStore(cookieStore)
                .setSSLSocketFactory(connectionFactory)
                .build();
    }

    @NotNull
    private static SSLConnectionSocketFactory createSSL() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
        SSLContext sslContext = SSLContextBuilder
                .create()
                .loadTrustMaterial(new TrustStrategy() {
                    @Override
                    public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        return true;
                    }
                })
                .build();
        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();
        return new SSLConnectionSocketFactory(sslContext, allowAllHosts);
    }

}
