package com.fr.design.mainframe.share.sort;

import com.fr.design.i18n.Toolkit;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.record.ShareWidgetInfoManager;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralContext;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by kerry on 2020-07-01
 */
public enum WidgetSortType implements SortType<SharableWidgetProvider> {
    INSTALL_TIME {
        @Override
        public void sort(SharableWidgetProvider[] widgetProviders) {
            Arrays.sort(widgetProviders, new Comparator<SharableWidgetProvider>() {
                @Override
                public int compare(SharableWidgetProvider o1, SharableWidgetProvider o2) {
                    long t1 = ShareWidgetInfoManager.getInstance().getCompInstallTime(o1.getName() + "." + o1.getId());
                    long t2 = ShareWidgetInfoManager.getInstance().getCompInstallTime(o2.getName() + "." + o2.getId());
                    int result = ComparatorUtils.compareCommonType(t2, t1);
                    if (result == 0) {
                        result = Collator.getInstance(GeneralContext.getLocale()).compare(o1.getName(), o2.getName());
                    }
                    return result;
                }
            });
        }

        @Override
        public String getDisplayName() {
            return Toolkit.i18nText("Fine-Design_Share_Install_Time");
        }
    },
    COMPONENT_NAME {
        @Override
        public void sort(SharableWidgetProvider[] widgetProviders) {
            Arrays.sort(widgetProviders, new Comparator<SharableWidgetProvider>() {
                @Override
                public int compare(SharableWidgetProvider o1, SharableWidgetProvider o2) {
                    return Collator.getInstance(GeneralContext.getLocale()).compare(o1.getName(), o2.getName());
                }
            });
        }

        @Override
        public String getDisplayName() {
            return Toolkit.i18nText("Fine-Design_Share_Sort_Name");
        }
    }
}
