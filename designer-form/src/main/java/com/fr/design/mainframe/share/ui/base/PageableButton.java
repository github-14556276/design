package com.fr.design.mainframe.share.ui.base;

import javax.swing.SwingConstants;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.RoundRectangle2D;

public class PageableButton extends BasicArrowButton {

    static final private Color BORDER_COLOR = new ColorUIResource(198, 198, 198);
    static final private Color ARROW_DISABLED_COLOR = new ColorUIResource(193, 193, 193);
    static final private Color ARROW_COLOR = new ColorUIResource(112, 112, 112);
    static final private Color BUTTON_COLOR = new ColorUIResource(255, 255, 255);
    static final private Color BUTTON_DISABLED_COLOR = new ColorUIResource(242, 242, 242);
    static final private Color BUTTON_PRESS_COLOR = new ColorUIResource(96, 189, 246);
    static final private Color BUTTON_ROLLOVER_COLOR = new ColorUIResource(0xd2d2d2);
    static final private int CONNER = 2;


    public PageableButton(int direction) {
        super(direction);
    }

    public void paint(Graphics g) {
        paintButton(g);
        paintArrow(g);
        paintButtonBorder(g);
    }

    private void paintButton(Graphics g) {

        int width = this.getWidth();
        int height = this.getHeight();
        if (!isEnabled()) {
            g.setColor(PageableButton.BUTTON_DISABLED_COLOR);
        } else if (getModel().isPressed()) {
            g.setColor(PageableButton.BUTTON_PRESS_COLOR);
        } else if (getModel().isRollover()){
            g.setColor(PageableButton.BUTTON_ROLLOVER_COLOR);
        } else {
            g.setColor(PageableButton.BUTTON_COLOR);
        }

        g.fillRoundRect(1, 1, width -2, height - 2, PageableButton.CONNER, PageableButton.CONNER);
    }

    private void paintArrow(Graphics g) {
        if (!this.isEnabled()) {
            g.setColor(PageableButton.ARROW_DISABLED_COLOR);
        } else {
            g.setColor(PageableButton.ARROW_COLOR);
        }
        switch (direction) {
            case SwingConstants.NORTH:
                g.drawLine(8, 5, 8, 5);
                g.drawLine(7, 6, 9, 6);
                g.drawLine(6, 7, 10, 7);
                g.drawLine(5, 8, 7, 8);
                g.drawLine(9, 8, 11, 8);
                g.drawLine(4, 9, 6, 9);
                g.drawLine(10, 9, 12, 9);
                g.drawLine(5, 10, 5, 10);
                g.drawLine(11, 10, 11, 10);
                break;
            case SwingConstants.SOUTH:
                g.drawLine(5, 6, 5, 6);
                g.drawLine(11, 6, 11, 6);
                g.drawLine(4, 7, 6, 7);
                g.drawLine(10, 7, 12, 7);
                g.drawLine(5, 8, 7, 8);
                g.drawLine(9, 8, 11, 8);
                g.drawLine(6, 9, 10, 9);
                g.drawLine(7, 10, 9, 10);
                g.drawLine(8, 11, 8, 11);
                break;
            case SwingConstants.EAST:
                g.drawLine(6, 5, 6, 5);
                g.drawLine(6, 11, 6, 11);
                g.drawLine(7, 4, 7, 6);
                g.drawLine(7, 10, 7, 12);
                g.drawLine(8, 5, 8, 7);
                g.drawLine(8, 9, 8, 11);
                g.drawLine(9, 6, 9, 10);
                g.drawLine(10, 7, 10, 9);
                g.drawLine(11, 8, 11, 8);
                break;
            case SwingConstants.WEST:
                g.drawLine(4, 8, 4, 8);
                g.drawLine(5, 7, 5, 9);
                g.drawLine(6, 6, 6, 10);
                g.drawLine(7, 5, 7, 7);
                g.drawLine(7, 9, 7, 11);
                g.drawLine(8, 4, 8, 6);
                g.drawLine(8, 10, 8, 12);
                g.drawLine(9, 5, 9, 5);
                g.drawLine(9, 11, 9, 11);
                break;
        }
    }

    private void paintButtonBorder(Graphics g) {
        int offs = 1;
        int width = this.getWidth();
        int height = this.getHeight();

        Graphics2D g2d = (Graphics2D) g;
        Color oldColor = g2d.getColor();
        g2d.setColor(PageableButton.BORDER_COLOR);

        Shape outer;
        Shape inner;
        int size = offs + offs;

        outer = new RoundRectangle2D.Float(0, 0, width, height, offs, offs);
        inner = new RoundRectangle2D.Float(offs, offs, width - size, height - size, PageableButton.CONNER, PageableButton.CONNER);

        Path2D path = new Path2D.Float(Path2D.WIND_EVEN_ODD);
        path.append(outer, false);
        path.append(inner, false);
        g2d.fill(path);
        g2d.setColor(oldColor);
    }
}
