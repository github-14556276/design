package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ArrangementType;
import com.fr.design.mainframe.MultiSelectionArrangement;
import com.fr.general.IOUtils;

import javax.swing.Icon;

public class HorizontalDistributionButton extends AbstractMultiSelectionArrangementButton {
    private static final long serialVersionUID = 5052092252720664954L;

    public HorizontalDistributionButton(MultiSelectionArrangement arrangement) {
        super(arrangement);
    }

    @Override
    public Icon getIcon() {
        return IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_horizontal_auto_spacing.png");
    }

    @Override
    public String getTipText() {
        return Toolkit.i18nText("Fine-Design_Multi_Selection_Auto_Horizontal_Spacing");
    }

    @Override
    public ArrangementType getArrangementType() {
        return ArrangementType.HORIZONTAL_AUTO_DISTRIBUTION;
    }
}
