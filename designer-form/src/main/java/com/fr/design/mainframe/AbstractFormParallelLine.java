package com.fr.design.mainframe;

import java.awt.Point;

public abstract class AbstractFormParallelLine {
    protected int parallelValue;
    protected int startPosition;
    protected int endPosition;

    public AbstractFormParallelLine(int parallelValue, int startPosition, int endPosition) {
        this.parallelValue = parallelValue;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    public int getCenterPosition() {
        return (startPosition + endPosition) / 2;
    }

    /**
     * 获取当前直线的中垂线起点位置
     * @return 中垂线起点位置
     */
    abstract public Point getStartPointOnVerticalCenterLine();

    /**
     * 获取当前直线的中垂线的重点位置，重点位置即为重垂线与另一条平行线相交的点
     * @param parallelValue 平行线
     * @return 中垂线重点位置
     */
    abstract public Point getEndPointOnVerticalCenterLine(int parallelValue);

    public boolean isVerticalCenterLineBeforeTheParallelLine(AbstractFormParallelLine parallelLine) {
        return this.getCenterPosition() < parallelLine.getStartPosition();
    }

    public boolean isVerticalCenterLineBehindTheParallelLine(AbstractFormParallelLine parallelLine) {
        return this.getCenterPosition() > parallelLine.getEndPosition();
    }

    /**
     * 传一个平行线，当 当前直线和平行线中心点位置无平行相交的部分的时候，需要绘制一条延长线，一直延长到平行线边界位置
     * @param parallelLine 平行线
     * @return 延长线的起点位置
     */
    abstract public Point getExtendedLineStartPoint(AbstractFormParallelLine parallelLine);

    /**
     * 传一个平行线，当 当前直线和平行线中心点位置无平行相交的部分的时候，需要绘制一条延长线，一直延长到平行线边界位置
     * @param parallelLine 平行线
     * @return 延长线的重点位置
     */
    abstract public Point getExtendedLineEndPoint(AbstractFormParallelLine parallelLine);

    public int getDistanceWithLine(AbstractFormParallelLine parallelLine) {
        return Math.abs(this.getParallelValue() - parallelLine.getParallelValue());
    }

    public int getParallelValue() {
        return parallelValue;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }
}
