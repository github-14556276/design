package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ArrangementType;
import com.fr.design.mainframe.MultiSelectionArrangement;
import com.fr.general.IOUtils;

import javax.swing.Icon;

public class VerticalCenterButton extends AbstractMultiSelectionArrangementButton {
    public VerticalCenterButton(MultiSelectionArrangement arrangement) {
        super(arrangement);
    }

    @Override
    public Icon getIcon() {
        return IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_vertical_center_align.png");
    }

    @Override
    public String getTipText() {
        return Toolkit.i18nText("Fine-Design_Multi_Selection_Vertical_Center_Align");
    }

    @Override
    public ArrangementType getArrangementType() {
        return ArrangementType.VERTICAL_CENTER_ALIGN;
    }
}
