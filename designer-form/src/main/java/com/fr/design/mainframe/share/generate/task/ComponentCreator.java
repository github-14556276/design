package com.fr.design.mainframe.share.generate.task;

import com.fr.base.TableData;
import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.base.iofile.attr.SharableAttrMark;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.share.generate.impl.AbstractComponentCreatorProcessor;
import com.fr.form.main.Form;
import com.fr.form.main.WidgetGatherAdapter;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.ShareEmbeddedConverter;
import com.fr.form.share.bean.ComponentReuBean;
import com.fr.form.share.editor.DefaultSharableEditor;
import com.fr.form.share.editor.PlainSharableEditor;
import com.fr.form.share.utils.ShareUtils;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.fun.IOFileAttrMark;
import com.fr.workspace.WorkContext;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * created by Harrison on 2020/04/13
 **/
public class ComponentCreator extends AbstractComponentCreatorProcessor {

    /**
     * 生成组件
     * 返回组件的路径
     *
     * @return 路径
     */
    @Override
    public ComponentReuBean create(JTemplate<?, ?> jt, Map<String, Object> paraMap, Widget widget, DefaultSharableWidget info) throws Exception {


        // 遍历判断是否存在共享组件
        checkOriginStatus(widget, info);

        // 导出内置数据集
        Form form = embeddedForm(jt, paraMap);

        // 创建组件
        DefaultSharableEditor editor = createSharableEditor(form, paraMap, widget, info);

        // 生成组件
        String generatePath = generate(editor, info);


        return new ComponentReuBean(generatePath, editor, info);
    }

    protected void checkOriginStatus(Widget widget, final DefaultSharableWidget info) {

        Form.traversalWidget(widget, new WidgetGatherAdapter() {
            @Override
            public void dealWith(Widget widget) {

                AbstractBorderStyleWidget borderStyleWidget = (AbstractBorderStyleWidget) widget;
                IOFileAttrMark attrMark = borderStyleWidget.getWidgetAttrMark(SharableAttrMark.XML_TAG);
                if (attrMark != null) {
                    info.setTransform();
                }
            }
        }, AbstractBorderStyleWidget.class);
    }

    @Override
    public String getLoadingText() {

        return Toolkit.i18nText("Fine-Design_Share_Generate_Ing");
    }

    /**
     * 内置数据集处理
     *
     * @return 返回
     */
    protected Form embeddedForm(JTemplate<?, ?> jt, Map<String, Object> paraMap) throws Exception {

        jt.stopEditing();

        Form tpl = null;
        try {
            tpl = (Form) jt.getTarget().clone();
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }

        //内置数据集
        if (tpl != null) {
            ShareEmbeddedConverter embeddedConverter = WorkContext.getCurrent().get(ShareEmbeddedConverter.class);
            Map<String, TableData> map = embeddedConverter.convertToEmbeddedTableData(tpl, paraMap);
            Iterator<Map.Entry<String, TableData>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, TableData> entry = iterator.next();
                tpl.putTableData(entry.getKey(), entry.getValue());
            }
        }
        return tpl;
    }

    @NotNull
    protected DefaultSharableEditor createSharableEditor(Form form, Map<String, Object> paraMap, Widget widget, DefaultSharableWidget info) {

        String uuid = info.getId();
        AbstractBorderStyleWidget abstractBorderStyleWidget = (AbstractBorderStyleWidget) widget;
        abstractBorderStyleWidget.addWidgetAttrMark(new SharableAttrMark());
        ExtendSharableAttrMark extendSharableAttrMark = abstractBorderStyleWidget.getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
        if (extendSharableAttrMark != null) {
            extendSharableAttrMark.setShareId(uuid);
        } else {
            abstractBorderStyleWidget.addWidgetAttrMark(new ExtendSharableAttrMark(uuid));
        }
        return new PlainSharableEditor(uuid, widget, form, (HashMap<String, Object>) paraMap);
    }

    protected String generate(Widget editor, DefaultSharableWidget info) throws Exception {

        return ShareUtils.generateModule(editor, info);
    }
}
