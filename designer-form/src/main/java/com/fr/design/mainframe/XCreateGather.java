package com.fr.design.mainframe;

import com.fr.design.designer.creator.XCreator;

public interface XCreateGather {
    void dealWith(XCreator xCreator);
}
