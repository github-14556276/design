package com.fr.design.mainframe.share.encrypt.clipboard;

import com.fr.form.main.Form;
import com.fr.form.main.WidgetUtil;
import com.fr.base.iofile.attr.EncryptSharableAttrMark;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.container.WLayout;
import com.fr.stable.fun.IOFileAttrMark;

import java.util.HashMap;
import java.util.Map;

/**
 * created by Harrison on 2020/05/14
 **/
public abstract class RestrictTemplateSet {
    
    private static Map<String, Boolean> restrictMap = new HashMap<>(8);
    
    public static void monitorGracefully(Form form) {
        monitor(form, true);
    }
    
    public static void monitorForcefully(Form form) {
        monitor(form, false);
    }
    
    private static void monitor(Form form, boolean useCache) {
        
        String templateID = form.getTemplateID();
        if (useCache) {
            if (restrictMap.containsKey(templateID)) {
                return;
            }
        }
        //检测 + 缓存
        monitor0(form);
    }
    
    private static void monitor0(Form form) {
        
        final String templateID = form.getTemplateID();
        WLayout container = form.getContainer();
        WidgetUtil.bfsTraversalWidget(container, new WidgetUtil.BfsWidgetGather<AbstractBorderStyleWidget>() {
            @Override
            public boolean dealWith(AbstractBorderStyleWidget widget) {
                IOFileAttrMark mark = widget.getWidgetAttrMark(EncryptSharableAttrMark.XML_TAG);
                boolean existEncrypt = mark != null;
                if (existEncrypt) {
                    restrictMap.put(templateID, existEncrypt);
                }
                return existEncrypt;
            }
        }, AbstractBorderStyleWidget.class);
        
        initIfAbsent(templateID);
    }
    
    private static void initIfAbsent(String templateID) {
        
        if (!restrictMap.containsKey(templateID)) {
            restrictMap.put(templateID, false);
        }
    }
    
    public static boolean isRestrict(String templateId) {
        
        Boolean restrict = restrictMap.get(templateId);
        return restrict == null ? false : restrict;
    }
    
}
