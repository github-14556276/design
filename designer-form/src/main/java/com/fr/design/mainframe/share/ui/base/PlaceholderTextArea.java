package com.fr.design.mainframe.share.ui.base;

import com.fr.design.gui.itextarea.UITextArea;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * created by Harrison on 2020/04/21
 **/
public class PlaceholderTextArea extends UITextArea {
    
    private String placeholder;
    
    public PlaceholderTextArea() {
    }
    
    public PlaceholderTextArea(String s, String placeholder) {
        super(s);
        this.placeholder = placeholder;
    }
    
    public void setPlaceholder(String placeholder) {
        
        this.placeholder = placeholder;
    }
    
    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);
        if (placeholder.length() == 0 || getText().length() > 0) {
            return;
        }
        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(getDisabledTextColor());
        g.drawString(placeholder, getInsets().left, pG.getFontMetrics()
                .getMaxAscent() + getInsets().top + 1);
    }}
