package com.fr.design.mainframe.share.encrypt.clipboard;

import com.fr.base.io.BaseBook;
import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.design.DesignModelAdapter;
import com.fr.design.data.datapane.TableDataTreePane;
import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XWTitleLayout;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.fun.impl.AbstractDesignerEditListenerProvider;
import com.fr.design.mainframe.JTemplate;
import com.fr.form.main.Form;
import com.fr.form.main.WidgetGatherAdapter;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.editor.SharableEditorProvider;
import com.fr.form.share.utils.ShareUtils;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WLayout;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;

import java.util.Iterator;

/**
 * 创建，删除的时候要初始化部分状态
 * <p>
 * created by Harrison on 2020/05/19
 **/
public class TemplateStateListenerProvider extends AbstractDesignerEditListenerProvider {

    public static final int CREATOR_ADDED = 1;

    public static final int CREATOR_DELETED = 2;

    private static final String SEPARATOR = "-";

    private String lastAffectedCreatorShareID;


    @Override
    public void fireCreatorModified(DesignerEvent evt) {

        int eventId = evt.getCreatorEventID();
        if (eventId == CREATOR_ADDED || eventId == CREATOR_DELETED) {
            JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
            BaseBook book = template.getTarget();
            if (book instanceof Form) {
                Form form = (Form) book;
                RestrictTemplateSet.monitorForcefully(form);
                refreshTableDataTree(form, evt);
            }
        }
        if (eventId == DesignerEvent.CREATOR_SELECTED) {
            XCreator lastAffectedCreator = (XCreator) evt.getAffectedCreator();
            if (lastAffectedCreator == null) {
                return;
            }
            lastAffectedCreatorShareID = lastAffectedCreator.getShareId();
            //做下兼容处理，有标题的老的组件其外层的creator上是没有shareID的，新生成的组件是有的
            if (!lastAffectedCreator.acceptType(XWTitleLayout.class) || StringUtils.isNotEmpty(lastAffectedCreatorShareID)) {
                return;
            }
            XCreator body = getBodyCreator((XWTitleLayout) lastAffectedCreator);
            if (body != null) {
                lastAffectedCreatorShareID = body.getShareId();
            }

        }
    }

    private XCreator getBodyCreator(XWTitleLayout titleLayout) {
        for (int i = 0; i < titleLayout.getXCreatorCount(); i++) {
            XCreator creator = titleLayout.getXCreator(i);
            if (creator.hasTitleStyle()) {
                return creator;
            }
        }
        return null;
    }

    private void refreshTableDataTree(Form form, DesignerEvent evt) {
        if (evt.getCreatorEventID() == DesignerEvent.CREATOR_DELETED && StringUtils.isNotEmpty(lastAffectedCreatorShareID)) {
            if (!needDeleteTableData(form.getContainer(), lastAffectedCreatorShareID)) {
                return;
            }
            //TODO 目前组件没版本号，可以直接遍历，之后可能还是要改的
            SharableWidgetProvider bindInfo = ShareUtils.getElCaseBindInfoById(lastAffectedCreatorShareID);
            SharableEditorProvider sharableEditor = ShareUtils.getSharedElCaseEditorById(lastAffectedCreatorShareID);
            if (sharableEditor == null || bindInfo == null) {
                return;
            }
            Iterator tdIterator = sharableEditor.getTableDataSource().getTableDataNameIterator();
            while (tdIterator.hasNext()) {
                String tdName = bindInfo.getName() + SEPARATOR + tdIterator.next();
                TableDataTreePane.getInstance(DesignModelAdapter.getCurrentModelAdapter()).removeTableData(tdName);
            }
        }
    }

    private boolean needDeleteTableData(WLayout widget, final String shareId) {
        final boolean[] needDeleteTableData = {true};
        Form.traversalWidget(widget, new WidgetGatherAdapter() {
            @Override
            public void dealWith(Widget widget) {
                AbstractBorderStyleWidget borderStyleWidget = (AbstractBorderStyleWidget) widget;
                ExtendSharableAttrMark attrMark = borderStyleWidget.getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
                if (attrMark != null) {
                    needDeleteTableData[0] &= !ComparatorUtils.equals(shareId, attrMark.getShareId());
                }
            }
        }, AbstractBorderStyleWidget.class);
        return needDeleteTableData[0];
    }
}
