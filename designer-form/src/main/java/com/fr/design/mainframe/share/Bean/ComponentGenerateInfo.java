package com.fr.design.mainframe.share.Bean;

import com.fr.design.mainframe.JTemplate;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.ui.Widget;

import java.util.Map;

/**
 * created by Harrison on 2020/04/13
 *
 * 组件生成信息
 **/
public class ComponentGenerateInfo {
    
    private boolean autoUpload;
    
    private JTemplate<?, ?> jt;
    
    private Map<String, Object> paraMap;
    
    private Widget widget;
    
    private DefaultSharableWidget info;
    
    public ComponentGenerateInfo(boolean autoUpload, JTemplate<?, ?> jt, Map<String, Object> paraMap, Widget widget, DefaultSharableWidget info) {
        
        this.autoUpload = autoUpload;
        this.jt = jt;
        this.paraMap = paraMap;
        this.widget = widget;
        this.info = info;
    }
    
    public boolean isAutoUpload() {
        
        return autoUpload;
    }
    
    public JTemplate<?, ?> getJt() {
        
        return jt;
    }
    
    public Map<String, Object> getParaMap() {
        
        return paraMap;
    }
    
    public Widget getWidget() {
        
        return widget;
    }
    
    public DefaultSharableWidget getInfo() {
        
        return info;
    }
}
