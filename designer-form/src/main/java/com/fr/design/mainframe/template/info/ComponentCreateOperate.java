package com.fr.design.mainframe.template.info;

import com.fr.form.ui.Widget;
import com.fr.json.JSONObject;

import java.util.UUID;

/**
 * Created by kerry on 2020-05-08
 */
public class ComponentCreateOperate extends ComponentOperate {
    public static final String OPERATE_TYPE = "componentCreate";
    private static final String ATTR_CREATE_TIME = "createTime";
    private long createTime = 0L;


    public ComponentCreateOperate(Widget widget) {
        super(widget);
        widget.setWidgetID(UUID.randomUUID().toString());
        this.createTime = System.currentTimeMillis();
    }


    @Override
    public String getOperateType() {
        return OPERATE_TYPE;
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = super.toJSONObject();
        jo.put(ATTR_CREATE_TIME, createTime);
        return jo;
    }

}
