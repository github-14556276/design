package com.fr.design.mainframe.share.constants;

import com.fr.design.i18n.Toolkit;
import com.fr.form.share.bean.StyleThemeBean;
import com.fr.form.share.bean.WidgetFilterInfo;
import com.fr.form.share.bean.WidgetFilterTypeInfo;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.form.share.utils.ShareUtils;
import com.fr.general.ComparatorUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kerry on 2020-12-09
 */
public enum StyleTheme {

    SIMPLE_FRESH("Fine-Design_Share_Style_Theme_Simple", "1"),
    BUSINESS_STABLE("Fine-Design_Share_Style_Theme_Business", "2"),
    LIVE_GORGEOUS("Fine-Design_Share_Style_Theme_Live", "3"),
    COOL_TECHNOLOGY("Fine-Design_Share_Style_Theme_Cool", "4"),
    OTHER_THEME("Fine-Design_Share_Style_Theme_Other", "5");


    private final String local;
    private final String id;

    StyleTheme(String name, String id) {
        this.local = name;
        this.id = id;
    }

    public String getLocText() {
        return Toolkit.i18nText(this.local);
    }

    public String getId() {
        return this.id;
    }

    private static List<StyleThemeBean> types() {
        List<StyleThemeBean> list = new ArrayList<>();
        for (StyleTheme type : StyleTheme.values()) {
            list.add(new StyleThemeBean(type.getId(), type.getLocText()));
        }
        return list;
    }

    /**
     * 获取样式风格属性list
     *
     * @return List
     */
    public static List<StyleThemeBean> getStyleThemeTypeInfo() {
        List<WidgetFilterTypeInfo> widgetFilterTypeInfos = ShareUtils.getWidgetFilterTypeInfos();
        if (widgetFilterTypeInfos.isEmpty()) {
            return types();
        }
        WidgetFilterTypeInfo styleThemeFilterInfo = new WidgetFilterTypeInfo();
        for (WidgetFilterTypeInfo typeInfo : widgetFilterTypeInfos) {
            if (ComparatorUtils.equals(ShareComponentConstants.STYLE_THEME_KEY, typeInfo.getKey())) {
                styleThemeFilterInfo = typeInfo;
                break;
            }
        }
        List<StyleThemeBean> resultList = new ArrayList<>();
        List<WidgetFilterInfo> filterInfoList = styleThemeFilterInfo.getFilterItems();
        Iterator<WidgetFilterInfo> infoIterator = filterInfoList.iterator();
        while (infoIterator.hasNext()) {
            WidgetFilterInfo filterInfo = infoIterator.next();
            if (!ComparatorUtils.equals(ShareComponentConstants.ALL_STYLE_THEME, filterInfo.getId())) {
                resultList.add(new StyleThemeBean(filterInfo.getId(), filterInfo.getName()));
            }
        }
        return resultList;
    }


}
