package com.fr.design.mainframe.share.ui.base;

import com.fr.design.constants.UIConstants;
import com.fr.design.mainframe.share.ui.base.ui.SharePopupMenuItemUI;
import com.fr.stable.StringUtils;

import javax.swing.Action;
import javax.swing.JMenuItem;
import java.awt.Dimension;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/30
 * 弹窗菜单项
 */
public class PopupMenuItem extends JMenuItem {

    public PopupMenuItem(Action action) {
        super(StringUtils.EMPTY, null);
        setBackground(UIConstants.DEFAULT_BG_RULER);
        setAction(action);
        setUI(new SharePopupMenuItemUI());
        this.setPreferredSize(new Dimension(60, 21));
    }

    @Override
    public String getText() {
        return StringUtils.EMPTY + super.getText();
    }

}
