package com.fr.design.mainframe.share.exception;

import com.fr.design.i18n.Toolkit;

/**
 * created by Harrison on 2020/04/21
 **/
public class LackOfValueException extends RuntimeException{
    
    public LackOfValueException() {
        
        super(Toolkit.i18nText("Fine-Design_Share_Lack_Val"));
    }
}
