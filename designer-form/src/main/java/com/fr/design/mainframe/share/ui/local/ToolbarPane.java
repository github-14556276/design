package com.fr.design.mainframe.share.ui.local;

import com.fr.base.BaseUtils;
import com.fr.design.actions.UpdateAction;
import com.fr.design.constants.UIConstants;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.imenu.UIPopupMenu;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.sort.WidgetSortType;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.design.mainframe.share.ui.base.SortPopupMenuItem;
import com.fr.design.mainframe.share.ui.widgetfilter.FilterPane;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/29
 */
class ToolbarPane extends JPanel {
    private static final Color SEARCH_BORDER_COLOR = Color.decode("#F5F5F7");
    private static final Color SEARCH_BORDER_INPUT_COLOR = Color.decode("#419BF9");

    private static final String NORMAL = "NORMAL";
    private static final String SEARCH = "SEARCH";

    private UITextField searchTextField;
    private final JPanel centerPane;

    //查询按钮
    private final UIButton searchButton;
    //排序按钮
    private final UIButton sortButton;
    //刷新按钮
    private final UIButton refreshButton;

    private final CardLayout cardLayout;

    private FilterPane filterPanel;

    public ToolbarPane() {
        cardLayout = new CardLayout();
        centerPane = new JPanel(cardLayout);

        searchButton = createSearchButton();
        sortButton = createSortButton();
        refreshButton = createRefreshButton();
        filterPanel = createFilterPane();
        JPanel normalPanel = createNormalPanel();
        JPanel searchPanel = createSearchField();
        centerPane.add(normalPanel, NORMAL);
        centerPane.add(searchPanel, SEARCH);
        this.add(centerPane, BorderLayout.CENTER);
    }

    public void addFilterPopupStateChangeListener(FilterPane.PopStateChangeListener listener) {
        filterPanel.addPopupStateChangeListener(listener);
    }

    private UIButton createSearchButton() {
        return createToolButton(
                IOUtils.readIcon("/com/fr/base/images/share/search_icon.png"),
                Toolkit.i18nText("Fine-Design_Share_Search"),
                e -> {
                    cardLayout.show(centerPane, SEARCH);
                    searchTextField.requestFocus();
                }
        );
    }

    private UIButton createSortButton() {
        return createToolButton(
                IOUtils.readIcon("/com/fr/base/images/share/sort_icon.png"),
                Toolkit.i18nText("Fine-Design_Share_Sort"),
                e -> {
                    UIPopupMenu popupMenu = new UIPopupMenu();
                    popupMenu.setOnlyText(true);
                    popupMenu.setBackground(UIConstants.DEFAULT_BG_RULER);
                    popupMenu.add(new SortPopupMenuItem(new SortAction(WidgetSortType.INSTALL_TIME)));
                    popupMenu.add(new SortPopupMenuItem(new SortAction(WidgetSortType.COMPONENT_NAME)));

                    GUICoreUtils.showPopupMenu(popupMenu, sortButton, 0, sortButton.getSize().height);
                }
        );
    }

    private UIButton createRefreshButton() {
        return createToolButton(
                IOUtils.readIcon("/com/fr/base/images/share/refresh.png"),
                Toolkit.i18nText("Fine-Design_Basic_Refresh"),
                e -> {
                    filterPanel.reset();
                    LocalWidgetRepoPane.getInstance().refreshPane();
                }
        );
    }

    private FilterPane createFilterPane() {
        filterPanel = FilterPane.createLocalFilterPane();
        filterPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        filterPanel.registerChangeListener(e -> LocalWidgetRepoPane.getInstance().refreshShowPanel());
        return filterPanel;
    }

    private JPanel createNormalPanel() {
        JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        buttonPane.add(searchButton);
        buttonPane.add(sortButton);
        buttonPane.add(refreshButton);
        panel.add(filterPanel, BorderLayout.CENTER);
        panel.add(buttonPane, BorderLayout.EAST);
        return panel;
    }

    private JPanel createSearchField() {
        final JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setPreferredSize(new Dimension(228, 10));
        jPanel.setBorder(BorderFactory.createLineBorder(SEARCH_BORDER_COLOR));
        jPanel.setBackground(Color.WHITE);
        UILabel label = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/search_icon.png"));
        label.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 0));
        jPanel.add(label, BorderLayout.WEST);
        this.searchTextField = new UITextField();
        this.searchTextField.setBorderPainted(false);
        this.searchTextField.setPlaceholder(Toolkit.i18nText("Fine-Design_Basic_Plugin_Search"));
        this.searchTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jPanel.setBorder(BorderFactory.createLineBorder(SEARCH_BORDER_INPUT_COLOR));
                jPanel.repaint();
            }

            @Override
            public void focusLost(FocusEvent e) {
                jPanel.setBorder(BorderFactory.createLineBorder(SEARCH_BORDER_COLOR));
                jPanel.repaint();
            }
        });
        this.searchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filterByName();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filterByName();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filterByName();
            }
        });
        jPanel.add(this.searchTextField, BorderLayout.CENTER);
        UILabel xLabel = new UILabel(BaseUtils.readIcon("/com/fr/design/images/buttonicon/close_icon.png"));
        xLabel.addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                searchTextField.setText(StringUtils.EMPTY);
                cardLayout.show(centerPane, NORMAL);
            }
        });
        xLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jPanel.add(xLabel, BorderLayout.EAST);

        return jPanel;
    }

    private void filterByName() {
        String text = searchTextField.getText();
        boolean searchStatus = !StringUtils.isEmpty(text);
        sortButton.setEnabled(!searchStatus);
        LocalWidgetRepoPane.getInstance().searchByKeyword(text);
    }

    private static class SortAction extends UpdateAction {
        private final WidgetSortType sortType;

        public SortAction(WidgetSortType sortType) {
            this.putValue(Action.SMALL_ICON, null);
            this.setName(sortType.getDisplayName());
            this.sortType = sortType;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            LocalWidgetRepoPane.getInstance().sortWidget(sortType);
        }
    }

    /**
     * 创建工具条按钮
     */
    private UIButton createToolButton(Icon icon, String toolTip, ActionListener actionListener) {
        UIButton toolButton = new UIButton();
        toolButton.setIcon(icon);
        toolButton.setToolTipText(toolTip);
        toolButton.set4ToolbarButton();
        toolButton.addActionListener(actionListener);
        return toolButton;
    }

}
