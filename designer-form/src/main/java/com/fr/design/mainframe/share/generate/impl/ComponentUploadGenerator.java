package com.fr.design.mainframe.share.generate.impl;

import com.fr.design.mainframe.share.Bean.ComponentGenerateInfo;
import com.fr.design.mainframe.share.generate.ComponentCreatorProcessor;
import com.fr.design.mainframe.share.generate.ComponentTaskAdaptor;
import com.fr.design.mainframe.share.generate.task.ComponentDesensitize;
import com.fr.design.mainframe.share.generate.task.ComponentEncrypt;
import com.fr.design.mainframe.share.generate.task.ComponentUploadComplete;
import com.fr.design.mainframe.share.generate.task.ComponentUploader;
import com.fr.form.share.bean.ComponentReuBean;

import java.util.concurrent.ExecutionException;

/**
 * created by Harrison on 2020/04/16
 **/
public class ComponentUploadGenerator extends AbstractComponentGenerator {

    private ComponentCreatorProcessor creator;

    private ComponentEncrypt encrypt;

    private ComponentDesensitize desensitize;

    private ComponentUploader uploader;

    private ComponentUploadComplete complete;

    private ComponentUploadGenerator(ComponentGenerateInfo info, ComponentCreatorProcessor creator, ComponentEncrypt encrypt, ComponentDesensitize desensitize, ComponentUploader uploader, ComponentUploadComplete complete) {

        super(info, creator, desensitize, encrypt, uploader);
        this.creator = creator;
        this.encrypt = encrypt;
        this.desensitize = desensitize;
        this.uploader = uploader;
        this.complete = complete;
    }

    public static ComponentUploadGenerator create(ComponentGenerateInfo info) {

        ComponentCreatorProcessor creator = getComponentCreator();
        ComponentUploader uploader = new ComponentUploader();
        ComponentEncrypt encrypt = new ComponentEncrypt();
        ComponentDesensitize desensitize = new ComponentDesensitize();
        ComponentUploadComplete complete = new ComponentUploadComplete();
        return new ComponentUploadGenerator(info, creator, encrypt, desensitize, uploader, complete);
    }

    @Override
    protected boolean generate0() throws InterruptedException, ExecutionException {
        final ComponentGenerateInfo info = getInfo();
        //创建
        ComponentTaskAdaptor<ComponentReuBean> createTask = new ComponentTaskAdaptor<ComponentReuBean>(0.20, creator.getLoadingText()) {
            @Override
            public ComponentReuBean execute() throws Exception {
                return creator.create(info.getJt(), info.getParaMap(), info.getWidget(), info.getInfo());
            }
        };
        final ComponentReuBean plainBean = execute(createTask);

        //脱敏
        ComponentTaskAdaptor<Object> desensitizeTask = new ComponentTaskAdaptor<Object>(0.60, desensitize.getLoadingText()) {
            @Override
            public Object execute() throws Exception {
                desensitize.execute();
                return null;
            }
        };
        execute(desensitizeTask);

        //加密
        ComponentTaskAdaptor<ComponentReuBean> encryptTask = new ComponentTaskAdaptor<ComponentReuBean>(0.85, encrypt.getLoadingText()) {
            @Override
            public ComponentReuBean execute() throws Exception {
                return encrypt.execute(plainBean);
            }
        };
        final ComponentReuBean encryptBean = execute(encryptTask);

        //上传
        ComponentTaskAdaptor<Boolean> uploadTask = new ComponentTaskAdaptor<Boolean>(1.0, uploader.getLoadingText()) {
            @Override
            public Boolean execute() throws Exception {
                return uploader.upload(encryptBean.getPath());
            }
        };
        Boolean success = execute(uploadTask);

        //完成
        final ComponentTaskAdaptor<Boolean> completeTask = new ComponentTaskAdaptor<Boolean>(1.0, complete.getLoadingText()) {
            @Override
            public Boolean execute() throws Exception {
                return complete.execute();
            }
        };
        return success && execute(completeTask);
    }

}
