package com.fr.design.mainframe.adaptve.config.impl;

import com.fr.design.mainframe.DesignOperationEvent;
import com.fr.design.mainframe.ReuseTriggerPointManager;
import com.fr.design.mainframe.adaptve.config.ReuseNotifyInfo;
import com.fr.design.mainframe.adaptve.config.TriggerPointProvider;
import com.fr.event.Event;

/**
 * Created by kerry on 5/7/21
 */
public class CellStyleTriggerPoint implements TriggerPointProvider {
    @Override
    public void triggerAction() {
        ReuseNotifyInfo notifyInfo = ReuseTriggerPointManager.getInstance().getReuseNotifyInfo();
        if (notifyInfo == null) {
            return;
        }
        notifyInfo.addCellStyleModify();
        ReuseTriggerPointManager.getInstance().reuseNotify(notifyInfo);

    }

    @Override
    public Event triggerEvent() {
        return DesignOperationEvent.CELL_STYLE_MODIFY;
    }
}
