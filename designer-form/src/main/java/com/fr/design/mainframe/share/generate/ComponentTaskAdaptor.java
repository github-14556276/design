package com.fr.design.mainframe.share.generate;

/**
 * created by Harrison on 2020/04/23
 **/
public abstract class ComponentTaskAdaptor<T> extends AbstractComponentTask<T> {
    
    private double indicator;
    
    private String loadingText;
    
    public ComponentTaskAdaptor(double indicator, String loadingText) {
        this.indicator = indicator;
        this.loadingText = loadingText;
    }
    
    @Override
    public double indicator() {
        return this.indicator;
    }
    
    @Override
    public String getLoadingText() {
        return this.loadingText;
    }
}
