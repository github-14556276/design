package com.fr.design.mainframe.share.ui.local;

import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.imenu.UIPopupMenu;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.share.action.CreateComponentAction;
import com.fr.design.mainframe.share.action.InstallComponentAction;
import com.fr.design.mainframe.share.action.ShareUIAspect;
import com.fr.design.mainframe.share.group.ui.GroupFileDialog;
import com.fr.design.mainframe.share.group.ui.GroupMoveDialog;
import com.fr.design.mainframe.share.ui.base.PopupMenuItem;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/29
 */
class ManagePane extends JPanel implements ShareUIAspect {
    private static final String NORMAL = "NORMAL";
    private static final String MANAGE = "MANAGE";
    private final UIButton addComponentButton;

    UIButton deleteSelectedButton;
    UIButton moveGroupButton;
    CardLayout cardLayout;

    public ManagePane() {
        //添加组件
        addComponentButton = createAddComponentButton();
        //管理组件
        UIButton manageButton = createManageButton();
        //添加分组
        UIButton addGroupButton = createAddGroupButton();
        //删除选中
        deleteSelectedButton = createDeleteSelectedButton();
        //移动分组
        moveGroupButton = createMoveGroupButton();
        //取消
        UIButton cancelButton = createCancelButton();

        deleteSelectedButton.setEnabled(false);
        moveGroupButton.setEnabled(false);

        JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        leftPanel.add(addComponentButton);

        JPanel rightPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        rightPanel.add(manageButton);
        rightPanel.add(addGroupButton);

        JPanel normalPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        normalPanel.add(leftPanel, BorderLayout.CENTER);
        normalPanel.add(rightPanel, BorderLayout.EAST);

        JPanel managePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        managePanel.add(deleteSelectedButton);
        managePanel.add(moveGroupButton);
        managePanel.add(cancelButton);

        cardLayout = new CardLayout();
        this.setLayout(cardLayout);
        this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        this.add(normalPanel, NORMAL);
        this.add(managePanel, MANAGE);
    }

    public void setButtonEnable(boolean enable) {
        if (moveGroupButton.isEnabled() != enable) {
            deleteSelectedButton.setEnabled(enable);
            moveGroupButton.setEnabled(enable);
        }
    }

    /**
     * 添加组件按钮
     */
    private UIButton createAddComponentButton() {
        return createManageButton(
                Toolkit.i18nText("Fine-Design_Share_Add"),
                IOUtils.readIcon("/com/fr/base/images/share/filter_combo.png"),
                e -> {
                    //创建组件和安装组件的弹窗
                    UIPopupMenu popupMenu = new UIPopupMenu();
                    popupMenu.setPreferredSize(new Dimension(74, 42));
                    popupMenu.setOnlyText(true);
                    popupMenu.setBackground(UIConstants.DEFAULT_BG_RULER);
                    popupMenu.add(new PopupMenuItem(new CreateComponentAction(this)));
                    popupMenu.add(new PopupMenuItem(new InstallComponentAction()));
                    GUICoreUtils.showPopupMenu(popupMenu, addComponentButton, 0, addComponentButton.getSize().height);
                }
        );
    }

    /**
     * 管理组件面板
     */
    private UIButton createManageButton() {
        return createManageButton(
                Toolkit.i18nText("Fine-Design_Share_Manage"),
                e -> replacePanel(true)
        );
    }

    /**
     * 添加分组面板
     */
    private UIButton createAddGroupButton() {
        return createManageButton(
                Toolkit.i18nText("Fine-Design_Share_Group_Add"),
                e -> new GroupFileDialog(DesignerContext.getDesignerFrame(), Toolkit.i18nText("Fine-Design_Share_Group_Add")) {
                    @Override
                    protected void confirmClose() {
                        this.dispose();
                        // 处理不合法的文件夹名称
                        String userInput = getFileName().replaceAll("[\\\\/:*?\"<>|]", StringUtils.EMPTY);
                        //新建分组，刷新显示
                        if (DefaultShareGroupManager.getInstance().createGroup(userInput)) {
                            LocalWidgetRepoPane.getInstance().refreshAllGroupPane(GroupPane.GroupCreateStrategy.CLOSURE);
                        } else {
                            ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Basic_Make_Failure"));
                        }
                    }

                    @Override
                    protected boolean isDuplicate(String fileName) {
                        return DefaultShareGroupManager.getInstance().exist(fileName);
                    }
                }
        );
    }

    private UIButton createCancelButton() {
        return createManageButton(
                Toolkit.i18nText("Fine-Design_Share_Group_Cancel"),
                e -> replacePanel(false)
        );
    }

    /**
     * 切换面板并刷新
     */
    public void replacePanel(boolean isEditing) {
        switchPanel(isEditing);
        LocalWidgetRepoPane.getInstance().refreshShowPanel(isEditing);
    }

    /**
     * 切换面板
     *
     * @param isEditing 是否编辑
     */
    public void switchPanel(boolean isEditing) {
        setButtonEnable(false);
        WidgetSelectedManager.getInstance().clearSelect();
        cardLayout.show(this, isEditing ? MANAGE : NORMAL);
    }

    private UIButton createDeleteSelectedButton() {
        return createManageButton(
                Toolkit.i18nText("Fine-Design_Share_Select_Remove"),
                new Color(0xeb1d1f),
                e -> {
                    int rv = FineJOptionPane.showConfirmDialog(
                            DesignerContext.getDesignerFrame(),
                            Toolkit.i18nText("Fine-Design_Share_Remove_Info"),
                            Toolkit.i18nText("Fine-Design_Share_Group_Confirm"),
                            FineJOptionPane.YES_NO_OPTION
                    );
                    if (rv == FineJOptionPane.YES_OPTION) {
                        if (WidgetSelectedManager.getInstance().unInstallSelect()) {
                            //刷新界面的缓存
                            FineJOptionPane.showMessageDialog(null, Toolkit.i18nText("Fine-Design_Form_Share_Module_Removed_Successful"));
                            replacePanel(false);
                        } else {
                            ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Form_Share_Module_Removed_Failed"));
                        }
                    }
                });
    }

    private UIButton createMoveGroupButton() {
        return createManageButton(
                Toolkit.i18nText("Fine-Design_Share_Group_Move"),
                e -> new GroupMoveDialog(DesignerContext.getDesignerFrame()) {
                    @Override
                    protected void confirmClose() {
                        this.dispose();
                        Group group = (Group) getSelectGroupBox().getSelectedItem();
                        assert group != null;
                        if (WidgetSelectedManager.getInstance().moveSelect(group.getGroupName())) {
                            replacePanel(false);
                        } else {
                            LocalWidgetRepoPane.getInstance().refreshShowPanel();
                            ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Group_Move_Fail_Message"));
                        }
                    }

                }
        );
    }

    private UIButton createManageButton(String title, Icon icon, ActionListener actionListener) {
        UIButton button = createManageButton(title, actionListener);
        button.setIcon(icon);
        button.setHorizontalTextPosition(SwingConstants.LEFT);
        return button;
    }

    private UIButton createManageButton(String title, ActionListener actionListener) {
        return createManageButton(title, new Color(0x333334), actionListener);
    }

    private UIButton createManageButton(String title, Color foregroundColor, ActionListener actionListener) {
        UIButton button = new UIButton(title);
        button.setBackground(Color.white);
        button.setForeground(foregroundColor);
        button.addActionListener(actionListener);
        return button;
    }

    @Override
    public void afterOk() {
        LocalWidgetRepoPane.getInstance().refreshShowPanel(false);
    }
}
