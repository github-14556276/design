package com.fr.design.mainframe.share.generate;

/**
 * created by Harrison on 2020/04/23
 **/
public interface ComponentTask<T> {
    
    T execute() throws Exception;
    
    double indicator();
    
    String getLoadingText();
}
