package com.fr.design.mainframe.share.ui.block;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.form.share.constants.ShareComponentConstants;
import org.jetbrains.annotations.NotNull;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.Serializable;

/**
 * Created by kerry on 2020-10-21
 */
public abstract class PreviewWidgetBlock<T> extends JPanel implements MouseListener, MouseMotionListener, Serializable {
    protected T widget;
    private boolean showing = false;

    public PreviewWidgetBlock(T widget) {
        this.widget = widget;
        initPane();
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    protected void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setPreferredSize(new Dimension(ShareComponentConstants.SHARE_THUMB_WIDTH, ShareComponentConstants.SHARE_BLOCK_HEIGHT));
        JPanel labelPane = new JPanel(new BorderLayout());
        Image image = getCoverImage();
        labelPane.add(initCoverLabel(image));
        labelPane.setBackground(Color.WHITE);
        this.add(labelPane, BorderLayout.CENTER);
    }

    protected UILabel initCoverLabel(Image image) {
        return new UILabel(new ImageIcon(image));
    }

    public T getWidget() {
        return widget;
    }


    @NotNull
    protected abstract Image getCoverImage();

    protected Dimension getCoverDimension() {
        return new Dimension(ShareComponentConstants.SHARE_THUMB_WIDTH, ShareComponentConstants.SHARE_THUMB_HEIGHT);
    }

    private void showPreviewPane() {
        synchronized (this) {
            if (!showing) {
                showPreview(widget);
                showing = true;
            }
        }
    }
    protected abstract String getWidgetUuid();

    protected abstract void showPreview(T widget);

    protected abstract void hidePreview();

    private void hidePreviewPane() {
        if (showing) {
            hidePreview();
            showing = false;
        }
    }

    public abstract Image getPreviewImage();

    @Override
    public void mouseClicked(MouseEvent e) {
        this.hidePreviewPane();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.hidePreviewPane();
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Dimension dimension = getCoverDimension();
        Rectangle containerRec = new Rectangle(0, 0, dimension.width, dimension.height);
        if (containerRec.contains(e.getX(), e.getY())) {
            this.showPreviewPane();
        } else {
            this.hidePreviewPane();
        }
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth(), getHeight());
        super.paint(g);
    }

    protected void removeListener() {
        this.removeMouseListener(this);
        this.removeMouseMotionListener(this);
    }
}
