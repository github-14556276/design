package com.fr.design.mainframe;

public enum ArrangementType {
    LEFT_ALIGN(0),
    RIGHT_ALIGN(1),
    TOP_ALIGN(2),
    BOTTOM_ALIGN(3),
    HORIZONTAL_CENTER_ALIGN(4),
    VERTICAL_CENTER_ALIGN(5),
    HORIZONTAL_AUTO_DISTRIBUTION(6),
    HORIZONTAL_MANUAL_DISTRIBUTION(7),
    VERTICAL_AUTO_DISTRIBUTION(8),
    VERTICAL_MANUAL_DISTRIBUTION(9);

    private int type;

    ArrangementType(int type) {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }

    public static ArrangementType parse(int type) {
        for (ArrangementType arrangementType : ArrangementType.values()) {
            if (arrangementType.getType() == type) {
                return arrangementType;
            }
        }
        return LEFT_ALIGN;
    }
}
