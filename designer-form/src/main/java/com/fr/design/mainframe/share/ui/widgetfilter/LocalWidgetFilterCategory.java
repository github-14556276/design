package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.form.share.bean.WidgetFilterInfo;
import com.fr.form.share.bean.WidgetFilterTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/12
 */
public class LocalWidgetFilterCategory {

    public static List<WidgetFilterTypeInfo> getLocalCategory() {
        List<WidgetFilterTypeInfo> category = new ArrayList<>();

        WidgetFilterTypeInfo source = new WidgetFilterTypeInfo();
        source.setTitle("来源");
        source.setKey("2@source");
        source.addFilterItem(new WidgetFilterInfo("本地", "1", "source"));
        source.addFilterItem(new WidgetFilterInfo("商城", "2", "source"));
        source.addFilterItem(new WidgetFilterInfo("全部", "0", "source"));

        WidgetFilterTypeInfo displayDevice = new WidgetFilterTypeInfo();
        displayDevice.setTitle("展示终端");
        displayDevice.setKey("1@displayDevice");
        displayDevice.addFilterItem(new WidgetFilterInfo("PC端", "1", "displayDevice"));
        displayDevice.addFilterItem(new WidgetFilterInfo("移动端", "2", "displayDevice"));
        displayDevice.addFilterItem(new WidgetFilterInfo("全部", "0", "displayDevice"));

        WidgetFilterTypeInfo fee = new WidgetFilterTypeInfo();
        fee.setTitle("价格");
        fee.setKey("2@fee");
        fee.addFilterItem(new WidgetFilterInfo("付费", "2", "fee"));
        fee.addFilterItem(new WidgetFilterInfo("免费", "1", "fee"));
        fee.addFilterItem(new WidgetFilterInfo("全部", "0", "fee"));

        WidgetFilterTypeInfo chart = new WidgetFilterTypeInfo();
        chart.setTitle("基础元素");
        chart.setKey("3@chart");
        chart.addFilterItem(new WidgetFilterInfo("柱形图/条形图", "1", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("折线图", "3", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("组合图", "4", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("饼图", "2", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("仪表盘", "5", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("地图", "6", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("其他图表", "7", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("明细表", "8", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("基础控件", "9", "chart"));
        chart.addFilterItem(new WidgetFilterInfo("全部", "0", "chart"));

        WidgetFilterTypeInfo report = new WidgetFilterTypeInfo();
        report.setTitle("综合应用");
        report.setKey("4@report");
        report.addFilterItem(new WidgetFilterInfo("指标卡", "1", "report"));
        report.addFilterItem(new WidgetFilterInfo("标题头", "2", "report"));
        report.addFilterItem(new WidgetFilterInfo("特殊功能卡", "4", "report"));
        report.addFilterItem(new WidgetFilterInfo("多维度切换", "5", "report"));
        report.addFilterItem(new WidgetFilterInfo("移动目录导航", "6", "report"));
        report.addFilterItem(new WidgetFilterInfo("填报", "8", "report"));
        report.addFilterItem(new WidgetFilterInfo("全部", "0", "report"));

        WidgetFilterTypeInfo style = new WidgetFilterTypeInfo();
        style.setTitle("风格");
        style.setKey("5@style");
        style.addFilterItem(new WidgetFilterInfo("简约清新", "1", "style"));
        style.addFilterItem(new WidgetFilterInfo("商务稳重", "2", "style"));
        style.addFilterItem(new WidgetFilterInfo("活泼绚丽", "3", "style"));
        style.addFilterItem(new WidgetFilterInfo("酷炫科技", "4", "style"));
        style.addFilterItem(new WidgetFilterInfo("其他风格", "5", "style"));
        style.addFilterItem(new WidgetFilterInfo("全部", "0", "style"));

        category.add(displayDevice);
        category.add(source);
        category.add(chart);
        category.add(report);
        return category;

    }

}
