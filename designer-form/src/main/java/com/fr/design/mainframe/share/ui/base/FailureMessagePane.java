package com.fr.design.mainframe.share.ui.base;

import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.frpane.UITextPane;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.general.IOUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/9/14
 */
public class FailureMessagePane extends BasicPane {
    public FailureMessagePane(String str) {
        UILabel imageLabel = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/error_icon.png"));
        UILabel label = new UILabel(Toolkit.i18nText("Fine-Design_Share_Share_Modules_Error"));
        UITextPane textPane = new UITextPane();
        UIScrollPane jScrollPane = new UIScrollPane(textPane);
        JPanel panel = new JPanel();

        Style style = new StyleContext().new NamedStyle();
        StyleConstants.setLineSpacing(style, 0.1f);
        StyleConstants.setFontSize(style, 12);
        textPane.setLogicalStyle(style);
        textPane.setText(str);
        textPane.setCaretPosition(0);
        textPane.setEditable(false);
        textPane.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));

        jScrollPane.setBorder(BorderFactory.createEmptyBorder());
        label.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 8));

        panel.add(imageLabel);
        panel.add(label);
        panel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBorder(BorderFactory.createEmptyBorder(4, 0, 4, 0));
        add(panel, BorderLayout.NORTH);
        add(jScrollPane, BorderLayout.CENTER);
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Share_Dialog_Message");
    }

}
