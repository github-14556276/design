package com.fr.design.mainframe.share.generate;

import com.fr.design.mainframe.share.Bean.ComponentGenerateInfo;
import com.fr.design.mainframe.share.generate.impl.ComponentPureGenerator;
import com.fr.design.mainframe.share.generate.impl.ComponentUploadGenerator;
import com.fr.design.mainframe.share.ui.base.ShareProgressBar;
import com.fr.log.FineLoggerFactory;

import java.util.concurrent.Future;

/**
 * created by Harrison on 2020/04/13
 **/
public class ComponentGeneratorCenter {
    
    private ComponentGenerateInfo info;
    
    public ComponentGeneratorCenter(ComponentGenerateInfo info) {
    
        this.info = info;
    }
    
    public boolean generate() throws Exception {
    
        try {
            
            return generate0();
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable throwable) {
            FineLoggerFactory.getLogger().error(throwable.getMessage(), throwable);
            return false;
        } finally {
            ShareProgressBar.getInstance().completeNow();
        }
    }
    
    private boolean generate0() throws Throwable{
        
        ComponentGenerator generator = innerGenerator();
        
        //ui
        ShareProgressBar progressBar = ShareProgressBar.getInstance();
        progressBar.prepare(generator.speed());
        progressBar.monitor();
    
        return generator.generate();
    }
    
    private void waitComplete(Future<Boolean> complete) {
    
        try {
            complete.get();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }
    
    private ComponentGenerator innerGenerator() {
        
        if (info.isAutoUpload()) {
            return ComponentUploadGenerator.create(info);
        } else {
            return ComponentPureGenerator.create(info);
        }
    }

}
