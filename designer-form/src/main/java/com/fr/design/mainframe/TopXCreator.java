package com.fr.design.mainframe;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.design.utils.ComponentUtils;
import com.fr.stable.CoreGraphHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/8/31
 */
public class TopXCreator extends JComponent {
    private final FormDesigner designer;
    private final XCreator creator;

    public TopXCreator(FormDesigner designer, XCreator creator) {
        this.designer = designer;
        this.creator = creator;
        init();
    }

    private void init() {
        setOpaque(false);
        setBackground(null);
        setLayout(null);
        setBounds(calculateBounds());
    }


    /**
     * 重新设置组件大小
     * */
    public void resizeTopXCreator() {
        setBounds(calculateBounds());
    }

    /**
     * 计算显示大小
     * */
    private Rectangle calculateBounds() {
        Rectangle rect = ComponentUtils.getRelativeBounds(creator);
        Rectangle bounds = new Rectangle(0, 0, creator.getWidth(), creator.getHeight());
        bounds.x += (rect.x - designer.getHorizontalScaleValue());
        bounds.y += (rect.y - designer.getVerticalScaleValue());
        return bounds;
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);
        ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
        // 禁止双缓冲
        ComponentUtils.disableBuffer(creator, dbcomponents);
        creator.paint(g);
        // 恢复双缓冲
        ComponentUtils.resetBuffer(dbcomponents);
    }
}
