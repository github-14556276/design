package com.fr.design.mainframe.share.ui.online;

import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.share.ui.base.DownloadProgressPane;
import com.fr.design.mainframe.share.ui.base.ImitationProgress;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.design.mainframe.share.ui.block.OnlineWidgetBlock;
import com.fr.design.mainframe.share.ui.block.SimpleWidgetBlock;
import com.fr.design.mainframe.share.ui.local.LocalWidgetRepoPane;
import com.fr.design.mainframe.share.ui.online.widgetpackage.OnlineWidgetPackagesShowPane;
import com.fr.design.mainframe.share.util.DownloadUtils;
import com.fr.design.mainframe.share.util.InstallUtils;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.Group;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StableUtils;
import org.jetbrains.annotations.Nullable;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/22
 */
public class OnlineDownloadPackagePane extends OnlineWidgetSelectPane {
    private final String packageId;
    private volatile double process;


    private ImitationThread imitationThread;
    private SwingWorker<Group, Void> downloadWorker;
    private DownloadProgressPane downloadProgressPane;
    private final OnlineWidgetPackagesShowPane parentPane;


    public OnlineDownloadPackagePane(OnlineWidgetPackagesShowPane parentPane, OnlineShareWidget[] providers, int widgetsPerNum) {
        super(providers, widgetsPerNum);
        this.parentPane = parentPane;
        this.packageId = parentPane.getCurrentPackageId();
    }

    protected OnlineWidgetBlock createWidgetBlock(OnlineShareWidget provider) {
        return new SimpleWidgetBlock(provider, this);
    }

    protected void setScrollPaneStyle(UIScrollPane scrollPane) {
        scrollPane.setBorder(null);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setWheelScrollingEnabled(false);
    }

    @Override
    protected boolean pagePaneEnable() {
        return false;
    }

    protected JPanel createWidgetPane() {
        return new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                Graphics2D g2d = (Graphics2D) g;
                Composite oldComposite = g2d.getComposite();
                AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.65f);
                g2d.setComposite(composite);
                g2d.setColor(Color.DARK_GRAY);
                g2d.fillRect(0, 0, getWidth(), getHeight());
                g2d.setComposite(oldComposite);
            }
        };
    }

    protected JPanel createContentPane(JPanel widgetPane) {
        JPanel panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                for (int i = 0; i < getComponentCount(); i++) {
                    getComponent(i).setSize(getSize());
                }
                super.paint(g);
            }
        };
        panel.setLayout(null);
        downloadProgressPane = new DownloadProgressPane(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int returnVal = FineJOptionPane.showConfirmDialog(DesignerContext.getDesignerFrame(),
                        Toolkit.i18nText("Fine-Design_Share_Download_Cancel_Confirm"), Toolkit.i18nText("Fine-Design_Share_Group_Confirm"),
                        FineJOptionPane.OK_CANCEL_OPTION);
                if (returnVal == FineJOptionPane.OK_OPTION) {
                    //取消下载线程
                    imitationThread.interrupt();
                    downloadWorker.cancel(false);

                    //切换面板
                    parentPane.resetWidgetDetailPane(packageId, getSharableWidgetProviders());
                }
            }
        });
        panel.add(downloadProgressPane);
        panel.add(widgetPane, BorderLayout.CENTER);
        return panel;
    }

    public void downloadWidget(OnlineShareWidget onlineShareWidget) {

        final com.fr.design.extra.Process<Double> downloadProcess = aDouble -> {
            OnlineDownloadPackagePane.this.process = 0.8 * aDouble;
            downloadProgressPane.updateProgress(process);
        };
        final com.fr.design.extra.Process<Double> installProcess = aDouble -> {
            OnlineDownloadPackagePane.this.process = 0.8 + 0.2 * aDouble;
            downloadProgressPane.updateProgress(process);
        };

        downloadProcess.process(0.0D);

        //假进度线程
        final ImitationProgress imitationProgress = new ImitationProgress(downloadProcess, getSharableWidgetNum());
        imitationThread = new ImitationThread(imitationProgress);
        imitationThread.setName("Component-ImitationProcessThread");

        //下载线程
        downloadWorker = new DownLoadSwingWorker(installProcess, onlineShareWidget);

        parallel(imitationThread, downloadWorker);
    }

    private void parallel(Thread imitationProcessThread, SwingWorker<Group, Void> worker) {
        imitationProcessThread.start();
        worker.execute();
    }

    /**
     * 假进度线程
     */
    private static class ImitationThread extends Thread {

        private final ImitationProgress imitationProgress;

        public ImitationThread(ImitationProgress progress) {
            imitationProgress = progress;
        }

        @Override
        public void run() {
            imitationProgress.start();
        }

        public void complete() {
            imitationProgress.completed();
            this.interrupt();
        }

        public void stopThread() {
            imitationProgress.stop();
            this.interrupt();
        }
    }

    private class DownLoadSwingWorker extends SwingWorker<Group, Void> {
        final com.fr.design.extra.Process<Double> installProcess;
        final OnlineShareWidget onlineShareWidget;

        public DownLoadSwingWorker(com.fr.design.extra.Process<Double> installProcess, OnlineShareWidget onlineShareWidget) {
            this.installProcess = installProcess;
            this.onlineShareWidget = onlineShareWidget;
        }

        @Override
        @Nullable
        protected Group doInBackground() {
            final String filePath;
            List<String> failureList = new ArrayList<>();
            try {
                filePath = DownloadUtils.downloadPackage(onlineShareWidget.getId(), onlineShareWidget.getName(), DownLoadSwingWorker.this::isCancelled);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
                imitationThread.stopThread();
                return null;
            }
            if (this.isCancelled()) {
                imitationThread.stopThread();
                StableUtils.deleteFile(new File(filePath));
                return null;
            }

            //等待假进度线程结束
            imitationThread.complete();
            try {
                imitationThread.join();
            } catch (InterruptedException ignore) {
            }

            //再判断一次
            if (this.isCancelled()) {
                StableUtils.deleteFile(new File(filePath));
                return null;
            }
            ShareComponentUtils.checkReadMe();
            //安装
            File file = new File(filePath);
            installProcess.process(0.0D);
            downloadProgressPane.changeState();
            InstallUtils.InstallResult result = null;
            try {
                if (file.exists()) {
                    result = InstallUtils.installReusFile(file, System.currentTimeMillis(), failureList, installProcess);
                }
            } finally {
                //删掉下载组件的目录
                StableUtils.deleteFile(file);
            }
            return result == null ? null : result.group;
        }

        @Override
        protected void done() {
            OnlineDownloadPackagePane.this.process = 0.0D;
            //替换面板，显示下载label
            parentPane.resetWidgetDetailPane(packageId, getSharableWidgetProviders());
            parentPane.repaint();
            if (!isCancelled()) {
                try {
                    Group group = get();
                    if (group != null) {
                        LocalWidgetRepoPane.getInstance().addGroup(group);
                    }
                } catch (InterruptedException | ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e, e.getMessage());
                }
            }
        }
    }
}
