package com.fr.design.designer.beans.location;

import java.awt.Cursor;
import java.awt.Rectangle;

import com.fr.design.mainframe.FormDesigner;

public class LeftBottom extends AccessDirection  {

    public LeftBottom() {
    }

	@Override
	public Rectangle getDraggedBounds(int dx, int dy, Rectangle current_bounds, FormDesigner designer,
			Rectangle oldbounds) {
		int[] xy = sorption(oldbounds.x + dx, oldbounds.y + dy + oldbounds.height, current_bounds, designer);
		current_bounds.x = xy[0];
		current_bounds.width = oldbounds.width - current_bounds.x + oldbounds.x;
		current_bounds.height = xy[1] - oldbounds.y;

        if (designer.getStateModel().isAspectRatioLocked()) {
            Rectangle backupBounds = designer.getSelectionModel().getSelection().getBackupBounds();
            double current_diagonal = Math.pow(current_bounds.width, 2) + Math.pow(current_bounds.height, 2);
            double backup_diagonal = Math.pow(backupBounds.width, 2) + Math.pow(backupBounds.height, 2);

            int width = (int) (Math.sqrt((current_diagonal / backup_diagonal) * (Math.pow(backupBounds.width, 2))));
            int height = (int) (Math.sqrt((current_diagonal / backup_diagonal) * (Math.pow(backupBounds.height, 2))));

            int currentRight = current_bounds.x + current_bounds.width;
            current_bounds.width = width;
            current_bounds.height = height;
            current_bounds.x = currentRight - width;
        }

		return current_bounds;
	}

    @Override
    public int getCursor() {
        return Cursor.SW_RESIZE_CURSOR;
    }

    @Override
    public String getTooltip() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Lock_Aspect_Ratio_Mouse_ToolTip");
    }

     @Override
    public int getActual() {
        return Direction.LEFT_BOTTOM;
    }
}