package com.fr.design.fun;

import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.stable.fun.mark.Mutable;

/**
 * created by Harrison on 2020/05/14
 **/
public interface DesignerEditListenerProvider extends Mutable {
    
    String XML_TAG = "DesignerEditListenerProvider";
    
    int CURRENT_LEVEL = 1;
    
    /**
     * 触发设计器事件
     *
     * @param evt 事件
     */
    void fireCreatorModified(DesignerEvent evt);
    
}
