package com.fr.design.fun;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.mainframe.BaseJForm;
import com.fr.design.mainframe.FormDesigner;
import com.fr.stable.fun.mark.Immutable;

import javax.swing.JComponent;


/**
 * Created by kerry on 5/28/21
 */
public interface ShareWidgetUIProcessor extends Immutable {

    String MARK_STRING = "ShareWidgetUIProcessor";

    int CURRENT_LEVEL = 1;

    /**
     * 生成属性配置界面
     *
     * @param xCreator     选中的Xcreator
     * @param formDesigner 表单设计器
     * @return 属性配置界面
     */
    JComponent createToolPane(XCreator xCreator, BaseJForm jform, FormDesigner formDesigner);

}
