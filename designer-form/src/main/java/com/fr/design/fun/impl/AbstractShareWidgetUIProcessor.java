package com.fr.design.fun.impl;

import com.fr.design.fun.ShareWidgetUIProcessor;
import com.fr.stable.fun.mark.API;

/**
 * Created by kerry on 5/28/21
 */
@API(level = ShareWidgetUIProcessor.CURRENT_LEVEL)
public abstract class AbstractShareWidgetUIProcessor implements ShareWidgetUIProcessor {

    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    public int layerIndex() {
        return DEFAULT_LAYER_INDEX;
    }
}
