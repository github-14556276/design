package com.fr.design.gui.xpane;

import com.fr.config.predefined.BackgroundWithAlpha;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.PredefinedStyleSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.background.BackgroundWithAlphaSettingPane;
import com.fr.design.mainframe.predefined.ui.preview.StyleSettingPreviewPane;
import com.fr.form.ui.NameComponentBackground;
import com.fr.form.ui.NameFormBackground;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

/**
 * Created by kerry on 2020-09-02
 */
public class FormPredefinedBackgroundPane extends PredefinedStyleSettingPane<NameComponentBackground> {
    private BackgroundWithAlphaSettingPane backgroundPane;

    @Override
    protected StyleSettingPreviewPane createPreviewPane() {
        return new PreviewPane();
    }

    @Override
    protected JPanel createCustomDetailPane() {
        JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        backgroundPane = new BackgroundWithAlphaSettingPane();
        backgroundPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                previewPane.refresh();
            }
        });
        panel.add(backgroundPane, BorderLayout.CENTER);
        return panel;
    }

    @Override
    public void populateBean(NameComponentBackground ob) {
        this.setPopulating(true);
        super.populate(ob);
        this.backgroundPane.populateBean(ob.createRealStyle());
        this.previewPane.refresh();
        this.setPopulating(false);
    }


    @Override
    public NameComponentBackground updateBean() {
        if (this.predefinedRadioBtn.isSelected()) {
            return NameFormBackground.createPredefinedStyle(getPredefinedStyleName());
        } else {
            return NameFormBackground.createCustomStyle(this.backgroundPane.updateBean());
        }
    }

    private BackgroundWithAlpha getCurrentValue() {
        if (this.predefinedRadioBtn.isSelected()) {
            return updatePredefinedStyle();
        } else {
            return this.backgroundPane.updateBean();
        }
    }

    private BackgroundWithAlpha updatePredefinedStyle(){
        NameComponentBackground componentBackground = NameFormBackground.createPredefinedStyle(getPredefinedStyleName());
        return componentBackground.createRealStyle();
    }

    protected void populateCustomPane(){
        this.backgroundPane.populateBean(updatePredefinedStyle());
    }

    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background");
    }

    class PreviewPane extends StyleSettingPreviewPane {
        private BackgroundWithAlpha background;

        public PreviewPane() {
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            this.setPreferredSize(new Dimension(390, 511));
            this.setBackground(Color.WHITE);
        }


        public void refresh() {
            background = getCurrentValue();
            this.repaint();
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            if (background != null && background.getBackground() != null) {
                background.getBackground().paint(g, new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
            }
        }
    }

}
