package com.fr.design.gui.xpane;

import com.fr.base.Style;
import com.fr.base.background.ImageBackground;
import com.fr.base.background.ImageFileBackground;
import com.fr.design.border.UIRoundedBorder;
import com.fr.design.constants.UIConstants;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.frpane.ImgChooseWrapper;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.backgroundpane.ImagePreviewPane;
import com.fr.design.style.background.image.ImageFileChooser;
import com.fr.design.widget.ui.designer.component.UIBoundSpinner;
import com.fr.form.ui.WidgetTitle;
import com.fr.general.Background;
import com.fr.general.IOUtils;
import com.fr.general.ImageWithSuffix;
import com.fr.general.act.TitlePacker;
import com.fr.stable.Constants;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;

/**
 * @author Starryi
 * @version 10.0.18
 * Created by Starryi on 2021/7/3
 */
public class TitleInsetImagePane extends JPanel implements UIObserver {
    private final int SETTING_LABEL_WIDTH = LayoutStylePane.SETTING_LABEL_WIDTH;
    private final int DELETE_BUTTON_SIZE = 24;
    private final int IMAGE_PREVIEW_SIZE = 145;
    private final Color IMAGE_PREVIEW_OVERLAY_COLOR = new Color(255, 255, 255, 51);
    private final Style DEFAULT_IMAGE_LAYOUT_STYLE = Style.DEFAULT_STYLE.deriveImageLayout(Constants.IMAGE_DEFAULT);
    private final int DEFAULT_INSET_LOCATION_INDEX = 0;
    private final int DEFAULT_INSET_PADDING = 10;

    private UIObserverListener uiObserverListener;

    private UIButton imageChooseButton;
    private UIButton imageDeleteButton;
    private ImagePreviewPane imagePreviewPane;
    private JPanel imagePreviewOverlayPane;
    private UIButtonGroup<Integer> imageLocationPane;
    private UISpinner imagePaddingPane;

    private ImageFileChooser imageFileChooser;

    public TitleInsetImagePane() {
        this.initComponents();
        this.initLayout();
    }

    private JPanel createImageChooseComposedPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = {p};
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        return TableLayoutHelper.createCommonTableLayoutPane( new JComponent[][]{
                        {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Inset")), this.imageChooseButton},
                },
                rowSize, columnSize, IntervalConstants.INTERVAL_L1);
    }

    private JPanel createImageContentComposedPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = {p, p, p, p, p};
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        JPanel deletableImagePreviewPane = new JPanel();
        deletableImagePreviewPane.setLayout(null);
        deletableImagePreviewPane.setBorder(new UIRoundedBorder(UIConstants.LINE_COLOR, 1, 5));
        deletableImagePreviewPane.setPreferredSize(new Dimension(IMAGE_PREVIEW_SIZE, IMAGE_PREVIEW_SIZE));
        imagePreviewOverlayPane = new JPanel();
        imagePreviewOverlayPane.setPreferredSize(new Dimension(IMAGE_PREVIEW_SIZE - 2, IMAGE_PREVIEW_SIZE - 2));
        imagePreviewOverlayPane.setBackground(IMAGE_PREVIEW_OVERLAY_COLOR);
        imagePreviewPane.setBounds(0, 0, IMAGE_PREVIEW_SIZE, IMAGE_PREVIEW_SIZE);
        imagePreviewOverlayPane.setBounds(1, 1, IMAGE_PREVIEW_SIZE - 2, IMAGE_PREVIEW_SIZE - 2);
        imageDeleteButton.setBounds(IMAGE_PREVIEW_SIZE - DELETE_BUTTON_SIZE, 0, DELETE_BUTTON_SIZE, DELETE_BUTTON_SIZE);

        JPanel mousePane = new JPanel();
        mousePane.setBounds(0, 0, IMAGE_PREVIEW_SIZE, IMAGE_PREVIEW_SIZE);
        mousePane.setOpaque(false);
        mousePane.setBackground(null);

        deletableImagePreviewPane.add(mousePane, 0);
        deletableImagePreviewPane.add(imageDeleteButton, 1);
        deletableImagePreviewPane.add(imagePreviewOverlayPane, 2);
        deletableImagePreviewPane.add(imagePreviewPane, 3);

        imagePreviewOverlayPane.setVisible(false);
        imageDeleteButton.setVisible(false);
        imageDeleteButton.setEnabled(false);
        mousePane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                imagePreviewOverlayPane.setVisible(true);
                imageDeleteButton.setVisible(true);
                imageDeleteButton.setEnabled(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                imagePreviewOverlayPane.setVisible(false);
                imageDeleteButton.setVisible(false);
                imageDeleteButton.setEnabled(false);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int x = e.getX();
                int y = e.getY();
                Rectangle bounds = imageDeleteButton.getBounds();
                if (bounds.x < x && x < bounds.x + bounds.width && bounds.y < y && y < bounds.y + bounds.height) {
                    imagePreviewPane.setImageWithSuffix(null);
                    imageLocationPane.setSelectedIndex(DEFAULT_INSET_LOCATION_INDEX);
                    imagePaddingPane.setValue(DEFAULT_INSET_PADDING);
                    imagePreviewOverlayPane.setVisible(false);
                    imageDeleteButton.setVisible(false);
                    imageDeleteButton.setEnabled(false);
                    getComponent(1).setVisible(false);

                    fireStateChanged();
                }
            }
        });

        return TableLayoutHelper.createCommonTableLayoutPane(
                new JComponent[][]{
                        {null, deletableImagePreviewPane},
                        {null, new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Inset_Relative_Location"))},
                        {null, this.imageLocationPane},
                        {null, new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Inset_Padding"))},
                        {null, this.imagePaddingPane}
                },
                rowSize, columnSize, IntervalConstants.INTERVAL_L1);
    }

    private void initImageFileChooserIfNotExist() {
        if (imageFileChooser == null) {
            imageFileChooser = new ImageFileChooser();
            imageFileChooser.setMultiSelectionEnabled(false);
        }
    }

    private void initComponents() {
        imageChooseButton = new UIButton(IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_choose_inset.png"));

        imageDeleteButton = new OpaqueColorButton(
                IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_delete_inset.png"),
                new Color(51, 51, 52, 178),
                2);
        imageDeleteButton.setPreferredSize(new Dimension(DELETE_BUTTON_SIZE, DELETE_BUTTON_SIZE));

        imagePreviewPane = new ImagePreviewPane();
        imagePreviewPane.setImageStyle(DEFAULT_IMAGE_LAYOUT_STYLE);
        imagePreviewPane.setPreferredSize(new Dimension(IMAGE_PREVIEW_SIZE, IMAGE_PREVIEW_SIZE));

        imageLocationPane = new UIButtonGroup<Integer>(new Icon[][]{
                {
                        IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_inset_left_selected.png"),
                        IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_inset_left_unselected.png")
                },
                {
                        IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_inset_both_selected.png"),
                        IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_inset_both_unselected.png")
                },
                {
                        IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_inset_right_selected.png"),
                        IOUtils.readIcon("/com/fr/design/images/buttonicon/icon_inset_right_unselected.png")
                },
        });
        imageLocationPane.setSelectedIndex(DEFAULT_INSET_LOCATION_INDEX);
        imageLocationPane.setAllToolTips(new String[]{
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Inset_Relative_Left_Tooltip"),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Inset_Relative_Both_Tooltip"),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Inset_Relative_Right_Tooltip"),
        });

        imagePaddingPane = new UIBoundSpinner(0, Integer.MAX_VALUE, 1, WidgetTitle.DEFAULT_INSET_PADDING);
        imagePaddingPane.setValue(DEFAULT_INSET_PADDING);
    }

    private void initLayout() {


        this.setLayout(new BorderLayout(0, IntervalConstants.INTERVAL_L1));

        add(createImageChooseComposedPane(), BorderLayout.NORTH, 0);
        add(createImageContentComposedPane(), BorderLayout.CENTER, 1);

        getComponent(1).setVisible(false);

        this.imageChooseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                initImageFileChooserIfNotExist();

                int returnVal = imageFileChooser.showOpenDialog(DesignerContext.getDesignerFrame());
                ImgChooseWrapper.getInstance(imagePreviewPane, imageFileChooser, DEFAULT_IMAGE_LAYOUT_STYLE, new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        if (!getComponent(1).isVisible()) {
                            imageLocationPane.setSelectedIndex(DEFAULT_INSET_LOCATION_INDEX);
                            imagePaddingPane.setValue(DEFAULT_INSET_PADDING);
                            getComponent(1).setVisible(true);
                        }

                        fireStateChanged();
                    }
                }).dealWithImageFile(returnVal);
            }
        });
    }

    public void populateBean(TitlePacker packer) {
        Background insetImage = packer.getInsetImage();
        if (insetImage instanceof ImageBackground) {
            ImageWithSuffix image = ((ImageBackground) insetImage).getImageWithSuffix();
            if (image != null) {
                this.imagePreviewPane.setImageWithSuffix(image);

                if (!packer.isInsetRelativeTextLeft()) {
                    this.imageLocationPane.setSelectedIndex(2);
                } else if (!packer.isInsetRelativeTextRight()) {
                    this.imageLocationPane.setSelectedIndex(0);
                } else {
                    this.imageLocationPane.setSelectedIndex(1);
                }

                this.imagePaddingPane.setValue(packer.getInsetImagePadding());

                getComponent(1).setVisible(true);

                return;
            }
        }

        this.imagePreviewPane.setImageWithSuffix(null);
        this.imageLocationPane.setSelectedIndex(DEFAULT_INSET_LOCATION_INDEX);
        this.imagePaddingPane.setValue(DEFAULT_INSET_PADDING);

        getComponent(1).setVisible(false);
    }

    public void updateBean(TitlePacker packer) {
        Image image = imagePreviewPane.getImageWithSuffix();
        if (image != null) {
            packer.setInsetImage(new ImageFileBackground(image, Constants.IMAGE_DEFAULT));

            int imageLocationIndex = this.imageLocationPane.getSelectedIndex();
            packer.setInsetRelativeTextLeft(imageLocationIndex == 0 || imageLocationIndex == 1);
            packer.setInsetRelativeTextRight(imageLocationIndex == 2 || imageLocationIndex == 1);

            packer.setInsetImagePadding((int) this.imagePaddingPane.getValue());
        } else {
            packer.setInsetImage(null);
            packer.setInsetImagePadding(WidgetTitle.DEFAULT_INSET_PADDING);
            packer.setInsetRelativeTextLeft(WidgetTitle.DEFAULT_INSET_LEFT);
            packer.setInsetRelativeTextRight(WidgetTitle.DEFAULT_INSET_RIGHT);
        }
    }

    private void fireStateChanged() {
        if (uiObserverListener != null) {
            uiObserverListener.doChange();
        }
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.uiObserverListener = listener;
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    private static class OpaqueColorButton extends UIButton {
        private final Color color;
        private final int radius;

        public OpaqueColorButton(Icon icon, Color color, int radius) {
            super(icon);
            setUI(new BasicButtonUI());
            setOpaque(true);
            setBorderPainted(false);
            setBorder(null);
            setFocusPainted(false);
            setContentAreaFilled(false);
            this.color = color;
            this.radius = radius;
        }

        @Override
        public void paint(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            Color oldColor = g2d.getColor();

            Shape shape = new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), radius, radius);
            g2d.clip(shape);
            g2d.setColor(color);
            g2d.fillRect(0, 0, getWidth(), getHeight());

            g2d.setColor(oldColor);
            super.paint(g);
        }
    }
}
