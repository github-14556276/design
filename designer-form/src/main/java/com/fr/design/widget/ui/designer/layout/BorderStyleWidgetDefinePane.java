package com.fr.design.widget.ui.designer.layout;

import com.fr.design.data.DataCreatorUI;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.xpane.LayoutStylePane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.widget.accessibles.AccessibleWLayoutBorderStyleEditor;
import com.fr.design.widget.ui.designer.AbstractDataModify;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.LayoutBorderStyle;


import javax.swing.*;
import java.awt.*;

/**
 * Created by kerry on 2017/8/29.
 */
public class BorderStyleWidgetDefinePane extends AbstractDataModify<AbstractBorderStyleWidget> {
    private LayoutStylePane stylePane;

    public BorderStyleWidgetDefinePane(XCreator xCreator) {
        super(xCreator);
        initComponent();
    }


    public void initComponent() {
        stylePane = new LayoutStylePane();
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel advancePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        advancePane.add(stylePane, BorderLayout.CENTER);

        UIExpandablePane layoutExpandablePane = new UIExpandablePane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Advanced"), 280, 20, advancePane );

        this.add(layoutExpandablePane, BorderLayout.CENTER);
    }


    @Override
    public String title4PopupWindow() {
        return "borderStyleWidget";
    }

    @Override
    public void populateBean(AbstractBorderStyleWidget ob) {
        stylePane.populateBean((LayoutBorderStyle) ob.getBorderStyle());
    }


    @Override
    public AbstractBorderStyleWidget updateBean() {
        AbstractBorderStyleWidget abstractBorderStyleWidget = (AbstractBorderStyleWidget)creator.toData();
        abstractBorderStyleWidget.setBorderStyle(stylePane.updateBean());
        return abstractBorderStyleWidget;

    }


    @Override
    public DataCreatorUI dataUI() {
        return null;
    }

}
