package com.fr.design.widget.ui.designer.layout;

import com.fr.base.io.IOFile;
import com.fr.base.iofile.attr.WatermarkAttr;
import com.fr.design.data.DataCreatorUI;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XWFitLayout;
import com.fr.design.designer.properties.items.FRLayoutTypeItems;
import com.fr.design.designer.properties.items.Item;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.xpane.LayoutStylePane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.design.mainframe.widget.accessibles.AccessibleBodyWatermarkEditor;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.design.widget.ui.designer.component.WidgetBoundPane;
import com.fr.form.ui.LayoutBorderStyle;
import com.fr.form.ui.container.WAbsoluteBodyLayout;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WBodyLayoutType;
import com.fr.form.ui.container.WFitLayout;
import com.fr.report.core.ReportUtils;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by ibm on 2017/8/2.
 */
public class FRAbsoluteBodyLayoutDefinePane extends FRAbsoluteLayoutDefinePane {
    private static final int MAX_LABEL_WIDTH = 80;

    private LayoutStylePane stylePane;
    private AccessibleBodyWatermarkEditor watermarkEditor;
    private WidgetBoundPane boundPane;

    private UIComboBox layoutCombox;
    private WBodyLayoutType layoutType = WBodyLayoutType.ABSOLUTE;

    public FRAbsoluteBodyLayoutDefinePane(XCreator xCreator) {
        super(xCreator);
    }


    public void initComponent() {
        initUIComboBox();
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        boundPane = new WidgetBoundPane(creator);
        this.add(boundPane, BorderLayout.NORTH);

        JPanel panel1 = FRGUIPaneFactory.createBorderLayout_S_Pane();
        this.add(panel1, BorderLayout.CENTER);

        UIExpandablePane scalePane = new UIExpandablePane(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Area_Scaling"),
                280, 20,
                createAreaScalePane()
        );
        panel1.add(scalePane, BorderLayout.NORTH);

        UIExpandablePane advancedPane = new UIExpandablePane(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Advanced"),
                280, 20,
                this.createAdvancePane());
        panel1.add(advancedPane, BorderLayout.CENTER);

    }

    public JPanel createAdvancePane() {
        JPanel advancedContentPane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        stylePane = new LayoutStylePane();
        advancedContentPane.add(stylePane, BorderLayout.NORTH);

        watermarkEditor = new AccessibleBodyWatermarkEditor();
        JPanel watermarkPane = TableLayoutHelper.createGapTableLayoutPane(
                new Component[][]{
                        new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_WaterMark")), watermarkEditor}
                }, TableLayoutHelper.FILL_LASTCOLUMN, IntervalConstants.INTERVAL_W3, IntervalConstants.INTERVAL_L1);
        watermarkPane.setBorder(BorderFactory.createEmptyBorder(IntervalConstants.INTERVAL_L1, 0, IntervalConstants.INTERVAL_L1, 0));
        advancedContentPane.add(watermarkPane, BorderLayout.CENTER);

        return advancedContentPane;
    }

    public JPanel createAreaScalePane() {
        initLayoutComboBox();

        UILabel layoutTypeLabel = FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Attr_Layout_Type"));
        UILabel scaleModeLabel = FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Scaling_Mode"));

        JPanel contentPane = TableLayoutHelper.createGapTableLayoutPane(
                new Component[][]{
                        {layoutTypeLabel, layoutCombox},
                        {scaleModeLabel, comboBox}
                },
                TableLayoutHelper.FILL_LASTCOLUMN, IntervalConstants.INTERVAL_W1, IntervalConstants.INTERVAL_L1);

        contentPane.setBorder(BorderFactory.createEmptyBorder(IntervalConstants.INTERVAL_L1, 0, IntervalConstants.INTERVAL_L1, 0));

        JPanel containerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        containerPane.add(contentPane, BorderLayout.CENTER);
        containerPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));

        return containerPane;
    }

    public void initLayoutComboBox() {
        Item[] items = FRLayoutTypeItems.ITEMS;
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (Item item : items) {
            model.addElement(item);
        }
        layoutCombox = new UIComboBox(model);
        layoutCombox.setSelectedIndex(1);
    }

    @Override
    public String title4PopupWindow() {
        return "absoluteBodyLayout";
    }

    public void populateSubPane(WAbsoluteLayout ob) {
        layoutCombox.setSelectedIndex(1);
        stylePane.populateBean((LayoutBorderStyle) ob.getBorderStyle());
        boundPane.populate();
        //从全局读取水印
        watermarkEditor.setValue(ReportUtils.getWatermarkAttrFromTemplateAndGlobal(getCurrentIOFile()));
    }

    public WAbsoluteBodyLayout updateSubPane() {
        WAbsoluteBodyLayout layout = (WAbsoluteBodyLayout) creator.toData();
        boundPane.update();
        Item item = (Item) layoutCombox.getSelectedItem();
        Object value = item.getValue();
        int state = 0;
        if (value instanceof Integer) {
            state = (Integer) value;
        }

        if (layoutType == WBodyLayoutType.ABSOLUTE) {
            ((XWFitLayout) creator.getBackupParent()).toData().resetStyle();
            if (state == WBodyLayoutType.FIT.getTypeValue()) {
                XWFitLayout xwFitLayout = ((XWFitLayout)creator.getBackupParent());
                xwFitLayout.switch2FitBodyLayout(creator);
                WFitLayout wFitLayout = xwFitLayout.toData();
                // 切换布局类型时，保留body背景样式
                if (wFitLayout != null) {
                    wFitLayout.setBorderStyle(layout.getBorderStyle());
                }
                copyLayoutAttr(layout, xwFitLayout.toData());
                xwFitLayout.refreshStylePreviewEffect();
            }
        }
        layout.setBorderStyle(stylePane.updateBean());
        updateWatermark();
        return layout;
    }

    private void updateWatermark() {
        WatermarkAttr watermark = (WatermarkAttr) watermarkEditor.getValue();
        if (watermark != null) {
            IOFile ioFile = getCurrentIOFile();
            ioFile.addAttrMark(watermark);
        }
    }

    private IOFile getCurrentIOFile() {
        return WidgetPropertyPane.getInstance().getEditingFormDesigner().getTarget();
    }

    @Override
    public DataCreatorUI dataUI() {
        return null;
    }


}
